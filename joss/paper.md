---
title: 'SISSO++: A C++ Implementation of the Sure-Independence Screening and Sparsifying Operator Approach'
tags:
  - SISSO
  - Symbolic Regression
  - Physics
  - C++
  - Python
authors:
  - name: Thomas A. R. Purcell
    orcid: 0000-0003-4564-7206
    affiliation: 1
  - name: Matthias Scheffler
    affiliation: 1
  - name: Christian Carbogno
    orcid: 0000-0003-0635-8364
    affiliation: 1
  - name: Luca M. Ghiringhelli
    orcid: 0000-0001-5099-3029
    affiliation: 1
affiliations:
 - name: NOMAD Laboratory at the Fritz Haber Institute of the Max Planck Society and Humboldt University, Berlin, Germany
   index: 1
date: September 2021
bibliography: paper.bib
---

# Summary
The sure independence screening and sparsifying operator (SISSO) approach [@Ouyang2017] is an algorithm belonging to the field of artificial intelligence and more specifically a combination of symbolic regression and compressed sensing.
As a symbolic regression method, SISSO is used to identify mathematical functions, i.e. the descriptors, that best predict the target property of a data set.
Furthermore, the compressed sensing aspect of SISSO, allows it to find sparse linear models using tens to thousands of data points.
SISSO is introduced for both regression and classification tasks.
In practice, SISSO first constructs a large and exhaustive feature space of trillions of potential descriptors by taking in a set of user-provided *primary features* as a dataframe, and then iteratively applying a set of unary and binary operators, e.g. addition, multiplication, exponentiation, and squaring, according to a user-defined specification.
From this exhaustive pool of candidate descriptors, the ones most correlated to a target property are identified via sure-independence screening, from which the low-dimensional linear models with the lowest error are found via an $\ell_0$ regularization.

Because symbolic regression generates an interpretable equation, it has become an increasingly popular concept across scientific disciplines [@Wang2019a; @Neumann2020; @Udrescu2020a].
A particular advantage of these approaches are their capability to model complex phenomena using relatively simple descriptors.
SISSO has been used successfully in the past to model, explore, and predict important material properties, including the stability of different phases [@Bartel2018a; @Schleder2020]; the catalytic activity and reactivity [@Han2021; @Xu2020; @Andersen2019; @Andersen2021]; and glass transition temperatures [@Pilania2019].
Beyond regression problems, SISSO has also been used successfully to classify materials into different crystal prototypes [@Ouyang2019a], or whether a material crystallizes in its ground state as a perovskite [@Bartel2019a], or to determine whether a material is a topological insulator or not [@Cao2020].

The SISSO++ package is an open-source (Apache-2.0 licence), modular, and extensible C++ implementation of the SISSO method with Python bindings.
Specifically, SISSO++ applies this methodology for regression, log regression, and classification problems.
Additionally, the library includes multiple Python functions to facilitate the post-processing, analyzing, and visualizing of the resulting models.

# Statement of need
The main goal of the SISSO++ package is to provide a user-friendly, easily extendable version of the SISSO method for the scientific community.
While both a FORTRAN [@Ouyang] and a Matlab [@MATLAB_SISSO] implementation of SISSO exist, their lack of native Python interfaces led to the development of multiple separate Python wrappers [@SISSOKit; @pySISSO].
This package looks to rectify this situation by providing an implementation that has native Python bindings and can be used both in a massively parallel environment for discovering the descriptors and on personal computing devices for analyzing and visualizing the results.
For this reason, all computationally intensive task are written in C++ and support parallelization via MPI and OpenMP.
Additionally, the Python bindings allow one to easily incorporate the methods into computational workflows and postprocess results.
Furthermore, this enables the integration of SISSO into existing machine-learning frameworks, e.g. scikit-learn [@scikit-learn], via submodules.
The code is designed in a modular fashion, which simplifies the process of extending the code for other applications.
Finally the project's extensive documentation and tutorials provide a good access point for new users of the method.

# Features
The following features are implemented in SISSO++:

  - A C++ library for using SISSO to find analytical models for a given problem

  - Python bindings to be able to interface with the C++ objects in a Python environment

  - Postprocessing tools for visualizing models and analyzing results using Matplotlib [@Hunter:2007]

  - Access to solve an *n*-dimensional classification model using a combination of calculating the convex-hull overlap and a linear-support vector machine solver

  - The ability to include non-linear parameters within features (e.g. exp($\alpha$x) and ln(x + $\beta$)) [@Purcell2021]

  - A `scikit-learn` interface

  - Complete API documentation defining all functions of the code

  - Tutorials and Quick-Start Guides describing the basic functionality of the code

# Code Dependencies
The following libraries are used by SISSO++:

  - Boost Serialization, MPI, System, and Filesystem are used for MPI communication and file management

  - NLopt [@NLOpt] is used to optimize the non-linear bias and scale parameters within features

  - The CLP library from Coin-OR [@Coin-CLP] is used to find the number of points in the convex hull overlap region for classification problems

  - LIBSVM [@CC01a] is used to find the linear-SVM model for classification problems

  - pybind11 [@pybind11] is used to create the python bindings

  - Scikit-learn [@scikit-learn], [@sklearn_api] is used to update the SVM model for classification problems within Python

  - NumPy [@harris2020array] and pandas [@reback2020pandas; @mckinney-proc-scipy-2010] are used to represent the data structures within Python and perform array operations.

  - seaborn [@Waskom2021] and Matplotlib [@Hunter:2007] are used to generate the plots during postprocessing

# Acknowledgements
The authors would like to thank Markus Rampp and Meisam Tabriz of the MPCDF for technical support. We would also like to thank Lucas Foppa, Jingkai Quan, Aakash Naik, James Dean, Timur Bazhirov, and Luigi Sbailò for testing and providing valuable feedback. T.P. would like to thank the Alexander von Humboldt Foundation for their support through the Alexander von Humboldt Postdoctoral Fellowship Program. This project was supported by TEC1p (the European Research Council (ERC) Horizon 2020 research and innovation programme, grant agreement No. 740233), BiGmax (the Max Planck Society’s Research Network on Big-Data-Driven Materials-Science), and the FAIRmat consortium of the NFDI and FAIR-DI e.V. association.

# References
