.. _api_di:

Descriptor Identification
=========================
.. toctree::
    :maxdepth: 2

Solvers
-------
.. toctree::
    :maxdepth: 2

    solvers

Models
------
.. toctree::
    :maxdepth: 2

    model
