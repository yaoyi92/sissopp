.. _api_solvers:

SISSOSolver
-----------
.. doxygenfile:: SISSOSolver.hpp
   :project: SISSO++

SISSOClassifier
---------------
.. doxygenfile:: SISSOClassifier.hpp
   :project: SISSO++

SISSORegressor
--------------
.. doxygenfile:: SISSORegressor.hpp
   :project: SISSO++

SISSOLogRegressor
-----------------
.. doxygenfile:: SISSOLogRegressor.hpp
   :project: SISSO++

