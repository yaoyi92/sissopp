.. _tut:

Tutorial
========
.. toctree::
    :maxdepth: 2

    0_intro
    1_command_line
    2_python
    2b_sklearn
    3_classification
