# The Python Interface

## Running `SISSO++` through the python interface
An alternative approach to using the command line is to run `SISSO++` entirely through the python interface.
This tutorial will introduce the user to how to perform SISSO calculations using the python interface, but we will not go over the postprocessing steps demonstrated in [the command line interface tutorial](1_command_line.md).
The biggest advantage to using the python bindings is that you can run and analyze the calculations within the same session; however, for larger jobs on supercomputers the command line interface is likely to be more practical.
Despite this understanding how to use the python interface is important because it allows you to test/demonstrate results in a straightforward manner.

The first step in using the python interface is creating an `Inputs` object that will then be used to construct the `FeatureSpace` and `SISSOSolver` objects.
To construct the `Inputs` object you can use the input file approach as before
```python
>>> from sissopp import Inputs
>>> inputs = Inputs("sisso.json")
```
which will then use the same functions as the command line interface to read the input files.
A second approach would be to read in the data file to create the input object and then set the non-data related properties within a script.
```python
>>> from sissopp.py_interface import read_csv
>>> inputs = read_csv("data.csv", prop_key="E_RS - E_ZB", max_rung=2, leave_out_frac=0.0)
>>> inputs.allowed_ops = [
...     "exp",
...     "neg_exp",
...     "inv",
...     "sq",
...     "cb",
...     "six_pow",
...     "sqrt",
...     "cbrt",
...     "log",
...     "abs",
...     "sin",
...     "cos",
...     "add",
...     "sub",
...     "abs_diff",
...     "mult",
...     "div"
... ]
>>> inputs.n_sis_select = 10
>>> inputs.n_dim = 4
>>> inputs.calc_type = "regression"
>>> inputs.n_residual = 10
>>> inputs.n_models_store = 1
```
Finally if there are no input files an `Inputs` object can be created entirely by setting properties of an empty `Inputs`.
```python
>>> import numpy as np
>>> import pandas as pd
>>> from sissopp import Inputs, Unit, FeatureNode
>>> df = pd.read_csv("data.csv", index_col=0)
>>> inputs = Inputs()
>>> inputs.allowed_ops = [
...     "exp",
...     "neg_exp",
...     "inv",
...     "sq",
...     "cb",
...     "six_pow",
...     "sqrt",
...     "cbrt",
...     "log",
...     "abs",
...     "sin",
...     "cos",
...     "add",
...     "sub",
...     "abs_diff",
...     "mult",
...     "div"
... ]
>>> inputs.n_sis_select = 10
>>> inputs.max_rung = 2
>>> inputs.n_dim = 4
>>> inputs.calc_type = "regression"
>>> inputs.n_residual = 10
>>> inputs.n_models_store = 1
>>> inputs.leave_out_inds = []
>>> inputs.task_names = ["all_mats"]
>>> inputs.task_sizes_train = [82]
>>> inputs.task_sizes_test = [0]
>>> inputs.prop_train = df["E_RS - E_ZB (eV)"].to_numpy()
>>> inputs.prop_test = np.array([])
>>> inputs.prop_label = "E_RS - E_ZB"
>>> inputs.prop_unit = Unit("eV")
>>> inputs.sample_ids_train = df.index.tolist()
>>> inputs.sample_ids_test = []
>>> phi_0 = []
>>> for cc, col in enumerate(df.columns[1:]):
...     expr = col.split("(")[0].strip()
...     if len(col.split("(")) == 2:
...         unit = Unit(col.split("(")[1].split(")")[0].strip())
...     else:
...         unit = Unit()
...     phi_0.append(FeatureNode(cc, expr, df[col].tolist(), [], unit))
...
>>> inputs.phi_0 = phi_0
```
Once `inputs` is created it can then be used to construct the `FeatureSpace` and `SISSOSolver` with
```python
>>> from sissopp.py_interface import get_fs_solver
>>> 
>>> feature_space, sisso = get_fs_solver(inputs, allow_overwrite=False)
```
or with
```python
>>> from sissopp import FeatureSpace, SISSORegressor
>>> 
>>> feature_space = FeatureSpace(inputs)
>>> sisso = SISSORegressor(inputs, feature_space)
```
If you use the `get_fs_solver` there is an additional keword argement `allow_overwrite` that you have to set to `True` if you want to overwrite a previous calculation.
Once created the `SISSOSolver` can then be fit via:
```
>>> sisso.fit()
Projection time: 0.332887 s
Time to get best features on rank : 3.60012e-05 s
Complete final combination/selection from all ranks: 0.000165939 s
Time for SIS: 0.478256 s
Time for l0-norm: 0.0196509 s
Projection time: 0.401822 s
Time to get best features on rank : 6.50883e-05 s
Complete final combination/selection from all ranks: 0.000201225 s
Time for SIS: 0.548018 s
Time for l0-norm: 0.00176501 s
Projection time: 0.407797 s
Time to get best features on rank : 3.79086e-05 s
Complete final combination/selection from all ranks: 0.000196934 s
Time for SIS: 0.559887 s
Time for l0-norm: 0.00716209 s
Projection time: 0.416293 s
Time to get best features on rank : 3.40939e-05 s
Complete final combination/selection from all ranks: 0.000186205 s
Time for SIS: 0.573178 s
Time for l0-norm: 0.154032 s
```
To confirm the models are the same as the one we got from the command line we can access the models directly from `sisso`
```
>>> sisso.models[-1][0]
c0 + a0 * ((E_LUMO_B^3) / sin(period_A)) + a1 * ((r_pi / r_d_A) / (E_HOMO_B^3)) + a2 * (cos(r_sigma) + exp(r_pi)) + a3 * (ln(r_pi) / exp(r_s_A))
```
Additionally the output files generated from the command line will now be present in the current working directory

## Running Cross-Validation Using the python interface
Running cross-validation with the python interface is in principle the same as doing it with [command line interface](1_command_line.md), with the main job being performing multiple calculation with different indexes left out.
To do this we adapt the previous script in the following way
```python
>>> from pathlib import Path
>>> import os
>>> 
>>> from sissopp import Inputs
>>> from sissopp.py_interface import read_csv, get_fs_solver
>>> 
>>> sisso_regs = []
>>> inputs_base = Inputs("sisso.json")
>>> 
>>> for ii in range(100):
...     work_dir = Path(f"cv_{ii:02d}")
...     work_dir.mkdir(exist_ok=True)
...     os.chdir(work_dir)
...     inputs = read_csv("../data.csv", inputs_base.prop_key, inputs=inputs_base, leave_out_frac= 0.05)
...     feature_space, sisso = get_fs_solver(inputs)
...     sisso.fit()
...     sisso_regs.append(sisso)
...     os.chdir("../")
...
```
After the calculations are finished we can then run the same analysis as we did previously using the following
```python
>>> from sissopp.postprocess.check_cv_convergence import jackknife_cv_conv_est
>>> from sissopp.postprocess.plot.cv_error_plot import plot_validation_rmse
>>> import numpy as np
>>> models = np.array([[reg.models[dim][0] for reg in sisso_regs] for dim in range(4)])
>>> mean_val_rmse, var_val_rmse = jackknife_cv_conv_est(models)
>>> print(mean_val_rmse)
[0.19977604 0.14308363 0.09543538 0.09895675]
>>> print(np.sqrt(var_val_rmse))
[0.05247252 0.02463362 0.0322683  0.01908175]
>>> plot_validation_rmse(models, "cv_100_error.pdf").show()
```
It is important to note here that, while it is not necessary to setup separate directories when using the python bindings, if you don't all output files will be overwritten reducing the reproducibility of the code.

## Using the Python Interface to Reproduce Previous Calculations
The next goal of this tutorial will be to discuss how to use the python interface to reproduce previous calculations easily.
As previous examples illustrated how the Model output files can be used to interact and extract information from the models generated by `SISSO++`, but these alone can not be used to recreate a calculation.
To increase the reproducibility of the code `SISSO++` can construct a `FeatureSpace` using a text file, which can be generated using the `phi_out_file` option in the `Inputs` object.
Depending on what we want to study there are two ways of constructing a feature space form either the `phi.txt` file or `selected_features.txt` As show below for using `selected_features.txt`

```python
>>> from sissopp import phi_selected_from_file, FeatureSpace
>>> from sissopp.py_interface import read_csv
>>> 
>>> inputs = read_csv("data.csv", prop_key="E_RS - E_ZB", max_rung=0, leave_out_frac=0.0)
>>> phi_sel = phi_selected_from_file("feature_space/selected_features.txt", inputs.phi_0)
>>> inputs.phi_0 = phi_sel
>>> feature_space = FeatureSpace(inputs)
```

and for using `phi.txt`

```python
>>> from sissopp import phi_selected_from_file, FeatureSpace
>>> from sissopp.py_interface import read_csv
>>> 
>>> inputs = read_csv("data.csv", prop_key="E_RS - E_ZB", max_rung=0, leave_out_frac=0.0)
>>> 
>>> feature_space = FeatureSpace(
>>>     "phi.txt",
>>>     inputs.phi_0,
>>>     inputs.prop_train,
>>>     [82],
>>>     project_type='regression',
>>>     cross_corr_max=1.0,
>>>     n_sis_select=100
>>> )
```

From here calculations can continue as was done in the earlier examples.

## Using the Models to Predict New Values

The previous sections all focused on how to evaluate the models using only the data initially passed to SISSO; however, the models can also be used to predict the target property for a new set of data points.
To do this we need to use the `eval` and `eval_many` functions of the models, after loading in the new dataset.
For this example we will use the same `data.csv` file, but the same procedure can be done with a new data file.

```python
>>> from sissopp.postprocess.load_models import load_model
>>> from sissopp.py_interface.import_dataframe import strip_units
>>> 
>>> model = load_model("models/train_dim_3_model_0.dat")
>>> data_predict = strip_units("data.csv")
>>> y_true = data_predict["E_RS - E_ZB"]
>>> data_predict.drop(columns=["E_RS - E_ZB"], inplace=True)
```
Here `load_model` loads the model into python, and `strip_units` will read the data file and then strip the units out of the column headers leaving only the feature's expression.
We then remove the property column to match the column numbers of the `DataFrame` with the initial `feat_ind` of the primary features.

From here we can then evaluate the model for a single point using the `eval` function

```python
>>> y_pred = model.eval(data_predict.loc["C2", :].values)
>>> print(y_pred)
2.6014256262712045
```

Additionally we can store the data inside a dictionary where the keys are the feature expressions and the values are the data for each feature.

```python
>>> data_dict = {col: data_predict.loc["C2", col] for col in data_predict.columns}
>>> y_pred = model.eval(data_dict)
>>> print(y_pred)
2.6014256262712045
```

To perform the same calculation on multiple data points, we must use the `eval_many` function instead of `eval`

```python
>>> sample_ids = ["C2", "Si2", "Ge2"]
>>> y_pred = model.eval_many(data_predict.loc[sample_ids, :].values)
>>> print(y_pred)
[2.60142563 0.25088082 0.20091549]
>>> 
>>> data_dict = {col: data_predict.loc[sample_ids, col].values for col in data_predict.columns}
>>> y_pred = model.eval_many(data_dict)
>>> print(y_pred)
[2.60142563 0.25088082 0.20091549]
```

Finally, we can store the new predictions into a separate model file, similar to the training/test data using the `prediction_to_file`

```python
>>> model.prediction_to_file(
...     "model_predict/predict_from_dict_dim_3_model_0.dat",
...     y_true[sample_ids],
...     data_dict,
...     sample_ids,
...     [], # Task ID's if applicable
... )
>>> 
>>> model.prediction_to_file(
...     "model_predict/predict_from_arr_dim_3_model_0.dat",
...     y_true[sample_ids],
...     data_predict.loc[sample_ids, :],
...     sample_ids,
...     [], # Task ID's if applicable
... )
```
Resulting in the following output files
<details>
    <summary>The resulting prediction files made from prediction_to_file.</summary>

    # c0 + a0 * ((r_sigma / EA_B) / (r_s_A^6)) + a1 * ((EA_B - IP_A) * (|r_sigma - r_s_B|)) + a2 * ((E_HOMO_B / r_p_A) / (r_sigma + r_p_B))
    # Property Label: $E_{{RS}} - E_{{ZB}}$; Unit of the Property: eV
    # RMSE: 0.0226472214238799; Max AE: 0.0282850060434131
    # Coefficients
    # Task   a0                      a1                      a2                      c0
    # all , -2.221997340952212e-01, -2.740242210520960e-02, -2.264028616363616e-01, -3.129997800655474e-01,
    # Feature Rung, Units, and Expressions
    # 0;  2; AA^-5 * eV_IP^-1;                                 18|7|div|12|sp|div; ((r_sigma / EA_B) / (r_s_A^6)); $\left(\frac{ \left(\frac{ r_{sigma} }{ EA_{B} } \right) }{ \left(r_{s, A}^6\right) } \right)$; ((r_sigma ./ EA_B) ./ (r_s_A).^6); r_sigma,EA_B,r_s_A
    # 1;  2; AA * eV_IP;                                       7|4|sub|18|13|abd|mult; ((EA_B - IP_A) * (|r_sigma - r_s_B|)); $\left(\left(EA_{B} - IP_{A}\right) \left(\left|r_{sigma} - r_{s, B}\right|\right)\right)$; ((EA_B - IP_A) .* abs(r_sigma - r_s_B)); EA_B,IP_A,r_sigma,r_s_B
    # 2;  2; AA^-2 * eV;                                       9|14|div|18|15|add|div; ((E_HOMO_B / r_p_A) / (r_sigma + r_p_B)); $\left(\frac{ \left(\frac{ E_{HOMO, B} }{ r_{p, A} } \right) }{ \left(r_{sigma} + r_{p, B}\right) } \right)$; ((E_HOMO_B ./ r_p_A) ./ (r_sigma + r_p_B)); E_HOMO_B,r_p_A,r_sigma,r_p_B
    # Number of Samples Per Task
    # Task, n_samples
    # all , 3

    # Sample ID , Property Value        ,  Property Value (EST) ,  Feature 0 Value      ,  Feature 1 Value      ,  Feature 2 Value
    C2          ,  2.628603639133640e+00,  2.601425626271205e+00, -0.000000000000000e+00,  6.386751756964515e+00, -1.364575452594942e+01
    Si2         ,  2.791658215483040e-01,  2.508808155048909e-01, -0.000000000000000e+00,  6.358817979658935e+00, -3.260239754057512e+00
    Ge2         ,  2.008525260607710e-01,  2.009154914490295e-01, -0.000000000000000e+00,  6.088560028849320e+00, -3.006837274572150e+00
</details>

