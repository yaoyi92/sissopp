.. _py_api_read_data

Reading Datafiles
-----------------

.. currentmodule:: sissopp.py_interface.import_dataframe

.. autofunction:: read_csv
.. autofunction:: create_inputs
.. autofunction:: extract_col
.. autofunction:: get_unit
.. autofunction:: strip_units
