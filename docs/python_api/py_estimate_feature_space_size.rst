.. _py_api_est_feat_space:


Estimate Feature Space Size
---------------------------

.. currentmodule:: sissopp.py_interface.estimate_feat_space_size

.. autofunction:: get_max_number_feats
.. autofunction:: get_estimate_n_feat_next_rung
