.. _py_api_inputs:

Inputs
------
.. autoclass:: sissopp.Inputs
    :special-members: __init__
    :members:
    :undoc-members:
