.. _py_api_feat_space:

FeatureSpace
------------
.. autoclass:: sissopp.FeatureSpace
    :special-members: __init__
    :members:
    :undoc-members:
