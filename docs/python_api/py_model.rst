.. _py_api_models:

Model
-----

.. autoclass:: sissopp.Model
    :special-members: __init__
    :members:
    :undoc-members:

ModelClassifier
---------------

.. currentmodule:: sissopp._sissopp

.. autoclass:: sissopp.ModelClassifier
    :special-members: __init__
    :members:
    :undoc-members:


ModelRegressor
--------------

.. currentmodule:: sissopp._sissopp

.. autoclass:: sissopp.ModelRegressor
    :special-members: __init__
    :members:
    :undoc-members:


ModelLogRegressor
-----------------

.. currentmodule:: sissopp._sissopp

.. autoclass:: sissopp.ModelLogRegressor
    :special-members: __init__
    :members:
    :undoc-members:


