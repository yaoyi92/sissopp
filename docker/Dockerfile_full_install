# Simple Docker image with Anaconda Python 3, boost, build-essential

FROM nvidia/cuda:11.6.0-devel-ubuntu20.04

USER root

MAINTAINER Thomas Purcell <purcell@fhi-berlin.mpg.de>

ENV DEBIAN_FRONTEND="noninteractive"
ENV TZ="Europe/Berlin"

RUN apt-get update &&\
    apt-get install -y build-essential g++ cmake git vim xterm &&\
    apt-get install -y liblapack-dev libblas-dev &&\
    apt-get install -y openssh-client openssh-server rsync &&\
    apt-get install -y openmpi-bin openmpi-common libopenmpi-dev &&\
    apt-get install -y doxygen &&\
    apt-get install -y gcovr &&\
    apt-get install -y pkgconf &&\
    apt-get install -y zlib1g-dev &&\
    apt-get install -y libboost-mpi-dev libboost-filesystem-dev libboost-system-dev libboost-serialization-dev &&\
    apt-get clean

COPY kitware-archive.sh kitware-archive.sh
RUN ./kitware-archive.sh
RUN apt-get upgrade -y

RUN git clone --depth=1 --branch=develop https://github.com/kokkos/kokkos.git
RUN cmake -S kokkos -B kokkos-gpu-build -DCMAKE_BUILD_TYPE=Release -DKokkos_CXX_STANDARD=17 -DKokkos_ARCH_AMPERE80=ON -DKokkos_ENABLE_SERIAL=ON -DKokkos_ENABLE_OPENMP=ON -DKokkos_ENABLE_CUDA=ON -DKokkos_ENABLE_CUDA_CONSTEXPR=ON -DKokkos_ENABLE_CUDA_LAMBDA=ON -DKokkos_ENABLE_CUDA_UVM=ON -DCMAKE_POSITION_INDEPENDENT_CODE=ON -DKokkos_ENABLE_DEBUG=ON -DKokkos_ENABLE_DEBUG_BOUNDS_CHECK=ON
RUN cmake --build kokkos-gpu-build --parallel 2
RUN cmake --install kokkos-gpu-build

ARG dst=/opt/anaconda/3/2021.05

RUN wget https://repo.anaconda.com/archive/Anaconda3-2021.05-Linux-x86_64.sh -q
RUN mv Anaconda3-2021.05-Linux-x86_64.sh /tmp/anaconda.sh
RUN bash /tmp/anaconda.sh -b -f -p ${dst} &&\
    rm -f /tmp/anaconda.sh

RUN ${dst}/bin/conda install -y -q -c conda-forge numpy scipy pandas pytest seaborn tornado jupyter
RUN ${dst}/bin/pip install -q sphinx sphinx-rtd-theme breathe sphinx-sitemap myst_parser

# Setup the user
RUN useradd -d /home/runner -ms /bin/bash runner

USER runner
WORKDIR /home/runner

ENV CONDA_DEFAULT_ENV='base'
ENV CONDA_EXE='/opt/anaconda/3/2021.05/bin/conda'
ENV CONDA_PREFIX='/opt/anaconda/3/2021.05'
ENV CONDA_PROMPT_MODIFIER='(base) '
ENV CONDA_PYTHON_EXE='/opt/anaconda/3/2021.05/bin/python'
ENV CONDA_SHLVL='1'
ENV PATH="/opt/anaconda/3/2021.05/bin:/opt/anaconda/3/2021.05/condabin:$PATH"
ENV PYTHONPATH="/home/runner/.local/lib/python3.9/site-packages/"
ENV _CE_CONDA=''
ENV _CE_M=''

ARG BRANCH=master
RUN mkdir /home/runner/.local/ /home/runner/.local/lib/ /home/runner/.local/lib/python3.9/ /home/runner/.local/lib/python3.9/site-packages/

RUN /opt/anaconda/3/2021.05/bin/conda init bash &&\
    . "/opt/anaconda/3/2021.05/etc/profile.d/conda.sh" &&\
    git clone --recursive --depth 1 --branch ${BRANCH} --single-branch https://gitlab.com/sissopp_developers/sissopp.git &&\
    cd sissopp &&\
    mkdir build &&\
    cd build &&\
    cmake -DCMAKE_CXX_COMPILER=g++ -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_FLAGS="-O3" -DBUILD_TESTS=ON -DBUILD_PARAMS=ON -DBUILD_PYTHON=ON -DEXTERNAL_BOOST=ON -DPYTHON_INSTDIR=/home/runner/.local/lib/python3.9/site-packages/ ../ &&\
    make &&\
    make install &&\
    cd ../../

RUN echo "ls /home/runner/data/" >> /home/runner/.bashrc
ENV PATH="/home/runner/sissopp/bin/:$PATH"

ENTRYPOINT ["sisso++"]
CMD [""]
