// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include <gtest/gtest.h>

#include "feature_creation/node/FeatureNode.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/div/divide.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/mult/multiply.hpp"
#include "feature_creation/node/value_storage/nodes_value_containers.hpp"
#include "mpi_interface/MPI_Interface.hpp"

namespace
{
class MultNodeTest : public ::testing::Test
{
protected:
    void SetUp() override
    {
#ifdef PARAMETERIZE
        node_value_arrs::initialize_values_arr({4}, {1}, 4, 2, true);
        nlopt_wrapper::MAX_PARAM_DEPTH = 1;
#else
        node_value_arrs::initialize_values_arr({4}, {1}, 4, 2, false);
#endif

        std::vector<double> value_1 = {1.0, 2.0, 3.0, 4.0};
        std::vector<double> test_value_1 = {5.0};

        std::vector<double> value_2 = {10.0, 25.0, 30.0, 40.0};
        std::vector<double> test_value_2 = {50.0};

        std::vector<double> value_3 = {1.0, 0.5, 1.0 / 3.0, 0.25};
        std::vector<double> test_value_3 = {-5.0};

        _feat_1 = std::make_shared<FeatureNode>(0, "A", value_1, test_value_1, Unit("m"));
        _feat_2 = std::make_shared<FeatureNode>(1, "B", value_2, test_value_2, Unit("m"));
        _feat_3 = std::make_shared<FeatureNode>(2, "C", value_2, test_value_2, Unit("s"));
        _feat_4 = std::make_shared<FeatureNode>(3, "D", value_3, test_value_3, Unit("s"));

        _phi = {_feat_1, _feat_2, _feat_3, _feat_4};
        _phi.push_back(std::make_shared<DivNode>(_feat_1, _feat_2, 4, 4, -1e-50, 1e50));
    }

    void TearDown() override { node_value_arrs::finalize_values_arr(); }

    node_ptr _feat_1;
    node_ptr _feat_2;
    node_ptr _feat_3;
    node_ptr _feat_4;
    node_ptr _node_test;

    std::vector<node_ptr> _phi;
};

TEST_F(MultNodeTest, GeneratorTest)
{
    unsigned long int feat_ind = _phi.size();

    generateMultNode(_phi, _phi[0], _phi[3], feat_ind, 4, 1e-50, 1e50);
    EXPECT_EQ(_phi.size(), 5) << " (Creation of MultNode led to a feature with a constant value)";

    generateMultNode(_phi, _phi[0], _phi[1], feat_ind, 4, 1e-50, 1.0);
    EXPECT_EQ(_phi.size(), 5) << " (MultNode created with an absolute value above the upper bound)";

    generateMultNode(_phi, _phi[0], _phi[1], feat_ind, 4, 1e3, 1e50);
    EXPECT_EQ(_phi.size(), 5) << " (MultNode created with an absolute value below the lower bound)";

    generateMultNode(_phi, _phi[0], _phi[0], feat_ind, 4, 1e-50, 1e50);
    EXPECT_EQ(_phi.size(), 5) << " (MultNode created with only one primary feature present)";

    generateMultNode(_phi, _phi[4], _phi[4], feat_ind, 4, 1e-50, 1e50);
    EXPECT_EQ(_phi.size(), 5) << " (MultNode created when some terms cancel out)";

    generateMultNode(_phi, _phi[0], _phi[1], feat_ind, 1, 1e-50, 1e50);
    EXPECT_EQ(_phi.size(), 5) << " (MultNode created with more leaves than the maximum allowed.)";

    generateMultNode(_phi, _phi[0], _phi[1], feat_ind, 4, 1e-50, 1e50);
    EXPECT_EQ(_phi.size(), 6) << " (Failure to create a valid feature)";

    generateMultNode(_phi, _phi[5], _phi[4], feat_ind, 4, 1e-50, 1e50);
    EXPECT_EQ(_phi.size(), 6) << " (MultNode created that is a duplicate of a second feature)";

    generateMultNode(_phi, _phi[0], _phi[2], feat_ind, 4, 1e-50, 1e50);
    EXPECT_EQ(_phi.size(), 7) << " (MultNode not created when units do not match.)";
}

TEST_F(MultNodeTest, ConstructorTest)
{
    unsigned long int feat_ind = _phi.size();

    try
    {
        _node_test = std::make_shared<MultNode>(_phi[0], _phi[2], feat_ind, 4, 1e-50, 1e50);
    }
    catch (const InvalidFeatureException& e)
    {
        EXPECT_TRUE(false) << " (MultNode not created when units do not match.)";
    }

    try
    {
        _node_test = std::make_shared<MultNode>(_phi[0], _phi[3], feat_ind, 4, 1e-50, 1e50);
        EXPECT_TRUE(false) << " (Creation of MultNode led to a feature with a constant value)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<MultNode>(_phi[0], _phi[1], feat_ind, 4, 1e-50, 1.0);
        EXPECT_TRUE(false) << " (MultNode created with an absolute value above the upper bound)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<MultNode>(_phi[0], _phi[1], feat_ind, 4, 1e3, 1e50);
        EXPECT_TRUE(false) << " (MultNode created with an absolute value below the lower bound)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<MultNode>(_phi[0], _phi[0], feat_ind, 4, 1e-50, 1e50);
        EXPECT_TRUE(false) << " (MultNode created with only one primary feature present)";
    }
    catch (const InvalidFeatureException& e)
    {
    }
    try
    {
        _node_test = std::make_shared<MultNode>(_phi[1], _phi[4], feat_ind, 4, 1e-50, 1e50);
        EXPECT_TRUE(false) << " (MultNode created when some terms cancel out)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<MultNode>(_phi[0], _phi[1], feat_ind, 1, 1e-50, 1e50);
        EXPECT_TRUE(false) << " (MultNode created with more leaves than the maximum allowed.)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<MultNode>(_phi[0], _phi[1], feat_ind, 4, 1e-50, 1e50);
        ++feat_ind;
    }
    catch (const InvalidFeatureException& e)
    {
        ASSERT_TRUE(false) << " (Failure to create a valid feature)";
    }
    try
    {
        _node_test = std::make_shared<MultNode>(_node_test, _node_test, feat_ind, 4, 1e-50, 1e50);
        EXPECT_TRUE(false) << " (MultNode created that is a duplicate of a second feature)";
    }
    catch (const InvalidFeatureException& e)
    {
    }
}

TEST_F(MultNodeTest, HardCopyTest)
{
    _node_test = std::make_shared<MultNode>(_phi[0], _phi[1], 5, 4, 1e-50, 1e50);
    _node_test = std::make_shared<MultNode>(_node_test, _phi[1], 6, 4, 1e-50, 1e50);

    node_ptr copy_test = _node_test->hard_copy();

    EXPECT_EQ(copy_test->rung(), 2);

    EXPECT_EQ(copy_test->value_ptr()[0], 100.0);
    EXPECT_EQ(copy_test->test_value_ptr()[0], 12500);

    EXPECT_EQ(copy_test->value()[0], 100.0);
    EXPECT_EQ(copy_test->test_value()[0], 12500.0);

    EXPECT_STREQ(copy_test->unit().toString().c_str(), "m^3");

    EXPECT_STREQ(copy_test->expr().c_str(), "((A * B) * B)");
    EXPECT_STREQ(copy_test->postfix_expr().c_str(), "0|1|mult|1|mult");
}

TEST_F(MultNodeTest, AttributesTest)
{
    if (mpi_setup::comm->rank() == 0)
    {
        _node_test = std::make_shared<MultNode>(_phi[0], _phi[1], 5, 4, 1e-50, 1e50);
        _node_test = std::make_shared<MultNode>(_node_test, _phi[1], 6, 4, 1e-50, 1e50);
    }
    mpi::broadcast(*mpi_setup::comm, _node_test, 0);

    EXPECT_EQ(_node_test->rung(), 2);

    EXPECT_EQ(_node_test->value_ptr()[0], 100.0);
    EXPECT_EQ(_node_test->test_value_ptr()[0], 12500);

    EXPECT_EQ(_node_test->value()[0], 100.0);
    EXPECT_EQ(_node_test->test_value()[0], 12500.0);

    EXPECT_STREQ(_node_test->unit().toString().c_str(), "m^3");

    EXPECT_STREQ(_node_test->expr().c_str(), "((A * B) * B)");
    EXPECT_STREQ(_node_test->postfix_expr().c_str(), "0|1|mult|1|mult");

#ifdef PARAMETERIZE
    EXPECT_THROW(_node_test->param_pointer(), std::logic_error);
    EXPECT_EQ(_node_test->n_params(), 0);

    nlopt_wrapper::MAX_PARAM_DEPTH = 0;
    EXPECT_EQ(_node_test->n_params_possible(), 0);
    nlopt_wrapper::MAX_PARAM_DEPTH = 1;
    EXPECT_EQ(_node_test->n_params_possible(), 2);

    EXPECT_EQ(_node_test->parameters().size(), 0);

    std::vector<double> params = {1.0, 0.0};
    std::vector<double> dfdp(4, 0.0);
    std::vector<double> grad(4, 0.0);
    EXPECT_THROW(_node_test->gradient(grad.data(), dfdp.data()), std::logic_error);

    _node_test->set_parameters({}, true);
    _node_test->set_parameters(nullptr);
    _node_test->param_derivative(params.data(), dfdp.data());

    EXPECT_EQ(dfdp[0], 10.0);
    EXPECT_EQ(dfdp[1], 50.0);

    EXPECT_EQ(dfdp[2], 90.0);
    EXPECT_EQ(dfdp[3], 160.0);
#endif
}
}  // namespace
