// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include <gtest/gtest.h>

#include <boost/filesystem.hpp>

#include "feature_creation/feature_space/FeatureSpace.hpp"

namespace
{
class FeatSpaceTest : public ::testing::Test
{
protected:
    void SetUp() override
    {
        allowed_op_maps::set_node_maps();
#ifdef PARAMETERIZE
        allowed_op_maps::set_param_node_maps();
#endif
        mpi_setup::init_mpi_env();

        std::vector<int> task_sizes = {5, 5};
        int n_samp = std::accumulate(task_sizes.begin(), task_sizes.end(), 0);

        node_value_arrs::initialize_values_arr(task_sizes, {0, 0}, 4, 2, false);
        node_value_arrs::initialize_d_matrix_arr();

        std::vector<double> value_1(n_samp, 0.0);
        std::vector<double> value_2(n_samp, 0.0);
        std::vector<double> value_3(n_samp, 0.0);

        _prop = std::vector<double>(n_samp, 0.0);
        _prop_class = std::vector<double>(n_samp, 0.0);
        _prop_log_reg = std::vector<double>(n_samp, 0.0);

        for (int ii = 0; ii < n_samp; ++ii)
        {
            _prop_class[ii] = ii % 2;
            value_1[ii] = static_cast<double>(ii + 1);
            value_2[ii] = static_cast<double>(ii + 1) + 0.1 + 0.1 * (ii % 2);
            value_3[ii] = static_cast<double>(2 * ii + 1) * std::pow(-1, ii);
        }

        value_1[0] = 3.0;
        value_3[0] = 3.0;

        std::transform(value_2.begin(),
                       value_2.begin() + task_sizes[0],
                       value_3.begin(),
                       _prop.begin(),
                       [](double v1, double v2) { return v1 / (v2 * v2); });
        std::transform(value_2.begin() + task_sizes[0],
                       value_2.end(),
                       value_3.begin() + task_sizes[0],
                       _prop.begin() + task_sizes[0],
                       [](double v1, double v2) { return -6.5 + 1.25 * v1 / (v2 * v2); });

        std::transform(value_2.begin(),
                       value_2.end(),
                       value_3.begin(),
                       _prop_log_reg.begin(),
                       [](double v1, double v2) { return v1 / (v2 * v2); });

        FeatureNode feat_1(0, "A", value_1, std::vector<double>(), Unit("m"));
        FeatureNode feat_2(1, "B", value_2, std::vector<double>(), Unit("m"));
        FeatureNode feat_3(2, "C", value_3, std::vector<double>(), Unit("s"));
        FeatureNode feat_4(3, "D", std::vector<double>(10, 1.0), std::vector<double>(), Unit("s"));

        std::vector<FeatureNode> phi_0 = {feat_1, feat_2, feat_3};

        _inputs.set_phi_0(phi_0);
        _inputs.set_task_sizes_train(task_sizes);
        _inputs.set_allowed_ops({"sq", "cb", "div", "add"});
        _inputs.set_allowed_param_ops({});
        _inputs.set_cross_cor_max(1.0);
        _inputs.set_l_bound(1e-50);
        _inputs.set_u_bound(1e50);
        _inputs.set_n_rung_store(1);
        _inputs.set_max_rung(2);
        _inputs.set_n_sis_select(10);
        _inputs.set_n_rung_generate(0);
        _inputs.set_max_param_depth(0);
        _inputs.set_reparam_residual(false);
    }

    void TearDown() override { node_value_arrs::finalize_values_arr(); }

    InputParser _inputs;
    std::vector<double> _prop;
    std::vector<double> _prop_log_reg;
    std::vector<double> _prop_class;
};

TEST_F(FeatSpaceTest, RegTest)
{
    _inputs.set_calc_type("regression");
    _inputs.set_prop_train(_prop);

    // Check to ensure that the fix fo this situation actually happens
    node_value_arrs::finalize_values_arr();
    FeatureSpace feat_space(_inputs);

    feat_space.sis(_prop);

    EXPECT_EQ(feat_space.task_sizes_train()[0], _inputs.task_sizes_train()[0]);
    EXPECT_STREQ(feat_space.feature_space_file().c_str(), "feature_space/selected_features.txt");
    EXPECT_EQ(feat_space.l_bound(), 1e-50);
    EXPECT_EQ(feat_space.u_bound(), 1e50);
    EXPECT_EQ(feat_space.max_rung(), 2);
    EXPECT_EQ(feat_space.n_sis_select(), 10);
    EXPECT_EQ(feat_space.n_samp_train(), 10);
    EXPECT_EQ(feat_space.n_rung_store(), 1);
    EXPECT_EQ(feat_space.n_rung_generate(), 0);

    int n_feat = 148 - feat_space.start_rung().back();
    n_feat = n_feat / mpi_setup::comm->size() +
             (mpi_setup::comm->rank() < n_feat % mpi_setup::comm->size());
    n_feat += feat_space.start_rung().back();
    EXPECT_EQ(feat_space.n_feat(), n_feat);

    EXPECT_LT(std::abs(feat_space.phi_selected().back()->value()[0] - _prop[0]), 1e-10);
    EXPECT_LT(std::abs(feat_space.phi0()[0]->value()[0] - _inputs.phi_0()[0].value()[0]), 1e-10);
    EXPECT_LT(std::abs(feat_space.phi()[0]->value()[0] - _inputs.phi_0()[0].value()[0]), 1e-10);

    EXPECT_TRUE(feat_space.feat_in_phi(0));
    EXPECT_FALSE(feat_space.feat_in_phi(100000000));

    if (mpi_setup::comm->rank() == 0)
    {
        boost::filesystem::remove_all("feature_space/");
    }
}

TEST_F(FeatSpaceTest, ProjectGenTest)
{
    _inputs.set_n_rung_generate(1);
    _inputs.set_calc_type("regression");
    _inputs.set_prop_train(_prop);

    FeatureSpace feat_space(_inputs);

    feat_space.sis(_prop);

    EXPECT_EQ(feat_space.task_sizes_train()[0], _inputs.task_sizes_train()[0]);
    EXPECT_STREQ(feat_space.feature_space_file().c_str(), "feature_space/selected_features.txt");
    EXPECT_EQ(feat_space.l_bound(), 1e-50);
    EXPECT_EQ(feat_space.u_bound(), 1e50);
    EXPECT_EQ(feat_space.max_rung(), 2);
    EXPECT_EQ(feat_space.n_sis_select(), 10);
    EXPECT_EQ(feat_space.n_samp_train(), 10);
    EXPECT_EQ(feat_space.n_feat(), 16);
    EXPECT_EQ(feat_space.n_rung_store(), 1);
    EXPECT_EQ(feat_space.n_rung_generate(), 1);

    EXPECT_LT(std::abs(feat_space.phi_selected().back()->value()[0] - _prop[0]), 1e-10);
    EXPECT_LT(std::abs(feat_space.phi0()[0]->value()[0] - _inputs.phi_0()[0].value()[0]), 1e-10);
    EXPECT_LT(std::abs(feat_space.phi()[0]->value()[0] - _inputs.phi_0()[0].value()[0]), 1e-10);

    if (mpi_setup::comm->rank() == 0)
    {
        boost::filesystem::remove_all("feature_space/");
    }
}

TEST_F(FeatSpaceTest, MaxCorrTest)
{
    _inputs.set_cross_cor_max(0.95);
    _inputs.set_calc_type("regression");
    _inputs.set_prop_train(_prop);

    FeatureSpace feat_space(_inputs);

    feat_space.sis(_prop);
    EXPECT_EQ(feat_space.task_sizes_train()[0], _inputs.task_sizes_train()[0]);
    EXPECT_STREQ(feat_space.feature_space_file().c_str(), "feature_space/selected_features.txt");
    EXPECT_EQ(feat_space.l_bound(), 1e-50);
    EXPECT_EQ(feat_space.u_bound(), 1e50);
    EXPECT_EQ(feat_space.max_rung(), 2);
    EXPECT_EQ(feat_space.n_sis_select(), 10);
    EXPECT_EQ(feat_space.n_samp_train(), 10);
    EXPECT_EQ(feat_space.n_rung_store(), 1);
    EXPECT_EQ(feat_space.n_rung_generate(), 0);

    int n_feat = 148 - feat_space.start_rung().back();
    n_feat = n_feat / mpi_setup::comm->size() +
             (mpi_setup::comm->rank() < n_feat % mpi_setup::comm->size());
    n_feat += feat_space.start_rung().back();
    EXPECT_EQ(feat_space.n_feat(), n_feat);

    EXPECT_LT(std::abs(feat_space.phi_selected().back()->value()[0] - _prop[0]), 1e-10);
    EXPECT_LT(std::abs(feat_space.phi0()[0]->value()[0] - _inputs.phi_0()[0].value()[0]), 1e-10);
    EXPECT_LT(std::abs(feat_space.phi()[0]->value()[0] - _inputs.phi_0()[0].value()[0]), 1e-10);

    if (mpi_setup::comm->rank() == 0)
    {
        boost::filesystem::remove_all("feature_space/");
    }
}

TEST_F(FeatSpaceTest, LogRegTest)
{
    _inputs.set_calc_type("log_regression");
    _inputs.set_prop_train(_prop_log_reg);

    FeatureSpace feat_space(_inputs);

    std::transform(
        _prop_log_reg.begin(), _prop_log_reg.end(), _prop_log_reg.begin(), [](double pl) {
            return std::log(pl);
        });
    feat_space.sis(_prop_log_reg);

    EXPECT_EQ(feat_space.task_sizes_train()[0], _inputs.task_sizes_train()[0]);
    EXPECT_STREQ(feat_space.feature_space_file().c_str(), "feature_space/selected_features.txt");
    EXPECT_EQ(feat_space.l_bound(), 1e-50);
    EXPECT_EQ(feat_space.u_bound(), 1e50);
    EXPECT_EQ(feat_space.max_rung(), 2);
    EXPECT_EQ(feat_space.n_sis_select(), 10);
    EXPECT_EQ(feat_space.n_samp_train(), 10);
    EXPECT_EQ(feat_space.n_rung_store(), 1);
    EXPECT_EQ(feat_space.n_rung_generate(), 0);

    int n_feat = 148 - feat_space.start_rung().back();
    n_feat = n_feat / mpi_setup::comm->size() +
             (mpi_setup::comm->rank() < n_feat % mpi_setup::comm->size());
    n_feat += feat_space.start_rung().back();
    EXPECT_EQ(feat_space.n_feat(), n_feat);

    std::vector<double> log_a(10, 0.0);
    EXPECT_LT(std::abs(1.0 - util_funcs::log_r2(feat_space.phi_selected().back()->value_ptr(),
                                                _prop_log_reg.data(),
                                                log_a.data(),
                                                10)),
              1e-8);
    EXPECT_LT(std::abs(feat_space.phi0()[0]->value()[0] - _inputs.phi_0()[0].value()[0]), 1e-8);
    EXPECT_LT(std::abs(feat_space.phi()[0]->value()[0] - _inputs.phi_0()[0].value()[0]), 1e-8);
    if (mpi_setup::comm->rank() == 0)
    {
        boost::filesystem::remove_all("feature_space/");
    }
}

TEST_F(FeatSpaceTest, ClassTest)
{
    _inputs.set_max_rung(0);
    _inputs.set_n_sis_select(1);
    _inputs.set_n_rung_store(0);
    _inputs.set_calc_type("classification");
    _inputs.set_prop_train(_prop_class);

    FeatureSpace feat_space(_inputs);
    feat_space.sis(_prop_class);

    EXPECT_EQ(feat_space.task_sizes_train()[0], _inputs.task_sizes_train()[0]);
    EXPECT_STREQ(feat_space.feature_space_file().c_str(), "feature_space/selected_features.txt");
    EXPECT_EQ(feat_space.l_bound(), 1e-50);
    EXPECT_EQ(feat_space.u_bound(), 1e50);
    EXPECT_EQ(feat_space.max_rung(), 0);
    EXPECT_EQ(feat_space.n_sis_select(), 1);
    EXPECT_EQ(feat_space.n_samp_train(), 10);
    EXPECT_EQ(feat_space.n_feat(), 3);
    EXPECT_EQ(feat_space.n_rung_store(), 0);
    EXPECT_EQ(feat_space.n_rung_generate(), 0);

    EXPECT_LT(std::abs(feat_space.phi_selected().back()->value()[1] + 3.0), 1e-10);
    EXPECT_LT(std::abs(feat_space.phi0()[0]->value()[0] - _inputs.phi_0()[0].value()[0]), 1e-10);
    EXPECT_LT(std::abs(feat_space.phi()[0]->value()[0] - _inputs.phi_0()[0].value()[0]), 1e-10);

    if (mpi_setup::comm->rank() == 0)
    {
        boost::filesystem::remove_all("feature_space/");
    }
    prop_sorted_d_mat::finalize_sorted_d_matrix_arr();
}

TEST_F(FeatSpaceTest, MaxLeaves)
{
    _inputs.set_calc_type("regression");
    _inputs.set_max_leaves(3);
    _inputs.set_prop_train(_prop);

    FeatureSpace feat_space(_inputs);

    for (auto& feat : feat_space.phi())
    {
        EXPECT_LE(feat->n_leaves(), 3);
    }
    feat_space.sis(_prop);

    EXPECT_EQ(feat_space.task_sizes_train()[0], _inputs.task_sizes_train()[0]);
    EXPECT_EQ(feat_space.max_leaves(), 3);

    EXPECT_LT(std::abs(feat_space.phi_selected().back()->value()[0] - _prop[0]), 1e-10);
    if (mpi_setup::comm->rank() == 0)
    {
        boost::filesystem::remove_all("feature_space/");
    }
}

TEST_F(FeatSpaceTest, CheckFailedSISTest)
{
    _inputs.set_calc_type("regression");
    _inputs.set_prop_train(_prop);
    _inputs.set_n_sis_select(1000);

    FeatureSpace feat_space(_inputs);
    EXPECT_THROW(feat_space.sis(_prop), std::logic_error);
}

TEST_F(FeatSpaceTest, RemoveDuplicatesTest)
{
    node_value_arrs::finalize_values_arr();
    node_value_arrs::initialize_values_arr({10}, {0}, 8, 0, false);

    InputParser inputs;
    inputs.set_task_sizes_train({10});
    inputs.set_allowed_ops({"sq"});
    inputs.set_allowed_param_ops({});
    inputs.set_cross_cor_max(1.0);
    inputs.set_l_bound(1e-50);
    inputs.set_u_bound(1e50);
    inputs.set_n_rung_store(0);
    inputs.set_max_rung(0);
    inputs.set_n_sis_select(10);
    inputs.set_n_rung_generate(0);
    inputs.set_max_param_depth(0);
    inputs.set_reparam_residual(false);
    inputs.set_calc_type("regression");

    std::vector<double> value_1 = {1.0, -1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    std::vector<double> value_2 = {0.0, 0.0, 1.0, -1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    std::vector<double> value_3 = {1.0, -1.0, 1.0, -1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    std::vector<double> value_4 = {0.0, 0.0, 0.0, 0.0, -1.0, 1.0, 0.0, 0.0, 0.0, 0.0};
    std::vector<double> value_6 = {0.0, 0.0, 0.0, 0.0, -1.0, 1.0, -1.0, 1.0, 0.0, 0.0};
    std::vector<double> value_5 = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -1.0, 1.0, 0.0, 0.0};
    std::vector<double> prop = {0.0, 0.0, 0.0, 0.0, -1.0, 1.0, -1.0, 1.0, 0.0, 0.0};
    inputs.set_prop_train(prop);

    FeatureNode feat_1(0, "A", value_1, std::vector<double>(), Unit());
    FeatureNode feat_2(1, "B", value_2, std::vector<double>(), Unit());
    FeatureNode feat_3(2, "C", value_3, std::vector<double>(), Unit());
    FeatureNode feat_4(3, "D", value_4, std::vector<double>(), Unit());
    FeatureNode feat_5(4, "E", value_5, std::vector<double>(), Unit());
    FeatureNode feat_6(5, "F", value_6, std::vector<double>(), Unit());
    FeatureNode feat_7(6, "G", value_4, std::vector<double>(), Unit());
    FeatureNode feat_8(7, "H", value_1, std::vector<double>(), Unit());

    std::vector<FeatureNode> phi_0 = {
        feat_1, feat_2, feat_3, feat_4, feat_5, feat_6, feat_7, feat_8};
    inputs.set_phi_0(phi_0);

    std::vector<node_ptr> phi(8, nullptr);
    std::transform(phi_0.begin(), phi_0.end(), phi.begin(), [](FeatureNode feat) {
        return std::make_shared<FeatureNode>(feat);
    });
    FeatureSpace feat_space(inputs);

    feat_space.remove_duplicate_features(phi, 2);
    EXPECT_EQ(phi.size(), 7);

    feat_space.remove_duplicate_features(phi, 0);
    EXPECT_EQ(phi.size(), 6);
}

TEST_F(FeatSpaceTest, OverrideSISTest)
{
    _inputs.set_calc_type("regression");
    _inputs.set_prop_train(_prop);
    _inputs.set_n_sis_select(10000000);

    // Check to ensure that the fix fo this situation actually happens
    node_value_arrs::finalize_values_arr();
    FeatureSpace feat_space(_inputs);
    EXPECT_THROW(feat_space.sis(_prop), std::logic_error);

    _inputs.set_override_n_sis_select(true);
    FeatureSpace feat_space_pass(_inputs);
    feat_space_pass.sis(_prop);

    EXPECT_EQ(feat_space_pass.n_sis_select(), feat_space_pass.phi_selected().size());

    if (mpi_setup::comm->rank() == 0)
    {
        boost::filesystem::remove_all("feature_space/");
    }
}
#ifdef PARAMETERIZE
TEST_F(FeatSpaceTest, ReorderByNparamsTest)
{
    nlopt_wrapper::MAX_PARAM_DEPTH = 2;
    node_value_arrs::initialize_param_storage();
    _inputs.set_max_rung(2);
    _inputs.set_max_param_depth(2);
    _inputs.set_n_rung_store(0);
    _inputs.set_n_rung_generate(0);
    _inputs.set_prop_train(_prop);

    FeatureSpace feat_space(_inputs);
    std::vector<node_ptr> phi(8);

    phi[0] = feat_space.phi0()[0];
    phi[1] = feat_space.phi0()[1];
    phi[2] = feat_space.phi0()[2];
    phi[0]->set_value();
    phi[1]->set_value();
    phi[2]->set_value();

    double* val_ptr = feat_space.phi0()[1]->value_ptr();

    std::vector<double> prop_cb(_prop.size(), 0.0);
    std::transform(val_ptr, val_ptr + prop_cb.size(), prop_cb.begin(), [](double val) {
        return std::pow(val + 2.0, 3.0);
    });
    std::shared_ptr<NLOptimizer> optimizer_cb = nlopt_wrapper::get_optimizer(
        "regression", _inputs.task_sizes_train(), prop_cb, 2);

    std::vector<double> prop_lor(_prop.size(), 0.0);
    std::transform(val_ptr, val_ptr + prop_lor.size(), prop_lor.begin(), [](double val) {
        return 1.0 / (2.0 + std::pow(val, 2.0));
    });
    std::shared_ptr<NLOptimizer> optimizer_lor = nlopt_wrapper::get_optimizer(
        "regression", _inputs.task_sizes_train(), prop_lor, 2);

    phi[3] = std::make_shared<CbNode>(phi[1], 5, 1e-50, 1e50);
    phi[4] = std::make_shared<CbParamNode>(phi[1], 6, 1e-50, 1e50, optimizer_cb);

    phi[5] = std::make_shared<SqNode>(phi[1], 7, 1e-50, 1e50);
    phi[6] = std::make_shared<InvParamNode>(phi[5], 9, 1e-50, 1e50, optimizer_lor);
    phi[7] = std::make_shared<InvNode>(phi[5], 8, 1e-50, 1e50);

    EXPECT_EQ(feat_space.reorder_by_n_params(phi, 5), 7);
    EXPECT_EQ(phi[4]->n_params(), 2);
    EXPECT_EQ(phi[6]->n_params(), 0);
    EXPECT_EQ(phi[7]->n_params(), 4);

    EXPECT_EQ(feat_space.reorder_by_n_params(phi, 0), 6);
    EXPECT_EQ(phi[4]->n_params(), 0);
    EXPECT_EQ(phi[6]->n_params(), 2);
    EXPECT_EQ(phi[7]->n_params(), 4);
}
#endif
}  // namespace
