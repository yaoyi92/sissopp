// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include <gtest/gtest.h>

#include <inputs/InputParser.hpp>

namespace
{
class InputParserTests : public ::testing::Test
{
protected:
    void SetUp() override
    {
        std::vector<std::string> filepath = str_utils::split_string_trim(__FILE__, "/");
        _sample_ids_train = {"a", "b", "c"};
        _sample_ids_test = {"d"};
        _task_names = {"task_1", "task_2"};
        _allowed_ops = {"sq", "cb"};
        _prop_train = {1.0, 4.0, 9.0};
        _prop_test = {16.0};
        _leave_out_inds = {3};
        _task_sizes_train = {2, 1};
        _task_sizes_test = {1, 0};
        _phi_0 = {FeatureNode(0, "feat_1", {1.0, 2.0, 3.0}, {4.0}, Unit("m"))};
        _prop_unit = Unit("m");
        _data_file = (str_utils::join("/", filepath.data(), filepath.size() - 1) + "/data.csv");
        _prop_key = "property";
        _prop_label = "property";
        _task_key = "task";
        _calc_type = "regression";
        _cross_cor_max = 1.0;
        _l_bound = 1e-5;
        _u_bound = 1e8;
        _n_dim = 2;
        _max_rung = 1;
        _n_rung_store = 1;
        _n_rung_generate = 0;
        _n_sis_select = 1;
        _n_residual = 1;
        _n_models_store = 1;

        _fix_intercept = false;

#ifdef PARAMETERIZE
        _filename = (str_utils::join("/", filepath.data(), filepath.size() - 1) +
                     "/sisso_param.json");

        _allowed_param_ops = {"log"};

        _max_param_depth = 1;
        _nlopt_seed = 10;

        _global_param_opt = true;
        _reparam_residual = true;

#else
        _filename = (str_utils::join("/", filepath.data(), filepath.size() - 1) + "/sisso.json");
#endif
    }

    void TearDown() override { node_value_arrs::finalize_values_arr(); }

    std::vector<std::string>
        _sample_ids_train;  //!< Vector storing all sample ids for the training samples
    std::vector<std::string>
        _sample_ids_test;                  //!< Vector storing all sample ids for the test samples
    std::vector<std::string> _task_names;  //!< Vector storing the ID of the task names

    std::vector<std::string> _allowed_ops;  //!< Vector containing all allowed operators strings
    std::vector<double>
        _prop_train;  //!< The value of the property to evaluate the loss function against for the training set
    std::vector<double>
        _prop_test;  //!< The value of the property to evaluate the loss function against for the test set

    std::vector<int>
        _leave_out_inds;  //!< List of indexes from the initial data file in the test set
    std::vector<int> _task_sizes_train;  //!< Number of training samples per task
    std::vector<int> _task_sizes_test;   //!< Number of testing samples per task

    std::vector<FeatureNode> _phi_0;  //!< A vector of FeatureNodes for the primary feature space

    Unit _prop_unit;  //!< The Unit of the property

    std::string _filename;    //!< Name of the input file
    std::string _data_file;   //!< Name of the data file
    std::string _prop_key;    //!< Key used to find the property column in the data file
    std::string _prop_label;  //!< The label of the property
    std::string _task_key;    //!< Key used to find the task column in the data file
    std::string
        _calc_type;  //!< The type of LossFunction to use when projecting the features onto a property

    std::shared_ptr<MPI_Interface> _mpi_comm;  //!< The MPI communicator for the calculation

    double _cross_cor_max;  //!< Maximum cross-correlation used for selecting features
    double _l_bound;        //!< The lower bound for the maximum absolute value of the features
    double _u_bound;        //!< The upper bound for the maximum absolute value of the features

    int _n_dim;     //!< The maximum number of features allowed in the linear model
    int _max_rung;  //!< Maximum rung for the feature creation
    int _n_rung_store;  //!< The number of rungs to calculate and store the value of the features for all samples
    int _n_rung_generate;  //!< Either 0 or 1, and is the number of rungs to generate on the fly during SIS
    int _n_sis_select;    //!< Number of features to select during each SIS iteration
    int _n_samp;          //!< Number of samples in the data set
    int _n_samp_train;    //!< Number of samples in the training set
    int _n_samp_test;     //!< Number of samples in the test set
    int _n_residual;      //!< Number of residuals to pass to the next sis model
    int _n_models_store;  //!< The number of models to output to files

    bool _fix_intercept;  //!< If true the bias term is fixed at 0

#ifdef PARAMETERIZE
    std::vector<std::string>
        _allowed_param_ops;  //!< Vector containing all allowed operators strings for operators with free parameters

    int _max_param_depth;  //!< The maximum depth in the binary expression tree to set non-linear optimization
    int _nlopt_seed;  //!< The seed used for the nlOpt library

    bool
        _global_param_opt;  //!< True if global optimization is requested for non-linear optimization of parameters (Can break reproducibility)
    bool
        _reparam_residual;  //!< If True then reparameterize features using the residuals of each model
#endif
};

TEST_F(InputParserTests, DefaultConsructor)
{
    InputParser inputs;
    EXPECT_THROW(inputs.task_sizes_train(), std::logic_error);
    EXPECT_THROW(inputs.task_sizes_train_copy(), std::logic_error);

    EXPECT_THROW(inputs.task_sizes_test(), std::logic_error);
    EXPECT_THROW(inputs.task_sizes_test_copy(), std::logic_error);

    inputs.set_task_sizes_train(_task_sizes_train);
    EXPECT_EQ(inputs.task_sizes_train()[0], _task_sizes_train[0]);

    inputs.set_task_sizes_test(_task_sizes_test);
    EXPECT_EQ(inputs.task_sizes_test()[0], _task_sizes_test[0]);

    EXPECT_THROW(inputs.sample_ids_train(), std::logic_error);
    EXPECT_THROW(inputs.sample_ids_train_copy(), std::logic_error);
    inputs.set_sample_ids_train(_sample_ids_train);
    EXPECT_EQ(inputs.sample_ids_train()[0], _sample_ids_train[0]);

    EXPECT_THROW(inputs.sample_ids_test(), std::logic_error);
    EXPECT_THROW(inputs.sample_ids_test_copy(), std::logic_error);
    inputs.set_sample_ids_test(_sample_ids_test);
    EXPECT_EQ(inputs.sample_ids_test()[0], _sample_ids_test[0]);

    EXPECT_THROW(inputs.task_names(), std::logic_error);
    EXPECT_THROW(inputs.task_names_copy(), std::logic_error);
    inputs.set_task_names(_task_names);
    EXPECT_EQ(inputs.task_names()[0], _task_names[0]);

    inputs.set_allowed_ops(_allowed_ops);
    EXPECT_EQ(inputs.allowed_ops()[0], _allowed_ops[0]);

    EXPECT_THROW(inputs.prop_train(), std::logic_error);
    EXPECT_THROW(inputs.prop_train_copy(), std::logic_error);
    inputs.set_prop_train(_prop_train);
    EXPECT_EQ(inputs.prop_train()[0], _prop_train[0]);

    EXPECT_THROW(inputs.prop_test(), std::logic_error);
    EXPECT_THROW(inputs.prop_test_copy(), std::logic_error);
    inputs.set_prop_test(_prop_test);
    EXPECT_EQ(inputs.prop_test()[0], _prop_test[0]);

    EXPECT_THROW(inputs.leave_out_inds(), std::logic_error);
    EXPECT_THROW(inputs.leave_out_inds_copy(), std::logic_error);
    inputs.set_leave_out_inds(_leave_out_inds);
    EXPECT_EQ(inputs.leave_out_inds()[0], _leave_out_inds[0]);

    EXPECT_EQ(inputs.n_samp(), 4);
    EXPECT_EQ(inputs.n_samp_test(), 1);
    EXPECT_EQ(inputs.n_samp_train(), 3);

    EXPECT_THROW(inputs.phi_0(), std::logic_error);
    EXPECT_THROW(inputs.phi_0_copy(), std::logic_error);
    EXPECT_THROW(inputs.phi_0_ptrs(), std::logic_error);
    inputs.set_phi_0(_phi_0);
    EXPECT_EQ(inputs.phi_0()[0].feat_ind(), _phi_0[0].feat_ind());
    EXPECT_EQ(inputs.phi_0_ptrs()[0]->feat_ind(), _phi_0[0].feat_ind());

    inputs.set_prop_unit(_prop_unit);
    EXPECT_EQ(inputs.prop_unit(), _prop_unit);

    inputs.set_filename(_filename);
    EXPECT_EQ(inputs.filename(), _filename);

    inputs.set_data_file(_data_file);
    EXPECT_EQ(inputs.data_file(), _data_file);

    inputs.set_prop_key(_prop_key);
    EXPECT_EQ(inputs.prop_key(), _prop_key);

    inputs.set_prop_label(_prop_label);
    EXPECT_EQ(inputs.prop_label(), _prop_label);

    inputs.set_task_key(_task_key);
    EXPECT_EQ(inputs.task_key(), _task_key);

    inputs.set_calc_type(_calc_type);
    EXPECT_EQ(inputs.calc_type(), _calc_type);

    inputs.set_cross_cor_max(_cross_cor_max);
    EXPECT_EQ(inputs.cross_cor_max(), _cross_cor_max);

    inputs.set_l_bound(_l_bound);
    EXPECT_EQ(inputs.l_bound(), _l_bound);
    EXPECT_THROW(inputs.set_u_bound(_l_bound / 2), std::logic_error);

    inputs.set_u_bound(_u_bound);
    EXPECT_EQ(inputs.u_bound(), _u_bound);
    EXPECT_THROW(inputs.set_l_bound(_u_bound * 2), std::logic_error);

    inputs.set_n_dim(_n_dim);
    EXPECT_EQ(inputs.n_dim(), _n_dim);

    inputs.set_max_rung(_max_rung);
    EXPECT_EQ(inputs.max_rung(), _max_rung);

    inputs.set_n_rung_store(_n_rung_store);
    EXPECT_EQ(inputs.n_rung_store(), _n_rung_store);

    inputs.set_n_rung_generate(_n_rung_generate);
    EXPECT_EQ(inputs.n_rung_generate(), _n_rung_generate);

    inputs.set_n_sis_select(_n_sis_select);
    EXPECT_EQ(inputs.n_sis_select(), _n_sis_select);

    inputs.set_n_residual(_n_residual);
    EXPECT_EQ(inputs.n_residual(), _n_residual);

    inputs.set_n_models_store(_n_models_store);
    EXPECT_EQ(inputs.n_models_store(), _n_models_store);

    inputs.set_fix_intercept(_fix_intercept);
    EXPECT_EQ(inputs.fix_intercept(), _fix_intercept);

    EXPECT_FALSE(inputs.override_n_sis_select());
    inputs.set_override_n_sis_select(true);
    EXPECT_TRUE(inputs.override_n_sis_select());

#ifdef PARAMETERIZE
    inputs.set_allowed_param_ops(_allowed_param_ops);
    EXPECT_EQ(inputs.allowed_param_ops()[0], _allowed_param_ops[0]);

    inputs.set_max_param_depth(_max_param_depth);
    EXPECT_EQ(inputs.max_param_depth(), _max_param_depth);

    inputs.set_nlopt_seed(_nlopt_seed);
    EXPECT_EQ(inputs.nlopt_seed(), _nlopt_seed);

    inputs.set_global_param_opt(_global_param_opt);
    EXPECT_EQ(inputs.global_param_opt(), _global_param_opt);

    inputs.set_reparam_residual(_reparam_residual);
    EXPECT_EQ(inputs.reparam_residual(), _reparam_residual);
#endif
}

TEST_F(InputParserTests, FileConsructor)
{
    InputParser inputs(_filename);
    EXPECT_EQ(inputs.sample_ids_train()[0], _sample_ids_train[0]);
    EXPECT_EQ(inputs.sample_ids_test()[0], _sample_ids_test[0]);
    EXPECT_EQ(inputs.task_names()[0], _task_names[0]);
    EXPECT_EQ(inputs.allowed_ops()[0], _allowed_ops[0]);
    EXPECT_EQ(inputs.prop_train()[0], _prop_train[0]);
    EXPECT_EQ(inputs.prop_test()[0], _prop_test[0]);
    EXPECT_EQ(inputs.leave_out_inds()[0], _leave_out_inds[0]);
    EXPECT_EQ(inputs.task_sizes_train()[0], _task_sizes_train[0]);
    EXPECT_EQ(inputs.task_sizes_test()[0], _task_sizes_test[0]);
    EXPECT_EQ(inputs.n_samp(), 4);
    EXPECT_EQ(inputs.n_samp_test(), 1);
    EXPECT_EQ(inputs.n_samp_train(), 3);
    EXPECT_EQ(inputs.phi_0()[0].feat_ind(), _phi_0[0].feat_ind());
    EXPECT_EQ(inputs.phi_0_ptrs()[0]->feat_ind(), _phi_0[0].feat_ind());
    EXPECT_EQ(inputs.prop_unit(), _prop_unit);
    EXPECT_EQ(inputs.filename(), _filename);
    EXPECT_EQ(inputs.data_file(), _data_file);
    EXPECT_EQ(inputs.prop_key(), _prop_key);
    EXPECT_EQ(inputs.prop_label(), _prop_label);
    EXPECT_EQ(inputs.task_key(), _task_key);
    EXPECT_EQ(inputs.calc_type(), _calc_type);
    EXPECT_EQ(inputs.cross_cor_max(), _cross_cor_max);
    EXPECT_EQ(inputs.l_bound(), _l_bound);
    EXPECT_EQ(inputs.u_bound(), _u_bound);
    EXPECT_EQ(inputs.n_dim(), _n_dim);
    EXPECT_EQ(inputs.max_rung(), _max_rung);
    EXPECT_EQ(inputs.n_rung_store(), _n_rung_store);
    EXPECT_EQ(inputs.n_rung_generate(), _n_rung_generate);
    EXPECT_EQ(inputs.n_sis_select(), _n_sis_select);
    EXPECT_EQ(inputs.n_residual(), _n_residual);
    EXPECT_EQ(inputs.n_models_store(), _n_models_store);
    EXPECT_EQ(inputs.fix_intercept(), _fix_intercept);

#ifdef PARAMETERIZE
    EXPECT_EQ(inputs.allowed_param_ops()[0], _allowed_param_ops[0]);
    EXPECT_EQ(inputs.max_param_depth(), _max_param_depth);
    EXPECT_EQ(inputs.nlopt_seed(), _nlopt_seed);
    EXPECT_EQ(inputs.global_param_opt(), _global_param_opt);
    EXPECT_EQ(inputs.reparam_residual(), _reparam_residual);
#endif
}

TEST_F(InputParserTests, CheckSizes)
{
    InputParser inputs;
    inputs.clear_data();
    node_value_arrs::finalize_values_arr();

    inputs.set_task_sizes_train({2, 2, 2});
    EXPECT_THROW(inputs.set_task_sizes_test(_task_sizes_test), std::logic_error);
    EXPECT_THROW(inputs.set_task_names({"A"}), std::logic_error);
    EXPECT_THROW(inputs.set_prop_train(_prop_train), std::logic_error);
    EXPECT_THROW(inputs.set_sample_ids_train(_sample_ids_train), std::logic_error);
    EXPECT_THROW(inputs.set_phi_0((_phi_0)), std::logic_error);
    inputs.clear_data();

    inputs.set_task_sizes_test({2, 2, 2});
    EXPECT_THROW(inputs.set_task_sizes_train(_task_sizes_train), std::logic_error);
    EXPECT_THROW(inputs.set_task_names({"A"}), std::logic_error);
    EXPECT_THROW(inputs.set_prop_test(_prop_test), std::logic_error);
    EXPECT_THROW(inputs.set_sample_ids_test(_sample_ids_test), std::logic_error);
    EXPECT_THROW(inputs.set_leave_out_inds(_leave_out_inds), std::logic_error);
    EXPECT_THROW(inputs.set_phi_0((_phi_0)), std::logic_error);
    inputs.clear_data();

    inputs.set_task_names({"A", "B", "C", "D"});
    EXPECT_THROW(inputs.set_task_sizes_train(_task_sizes_train), std::logic_error);
    EXPECT_THROW(inputs.set_task_sizes_test(_task_sizes_test), std::logic_error);
    inputs.clear_data();

    inputs.set_prop_train(std::vector<double>(6, 0.0));
    EXPECT_THROW(inputs.set_task_sizes_train(_task_sizes_train), std::logic_error);
    EXPECT_THROW(inputs.set_sample_ids_train(_sample_ids_train), std::logic_error);
    EXPECT_THROW(inputs.set_phi_0(_phi_0), std::logic_error);
    inputs.clear_data();

    inputs.set_sample_ids_train(std::vector<std::string>(6, "A"));
    EXPECT_THROW(inputs.set_task_sizes_train(_task_sizes_train), std::logic_error);
    EXPECT_THROW(inputs.set_prop_train(_prop_train), std::logic_error);
    EXPECT_THROW(inputs.set_phi_0(_phi_0), std::logic_error);
    inputs.clear_data();

    inputs.set_prop_test(std::vector<double>(6, 0.0));
    EXPECT_THROW(inputs.set_task_sizes_test(_task_sizes_test), std::logic_error);
    EXPECT_THROW(inputs.set_sample_ids_test(_sample_ids_test), std::logic_error);
    EXPECT_THROW(inputs.set_phi_0(_phi_0), std::logic_error);
    EXPECT_THROW(inputs.set_leave_out_inds(_leave_out_inds), std::logic_error);
    inputs.clear_data();

    inputs.set_leave_out_inds({1, 2, 3});
    EXPECT_THROW(inputs.set_task_sizes_test(_task_sizes_test), std::logic_error);
    EXPECT_THROW(inputs.set_sample_ids_test(_sample_ids_test), std::logic_error);
    EXPECT_THROW(inputs.set_phi_0(_phi_0), std::logic_error);
    EXPECT_THROW(inputs.set_prop_test(_prop_test), std::logic_error);
    inputs.clear_data();

    inputs.set_sample_ids_test(std::vector<std::string>(6, "A"));
    EXPECT_THROW(inputs.set_task_sizes_test(_task_sizes_test), std::logic_error);
    EXPECT_THROW(inputs.set_prop_test(_prop_test), std::logic_error);
    EXPECT_THROW(inputs.set_phi_0(_phi_0), std::logic_error);
    EXPECT_THROW(inputs.set_leave_out_inds(_leave_out_inds), std::logic_error);
    inputs.clear_data();

    _phi_0 = {FeatureNode(0, "feat_1", {1.0, 2.0, 3.0, 4.0}, {5.0, 6.0}, Unit("m"))};
    inputs.set_phi_0(_phi_0);
    EXPECT_THROW(inputs.set_prop_train(_prop_train), std::logic_error);
    EXPECT_THROW(inputs.set_task_sizes_train(_task_sizes_train), std::logic_error);
    EXPECT_THROW(inputs.set_sample_ids_train(_sample_ids_train), std::logic_error);

    EXPECT_THROW(inputs.set_prop_test(_prop_test), std::logic_error);
    EXPECT_THROW(inputs.set_task_sizes_test(_task_sizes_test), std::logic_error);
    EXPECT_THROW(inputs.set_sample_ids_test(_sample_ids_test), std::logic_error);
    EXPECT_THROW(inputs.set_leave_out_inds(_leave_out_inds), std::logic_error);
    inputs.clear_data();
}
}  // namespace
