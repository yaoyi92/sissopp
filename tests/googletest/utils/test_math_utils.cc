// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include <gtest/gtest.h>

#include <utils/math_funcs.hpp>

namespace
{
// Setup initial test vectors
std::vector<double> dVec1(16, 1.0);
std::vector<double> dVec2 = {1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0};
std::vector<double> dVec3 = {2.0, 4.0, 4.0, 4.0, 5.0, 5.0, 7.0, 9.0};

std::vector<int> iVec1(16, 1);
std::vector<int> iVec2 = {1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
std::vector<int> iVec3 = {2, 4, 4, 4, 5, 5, 7, 9};

//test mean calculations
TEST(MathUtils, MeanTest)
{
    EXPECT_EQ(util_funcs::mean(dVec1), 1.0);
    EXPECT_EQ(util_funcs::mean(iVec1), 1.0);

    EXPECT_EQ(util_funcs::mean(dVec1.data(), 16), 1.0);
    EXPECT_EQ(util_funcs::mean(iVec1.data(), 16), 1.0);

    EXPECT_EQ(util_funcs::mean(dVec2), 0.25);
    EXPECT_EQ(util_funcs::mean(iVec2), 0.25);

    EXPECT_EQ(util_funcs::mean(dVec2.data(), 16), 0.25);
    EXPECT_EQ(util_funcs::mean(iVec2.data(), 16), 0.25);

    EXPECT_EQ(util_funcs::mean(dVec3), 5.0);
    EXPECT_EQ(util_funcs::mean(iVec3), 5.0);

    EXPECT_EQ(util_funcs::mean(dVec3.data(), 8), 5.0);
    EXPECT_EQ(util_funcs::mean(iVec3.data(), 8), 5.0);
}

//test standard deviation calculations
TEST(MathUtils, STDTest)
{
    EXPECT_EQ(util_funcs::stand_dev(dVec1), 0.0);
    EXPECT_EQ(util_funcs::stand_dev(dVec1.data(), 16), 0.0);
    EXPECT_EQ(util_funcs::stand_dev(dVec1, 1.0), 0.0);
    EXPECT_EQ(util_funcs::stand_dev(dVec1.data(), 16, 1.0), 0.0);

    EXPECT_EQ(util_funcs::stand_dev(dVec3), 2.0);
    EXPECT_EQ(util_funcs::stand_dev(dVec3.data(), 8), 2.0);
    EXPECT_EQ(util_funcs::stand_dev(dVec3, 5.0), 2.0);
    EXPECT_EQ(util_funcs::stand_dev(dVec3.data(), 8, 5.0), 2.0);
}

//test norm calculations
TEST(MathUtils, NormTest)
{
    EXPECT_EQ(util_funcs::norm(dVec1), 4.0);
    EXPECT_EQ(util_funcs::norm(dVec1.data(), 16), 4.0);

    EXPECT_EQ(util_funcs::norm(dVec2), 2.0);
    EXPECT_EQ(util_funcs::norm(dVec2.data(), 16), 2.0);
}

//test Pearson correlation
TEST(MathUtils, RTest)
{
    std::vector<double> dNeg2(16, 0);
    std::transform(dVec2.begin(), dVec2.end(), dNeg2.begin(), [](double dd) { return -1.0 * dd; });

    std::vector<int> szs = {2, 14};
    EXPECT_FALSE(std::isfinite(util_funcs::r(dVec1.data(), dVec2.data(), 16)));
    EXPECT_FALSE(std::isfinite(util_funcs::r(dVec1.data(), dVec2.data(), szs)));
    EXPECT_FALSE(std::isfinite(util_funcs::r(dVec1.data(), dVec2.data(), szs.data(), 2)));

    EXPECT_TRUE(std::isfinite(util_funcs::r(dVec2.data(), dVec2.data(), 16)));
    EXPECT_FALSE(std::isfinite(util_funcs::r(dVec2.data(), dVec2.data(), szs)));
    EXPECT_FALSE(std::isfinite(util_funcs::r(dVec2.data(), dVec2.data(), szs.data(), 2)));

    szs = {8, 8};
    double mean_a = util_funcs::mean(dVec2);
    double std_a = util_funcs::stand_dev(dVec2);

    double mean_b = util_funcs::mean(dNeg2);
    double std_b = util_funcs::stand_dev(dNeg2);

    std::vector<double> mean_a_vec = {util_funcs::mean(dVec2.data(), 8),
                                      util_funcs::mean(dVec2.data() + 8, 8)};
    std::vector<double> std_a_vec = {util_funcs::stand_dev(dVec2.data(), 8, mean_a_vec[0]),
                                     util_funcs::stand_dev(dVec2.data() + 8, 8, mean_a_vec[1])};

    std::vector<double> mean_b_vec = {util_funcs::mean(dNeg2.data(), 8),
                                      util_funcs::mean(dNeg2.data() + 8, 8)};
    std::vector<double> std_b_vec = {util_funcs::stand_dev(dNeg2.data(), 8, mean_b_vec[0]),
                                     util_funcs::stand_dev(dNeg2.data() + 8, 8, mean_b_vec[1])};

    EXPECT_LT(
        std::abs(1.0 - util_funcs::r(dVec2.data(), dVec2.data(), 16, mean_a, std_a, mean_a, std_a)),
        1.0e-10);
    EXPECT_LT(
        std::abs(
            1.0 -
            util_funcs::r(
                dVec2.data(), dVec2.data(), szs, mean_a_vec, std_a_vec, mean_a_vec, std_a_vec)),
        1.0e-10);
    EXPECT_LT(std::abs(1.0 - util_funcs::r(dVec2.data(),
                                           dVec2.data(),
                                           szs.data(),
                                           mean_a_vec.data(),
                                           std_a_vec.data(),
                                           mean_a_vec.data(),
                                           std_a_vec.data(),
                                           2)),
              1.0e-10);

    EXPECT_LT(
        std::abs(1.0 + util_funcs::r(dVec2.data(), dNeg2.data(), 16, mean_a, std_a, mean_b, std_b)),
        1e-10);
    EXPECT_LT(
        std::abs(
            1.0 -
            util_funcs::r(
                dVec2.data(), dNeg2.data(), szs, mean_a_vec, std_a_vec, mean_b_vec, std_b_vec)),
        1e-10);
    EXPECT_LT(std::abs(1.0 - util_funcs::r(dVec2.data(),
                                           dNeg2.data(),
                                           szs.data(),
                                           mean_a_vec.data(),
                                           std_a_vec.data(),
                                           mean_b_vec.data(),
                                           std_b_vec.data(),
                                           2)),
              1e-10);

    EXPECT_LT(std::abs(1.0 - util_funcs::r(dVec2.data(), dVec2.data(), 16, mean_a, std_a)),
              1.0e-10);
    EXPECT_LT(std::abs(1.0 - util_funcs::r(dVec2.data(), dVec2.data(), szs, mean_a_vec, std_a_vec)),
              1.0e-10);
    EXPECT_LT(
        std::abs(
            1.0 -
            util_funcs::r(
                dVec2.data(), dVec2.data(), szs.data(), mean_a_vec.data(), std_a_vec.data(), 2)),
        1.0e-10);

    EXPECT_LT(std::abs(1.0 + util_funcs::r(dVec2.data(), dNeg2.data(), 16, mean_a, std_a)), 1e-10);
    EXPECT_LT(std::abs(1.0 - util_funcs::r(dVec2.data(), dNeg2.data(), szs, mean_a_vec, std_a_vec)),
              1e-10);
    EXPECT_LT(
        std::abs(
            1.0 -
            util_funcs::r(
                dVec2.data(), dNeg2.data(), szs.data(), mean_a_vec.data(), std_a_vec.data(), 2)),
        1e-10);

    EXPECT_LT(std::abs(1.0 - util_funcs::r(dVec2.data(), dVec2.data(), 16)), 1.0e-10);
    EXPECT_LT(std::abs(1.0 - util_funcs::r(dVec2.data(), dVec2.data(), szs)), 1.0e-10);
    EXPECT_LT(std::abs(1.0 - util_funcs::r(dVec2.data(), dVec2.data(), szs.data(), 2)), 1.0e-10);

    EXPECT_LT(std::abs(1.0 + util_funcs::r(dVec2.data(), dNeg2.data(), 16)), 1e-10);
    EXPECT_LT(std::abs(1.0 - util_funcs::r(dVec2.data(), dNeg2.data(), szs)), 1e-10);
    EXPECT_LT(std::abs(1.0 - util_funcs::r(dVec2.data(), dNeg2.data(), szs.data(), 2)), 1e-10);
}

//test r^2
TEST(MathUtils, R2Test)
{
    std::vector<double> dNeg2(16, 0);
    std::transform(dVec2.begin(), dVec2.end(), dNeg2.begin(), [](double dd) { return -1.0 * dd; });

    std::vector<int> szs = {2, 14};
    EXPECT_FALSE(std::isfinite(util_funcs::r2(dVec1.data(), dVec2.data(), 16)));
    EXPECT_FALSE(std::isfinite(util_funcs::r2(dVec1.data(), dVec2.data(), szs)));
    EXPECT_FALSE(std::isfinite(util_funcs::r2(dVec1.data(), dVec2.data(), szs.data(), 2)));

    EXPECT_TRUE(std::isfinite(util_funcs::r2(dVec2.data(), dVec2.data(), 16)));
    EXPECT_FALSE(std::isfinite(util_funcs::r2(dVec2.data(), dVec2.data(), szs)));
    EXPECT_FALSE(std::isfinite(util_funcs::r2(dVec2.data(), dVec2.data(), szs.data(), 2)));

    szs = {8, 8};
    double mean_a = util_funcs::mean(dVec2);
    double std_a = util_funcs::stand_dev(dVec2, mean_a);

    double mean_b = util_funcs::mean(dNeg2);
    double std_b = util_funcs::stand_dev(dNeg2, mean_b);

    std::vector<double> mean_a_vec = {util_funcs::mean(dVec2.data(), 8),
                                      util_funcs::mean(dVec2.data() + 8, 8)};
    std::vector<double> std_a_vec = {util_funcs::stand_dev(dVec2.data(), 8, mean_a_vec[0]),
                                     util_funcs::stand_dev(dVec2.data() + 8, 8, mean_a_vec[1])};

    std::vector<double> mean_b_vec = {util_funcs::mean(dNeg2.data(), 8),
                                      util_funcs::mean(dNeg2.data() + 8, 8)};
    std::vector<double> std_b_vec = {util_funcs::stand_dev(dNeg2.data(), 8, mean_b_vec[0]),
                                     util_funcs::stand_dev(dNeg2.data() + 8, 8, mean_b_vec[1])};

    EXPECT_LT(std::abs(1.0 - util_funcs::r2(
                                 dVec2.data(), dVec2.data(), 16, mean_a, std_a, mean_a, std_a)),
              1.0e-10);
    EXPECT_LT(
        std::abs(
            1.0 -
            util_funcs::r2(
                dVec2.data(), dVec2.data(), szs, mean_a_vec, std_a_vec, mean_a_vec, std_a_vec)),
        1.0e-10);
    EXPECT_LT(std::abs(1.0 - util_funcs::r2(dVec2.data(),
                                            dVec2.data(),
                                            szs.data(),
                                            mean_a_vec.data(),
                                            std_a_vec.data(),
                                            mean_a_vec.data(),
                                            std_a_vec.data(),
                                            2)),
              1.0e-10);

    EXPECT_LT(std::abs(1.0 - util_funcs::r2(
                                 dVec2.data(), dNeg2.data(), 16, mean_a, std_a, mean_b, std_b)),
              1e-10);
    EXPECT_LT(
        std::abs(
            1.0 -
            util_funcs::r2(
                dVec2.data(), dNeg2.data(), szs, mean_a_vec, std_a_vec, mean_b_vec, std_b_vec)),
        1e-10);
    EXPECT_LT(std::abs(1.0 - util_funcs::r2(dVec2.data(),
                                            dNeg2.data(),
                                            szs.data(),
                                            mean_a_vec.data(),
                                            std_a_vec.data(),
                                            mean_b_vec.data(),
                                            std_b_vec.data(),
                                            2)),
              1e-10);

    EXPECT_LT(std::abs(1.0 - util_funcs::r2(dVec2.data(), dVec2.data(), 16, mean_a, std_a)),
              1.0e-10);
    EXPECT_LT(
        std::abs(1.0 - util_funcs::r2(dVec2.data(), dVec2.data(), szs, mean_a_vec, std_a_vec)),
        1.0e-10);
    EXPECT_LT(
        std::abs(
            1.0 -
            util_funcs::r2(
                dVec2.data(), dVec2.data(), szs.data(), mean_a_vec.data(), std_a_vec.data(), 2)),
        1.0e-10);

    EXPECT_LT(std::abs(1.0 - util_funcs::r2(dVec2.data(), dNeg2.data(), 16, mean_a, std_a)), 1e-10);
    EXPECT_LT(
        std::abs(1.0 - util_funcs::r2(dVec2.data(), dNeg2.data(), szs, mean_a_vec, std_a_vec)),
        1e-10);
    EXPECT_LT(
        std::abs(
            1.0 -
            util_funcs::r2(
                dVec2.data(), dNeg2.data(), szs.data(), mean_a_vec.data(), std_a_vec.data(), 2)),
        1e-10);

    EXPECT_LT(std::abs(1.0 - util_funcs::r2(dVec2.data(), dVec2.data(), 16)), 1.0e-10);
    EXPECT_LT(std::abs(1.0 - util_funcs::r2(dVec2.data(), dVec2.data(), szs)), 1.0e-10);
    EXPECT_LT(std::abs(1.0 - util_funcs::r2(dVec2.data(), dVec2.data(), szs.data(), 2)), 1.0e-10);

    EXPECT_LT(std::abs(1.0 - util_funcs::r2(dVec2.data(), dNeg2.data(), 16)), 1e-10);
    EXPECT_LT(std::abs(1.0 - util_funcs::r2(dVec2.data(), dNeg2.data(), szs)), 1e-10);
    EXPECT_LT(std::abs(1.0 - util_funcs::r2(dVec2.data(), dNeg2.data(), szs.data(), 2)), 1e-10);
}

//test log_r^2
TEST(MathUtils, LogR2Test)
{
    std::vector<double> dNeg2(16, 0);
    std::vector<double> log_x(16, 0);
    std::transform(dVec2.begin(), dVec2.end(), dNeg2.begin(), [](double dd) { return -1.0 * dd; });

    std::vector<int> szs = {2, 14};
    EXPECT_FALSE(std::isfinite(util_funcs::log_r2(dVec1.data(), dVec2.data(), log_x.data(), 16)));
    EXPECT_FALSE(std::isfinite(util_funcs::log_r2(dVec1.data(), dVec2.data(), log_x.data(), szs)));
    EXPECT_FALSE(
        std::isfinite(util_funcs::log_r2(dVec1.data(), dVec2.data(), log_x.data(), szs.data(), 2)));

    EXPECT_FALSE(std::isfinite(util_funcs::log_r2(dVec2.data(), dVec2.data(), log_x.data(), 16)));
    EXPECT_FALSE(std::isfinite(util_funcs::log_r2(dVec2.data(), dVec2.data(), log_x.data(), szs)));
    EXPECT_FALSE(
        std::isfinite(util_funcs::log_r2(dVec2.data(), dVec2.data(), log_x.data(), szs.data(), 2)));

    szs = {2, 2};
    std::vector<double> x = {1, 10, 1000, 10000};
    std::vector<double> y = {0, 1, 3, 4};
    EXPECT_LT(std::abs(1.0 - util_funcs::log_r2(x.data(), y.data(), log_x.data(), 4)), 1.0e-10);
    EXPECT_LT(std::abs(1.0 - util_funcs::log_r2(x.data(), y.data(), log_x.data(), szs)), 1.0e-10);
    EXPECT_LT(std::abs(1.0 - util_funcs::log_r2(x.data(), y.data(), log_x.data(), szs.data(), 2)),
              1.0e-10);

    double mean_b = util_funcs::mean(y);
    double std_b = util_funcs::stand_dev(y, mean_b);

    std::vector<double> mean_b_vec = {util_funcs::mean(y.data(), 2),
                                      util_funcs::mean(y.data() + 2, 2)};
    std::vector<double> std_b_vec = {util_funcs::stand_dev(y.data(), 2),
                                     util_funcs::stand_dev(y.data() + 2, 2)};

    EXPECT_LT(
        std::abs(1.0 - util_funcs::log_r2(x.data(), y.data(), log_x.data(), 4, mean_b, std_b)),
        1.0e-10);
    EXPECT_LT(std::abs(1.0 - util_funcs::log_r2(
                                 x.data(), y.data(), log_x.data(), szs, mean_b_vec, std_b_vec)),
              1.0e-10);
    EXPECT_LT(std::abs(1.0 - util_funcs::log_r2(x.data(),
                                                y.data(),
                                                log_x.data(),
                                                szs.data(),
                                                mean_b_vec.data(),
                                                std_b_vec.data(),
                                                2)),
              1.0e-10);
}

//test Spearman correlation
TEST(MathUtils, SpearmanRTest)
{
    std::vector<double> dNeg2(16, 0);

    std::vector<double> rank_a(16, 0);
    std::vector<double> rank_b(16, 0);
    std::vector<int> index(16, 0);

    std::transform(dVec2.begin(), dVec2.end(), dNeg2.begin(), [](double dd) { return -1.0 * dd; });

    std::vector<int> szs = {2, 14};
    EXPECT_FALSE(std::isfinite(util_funcs::r(dVec1.data(), dVec2.data(), 16)));
    EXPECT_FALSE(std::isfinite(util_funcs::r(dVec1.data(), dVec2.data(), szs)));
    EXPECT_FALSE(std::isfinite(util_funcs::r(dVec1.data(), dVec2.data(), szs.data(), 2)));

    EXPECT_TRUE(std::isfinite(util_funcs::r(dVec2.data(), dVec2.data(), 16)));
    EXPECT_FALSE(std::isfinite(util_funcs::r(dVec2.data(), dVec2.data(), szs)));
    EXPECT_FALSE(std::isfinite(util_funcs::r(dVec2.data(), dVec2.data(), szs.data(), 2)));

    szs = {8, 8};
    double mean_a = util_funcs::mean(dVec2);
    double std_a = util_funcs::stand_dev(dVec2);

    double mean_b = util_funcs::mean(dNeg2);
    double std_b = util_funcs::stand_dev(dNeg2);

    std::vector<double> mean_a_vec = {util_funcs::mean(dVec2.data(), 8),
                                      util_funcs::mean(dVec2.data() + 8, 8)};
    std::vector<double> std_a_vec = {util_funcs::stand_dev(dVec2.data(), 8, mean_a_vec[0]),
                                     util_funcs::stand_dev(dVec2.data() + 8, 8, mean_a_vec[1])};

    std::vector<double> mean_b_vec = {util_funcs::mean(dNeg2.data(), 8),
                                      util_funcs::mean(dNeg2.data() + 8, 8)};
    std::vector<double> std_b_vec = {util_funcs::stand_dev(dNeg2.data(), 8, mean_b_vec[0]),
                                     util_funcs::stand_dev(dNeg2.data() + 8, 8, mean_b_vec[1])};

    EXPECT_LT(
        std::abs(1.0 -
                 util_funcs::spearman_r(
                     dVec2.data(), dVec2.data(), rank_a.data(), rank_b.data(), index.data(), 16)),
        1.0e-10);
    EXPECT_LT(
        std::abs(1.0 -
                 util_funcs::spearman_r(
                     dVec2.data(), dVec2.data(), rank_a.data(), rank_b.data(), index.data(), szs)),
        1.0e-10);
    EXPECT_LT(std::abs(1.0 - util_funcs::spearman_r(dVec2.data(),
                                                    dVec2.data(),
                                                    rank_a.data(),
                                                    rank_b.data(),
                                                    index.data(),
                                                    szs.data(),
                                                    2)),
              1.0e-10);
}

//test argsort
TEST(MathUtils, ArgSortTest)
{
    std::vector<double> to_sort = {3.0, 4.0, 2.0};
    std::vector<int> inds;

    inds = util_funcs::argsort(to_sort);
    EXPECT_EQ(std::abs(inds[0] - 2) + std::abs(inds[1] - 0) + std::abs(inds[2] - 1), 0.0);

    util_funcs::argsort(inds.data(), inds.data() + inds.size(), to_sort);
    EXPECT_EQ(std::abs(inds[0] - 2) + std::abs(inds[1] - 0) + std::abs(inds[2] - 1), 0.0);

    util_funcs::argsort(inds.data(), inds.data() + inds.size(), to_sort.data());
    EXPECT_EQ(std::abs(inds[0] - 2) + std::abs(inds[1] - 0) + std::abs(inds[2] - 1), 0.0);

    std::vector<double> to_sort_int = {3, 4, 2};
    inds = util_funcs::argsort(to_sort_int);
    EXPECT_EQ(std::abs(inds[0] - 2) + std::abs(inds[1] - 0) + std::abs(inds[2] - 1), 0.0);

    util_funcs::argsort(inds.data(), inds.data() + inds.size(), to_sort_int);
    EXPECT_EQ(std::abs(inds[0] - 2) + std::abs(inds[1] - 0) + std::abs(inds[2] - 1), 0.0);

    util_funcs::argsort(inds.data(), inds.data() + inds.size(), to_sort_int.data());
    EXPECT_EQ(std::abs(inds[0] - 2) + std::abs(inds[1] - 0) + std::abs(inds[2] - 1), 0.0);
}

// test max_abs_val
TEST(MathUtils, MaxAbsValTest)
{
    std::vector<double> dNeg3(16, 0);
    std::transform(dVec3.begin(), dVec3.end(), dNeg3.begin(), [](double dd) { return -1.0 * dd; });

    EXPECT_EQ(util_funcs::max_abs_val<double>(dVec3.data(), dVec3.data() + dVec3.size()), 9.0);
    EXPECT_EQ(util_funcs::max_abs_val<double>(dVec3.data(), dVec3.size()), 9.0);

    EXPECT_EQ(util_funcs::max_abs_val<double>(dNeg3.data(), dNeg3.data() + dNeg3.size()), 9.0);
    EXPECT_EQ(util_funcs::max_abs_val<double>(dNeg3.data(), dNeg3.size()), 9.0);
}

// test standardize
TEST(MathUtils, StandardizeTest)
{
    std::vector<double> test = {2, 4, 4, 4, 5, 5, 7, 9};
    std::vector<double> test_std = {-1.5, -0.5, -0.5, -0.5, 0, 0, 1, 2};

    util_funcs::standardize(test.data(), test.size(), test.data());

    EXPECT_LT(std::abs(util_funcs::mean(test)), 1e-10);
    EXPECT_LT(std::abs(util_funcs::stand_dev(test) - 1.0), 1e-10);

    std::transform(test.begin(), test.end(), test_std.begin(), test.begin(), std::minus<double>());
    EXPECT_TRUE(
        std::all_of(test.begin(), test.end(), [](double val) { return std::abs(val) < 1e-10; }));
}

// test iterate
TEST(MathUtils, IterateTest)
{
    std::vector<int> inds = {5, 4, 3};

    util_funcs::iterate(inds, 3, 1);
    EXPECT_EQ(std::abs(inds[0] - 5) + std::abs(inds[1] - 4) + std::abs(inds[2] - 2), 0.0);

    util_funcs::iterate(inds, 3, 2);
    EXPECT_EQ(std::abs(inds[0] - 5) + std::abs(inds[1] - 4) + std::abs(inds[2] - 0), 0.0);

    util_funcs::iterate(inds, 3, 7);
    EXPECT_EQ(std::abs(inds[0] - 4) + std::abs(inds[1] - 3) + std::abs(inds[2] - 2), 0.0);

    util_funcs::iterate(inds, 3, 9);
    EXPECT_EQ(std::abs(inds[0] - 2) + std::abs(inds[1] - 1) + std::abs(inds[2] - 0), 0.0);

    EXPECT_FALSE(util_funcs::iterate(inds, 3, 1));
}
}  // namespace
