// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include <gtest/gtest.h>

#include "feature_creation/node/ModelNode.hpp"
#include "mpi_interface/MPI_Ops.hpp"
#include "utils/compare_features.hpp"

namespace
{
//test mean calculations
TEST(CompFeats, CompFeatTest)
{
    std::vector<double> val_1 = {1.0, 2.0, 3.0, 4.0};
    std::vector<double> val_2 = {2.0, 1.0, 3.0, 4.0};
    std::vector<double> val_3 = {2.0, 4.0, 6.0, 8.0};

    std::vector<double> stand_val_1(4, 0);
    util_funcs::standardize(val_1.data(), 4, stand_val_1.data());

    std::vector<double> stand_val_2(4, 0);
    util_funcs::standardize(val_2.data(), 4, stand_val_2.data());

    std::vector<double> stand_val_3(4, 0);
    util_funcs::standardize(val_3.data(), 4, stand_val_3.data());

    std::vector<double> target = {1.0, 3.0, 5.0, 6.0};
    std::vector<double> scores = {0.9897782665572893};
    std::vector<int> scores_inds = {0};

    std::vector<node_ptr> selected(1);

    node_value_arrs::initialize_values_arr({4}, {0}, 1, 0, false);
    selected[0] = std::make_shared<FeatureNode>(0, "A", val_3, std::vector<double>(), Unit());
    std::vector<node_sc_pair> mpi_op_sel(1, node_sc_pair(selected[0], scores[0]));

    node_value_arrs::initialize_d_matrix_arr();
    node_value_arrs::resize_d_matrix_arr(1);

    std::copy_n(val_3.data(), val_3.size(), node_value_arrs::get_d_matrix_ptr(0));
    std::copy_n(stand_val_3.data(), stand_val_3.size(), node_value_arrs::get_stand_d_matrix_ptr(0));

    EXPECT_FALSE(comp_feats::valid_feature_against_selected_pearson_max_corr_1(
        stand_val_1.data(), 4, 1.0, scores, scores_inds, 0.9897782665572893, 1, 0));
    EXPECT_NE(comp_feats::valid_feature_against_selected_pearson_max_corr_1_feat_list(
                  stand_val_1.data(), 4, 1.0, mpi_op_sel, 0.9897782665572893),
              1);
    EXPECT_FALSE(comp_feats::valid_feature_against_selected_pearson_max_corr_1_mpi_op(
        stand_val_1.data(), 4, 1.0, mpi_op_sel, 0.9897782665572893));

    EXPECT_FALSE(comp_feats::valid_feature_against_selected_pearson(
        stand_val_1.data(), 4, 0.99, scores, scores_inds, 0.9897782665572893, 1, 0));
    EXPECT_NE(comp_feats::valid_feature_against_selected_pearson_feat_list(
                  stand_val_1.data(), 4, 0.99, mpi_op_sel, 0.9897782665572893),
              1);
    EXPECT_FALSE(comp_feats::valid_feature_against_selected_pearson_mpi_op(
        stand_val_1.data(), 4, 0.99, mpi_op_sel, 0.9897782665572893));

    EXPECT_TRUE(comp_feats::valid_feature_against_selected_pearson_max_corr_1(
        stand_val_2.data(), 4, 1.0, scores, scores_inds, 0.9028289727756884, 1, 0));
    EXPECT_EQ(comp_feats::valid_feature_against_selected_pearson_max_corr_1_feat_list(
                  stand_val_2.data(), 4, 1.0, mpi_op_sel, 0.9028289727756884),
              1);
    EXPECT_TRUE(comp_feats::valid_feature_against_selected_pearson_max_corr_1_mpi_op(
        stand_val_2.data(), 4, 1.0, mpi_op_sel, 0.9028289727756884));

    EXPECT_TRUE(comp_feats::valid_feature_against_selected_pearson(
        stand_val_2.data(), 4, 0.99, scores, scores_inds, 0.9028289727756884, 1, 0));
    EXPECT_EQ(comp_feats::valid_feature_against_selected_pearson_feat_list(
                  stand_val_2.data(), 4, 0.99, mpi_op_sel, 0.9028289727756884),
              1);
    EXPECT_TRUE(comp_feats::valid_feature_against_selected_pearson_mpi_op(
        stand_val_2.data(), 4, 0.99, mpi_op_sel, 0.9028289727756884));

    EXPECT_FALSE(comp_feats::valid_feature_against_selected_spearman_max_corr_1(
        stand_val_1.data(), 4, 1.0, scores, scores_inds, 0.9897782665572893, 1, 0));
    EXPECT_NE(comp_feats::valid_feature_against_selected_spearman_max_corr_1_feat_list(
                  stand_val_1.data(), 4, 1.0, mpi_op_sel, 0.9897782665572893),
              1);
    EXPECT_FALSE(comp_feats::valid_feature_against_selected_spearman_max_corr_1_mpi_op(
        stand_val_1.data(), 4, 1.0, mpi_op_sel, 0.9897782665572893));

    EXPECT_FALSE(comp_feats::valid_feature_against_selected_spearman(
        stand_val_1.data(), 4, 0.99, scores, scores_inds, 0.9897782665572893, 1, 0));
    EXPECT_NE(comp_feats::valid_feature_against_selected_spearman_feat_list(
                  stand_val_1.data(), 4, 0.99, mpi_op_sel, 0.9897782665572893),
              1);
    EXPECT_FALSE(comp_feats::valid_feature_against_selected_spearman_mpi_op(
        stand_val_1.data(), 4, 0.99, mpi_op_sel, 0.9897782665572893));

    EXPECT_TRUE(comp_feats::valid_feature_against_selected_spearman_max_corr_1(
        stand_val_2.data(), 4, 1.0, scores, scores_inds, 0.9028289727756884, 1, 0));
    EXPECT_EQ(comp_feats::valid_feature_against_selected_spearman_max_corr_1_feat_list(
                  stand_val_2.data(), 4, 1.0, mpi_op_sel, 0.9028289727756884),
              1);
    EXPECT_TRUE(comp_feats::valid_feature_against_selected_spearman_max_corr_1_mpi_op(
        stand_val_2.data(), 4, 1.0, mpi_op_sel, 0.9028289727756884));

    EXPECT_TRUE(comp_feats::valid_feature_against_selected_spearman(
        stand_val_2.data(), 4, 0.99, scores, scores_inds, 0.9028289727756884, 1, 0));
    EXPECT_EQ(comp_feats::valid_feature_against_selected_spearman_feat_list(
                  stand_val_2.data(), 4, 0.99, mpi_op_sel, 0.9028289727756884),
              1);
    EXPECT_TRUE(comp_feats::valid_feature_against_selected_spearman_mpi_op(
        stand_val_2.data(), 4, 0.99, mpi_op_sel, 0.9028289727756884));

    node_value_arrs::finalize_values_arr();
}
}  // namespace
