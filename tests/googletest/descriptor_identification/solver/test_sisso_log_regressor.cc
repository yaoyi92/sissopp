// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include <gtest/gtest.h>

#include <boost/filesystem.hpp>
#include <descriptor_identifier/solver/SISSOLogRegressor.hpp>
#include <random>

namespace
{
class SISSOLogRegressorTests : public ::testing::Test
{
protected:
    void SetUp() override
    {
        allowed_op_maps::set_node_maps();
        mpi_setup::init_mpi_env();

        std::vector<int> task_sizes_train = {90};
        std::vector<int> task_sizes_test = {10};

        node_value_arrs::initialize_values_arr(task_sizes_train, task_sizes_test, 3, 2, false);
        node_value_arrs::initialize_d_matrix_arr();

        std::vector<int> leave_out_inds = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

        std::vector<std::string> task_names = {"all"};
        std::vector<std::string> sample_ids_train(task_sizes_train[0]);
        for (int ii = 0; ii < task_sizes_train[0]; ++ii)
        {
            sample_ids_train[ii] = std::to_string(ii);
        }

        std::vector<std::string> sample_ids_test(task_sizes_test[0]);
        for (int ii = 0; ii < task_sizes_test[0]; ++ii)
        {
            sample_ids_test[ii] = std::to_string(ii);
        }

        std::vector<double> value_1(task_sizes_train[0], 0.0);
        std::vector<double> value_2(task_sizes_train[0], 0.0);
        std::vector<double> value_3(task_sizes_train[0], 0.0);

        std::vector<double> test_value_1(task_sizes_test[0], 0.0);
        std::vector<double> test_value_2(task_sizes_test[0], 0.0);
        std::vector<double> test_value_3(task_sizes_test[0], 0.0);

        std::default_random_engine generator(0);
        std::uniform_real_distribution<double> distribution_feats(0.01, 100.0);
        std::uniform_real_distribution<double> distribution_params(0.9, 1.1);

        for (int ii = 0; ii < task_sizes_train[0]; ++ii)
        {
            value_1[ii] = distribution_feats(generator);
            value_2[ii] = distribution_feats(generator);
            value_3[ii] = distribution_feats(generator);
        }

        for (int ii = 0; ii < task_sizes_test[0]; ++ii)
        {
            test_value_1[ii] = distribution_feats(generator);
            test_value_2[ii] = distribution_feats(generator);
            test_value_3[ii] = distribution_feats(generator);
        }

        FeatureNode feat_1(0, "A", value_1, test_value_1, Unit("m"), Domain("[0.01, 100.0]"));
        FeatureNode feat_2(1, "B", value_2, test_value_2, Unit("m"), Domain("[0.01, 100.0]"));
        FeatureNode feat_3(2, "C", value_3, test_value_3, Unit("s"), Domain("[0.01, 100.0]"));

        std::vector<FeatureNode> phi_0 = {feat_1, feat_2, feat_3};

        double a00 = distribution_params(generator);
        double a10 = distribution_params(generator);
        double c00 = distribution_feats(generator);

        _prop = std::vector<double>(task_sizes_train[0], 0.0);
        std::transform(value_1.begin(),
                       value_1.end(),
                       value_2.begin(),
                       _prop.begin(),
                       [&c00, &a00, &a10](double v1, double v2) {
                           return c00 * std::pow(v1 * v1, a00) * std::pow(v2, a10);
                       });

        _prop_test = std::vector<double>(task_sizes_test[0], 0.0);
        std::transform(test_value_1.begin(),
                       test_value_1.end(),
                       test_value_2.begin(),
                       _prop_test.begin(),
                       [&c00, &a00, &a10](double v1, double v2) {
                           return c00 * std::pow(v1 * v1, a00) * std::pow(v2, a10);
                       });

        _prop_zero_int = std::vector<double>(task_sizes_train[0], 0.0);
        std::transform(value_1.begin(),
                       value_1.end(),
                       value_2.begin(),
                       _prop_zero_int.begin(),
                       [&a00, &a10](double v1, double v2) {
                           return std::pow(v1 * v1, a00) * std::pow(v2, a10);
                       });

        _prop_test_zero_int = std::vector<double>(task_sizes_test[0], 0.0);
        std::transform(test_value_1.begin(),
                       test_value_1.end(),
                       test_value_2.begin(),
                       _prop_test_zero_int.begin(),
                       [&a00, &a10](double v1, double v2) {
                           return std::pow(v1 * v1, a00) * std::pow(v2, a10);
                       });

        std::vector<std::string> allowed_ops = {"div", "add", "mult", "sub"};
        std::vector<std::string> allowed_param_ops = {};

        inputs.set_task_sizes_train(task_sizes_train);
        inputs.set_task_sizes_test(task_sizes_test);
        inputs.set_sample_ids_train(sample_ids_train);
        inputs.set_sample_ids_test(sample_ids_test);
        inputs.set_task_names(task_names);
        inputs.set_allowed_param_ops(allowed_param_ops);
        inputs.set_allowed_ops(allowed_ops);
        inputs.set_phi_0(phi_0);
        inputs.set_task_sizes_train(task_sizes_train);
        inputs.set_task_sizes_test(task_sizes_test);
        inputs.set_leave_out_inds(leave_out_inds);
        inputs.set_calc_type("log_regression");
        inputs.set_max_rung(2);
        inputs.set_n_sis_select(10);
        inputs.set_n_rung_store(1);
        inputs.set_n_rung_generate(0);

        inputs.set_prop_label("Property");
        inputs.set_prop_unit(Unit("m"));
        inputs.set_n_dim(2);
        inputs.set_n_residual(2);
        inputs.set_n_models_store(3);
    }

    void TearDown() override { node_value_arrs::finalize_values_arr(); }

    InputParser inputs;

    std::vector<double> _prop;
    std::vector<double> _prop_test;

    std::vector<double> _prop_zero_int;
    std::vector<double> _prop_test_zero_int;
};

TEST_F(SISSOLogRegressorTests, FixInterceptFalseTest)
{
    inputs.set_prop_train(_prop);
    inputs.set_prop_test(_prop_test);
    inputs.set_fix_intercept(false);

    std::shared_ptr<FeatureSpace> feat_space = std::make_shared<FeatureSpace>(inputs);
    SISSOLogRegressor sisso(inputs, feat_space);

    std::vector<double> prop_comp(90, 0.0);
    std::transform(_prop.begin(),
                   _prop.end(),
                   sisso.prop_train().begin(),
                   prop_comp.begin(),
                   [](double p1, double p2) { return std::abs(std::log(p1) - p2); });
    EXPECT_FALSE(
        std::any_of(prop_comp.begin(), prop_comp.end(), [](double p) { return p > 1e-10; }));

    std::transform(_prop_test.begin(),
                   _prop_test.end(),
                   sisso.prop_test().begin(),
                   prop_comp.begin(),
                   [](double p1, double p2) { return std::abs(std::log(p1) - p2); });
    EXPECT_FALSE(
        std::any_of(prop_comp.begin(), prop_comp.begin() + 10, [](double p) { return p > 1e-10; }));

    EXPECT_EQ(sisso.n_samp(), 90);
    EXPECT_EQ(sisso.n_dim(), 2);
    EXPECT_EQ(sisso.n_residual(), 2);
    EXPECT_EQ(sisso.n_models_store(), 3);

    sisso.fit();

    EXPECT_EQ(sisso.models().size(), 2);
    EXPECT_EQ(sisso.models()[0].size(), 3);

    EXPECT_LT(sisso.models().back()[0].rmse(), 1e-7);
    EXPECT_LT(sisso.models().back()[0].test_rmse(), 1e-7);

    if (mpi_setup::comm->rank() == 0)
    {
        boost::filesystem::remove_all("feature_space/");
        boost::filesystem::remove_all("models/");
    }
}

TEST_F(SISSOLogRegressorTests, FixInterceptTrueTest)
{
    inputs.set_prop_train(_prop_zero_int);
    inputs.set_prop_test(_prop_test_zero_int);
    inputs.set_fix_intercept(true);

    std::shared_ptr<FeatureSpace> feat_space = std::make_shared<FeatureSpace>(inputs);
    SISSOLogRegressor sisso(inputs, feat_space);

    std::vector<double> prop_comp(90, 0.0);
    std::transform(_prop_zero_int.begin(),
                   _prop_zero_int.end(),
                   sisso.prop_train().begin(),
                   prop_comp.begin(),
                   [](double p1, double p2) { return std::abs(std::log(p1) - p2); });
    EXPECT_FALSE(
        std::any_of(prop_comp.begin(), prop_comp.end(), [](double p) { return p > 1e-10; }));

    std::transform(_prop_test_zero_int.begin(),
                   _prop_test_zero_int.end(),
                   sisso.prop_test().begin(),
                   prop_comp.begin(),
                   [](double p1, double p2) { return std::abs(std::log(p1) - p2); });
    EXPECT_FALSE(
        std::any_of(prop_comp.begin(), prop_comp.begin() + 10, [](double p) { return p > 1e-10; }));

    EXPECT_EQ(sisso.n_samp(), 90);
    EXPECT_EQ(sisso.n_dim(), 2);
    EXPECT_EQ(sisso.n_residual(), 2);
    EXPECT_EQ(sisso.n_models_store(), 3);

    sisso.fit();

    EXPECT_EQ(sisso.models().size(), 2);
    EXPECT_EQ(sisso.models()[0].size(), 3);

    EXPECT_LT(sisso.models().back()[0].rmse(), 1e-7);
    EXPECT_LT(sisso.models().back()[0].test_rmse(), 1e-7);

    if (mpi_setup::comm->rank() == 0)
    {
        boost::filesystem::remove_all("feature_space/");
        boost::filesystem::remove_all("models/");
    }
}
}  // namespace
