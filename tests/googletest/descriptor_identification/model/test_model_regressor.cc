// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include <gtest/gtest.h>

#include <boost/filesystem.hpp>
#include <descriptor_identifier/model/ModelRegressor.hpp>

namespace
{
class ModelRegssorTests : public ::testing::Test
{
protected:
    void SetUp() override
    {
        _leave_out_inds = {5, 11};
        _task_sizes_train = {5, 5};
        _task_sizes_test = {1, 1};

        std::vector<double> value_1 = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0};
        std::vector<double> test_value_1 = {1.0, 7.0};

        std::vector<double> value_2 = {1.10, 2.20, 3.10, 4.20, 5.10, 6.20, 7.10, 8.20, 9.10, 10.20};
        std::vector<double> test_value_2 = {2.10, 4.80};

        model_node_ptr feat_1 = std::make_shared<ModelNode>(0,
                                                            0,
                                                            "A",
                                                            "$A$",
                                                            "0",
                                                            "A",
                                                            value_1,
                                                            test_value_1,
                                                            std::vector<std::string>(1, "A"),
                                                            Unit("m"));
        model_node_ptr feat_2 = std::make_shared<ModelNode>(1,
                                                            0,
                                                            "B",
                                                            "$B$",
                                                            "1",
                                                            "B",
                                                            value_2,
                                                            test_value_2,
                                                            std::vector<std::string>(1, "B"),
                                                            Unit("m"));

        _features = {feat_1, feat_2};

        _prop = std::vector<double>(10, 0.0);
        _prop_test = std::vector<double>(2, 0.0);
        _error.resize(10, 0.0);

        std::transform(value_1.begin(),
                       value_1.begin() + 5,
                       value_2.begin(),
                       _prop.begin(),
                       [](double v1, double v2) { return 0.001 + v1 + v2; });
        std::transform(value_1.begin() + 5,
                       value_1.end(),
                       value_2.begin() + 5,
                       _prop.begin() + 5,
                       [](double v1, double v2) { return -6.5 + 1.25 * v1 - 0.4 * v2; });

        std::transform(test_value_1.begin(),
                       test_value_1.begin() + 1,
                       test_value_2.begin(),
                       _prop_test.begin(),
                       [](double v1, double v2) { return 0.001 + v1 + v2; });
        std::transform(test_value_1.begin() + 1,
                       test_value_1.end(),
                       test_value_2.begin() + 1,
                       _prop_test.begin() + 1,
                       [](double v1, double v2) { return -6.5 + 1.25 * v1 - 0.4 * v2; });

        task_names = {"task_1", "task_2"};
        _sample_ids_train = {"0", "1", "2", "3", "4", "6", "7", "8", "9", "10"};
        _sample_ids_test = {"5", "11"};
    }
    std::vector<std::string> _sample_ids_train;
    std::vector<std::string> _sample_ids_test;
    std::vector<std::string> task_names;

    std::vector<int> _leave_out_inds;
    std::vector<int> _task_sizes_train;
    std::vector<int> _task_sizes_test;

    std::vector<double> _prop;
    std::vector<double> _prop_test;
    std::vector<double> _error;

    std::vector<model_node_ptr> _features;
    std::shared_ptr<LossFunction> _loss;
};

TEST_F(ModelRegssorTests, FixInterceptFalseTest)
{
    _loss = std::make_shared<LossFunctionPearsonRMSE>(
        _prop, _prop_test, _task_sizes_train, _task_sizes_test, false, 2);

    ModelRegressor model("Property",
                         Unit("m"),
                         _loss,
                         _features,
                         _leave_out_inds,
                         _sample_ids_train,
                         _sample_ids_test,
                         task_names);

    model.copy_error(_error.data());
    EXPECT_LT(std::accumulate(_error.begin(),
                              _error.end(),
                              0.0,
                              [](double tot, double el) { return tot + std::abs(el); }),
              1e-5);

    EXPECT_STREQ(model.toString().c_str(), "c0 + a0 * A + a1 * B");
    EXPECT_LT(model.rmse(), 1e-10);
    EXPECT_LT(model.test_rmse(), 1e-10);
    EXPECT_LT(model.max_ae(), 1e-10);
    EXPECT_LT(model.test_max_ae(), 1e-10);
    EXPECT_LT(model.mae(), 1e-10);
    EXPECT_LT(model.test_mae(), 1e-10);
    EXPECT_LT(model.mape(), 1e-10);
    EXPECT_LT(model.test_mape(), 1e-10);
    EXPECT_LT(model.percentile_25_ae(), 1e-10);
    EXPECT_LT(model.percentile_25_test_ae(), 1e-10);
    EXPECT_LT(model.percentile_50_ae(), 1e-10);
    EXPECT_LT(model.percentile_50_test_ae(), 1e-10);
    EXPECT_LT(model.percentile_75_ae(), 1e-10);
    EXPECT_LT(model.percentile_75_test_ae(), 1e-10);
    EXPECT_LT(model.percentile_95_ae(), 1e-10);
    EXPECT_LT(model.percentile_95_test_ae(), 1e-10);

    EXPECT_EQ(model.n_samp_train(), 10);
    EXPECT_EQ(model.n_samp_test(), 2);
    EXPECT_EQ(model.n_dim(), 2);
    EXPECT_EQ(model.prop_unit(), Unit("m"));
    EXPECT_EQ(model.prop_label(), "Property");

    EXPECT_FALSE(model.fix_intercept());

    EXPECT_LT(std::abs(model.coefs()[0][0] - 1.0), 1e-10);
    EXPECT_LT(std::abs(model.coefs()[0][1] - 1.0), 1e-10);
    EXPECT_LT(std::abs(model.coefs()[0][2] - 0.001), 1e-10);
    EXPECT_LT(std::abs(model.coefs()[1][0] - 1.25), 1e-10);
    EXPECT_LT(std::abs(model.coefs()[1][1] + 0.4), 1e-10);
    EXPECT_LT(std::abs(model.coefs()[1][2] + 6.5), 1e-10);

    EXPECT_STREQ(model.toLatexString().c_str(), "$c_0 + a_0A + a_1B$");
    model.to_file("train_false.dat", true);
    model.to_file("test_false.dat", false);
}

TEST_F(ModelRegssorTests, FixInterceptFalseFileTest)
{
    ModelRegressor model("train_false.dat", "test_false.dat");

    model.copy_error(_error.data());
    EXPECT_LT(std::accumulate(_error.begin(),
                              _error.end(),
                              0.0,
                              [](double tot, double el) { return tot + std::abs(el); }),
              1e-5);

    EXPECT_STREQ(model.toString().c_str(), "c0 + a0 * A + a1 * B");
    EXPECT_LT(model.rmse(), 1e-10);
    EXPECT_LT(model.test_rmse(), 1e-10);
    EXPECT_LT(model.max_ae(), 1e-10);
    EXPECT_LT(model.test_max_ae(), 1e-10);
    EXPECT_LT(model.mae(), 1e-10);
    EXPECT_LT(model.test_mae(), 1e-10);
    EXPECT_LT(model.mape(), 1e-10);
    EXPECT_LT(model.test_mape(), 1e-10);
    EXPECT_LT(model.percentile_25_ae(), 1e-10);
    EXPECT_LT(model.percentile_25_test_ae(), 1e-10);
    EXPECT_LT(model.percentile_50_ae(), 1e-10);
    EXPECT_LT(model.percentile_50_test_ae(), 1e-10);
    EXPECT_LT(model.percentile_75_ae(), 1e-10);
    EXPECT_LT(model.percentile_75_test_ae(), 1e-10);
    EXPECT_LT(model.percentile_95_ae(), 1e-10);
    EXPECT_LT(model.percentile_95_test_ae(), 1e-10);

    EXPECT_EQ(model.n_samp_train(), 10);
    EXPECT_EQ(model.n_samp_test(), 2);
    EXPECT_EQ(model.n_dim(), 2);
    EXPECT_EQ(model.prop_unit(), Unit("m"));
    EXPECT_EQ(model.prop_label(), "Property");

    EXPECT_FALSE(model.fix_intercept());

    EXPECT_LT(std::abs(model.coefs()[0][0] - 1.0), 1e-10);
    EXPECT_LT(std::abs(model.coefs()[0][1] - 1.0), 1e-10);
    EXPECT_LT(std::abs(model.coefs()[0][2] - 0.001), 1e-10);
    EXPECT_LT(std::abs(model.coefs()[1][0] - 1.25), 1e-10);
    EXPECT_LT(std::abs(model.coefs()[1][1] + 0.4), 1e-10);
    EXPECT_LT(std::abs(model.coefs()[1][2] + 6.5), 1e-10);

    boost::filesystem::remove("train_false.dat");
    boost::filesystem::remove("test_false.dat");
}

TEST_F(ModelRegssorTests, FixInterceptTrueTest)
{
    std::transform(_features[0]->value_ptr(),
                   _features[0]->value_ptr() + 5,
                   _features[1]->value_ptr(),
                   _prop.begin(),
                   [](double v1, double v2) { return v1 + v2; });
    std::transform(_features[0]->value_ptr() + 5,
                   _features[0]->value_ptr() + 10,
                   _features[1]->value_ptr() + 5,
                   _prop.begin() + 5,
                   [](double v1, double v2) { return 1.25 * v1 - 0.4 * v2; });

    std::transform(_features[0]->test_value_ptr(),
                   _features[0]->test_value_ptr() + 1,
                   _features[1]->test_value_ptr(),
                   _prop_test.begin(),
                   [](double v1, double v2) { return v1 + v2; });
    std::transform(_features[0]->test_value_ptr() + 1,
                   _features[0]->test_value_ptr() + 2,
                   _features[1]->test_value_ptr() + 1,
                   _prop_test.begin() + 1,
                   [](double v1, double v2) { return 1.25 * v1 - 0.4 * v2; });

    _loss = std::make_shared<LossFunctionPearsonRMSE>(
        _prop, _prop_test, _task_sizes_train, _task_sizes_test, true, 2);

    ModelRegressor model("Property",
                         Unit("m"),
                         _loss,
                         _features,
                         _leave_out_inds,
                         _sample_ids_train,
                         _sample_ids_test,
                         task_names);

    model.copy_error(_error.data());
    EXPECT_LT(std::accumulate(_error.begin(),
                              _error.end(),
                              0.0,
                              [](double tot, double el) { return tot + std::abs(el); }),
              1e-5);

    EXPECT_STREQ(model.toString().c_str(), "a0 * A + a1 * B");
    EXPECT_LT(model.rmse(), 1e-10);
    EXPECT_LT(model.test_rmse(), 1e-10);
    EXPECT_LT(model.max_ae(), 1e-10);
    EXPECT_LT(model.test_max_ae(), 1e-10);
    EXPECT_LT(model.mae(), 1e-10);
    EXPECT_LT(model.test_mae(), 1e-10);
    EXPECT_LT(model.mape(), 1e-10);
    EXPECT_LT(model.test_mape(), 1e-10);
    EXPECT_LT(model.percentile_25_ae(), 1e-10);
    EXPECT_LT(model.percentile_25_test_ae(), 1e-10);
    EXPECT_LT(model.percentile_50_ae(), 1e-10);
    EXPECT_LT(model.percentile_50_test_ae(), 1e-10);
    EXPECT_LT(model.percentile_75_ae(), 1e-10);
    EXPECT_LT(model.percentile_75_test_ae(), 1e-10);
    EXPECT_LT(model.percentile_95_ae(), 1e-10);
    EXPECT_LT(model.percentile_95_test_ae(), 1e-10);

    EXPECT_EQ(model.n_samp_train(), 10);
    EXPECT_EQ(model.n_samp_test(), 2);
    EXPECT_EQ(model.n_dim(), 2);
    EXPECT_EQ(model.prop_unit(), Unit("m"));
    EXPECT_EQ(model.prop_label(), "Property");

    EXPECT_TRUE(model.fix_intercept());

    EXPECT_LT(std::abs(model.coefs()[0][0] - 1.0), 1e-10);
    EXPECT_LT(std::abs(model.coefs()[0][1] - 1.0), 1e-10);
    EXPECT_LT(std::abs(model.coefs()[1][0] - 1.25), 1e-10);
    EXPECT_LT(std::abs(model.coefs()[1][1] + 0.4), 1e-10);

    EXPECT_STREQ(model.toLatexString().c_str(), "$a_0A + a_1B$");

    model.to_file("train_true.dat", true);
    model.to_file("test_true.dat", false);
}

TEST_F(ModelRegssorTests, FixInterceptTrueFileTest)
{
    ModelRegressor model("train_true.dat", "test_true.dat");

    model.copy_error(_error.data());
    EXPECT_LT(std::accumulate(_error.begin(),
                              _error.end(),
                              0.0,
                              [](double tot, double el) { return tot + std::abs(el); }),
              1e-5);

    EXPECT_STREQ(model.toString().c_str(), "a0 * A + a1 * B");
    EXPECT_LT(model.rmse(), 1e-10);
    EXPECT_LT(model.test_rmse(), 1e-10);
    EXPECT_LT(model.max_ae(), 1e-10);
    EXPECT_LT(model.test_max_ae(), 1e-10);
    EXPECT_LT(model.mae(), 1e-10);
    EXPECT_LT(model.test_mae(), 1e-10);
    EXPECT_LT(model.mape(), 1e-10);
    EXPECT_LT(model.test_mape(), 1e-10);
    EXPECT_LT(model.percentile_25_ae(), 1e-10);
    EXPECT_LT(model.percentile_25_test_ae(), 1e-10);
    EXPECT_LT(model.percentile_50_ae(), 1e-10);
    EXPECT_LT(model.percentile_50_test_ae(), 1e-10);
    EXPECT_LT(model.percentile_75_ae(), 1e-10);
    EXPECT_LT(model.percentile_75_test_ae(), 1e-10);
    EXPECT_LT(model.percentile_95_ae(), 1e-10);
    EXPECT_LT(model.percentile_95_test_ae(), 1e-10);

    EXPECT_EQ(model.n_samp_train(), 10);
    EXPECT_EQ(model.n_samp_test(), 2);
    EXPECT_EQ(model.n_dim(), 2);
    EXPECT_EQ(model.prop_unit(), Unit("m"));
    EXPECT_EQ(model.prop_label(), "Property");

    EXPECT_TRUE(model.fix_intercept());

    EXPECT_LT(std::abs(model.coefs()[0][0] - 1.0), 1e-10);
    EXPECT_LT(std::abs(model.coefs()[0][1] - 1.0), 1e-10);
    EXPECT_LT(std::abs(model.coefs()[1][0] - 1.25), 1e-10);
    EXPECT_LT(std::abs(model.coefs()[1][1] + 0.4), 1e-10);

    boost::filesystem::remove("train_true.dat");
    boost::filesystem::remove("test_true.dat");
}

TEST_F(ModelRegssorTests, EvalTest)
{
    _loss = std::make_shared<LossFunctionPearsonRMSE>(
        _prop, _prop_test, _task_sizes_train, _task_sizes_test, false, 2);

    ModelRegressor model("Property",
                         Unit("m"),
                         _loss,
                         _features,
                         _leave_out_inds,
                         _sample_ids_train,
                         _sample_ids_test,
                         task_names);

    EXPECT_EQ(model.get_task_eval(), 0);
    std::vector<double> pt = {1.0, 1.0};
    EXPECT_LT(model.eval(pt.data(), task_names[0]) - 2.001, 1e-10);

    std::vector<std::vector<double>> pts = {{1.0}, {1.0}};
    EXPECT_LT(model.eval(pts.data(), {task_names[0]})[0] - 2.001, 1e-10);

    model.set_task_eval(1);
    EXPECT_EQ(model.get_task_eval(), 1);
    EXPECT_LT(model.eval(pt.data(), task_names[1]) + 5.65, 1e-10);
    EXPECT_LT(model.eval(pts.data(), {task_names[1]})[0] + 5.65, 1e-10);
}
}  // namespace
