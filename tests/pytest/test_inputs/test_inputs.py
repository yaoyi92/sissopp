# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import shutil
import numpy as np

np.random.seed(0)
from sissopp import (
    FeatureNode,
    Inputs,
    Unit,
    finalize_values_arr,
)
import matplotlib.pyplot as plt


def test_inputs():
    finalize_values_arr()

    inputs = Inputs()

    sample_ids_train = ["a", "b", "c"]
    inputs.sample_ids_train = sample_ids_train
    assert inputs.sample_ids_train == sample_ids_train

    sample_ids_test = ["d"]
    inputs.sample_ids_test = sample_ids_test
    assert inputs.sample_ids_test == sample_ids_test

    task_sizes_train = [2, 1]
    inputs.task_sizes_train = task_sizes_train
    assert inputs.task_sizes_train == task_sizes_train

    task_sizes_test = [1, 0]
    inputs.task_sizes_test = task_sizes_test
    assert inputs.task_sizes_test == task_sizes_test

    task_names = ["task_1", "task_2"]
    inputs.task_names = task_names
    assert inputs.task_names == task_names

    allowed_param_ops = ["log"]
    inputs.allowed_param_ops = allowed_param_ops
    assert inputs.allowed_param_ops == allowed_param_ops

    allowed_ops = ["sq", "cb"]
    inputs.allowed_ops = allowed_ops
    assert inputs.allowed_ops == allowed_ops

    prop_train = np.array([1.0, 4.0, 9.0])
    inputs.prop_train = prop_train
    assert np.all(inputs.prop_train == prop_train)

    prop_test = np.array([16.0])
    inputs.prop_test = prop_test
    assert np.all(inputs.prop_test == prop_test)

    leave_out_inds = [3]
    inputs.leave_out_inds = leave_out_inds
    assert inputs.leave_out_inds == leave_out_inds

    phi_0 = [FeatureNode(0, "feat_1", [1.0, 2.0, 3.0], [4.0], Unit("m"))]
    inputs.phi_0 = phi_0
    assert inputs.phi_0[0].expr == phi_0[0].expr

    prop_unit = Unit("m")
    inputs.prop_unit = prop_unit
    assert inputs.prop_unit == prop_unit

    filename = "googletest/inputs/sisso.json"
    inputs.filename = filename
    assert inputs.filename == filename

    data_file = "googletest/inputs/data.csv"
    inputs.data_file = data_file
    assert inputs.data_file == data_file

    prop_key = "property"
    inputs.prop_key = prop_key
    assert inputs.prop_key == prop_key

    prop_label = "property"
    inputs.prop_label = prop_label
    assert inputs.prop_label == prop_label

    task_key = "task"
    inputs.task_key = task_key
    assert inputs.task_key == task_key

    calc_type = "regression"
    inputs.calc_type = calc_type
    assert inputs.calc_type == calc_type

    cross_cor_max = 1.0
    inputs.cross_cor_max = cross_cor_max
    assert inputs.cross_cor_max == cross_cor_max

    l_bound = 1e-5
    inputs.l_bound = l_bound
    assert inputs.l_bound == l_bound

    u_bound = 1e8
    inputs.u_bound = u_bound
    assert inputs.u_bound == u_bound

    n_dim = 2
    inputs.n_dim = n_dim
    assert inputs.n_dim == n_dim

    max_rung = 1
    inputs.max_rung = max_rung
    assert inputs.max_rung == max_rung

    n_rung_store = 1
    inputs.n_rung_store = n_rung_store
    assert inputs.n_rung_store == n_rung_store

    n_rung_generate = 0
    inputs.n_rung_generate = n_rung_generate
    assert inputs.n_rung_generate == n_rung_generate

    n_sis_select = 1
    inputs.n_sis_select = n_sis_select
    assert inputs.n_sis_select == n_sis_select

    n_residual = 1
    inputs.n_residual = n_residual
    assert inputs.n_residual == n_residual

    n_models_store = 1
    inputs.n_models_store = n_models_store
    assert inputs.n_models_store == n_models_store

    max_param_depth = 1
    inputs.max_param_depth = max_param_depth
    assert inputs.max_param_depth == max_param_depth

    nlopt_seed = 10
    inputs.nlopt_seed = nlopt_seed
    assert inputs.nlopt_seed == nlopt_seed

    fix_intercept = False
    inputs.fix_intercept = fix_intercept
    assert inputs.fix_intercept == fix_intercept

    global_param_opt = True
    inputs.global_param_opt = global_param_opt
    assert inputs.global_param_opt == global_param_opt

    reparam_residual = True
    inputs.reparam_residual = reparam_residual
    assert inputs.reparam_residual == reparam_residual

    assert inputs.override_n_sis_select == False
    override_n_sis_select = True
    inputs.override_n_sis_select = override_n_sis_select
    assert inputs.override_n_sis_select == override_n_sis_select


if __name__ == "__main__":
    test_inputs()
