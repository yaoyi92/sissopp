function P = model_log_regressor(X)
% Returns the value of Prop = ((B + A))^a0 * ((|D - B|))^a1
%
% X = [
%     B,
%     A,
%     D,
% ]

if(size(X, 2) ~= 3)
    error("ERROR: X must have a size of 3 in the second dimension.")
end
B = reshape(X(:, 1), 1, []);
A = reshape(X(:, 2), 1, []);
D = reshape(X(:, 3), 1, []);

f0 = (B + A);
f1 = abs(D - B);

c0 = 0.0;
a0 = 1.2000000000e+00;
a1 = -1.9500000000e+00;

P = reshape(exp(c0) .* f0.^a0 .* f1.^a1, [], 1);
end
