function P = model_classifier(X)
% Returns the value of Class = [(feat_9 - feat_8), (feat_1 * feat_0)]
%
% X = [
%     feat_9,
%     feat_8,
%     feat_1,
%     feat_0,
% ]

if(size(X, 2) ~= 4)
    error("ERROR: X must have a size of 4 in the second dimension.")
end
feat_9 = reshape(X(:, 1), 1, []);
feat_8 = reshape(X(:, 2), 1, []);
feat_1 = reshape(X(:, 3), 1, []);
feat_0 = reshape(X(:, 4), 1, []);

f0 = (feat_9 - feat_8);
f1 = (feat_1 .* feat_0);

c0 = 9.0759507278e-01;
a0 = 1.3262056497e+00;
a1 = -1.7442399997e+00;

P = reshape(c0 + a0 * f0 + a1 * f1, [], 1);
end
