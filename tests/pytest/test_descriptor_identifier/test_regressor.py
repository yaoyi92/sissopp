# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import shutil
import numpy as np

np.random.seed(0)
from sissopp import (
    FeatureNode,
    FeatureSpace,
    Inputs,
    SISSORegressor,
    Unit,
    initialize_values_arr,
)
import pickle


def test_sisso_regressor():
    task_sizes_train = [95]
    task_sizes_test = [5]
    initialize_values_arr(task_sizes_train, task_sizes_test, 10, 2)
    inputs = Inputs()
    inputs.phi_0 = [
        FeatureNode(
            ff,
            f"feat_{ff}",
            np.random.random(task_sizes_train[0]) * 1e2 - 50,
            np.random.random(task_sizes_test[0]) * 1e2 - 50,
            Unit(),
        )
        for ff in range(10)
    ]

    inputs.task_names = ["task"]
    inputs.sample_ids_train = [
        str(ii)
        for ii in range(task_sizes_test[0], task_sizes_test[0] + task_sizes_train[0])
    ]
    inputs.sample_ids_test = [str(ii) for ii in range(task_sizes_test[0])]

    a0 = np.random.random() * 5.0 - 2.5
    a1 = np.random.random() * 5.0 - 2.5
    c0 = np.random.random() * 100.0 - 50
    inputs.prop_train = (
        c0
        + a0 * np.power(inputs.phi_0[0].value + inputs.phi_0[1].value, 2.0)
        - a1 * inputs.phi_0[4].value
    )
    inputs.prop_test = (
        c0
        + a0 * np.power(inputs.phi_0[0].test_value + inputs.phi_0[1].test_value, 2.0)
        - a1 * inputs.phi_0[4].test_value
    )

    inputs.allowed_ops = ["add", "sub", "mult", "sq", "cb", "sqrt", "cbrt"]
    inputs.calc_type = "regression"
    inputs.max_rung = 2
    inputs.n_sis_select = 10
    inputs.n_dim = 2
    inputs.n_residual = 1
    inputs.n_models_store = 1
    inputs.task_sizes_train = task_sizes_train
    inputs.task_sizes_test = task_sizes_test
    inputs.leave_out_inds = list(range(task_sizes_test[0]))
    inputs.fix_intercept = False
    inputs.prop_label = "prop"
    inputs.prop_unit = Unit("m")

    feat_space = FeatureSpace(inputs)

    sisso = SISSORegressor(inputs, feat_space)
    sisso.fit()

    shutil.rmtree("models/")
    shutil.rmtree("feature_space/")

    assert sisso.feat_space.phi_selected[9].expr == sisso.models[0][0].feats[0].expr

    assert sisso.models[1][0].rmse < 1e-7
    assert sisso.models[1][0].test_rmse < 1e-7

    assert np.all((inputs.prop_train - sisso.models[1][0].prop_train) <= 1e-7)
    assert np.all((inputs.prop_test - sisso.models[1][0].prop_test) <= 1e-7)

    assert np.all(inputs.task_sizes_train == sisso.models[1][0].task_sizes_train)
    assert np.all(inputs.task_sizes_test == sisso.models[1][0].task_sizes_test)

    assert np.all(np.abs(sisso.prop_train - inputs.prop_train) < 1e-10)
    assert np.all(np.abs(sisso.prop_test - inputs.prop_test) < 1e-10)

    assert np.all(sisso.task_sizes_train == inputs.task_sizes_train)
    assert np.all(sisso.task_sizes_test == inputs.task_sizes_test)

    # Check Pickeling
    pickled = pickle.dumps(sisso)
    sisso_unpick = pickle.loads(pickled)

    assert np.all(sisso.sample_ids_train == sisso_unpick.sample_ids_train)
    assert np.all(sisso.sample_ids_test == sisso_unpick.sample_ids_test)
    assert np.all(sisso.task_names == sisso_unpick.task_names)
    assert np.all(sisso.task_sizes_train == sisso_unpick.task_sizes_train)
    assert np.all(sisso.task_sizes_test == sisso_unpick.task_sizes_test)
    assert np.all(sisso.leave_out_inds == sisso_unpick.leave_out_inds)
    assert np.all(sisso.prop_train == sisso_unpick.prop_train)
    assert np.all(sisso.prop_test == sisso_unpick.prop_test)
    assert np.all(
        [
            f1.expr == f2.expr
            for f1, f2 in zip(sisso.feat_space.phi, sisso_unpick.feat_space.phi)
        ]
    )
    assert sisso.prop_unit == sisso_unpick.prop_unit
    assert sisso.prop_label == sisso_unpick.prop_label
    assert sisso.n_dim == sisso_unpick.n_dim
    assert sisso.n_residual == sisso_unpick.n_residual
    assert sisso.n_models_store == sisso_unpick.n_models_store
    assert sisso.fix_intercept == sisso_unpick.fix_intercept
    assert np.all(
        [str(m1) == str(m2) for m1, m2 in zip(sisso.models, sisso_unpick.models)]
    )


if __name__ == "__main__":
    test_sisso_regressor()
