# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from sissopp import ModelClassifier
from sissopp.postprocess.load_models import load_model
from sissopp.postprocess.classification import update_model_svm
from pathlib import Path

import numpy as np

np.random.seed(0)


class InvalidFeatureMade(Exception):
    pass


parent = Path(__file__).parent


def test_class_model_retrain_svm():
    model = load_model(
        str(parent / "model_files/train_classifier.dat"),
        str(parent / "model_files/test_classifier.dat"),
    )
    updated_model_np = update_model_svm(model, c=1.0, max_iter=1000000)
    updated_model_list = ModelClassifier(
        model,
        [list(coefs) for coefs in updated_model_np.coefs],
        updated_model_np.fit,
        updated_model_np.predict,
    )
    assert np.all(
        [
            abs(c_list - c_np) < 1e-8
            for c_list, c_np in zip(
                updated_model_np.coefs[0], updated_model_list.coefs[0]
            )
        ]
    )


if __name__ == "__main__":
    test_class_model_retrain_svm()
