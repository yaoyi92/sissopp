# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from sissopp import (
    FeatureNode,
    InvNode,
    SqNode,
    CbNode,
    SixPowNode,
    SqrtNode,
    CbrtNode,
    Unit,
    initialize_values_arr,
)

import numpy as np
import pickle

np.random.seed(0)


class InvalidFeatureMade(Exception):
    pass


def test_six_pow_node():
    task_sizes_train = [90]
    task_sizes_test = [10]
    initialize_values_arr(task_sizes_train, task_sizes_test, 2, 2)

    data_1 = np.random.random(task_sizes_train[0]) * 1e1 + 1e-10
    test_data_1 = np.random.random(task_sizes_test[0]) * 1e1 + 1e-10

    data_2 = np.random.choice([1.0, -1.0], task_sizes_train[0])
    test_data_2 = np.random.choice([1.0, -1.0], task_sizes_test[0])

    feat_1 = FeatureNode(0, "t_a", data_1, test_data_1, Unit())
    feat_2 = FeatureNode(1, "x_a", data_2, test_data_2, Unit())

    feats = []
    try:
        feats.append(SixPowNode(feat_1, 2, 1e-50, 1e-10))
        raise InvalidFeatureMade(
            "Taking the sixth power of the feature leads to values outside of user specified bounds"
        )
    except RuntimeError:
        pass

    try:
        feats.append(SixPowNode(feat_1, 2, 1e7, 1e50))
        raise InvalidFeatureMade(
            "Taking the sixth power of the feature leads to values outside of user specified bounds"
        )
    except RuntimeError:
        pass

    try:
        feats.append(SixPowNode(feat_2, 2, 1e-50, 1e50))
        raise InvalidFeatureMade(
            "Taking the sixth power of the feature creates to a constant feature"
        )
    except RuntimeError:
        pass

    feats.append(CbrtNode(feat_1, 4, 1e-50, 1e50))
    feats.append(InvNode(feat_1, 5, 1e-50, 1e50))
    feats.append(SqNode(feat_1, 6, 1e-50, 1e50))
    feats.append(CbNode(feat_1, 7, 1e-50, 1e50))
    feats.append(SqrtNode(feat_1, 8, 1e-50, 1e50))
    feats.append(SixPowNode(feat_1, 3, 1e-50, 1e50))

    try:
        feats.append(SixPowNode(feats[0], 13, 1e-50, 1e50))
        raise InvalidFeatureMade("Taking the sixth power of a CbrtNode")
    except RuntimeError:
        pass

    try:
        feats.append(SixPowNode(feats[1], 13, 1e-50, 1e50))
        raise InvalidFeatureMade("Taking the sixth power of a InvNode")
    except RuntimeError:
        pass

    try:
        feats.append(SixPowNode(feats[2], 13, 1e-50, 1e50))
        raise InvalidFeatureMade("Taking the sixth power of an SqNode")
    except RuntimeError:
        pass

    try:
        feats.append(SixPowNode(feats[3], 13, 1e-50, 1e50))
        raise InvalidFeatureMade("Taking the sixth power of a CbNode")
    except RuntimeError:
        pass

    try:
        feats.append(SixPowNode(feats[4], 13, 1e-50, 1e50))
        raise InvalidFeatureMade("Taking the sixth power of a SqrtNode")
    except RuntimeError:
        pass

    decomp = feats[-1].primary_feat_decomp
    assert len(decomp.keys()) == 1
    assert decomp["t_a"] == 1

    assert feats[-1].n_leaves == 1
    assert feats[-1].n_feats == 1
    assert feats[-1].feat(0).expr == "t_a"
    try:
        feats[-1].feat(2)
        raise ValueError("Accessing feature that should throw an error")
    except:
        pass
    assert feats[-1].matlab_fxn_expr == "(t_a).^6"

    # Check pickling
    feats[-1].selected = True
    feats[-1].d_mat_ind = 1
    pickled = pickle.dumps(feats[-1])

    feat_unpick = pickle.loads(pickled)
    assert feat_unpick.d_mat_ind == feats[-1].d_mat_ind
    assert feat_unpick.selected == feats[-1].selected

    feats[-1].selected = False
    feat_unpick.selected = False
    assert np.all(feat_unpick.value == feats[-1].value)
    assert np.all(feat_unpick.test_value == feats[-1].test_value)
    assert feat_unpick.expr == feats[-1].expr
    assert feat_unpick.unit == feats[-1].unit
    assert feat_unpick.arr_ind == feats[-1].arr_ind
    assert feat_unpick.feat_ind == feats[-1].feat_ind


if __name__ == "__main__":
    test_six_pow_node()
