# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from sissopp import Unit
from pathlib import Path

import numpy as np

np.random.seed(0)


class InvalidFeatureMade(Exception):
    pass


parent = Path(__file__).parent


def test_units():
    m = Unit("m")
    s = Unit("s")
    kg = Unit("Kg")
    N = kg * m / s ** 2
    J = kg * m ** 2 / s ** 2
    assert m ** 2 == Unit("m^2")
    assert m * s == Unit("m*s")
    assert m / s == Unit("m/s")
    assert m / m == Unit()
    assert N == Unit("Kg * m / s^2")
    assert N * m == J


if __name__ == "__main__":
    test_units()
