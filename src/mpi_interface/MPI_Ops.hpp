// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file mpi_interface/MPI_Ops.hpp
 *  @brief Defines MPI reduce all operator to facilitate a distributed sorting algorithm
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 *
 *  This is based off of a project from Meisam Farzalipour Tabriz at the MPCDF (mpi_topk)
 */

#ifndef SISSO_MPI_OPS
#define SISSO_MPI_OPS

#include <boost/mpi.hpp>

#include "utils/compare_features.hpp"

namespace mpi_reduce_op
{
// extern MPI_Op top_feats;

// clang-format off
    extern std::function<bool(const double*, const int, const double, const std::vector<node_sc_pair>&, const double)> IS_VALID; //!< Function used to calculate the scores for SIS without changing omp environment
    extern double CROSS_COR_MAX; //!< The maximum cross correlation between features
    extern int N_SIS_SELECT; //!< The number of features to select
// clang-format on

/**
     * @brief Get the top features of the combined input vectors
     *
     * @param in_vec_1 First vector to compare against
     * @param in_vec_2 Second vector to compare against
     * @return The N_SIS_SELECT best features in in_vec_1 and in_vec_2
     */
std::vector<node_sc_pair> select_top_feats(std::vector<node_sc_pair> in_vec_1,
                                           std::vector<node_sc_pair> in_vec_2);

/**
     * @brief Set CROSS_COR_MAX and N_SIS_SELECT
     *
     * @param project_type Type of projection operator in use
     * @param cross_cor_max maximum cross correlation between the features
     * @param n_sis_select the N_SIS_SELECT to use
     */
void set_op(std::string project_type, double cross_cor_max, int n_sis_select);
}  // namespace mpi_reduce_op

#endif

// // Copyright 2021 Thomas A. R. Purcell
// //
// // Licensed under the Apache License, Version 2.0 (the "License");
// // you may not use this file except in compliance with the License.
// // You may obtain a copy of the License at
// //
// //     http://www.apache.org/licenses/LICENSE-2.0
// //
// // Unless required by applicable law or agreed to in writing, software
// // distributed under the License is distributed on an "AS IS" BASIS,
// // WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// // See the License for the specific language governing permissions and
// // limitations under the License.

// /** @file mpi_interface/MPI_Ops.hpp
//  *  @brief Defines MPI reduce all operator to facilitate a distributed sorting algorithm
//  *
//  *  @author Thomas A. R. Purcell (tpurcell90)
//  *  @bug No known bugs.
//  *
//  *  This is based off of a project from Meisam Farzalipour Tabriz at the MPCDF (mpi_topk)
//  */

// #ifndef SISSO_MPI_OPS
// #define SISSO_MPI_OPS

// #include <boost/mpi.hpp>

// #include "utils/compare_features.hpp"

// namespace mpi_reduce_op
// {
//     // extern MPI_Op top_feats;
//     extern std::vector<double> CROSS_CORR_MAT; //!< Matrix storing the cross_correlation data
//     extern std::vector<double> IN_DATA_1; //!< Matrix storing relevant data for in_vec_1
//     extern std::vector<double> IN_DATA_2; //!< Matrix storing relevant data for in_vec_2
//     extern std::function<void(const std::vector<node_sc_pair>&, std::vector<double>&)> SET_IN_DATA; //!< Set the input data for the dgemm call
//     extern double CROSS_COR_MAX; //!< The maximum cross correlation between features
//     extern int N_SIS_SELECT; //!< The number of features to select
//     extern int N_SAMP; //!< Number of samples per feature

//     /**
//      * @brief Set the IN_DATA vectors for a problem
//      *
//      * @param in_vec Set of features to transfer the data to
//      * @param in_data The IN_DATA vector to store the value of the feature's data
//      */
//     void set_in_vec_pearson(const std::vector<node_sc_pair>& in_vec, std::vector<double>& in_data);

//     /**
//      * @brief Set the IN_DATA vectors for a problem
//      *
//      * @param in_vec Set of features to transfer the data to
//      * @param in_data The IN_DATA vector to store the value of the feature's data
//      */
//     void set_in_vec_spearman(const std::vector<node_sc_pair>& in_vec, std::vector<double>& in_data);

//     /**
//      * @brief Get the top features of the combined input vectors
//      *
//      * @param in_vec_1 First vector to compare against
//      * @param in_vec_2 Second vector to compare against
//      * @return The N_SIS_SELECT best features in in_vec_1 and in_vec_2
//      */
//     std::vector<node_sc_pair> select_top_feats(std::vector<node_sc_pair> in_vec_1, std::vector<node_sc_pair> in_vec_2);

//     /**
//      * @brief Set CROSS_COR_MAX and N_SIS_SELECT
//      *
//      * @param project_type Type of projection operator in use
//      * @param cross_cor_max maximum cross correlation between the features
//      * @param n_sis_select the N_SIS_SELECT to use
//      * @param n_samp number of samples used in the calculation.
//      */
//     void set_op(std::string project_type, double cross_cor_max, int n_sis_select, int n_samp);
// }

// #endif
