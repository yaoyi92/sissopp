// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file mpi_interface/MPI_Ops.cpp
 *  @brief Implements MPI reduce all operator to facilitate a distributed sorting algorithm
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 *
 *  This is based off of a project from Meisam Farzalipour Tabriz at the MPCDF (mpi_topk)
 */

#include "mpi_interface/MPI_Ops.hpp"

// MPI_Op top_feats;
std::function<bool(
    const double*, const int, const double, const std::vector<node_sc_pair>&, const double)>
    mpi_reduce_op::IS_VALID;
double mpi_reduce_op::CROSS_COR_MAX;
int mpi_reduce_op::N_SIS_SELECT;

void mpi_reduce_op::set_op(std::string project_type, double cross_cor_max, int n_sis_select)
{
    // MPI_Op_create(*select_top_feats, 1, &top_feats)
    N_SIS_SELECT = n_sis_select;
    CROSS_COR_MAX = cross_cor_max;
    if (project_type.compare("classification") == 0)
    {
        if (CROSS_COR_MAX < 0.99999)
        {
            IS_VALID = comp_feats::valid_feature_against_selected_spearman_mpi_op;
        }
        else
        {
            IS_VALID = comp_feats::valid_feature_against_selected_spearman_max_corr_1_mpi_op;
        }
    }
    else
    {
        if (CROSS_COR_MAX < 0.99999)
        {
            IS_VALID = comp_feats::valid_feature_against_selected_pearson_mpi_op;
        }
        else
        {
            IS_VALID = comp_feats::valid_feature_against_selected_pearson_max_corr_1_mpi_op;
        }
    }
}

std::vector<node_sc_pair> mpi_reduce_op::select_top_feats(std::vector<node_sc_pair> in_vec_1,
                                                          std::vector<node_sc_pair> in_vec_2)
{
    // Set up an output vector
    std::vector<node_sc_pair> out_vec;
    out_vec.reserve(N_SIS_SELECT);

    // Merge input vectors and sort
    in_vec_2.insert(in_vec_2.end(), in_vec_1.begin(), in_vec_1.end());
    std::sort(in_vec_2.begin(), in_vec_2.end());

    // Populate the output vector
    int ff = 0;
    int out_ind = 0;
    while ((out_ind < N_SIS_SELECT) && (ff < 2 * N_SIS_SELECT))
    {
        if (in_vec_2[ff].obj() == nullptr)
        {
            break;
        }
        if (IS_VALID(in_vec_2[ff].obj()->stand_value_ptr(),
                     in_vec_2[ff].obj()->n_samp(),
                     CROSS_COR_MAX,
                     out_vec,
                     in_vec_2[ff].score()))
        {
            out_vec.push_back(in_vec_2[ff]);
            ++out_ind;
        }
        ++ff;
    }
    return out_vec;
}

// // Copyright 2021 Thomas A. R. Purcell
// //
// // Licensed under the Apache License, Version 2.0 (the "License");
// // you may not use this file except in compliance with the License.
// // You may obtain a copy of the License at
// //
// //     http://www.apache.org/licenses/LICENSE-2.0
// //
// // Unless required by applicable law or agreed to in writing, software
// // distributed under the License is distributed on an "AS IS" BASIS,
// // WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// // See the License for the specific language governing permissions and
// // limitations under the License.

// /** @file mpi_interface/MPI_Ops.cpp
//  *  @brief Implements MPI reduce all operator to facilitate a distributed sorting algorithm
//  *
//  *  @author Thomas A. R. Purcell (tpurcell90)
//  *  @bug No known bugs.
//  *
//  *  This is based off of a project from Meisam Farzalipour Tabriz at the MPCDF (mpi_topk)
//  */
// #include <iomanip>
// #include "mpi_interface/MPI_Ops.hpp"

// // MPI_Op top_feats;
// double mpi_reduce_op::CROSS_COR_MAX;
// int mpi_reduce_op::N_SIS_SELECT;
// int mpi_reduce_op::N_SAMP;
// std::vector<double> mpi_reduce_op::CROSS_CORR_MAT;
// std::vector<double> mpi_reduce_op::IN_DATA_1;
// std::vector<double> mpi_reduce_op::IN_DATA_2;

// std::function<void(const std::vector<node_sc_pair>&, std::vector<double>&)> mpi_reduce_op::SET_IN_DATA;

// void mpi_reduce_op::set_op(std::string project_type, double cross_cor_max, int n_sis_select, int n_samp)
// {
//     // MPI_Op_create(*select_top_feats, 1, &top_feats)
//     N_SIS_SELECT = n_sis_select;
//     CROSS_COR_MAX = cross_cor_max;
//     N_SAMP = n_samp;
//     CROSS_CORR_MAT.resize(N_SIS_SELECT * N_SIS_SELECT);
//     IN_DATA_1.resize(N_SIS_SELECT * N_SAMP);
//     IN_DATA_2.resize(N_SIS_SELECT * N_SAMP);

//     if(project_type.compare("classification") != 0)
//     {
//         SET_IN_DATA = set_in_vec_pearson;
//     }
//     else
//     {
//         SET_IN_DATA = set_in_vec_spearman;
//     }
// }

// void mpi_reduce_op::set_in_vec_pearson(const std::vector<node_sc_pair>& in_vec, std::vector<double>& in_data)
// {
//     int n_non_zero = (
//         std::find_if(in_vec.begin(), in_vec.end(), [](node_sc_pair ns){return ns.score() == std::numeric_limits<double>::infinity();}) -
//         in_vec.begin()
//     );

//     std::fill_n(&in_data[n_non_zero * N_SAMP], (N_SIS_SELECT - n_non_zero) * N_SAMP, 0.0);
//     #pragma omp parallel
//     {
//         int start = (
//             omp_get_thread_num() * (n_non_zero / omp_get_num_threads()) +
//             std::min(omp_get_thread_num(), n_non_zero % omp_get_num_threads())
//         );
//         int end = (
//             (omp_get_thread_num() + 1) * (n_non_zero / omp_get_num_threads()) +
//             std::min(omp_get_thread_num() + 1, n_non_zero % omp_get_num_threads())
//         );
//         for(int ff = start; ff < end; ++ff)
//         {
//             std::copy_n(in_vec[ff]._feat->stand_value_ptr(), N_SAMP, &in_data[ff * N_SAMP]);
//         }
//     }
// }

// void mpi_reduce_op::set_in_vec_spearman(const std::vector<node_sc_pair>& in_vec, std::vector<double>& in_data)
// {
//     int n_non_zero = (
//         std::find_if(in_vec.begin(), in_vec.end(), [](node_sc_pair ns){return ns.score() == std::numeric_limits<double>::infinity();}) -
//         in_vec.begin()
//     );

//     std::fill_n(&in_data[n_non_zero * N_SAMP], (N_SIS_SELECT - n_non_zero) * N_SAMP, 0.0);
//     #pragma omp parallel
//     {
//         int start = (
//             omp_get_thread_num() * (n_non_zero / omp_get_num_threads()) +
//             std::min(omp_get_thread_num(), n_non_zero % omp_get_num_threads())
//         );
//         int end = (
//             (omp_get_thread_num() + 1) * (n_non_zero / omp_get_num_threads()) +
//             std::min(omp_get_thread_num() + 1, n_non_zero % omp_get_num_threads())
//         );
//         std::vector<int> index(N_SAMP, 0.0);
//         std::vector<double> rank(N_SAMP, 0.0);
//         for(int ff = start; ff < end; ++ff)
//         {
//             util_funcs::rank(
//                 in_vec[ff]._feat->value_ptr(),
//                 rank.data(),
//                 index.data(),
//                 N_SAMP
//             );
//             util_funcs::standardize(
//                 rank.data(),
//                 N_SAMP,
//                 &in_data[ff * N_SAMP]
//             );
//         }
//     }
// }

// std::vector<node_sc_pair> mpi_reduce_op::select_top_feats(std::vector<node_sc_pair> in_vec_1, std::vector<node_sc_pair> in_vec_2)
// {
//     std::cout << N_SIS_SELECT << std::endl;
//     // Set up an output vector
//     std::vector<node_sc_pair> out_vec(N_SIS_SELECT, node_sc_pair());

//     std::cout << "sort invec"<< std::endl;
//     // Sort both input vectors
//     std::sort(in_vec_1.begin(), in_vec_1.end());
//     std::sort(in_vec_2.begin(), in_vec_2.end());

//     std::cout << "scores set up"<< std::endl;
//     std::vector<double> scores(2 * N_SIS_SELECT);
//     std::transform(
//         in_vec_1.begin(),
//         in_vec_1.end(),
//         scores.begin(),
//         [](node_sc_pair nsc){return nsc.score();}
//     );
//     std::transform(
//         in_vec_2.begin(),
//         in_vec_2.end(),
//         scores.begin() + N_SIS_SELECT,
//         [](node_sc_pair nsc){return nsc.score();}
//     );

//     std::cout << "inds"<< std::endl;
//     std::vector<int> inds(2 * N_SIS_SELECT);
//     std::iota(inds.begin(), inds.end(), -N_SIS_SELECT);
//     std::sort(inds.begin(), inds.end(), [&scores](int i1, int i2){return scores[i1 + N_SIS_SELECT] < scores[i2 + N_SIS_SELECT];});

//     // Copy the data from in_vec_1 and in_vec_2
//     SET_IN_DATA(in_vec_1, IN_DATA_1);
//     SET_IN_DATA(in_vec_2, IN_DATA_2);

//     std::cout << "dgemm"<< std::endl;
//     // Calculate Correlations
//     dgemm_(
//         'T',
//         'N',
//         N_SIS_SELECT,
//         N_SIS_SELECT,
//         N_SAMP,
//         1.0 / static_cast<double>(N_SAMP),
//         IN_DATA_2.data(),
//         N_SAMP,
//         IN_DATA_1.data(),
//         N_SAMP,
//         0.0,
//         CROSS_CORR_MAT.data(),
//         N_SIS_SELECT
//     );

//     std::cout << "output"<< std::endl;
//     // Populate the output vector
//     int oo = 0;
//     int o1 = 0;
//     int o2 = 0;
//     int ii = 0;
//     int max_corr_ind = 0;
//     while((oo < N_SIS_SELECT) && (ii < inds.size()))
//     {
//         if(scores[inds[ii] + N_SIS_SELECT] == std::numeric_limits<double>::infinity())
//         {
//             break;
//         }
//         std::cout << ii << '\t' << inds[ii] << '\t';
//         if(inds[ii] < 0)
//         {
//             std::cout << inds[ii] << '\t' << in_vec_1[inds[ii] + N_SIS_SELECT].score() << std::endl;;

//             max_corr_ind = -1 + idamax_(
//                 o2,
//                 &CROSS_CORR_MAT[(inds[ii] + N_SIS_SELECT) * N_SIS_SELECT],
//                 1
//             );

//             if((o2 == 0) || std::abs(CROSS_CORR_MAT[(inds[ii] + N_SIS_SELECT) * N_SIS_SELECT + max_corr_ind]) < CROSS_COR_MAX - 1e-10)
//             {
//                 out_vec[oo] = in_vec_1[inds[ii] + N_SIS_SELECT];
//                 ++oo;
//             }
//             else
//             {
//                 std::fill_n(&CROSS_CORR_MAT[(inds[ii] + N_SIS_SELECT) * N_SIS_SELECT], N_SIS_SELECT, 0.0);
//             }
//             ++o1;
//         }
//         else
//         {
//             std::cout << inds[ii] << '\t' << in_vec_2[inds[ii]].score() << std::endl;;
//             max_corr_ind = -1 + idamax_(
//                 o1,
//                 &CROSS_CORR_MAT[inds[ii]],
//                 N_SIS_SELECT
//             );

//             if((o1 == 0) || std::abs(CROSS_CORR_MAT[inds[ii] + max_corr_ind * N_SIS_SELECT]) < CROSS_COR_MAX - 1e-10)
//             {
//                 out_vec[oo] = in_vec_2[inds[ii]];
//                 ++oo;
//             }
//             else
//             {
//                 dscal_(N_SIS_SELECT, 0.0, &CROSS_CORR_MAT[inds[ii]], N_SIS_SELECT);
//             }
//             ++o2;
//         }
//         ++ii;
//     }
//     std::cout << "to return "<< std::endl;
//     return out_vec;
// }
