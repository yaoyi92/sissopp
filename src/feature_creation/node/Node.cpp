// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/Node.cpp
 *  @brief Implements the base class for the objects that represent features
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 *
 *  This package represents the features in SISSO as a binary expression tree and are accessible from the root node of that tree.
 *  The node class are the vertices of the tree and represent initial features (FeatureNode) and all algebraic operators (OperatorNodes).
 *  The edges are the represented via the _feats member in OperatorNode that store the features the operation acts on (its children).
 */

#include "feature_creation/node/Node.hpp"

Node::Node() {}

Node::Node(const unsigned long int feat_ind, const int n_samp, const int n_samp_test)
    : _n_samp_test(n_samp_test),
      _n_samp(n_samp),
      _feat_ind(feat_ind),
      _arr_ind(feat_ind),
      _d_mat_ind(-1),
      _selected(false)
{
}

Node::~Node() {}

std::map<std::string, int> Node::primary_feature_decomp() const
{
    std::map<std::string, int> pf_decomp;
    update_primary_feature_decomp(pf_decomp);
    return pf_decomp;
}
BOOST_SERIALIZATION_ASSUME_ABSTRACT(Node)

void Node::set_standardized_value(const bool for_comp) const
{
    double* stand_val_ptr;
    if (_selected)
    {
        stand_val_ptr = node_value_arrs::get_stand_d_matrix_ptr(_d_mat_ind);
    }
    else
    {
        stand_val_ptr = node_value_arrs::access_temp_stand_storage(_arr_ind, for_comp);
    }

    util_funcs::standardize(value_ptr(-1, for_comp), _n_samp, stand_val_ptr);
}

void Node::set_standardized_test_value(const bool for_comp) const
{
    double* val_ptr = value_ptr(-1, for_comp);
    double* test_val_ptr = test_value_ptr(-1, for_comp);
    double* stand_val_ptr = node_value_arrs::access_temp_stand_storage_test(_arr_ind, for_comp);

    double mean = util_funcs::mean(val_ptr, _n_samp);
    double stand_dev = util_funcs::stand_dev(val_ptr, _n_samp, mean);
    std::transform(test_val_ptr, test_val_ptr + _n_samp_test, stand_val_ptr, [&](double val) {
        return (val - mean) / stand_dev;
    });
}

double* Node::stand_value_ptr(const bool for_comp) const
{
    if (_selected)
    {
        return node_value_arrs::get_stand_d_matrix_ptr(_d_mat_ind);
    }
    set_standardized_value(for_comp);
    return node_value_arrs::access_temp_stand_storage(_arr_ind, for_comp);
}

double* Node::stand_test_value_ptr(const bool for_comp) const
{
    set_standardized_test_value(for_comp);
    return node_value_arrs::access_temp_stand_storage_test(_arr_ind, for_comp);
}

int Node::rung() const
{
    int cur_rung = 0;
    rung_update(cur_rung);
    return cur_rung;
}

int Node::n_leaves() const
{
    int cur_n_leaves = 0;
    update_n_leaves(cur_n_leaves);
    return cur_n_leaves;
}
