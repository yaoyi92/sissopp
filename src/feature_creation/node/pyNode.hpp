// Copyright 2022 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/pyNode.hpp
 *  @brief Defines the python wrapper class for pybind Node defintion
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef PY_NODE
#define PY_NODE

#include "feature_creation/node/Node.hpp"

#ifdef PARAMETERIZE
#include "nl_opt/NLOptimizer.hpp"
#endif

// GCOV_EXCL_START     GCOVR_EXCL_START       LCOV_EXCL_START
#ifdef PY_BINDINGS
template <class NodeBase = Node>
class PyNode : public NodeBase
{
    using NodeBase::NodeBase;

    std::shared_ptr<Node> hard_copy() const override
    {
        PYBIND11_OVERRIDE_PURE(std::shared_ptr<Node>, NodeBase, hard_copy, );
    }

    void reset_feats(std::vector<std::shared_ptr<Node>>& phi) override
    {
        PYBIND11_OVERRIDE_PURE(void, NodeBase, reset_feats, phi);
    }

    std::vector<std::string> x_in_expr_list() const override
    {
        PYBIND11_OVERRIDE_PURE(std::vector<std::string>, NodeBase, x_in_expr_list, );
    }

    void update_n_leaves(int& cur_n_leaves) const override
    {
        PYBIND11_OVERRIDE_PURE(void, NodeBase, update_n_leaves, cur_n_leaves);
    }

    unsigned long long sort_score(unsigned int max_ind) const override
    {
        PYBIND11_OVERRIDE_PURE(unsigned long long, NodeBase, sort_score, max_ind);
    }

    std::string expr() const override { PYBIND11_OVERRIDE_PURE(std::string, NodeBase, expr, ); }

    std::string get_latex_expr() const override
    {
        PYBIND11_OVERRIDE_PURE(std::string, NodeBase, get_latex_expr, );
    }

    Unit unit() const override { PYBIND11_OVERRIDE_PURE(Unit, NodeBase, unit, ); }

    Domain domain() const override { PYBIND11_OVERRIDE_PURE(Domain, NodeBase, domain, ); }

    std::vector<double> value() const override
    {
        PYBIND11_OVERRIDE_PURE(std::vector<double>, NodeBase, value, );
    }

    std::vector<double> test_value() const override
    {
        PYBIND11_OVERRIDE_PURE(std::vector<double>, NodeBase, test_value, );
    }

    void set_value(int offset = -1, const bool for_comp = false) const override
    {
        PYBIND11_OVERRIDE_PURE(void, NodeBase, set_value, offset, for_comp);
    }

    double* value_ptr(int offset = -1, const bool for_comp = false) const override
    {
        PYBIND11_OVERRIDE_PURE(double*, NodeBase, value_ptr, offset, for_comp);
    }

    void set_test_value(int offset = -1, const bool for_comp = false) const override
    {
        PYBIND11_OVERRIDE_PURE(void, NodeBase, set_test_value, offset, for_comp);
    }

    double* test_value_ptr(int offset = -1, const bool for_comp = false) const override
    {
        PYBIND11_OVERRIDE_PURE(double*, NodeBase, test_value_ptr, offset, for_comp);
    }

    bool is_nan() const override { PYBIND11_OVERRIDE_PURE(bool, NodeBase, is_nan, ); }

    bool is_const() const override { PYBIND11_OVERRIDE_PURE(bool, NodeBase, is_const, ); }

    NODE_TYPE type() const override { PYBIND11_OVERRIDE_PURE(NODE_TYPE, NodeBase, type, ); }

    void rung_update(int& cur_rung) const override
    {
        PYBIND11_OVERRIDE_PURE(void, NodeBase, rung_update, cur_rung);
    }

    void update_primary_feature_decomp(std::map<std::string, int>& pf_decomp) const override
    {
        PYBIND11_OVERRIDE_PURE(void, NodeBase, update_primary_feature_decomp, pf_decomp);
    }

    void update_postfix(std::string& cur_expr, const bool add_params = true) const override
    {
        PYBIND11_OVERRIDE_PURE(void, NodeBase, update_postfix, cur_expr, add_params);
    }

    std::string get_postfix_term() const override
    {
        PYBIND11_OVERRIDE_PURE(std::string, NodeBase, get_postfix_term, );
    }

    std::string matlab_fxn_expr() const override
    {
        PYBIND11_OVERRIDE_PURE(std::string, NodeBase, matlab_fxn_expr, );
    }

    void update_add_sub_leaves(std::map<std::string, int>& add_sub_leaves,
                               const int pl_mn,
                               int& expected_abs_tot) const override
    {
        PYBIND11_OVERRIDE_PURE(
            void, NodeBase, update_add_sub_leaves, add_sub_leaves, pl_mn, expected_abs_tot);
    }

    void update_div_mult_leaves(std::map<std::string, double>& div_mult_leaves,
                                const double fact,
                                double& expected_abs_tot) const override
    {
        PYBIND11_OVERRIDE_PURE(
            void, NodeBase, update_div_mult_leaves, div_mult_leaves, fact, expected_abs_tot);
    }

    int n_feats() const override { PYBIND11_OVERRIDE_PURE(int, NodeBase, n_feats, ); }

    std::shared_ptr<Node> feat(const int ind) const override
    {
        PYBIND11_OVERRIDE_PURE(std::shared_ptr<Node>, NodeBase, feat, ind);
    }

#ifdef PARAMETERIZE
    std::vector<double> parameters() const override
    {
        PYBIND11_OVERRIDE_PURE(std::vector<double>, NodeBase, parameters, );
    }

    const double* param_pointer() const override
    {
        PYBIND11_OVERRIDE(const double*, NodeBase, param_pointer, );
    }

    void set_parameters(const std::vector<double> params, const bool check_sz = true) override
    {
        PYBIND11_OVERRIDE_PURE(void, NodeBase, set_parameters, params, check_sz);
    }

    void set_parameters(const double* params) override
    {
        PYBIND11_OVERRIDE_PURE(void, NodeBase, set_parameters, params);
    }

    void get_parameters(std::shared_ptr<NLOptimizer> optimizer) override
    {
        PYBIND11_OVERRIDE_PURE(void, NodeBase, get_parameters, optimizer);
    }

    int n_params_possible(const int n_cur = 0, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(int, NodeBase, n_params_possible, n_cur, depth);
    }

    int n_params() const override { PYBIND11_OVERRIDE(int, NodeBase, n_params, ); }

    void set_value(const double* params,
                   int offset = -1,
                   const bool for_comp = false,
                   const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(void, NodeBase, set_value, params, offset, for_comp, depth);
    }

    double* value_ptr(const double* params,
                      int offset = -1,
                      const bool for_comp = false,
                      const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(double*, NodeBase, value_ptr, params, offset, for_comp, depth);
    }

    void set_test_value(const double* params,
                        int offset = -1,
                        const bool for_comp = false,
                        const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(void, NodeBase, set_test_value, params, offset, for_comp, depth);
    }

    double* test_value_ptr(const double* params,
                           int offset = -1,
                           const bool for_comp = false,
                           const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(double*, NodeBase, test_value_ptr, params, offset, for_comp, depth);
    }

    Domain domain(const double* params, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(Domain, NodeBase, domain, params, depth);
    }

    std::string expr(const double* params, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(std::string, NodeBase, expr, params, depth);
    }

    std::string get_latex_expr(const double* params, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(std::string, NodeBase, get_latex_expr, params, depth);
    }

    std::string matlab_fxn_expr(const double* params, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(std::string, NodeBase, matlab_fxn_expr, params, depth);
    }

    void set_bounds(double* lb, double* ub, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(void, NodeBase, set_bounds, lb, ub, depth);
    }

    void initialize_params(double* params, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(void, NodeBase, initialize_params, params, depth);
    }

    void param_derivative(const double* params, double* dfdp, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(void, NodeBase, param_derivative, params, dfdp, depth);
    }

    void gradient(double* grad, double* dfdp) const override
    {
        PYBIND11_OVERRIDE_PURE(void, NodeBase, gradient, grad, dfdp);
    }

    void gradient(double* grad,
                  double* dfdp,
                  const double* params,
                  const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(void, NodeBase, gradient, grad, dfdp, params, depth);
    }
#endif
};

#endif
// GCOV_EXCL_STOP     GCOVR_EXCL_STOP        LCOV_EXCL_STOP

#endif
