// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/value_storage/node_value_containers.cpp
 *  @brief Implements the functions for the central storage area for the feature's input data and the descriptor matrix
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "feature_creation/node/value_storage/nodes_value_containers.hpp"

int node_value_arrs::N_SELECTED = 0;
int node_value_arrs::N_SAMPLES = 0;
int node_value_arrs::N_STORE_FEATURES = 0;
int node_value_arrs::N_PRIMARY_FEATURES = 0;
int node_value_arrs::N_RUNGS_STORED = 0;
int node_value_arrs::N_SAMPLES_TEST = 0;
int node_value_arrs::MAX_N_THREADS = omp_get_max_threads();
int node_value_arrs::N_OP_SLOTS = 0;
int node_value_arrs::N_PARAM_OP_SLOTS = 0;
int node_value_arrs::MAX_RUNG = 0;

std::vector<int> node_value_arrs::TEMP_STORAGE_REG;
std::vector<int> node_value_arrs::TEMP_STORAGE_TEST_REG;

std::vector<int> node_value_arrs::TASK_SZ_TRAIN;
std::vector<int> node_value_arrs::TASK_START_TRAIN;
std::vector<int> node_value_arrs::TASK_SZ_TEST;

std::vector<double> node_value_arrs::PARAM_STORAGE_ARR;
std::vector<double> node_value_arrs::PARAM_STORAGE_TEST_ARR;
std::vector<double> node_value_arrs::D_MATRIX;
std::vector<double> node_value_arrs::VALUES_ARR;
std::vector<double> node_value_arrs::TEST_VALUES_ARR;
std::vector<double> node_value_arrs::TEMP_STORAGE_ARR;
std::vector<double> node_value_arrs::TEMP_STORAGE_TEST_ARR;

std::vector<double> node_value_arrs::STANDARDIZED_D_MATRIX;
std::vector<double> node_value_arrs::STANDARDIZED_STORAGE_ARR;
std::vector<double> node_value_arrs::STANDARDIZED_TEST_STORAGE_ARR;

void node_value_arrs::initialize_values_arr(const int n_samples,
                                            const int n_samples_test,
                                            const int n_primary_feat)
{
    N_SAMPLES = n_samples;
    N_SAMPLES_TEST = n_samples_test;
    N_RUNGS_STORED = 0;
    N_STORE_FEATURES = n_primary_feat;
    N_PRIMARY_FEATURES = n_primary_feat;

    VALUES_ARR = std::vector<double>(N_STORE_FEATURES * N_SAMPLES);
    TEST_VALUES_ARR = std::vector<double>(N_STORE_FEATURES * N_SAMPLES_TEST);
    STANDARDIZED_STORAGE_ARR = std::vector<double>(2 * (N_PRIMARY_FEATURES + 1) * N_SAMPLES *
                                                   MAX_N_THREADS);
    STANDARDIZED_TEST_STORAGE_ARR = std::vector<double>(2 * (N_PRIMARY_FEATURES + 1) *
                                                        N_SAMPLES_TEST * MAX_N_THREADS);
}

void node_value_arrs::initialize_values_arr(const std::vector<int> task_sz_train,
                                            const std::vector<int> task_sz_test,
                                            const int n_primary_feat,
                                            const int max_rung,
                                            const bool use_params)
{
    initialize_values_arr(std::accumulate(task_sz_train.begin(), task_sz_train.end(), 0),
                          std::accumulate(task_sz_test.begin(), task_sz_test.end(), 0),
                          n_primary_feat);

    set_task_sz_train(task_sz_train);
    set_task_sz_test(task_sz_test);

    set_max_rung(max_rung, use_params);
}

void node_value_arrs::initialize_param_storage()
{
    N_PARAM_OP_SLOTS = 2 * (static_cast<int>(std::pow(2, MAX_RUNG)) - 1);

    PARAM_STORAGE_ARR = std::vector<double>(N_SAMPLES * (N_PARAM_OP_SLOTS + 1) * MAX_N_THREADS);
    PARAM_STORAGE_TEST_ARR = std::vector<double>(N_SAMPLES_TEST * (N_PARAM_OP_SLOTS + 1) *
                                                 MAX_N_THREADS);
}

void node_value_arrs::set_max_rung(const int max_rung, bool use_params)
{
    if (max_rung < 0)
    {
        throw std::logic_error("Maximum rung of the features is less than 0");
    }

    if (max_rung == 0)
    {
        std::cerr << "Warning requested calculation has a maximum rung of 0" << std::endl;
    }

    MAX_RUNG = max_rung;
    N_OP_SLOTS = 2 * (static_cast<int>(std::pow(2, max_rung)) - 1);

    TEMP_STORAGE_ARR = std::vector<double>(MAX_N_THREADS * (N_OP_SLOTS * N_PRIMARY_FEATURES + 1) *
                                           N_SAMPLES);
    TEMP_STORAGE_REG = std::vector<int>(MAX_N_THREADS * (N_OP_SLOTS * N_PRIMARY_FEATURES + 1), -1);

    TEMP_STORAGE_TEST_ARR = std::vector<double>(
        MAX_N_THREADS * (N_OP_SLOTS * N_PRIMARY_FEATURES + 1) * N_SAMPLES_TEST);
    TEMP_STORAGE_TEST_REG = std::vector<int>(MAX_N_THREADS * (N_OP_SLOTS * N_PRIMARY_FEATURES + 1),
                                             -1);

    if (use_params || (N_PARAM_OP_SLOTS > 0))
    {
        initialize_param_storage();
    }
}

void node_value_arrs::set_task_sz_train(const std::vector<int> task_sz_train)
{
    if ((N_SAMPLES > 0) &&
        (std::accumulate(task_sz_train.begin(), task_sz_train.end(), 0) != N_SAMPLES))
    {
        int n_samp_new = std::accumulate(task_sz_train.begin(), task_sz_train.end(), 0);
        throw std::logic_error("The total number of samples has changed from " +
                               std::to_string(N_SAMPLES) + " to " + std::to_string(n_samp_new) +
                               ", task_sz_train is wrong.");
    }
    else if (N_SAMPLES == 0)
    {
        N_SAMPLES = std::accumulate(task_sz_train.begin(), task_sz_train.end(), 0);
    }

    TASK_SZ_TRAIN = task_sz_train;
    TASK_START_TRAIN = std::vector<int>(TASK_SZ_TRAIN.size(), 0);
    std::copy_n(TASK_SZ_TRAIN.begin(), TASK_SZ_TRAIN.size() - 1, &TASK_START_TRAIN[1]);
}

void node_value_arrs::set_task_sz_test(const std::vector<int> task_sz_test)
{
    if ((N_SAMPLES_TEST > 0) &&
        (std::accumulate(task_sz_test.begin(), task_sz_test.end(), 0) != N_SAMPLES_TEST))
    {
        int n_samp_new = std::accumulate(task_sz_test.begin(), task_sz_test.end(), 0);
        throw std::logic_error("The total number of test samples has changed from " +
                               std::to_string(N_SAMPLES_TEST) + " to " +
                               std::to_string(n_samp_new) + ", task_sz_test is wrong.");
    }
    else if (N_SAMPLES_TEST == 0)
    {
        N_SAMPLES_TEST = std::accumulate(task_sz_test.begin(), task_sz_test.end(), 0);
    }

    TASK_SZ_TEST = task_sz_test;
}

void node_value_arrs::resize_values_arr(const int n_dims, const int n_feat)
{
    if (n_dims > MAX_RUNG)
    {
        throw std::logic_error("Requested rung is larger MAX_RUNG.");
    }

    N_RUNGS_STORED = n_dims;
    N_STORE_FEATURES = n_feat;
    if (N_STORE_FEATURES == 0)
    {
        N_STORE_FEATURES = 1;
    }

    VALUES_ARR.resize(N_STORE_FEATURES * N_SAMPLES);
    VALUES_ARR.shrink_to_fit();

    TEST_VALUES_ARR.resize(N_STORE_FEATURES * N_SAMPLES_TEST);
    TEST_VALUES_ARR.shrink_to_fit();

    if (n_dims == 0)
    {
        N_PRIMARY_FEATURES = N_STORE_FEATURES;

        STANDARDIZED_STORAGE_ARR = std::vector<double>(2 * (N_PRIMARY_FEATURES + 1) * N_SAMPLES *
                                                       MAX_N_THREADS);
        STANDARDIZED_TEST_STORAGE_ARR = std::vector<double>(2 * (N_PRIMARY_FEATURES + 1) *
                                                            N_SAMPLES_TEST * MAX_N_THREADS);

        TEMP_STORAGE_ARR.resize(MAX_N_THREADS * (N_OP_SLOTS * N_PRIMARY_FEATURES + 1) * N_SAMPLES);
        TEMP_STORAGE_ARR.shrink_to_fit();

        TEMP_STORAGE_REG.resize(MAX_N_THREADS * (N_OP_SLOTS * N_PRIMARY_FEATURES + 1), -1);
        TEMP_STORAGE_REG.shrink_to_fit();

        TEMP_STORAGE_TEST_ARR = std::vector<double>(
            MAX_N_THREADS * (N_OP_SLOTS * N_PRIMARY_FEATURES + 1) * N_SAMPLES_TEST);
        TEMP_STORAGE_TEST_ARR.shrink_to_fit();

        TEMP_STORAGE_TEST_REG = std::vector<int>(
            MAX_N_THREADS * (N_OP_SLOTS * N_PRIMARY_FEATURES + 1), -1);
        TEMP_STORAGE_TEST_REG.shrink_to_fit();
    }
}

double* node_value_arrs::get_value_ptr(const unsigned long int arr_ind,
                                       const unsigned long int feat_ind,
                                       const int rung,
                                       const int offset,
                                       const bool for_comp)
{
    if ((rung <= N_RUNGS_STORED) && (arr_ind < N_STORE_FEATURES))
    {
        return access_value_arr(arr_ind);
    }

    int op_slot = get_op_slot(rung, offset, for_comp);
    temp_storage_reg(arr_ind, op_slot) = feat_ind;
    return access_temp_storage((arr_ind % N_PRIMARY_FEATURES) +
                               (op_slot % N_OP_SLOTS) * N_PRIMARY_FEATURES +
                               omp_get_thread_num() * (N_PRIMARY_FEATURES * N_OP_SLOTS + 1));
}

double* node_value_arrs::get_test_value_ptr(const unsigned long int arr_ind,
                                            const unsigned long int feat_ind,
                                            const int rung,
                                            const int offset,
                                            const bool for_comp)
{
    if (rung == 0)
    {
        if (arr_ind > N_PRIMARY_FEATURES)
        {
            throw std::logic_error("Requested arr_ind (" + std::to_string(arr_ind) +
                                   ") is too high (max " + std::to_string(N_PRIMARY_FEATURES) +
                                   ")");
        }
        return access_test_value_arr(arr_ind);
    }

    int op_slot = get_op_slot(rung, offset, for_comp);
    temp_storage_test_reg(arr_ind, op_slot) = feat_ind;
    return access_temp_storage_test((arr_ind % N_PRIMARY_FEATURES) +
                                    (op_slot % N_OP_SLOTS) * N_PRIMARY_FEATURES +
                                    omp_get_thread_num() * (N_PRIMARY_FEATURES * N_OP_SLOTS + 1));
}

void node_value_arrs::initialize_d_matrix_arr()
{
    N_SELECTED = 0;
    D_MATRIX = std::vector<double>(0);
    STANDARDIZED_D_MATRIX = std::vector<double>(0);
}

void node_value_arrs::resize_d_matrix_arr(const int n_select)
{
    N_SELECTED += n_select;
    D_MATRIX.resize(N_SELECTED * N_SAMPLES, 0.0);
    D_MATRIX.shrink_to_fit();

    STANDARDIZED_D_MATRIX.resize(N_SELECTED * N_SAMPLES, 0.0);
    STANDARDIZED_D_MATRIX.shrink_to_fit();
}

void node_value_arrs::finalize_values_arr()
{
    N_SELECTED = 0;
    N_SAMPLES = 0;
    N_STORE_FEATURES = 0;
    N_PRIMARY_FEATURES = 0;
    N_RUNGS_STORED = 0;
    N_SAMPLES_TEST = 0;
    MAX_N_THREADS = omp_get_max_threads();
    N_OP_SLOTS = 0;
    N_PARAM_OP_SLOTS = 0;
    MAX_RUNG = 0;

    TEMP_STORAGE_REG.resize(0);
    TEMP_STORAGE_TEST_REG.resize(0);

    TASK_SZ_TRAIN.resize(0);
    TASK_START_TRAIN.resize(0);
    TASK_SZ_TEST.resize(0);

    D_MATRIX.resize(0);

    VALUES_ARR.resize(0);
    TEST_VALUES_ARR.resize(0);

    TEMP_STORAGE_ARR.resize(0);
    TEMP_STORAGE_TEST_ARR.resize(0);

    PARAM_STORAGE_ARR.resize(0);
    PARAM_STORAGE_TEST_ARR.resize(0);

    STANDARDIZED_D_MATRIX.resize(0);
    STANDARDIZED_STORAGE_ARR.resize(0);
    STANDARDIZED_TEST_STORAGE_ARR.resize(0);
}
