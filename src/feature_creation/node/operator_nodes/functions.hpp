// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/operator_nodes/functions.hpp
 *  @brief Defines all functions used to calculate the data for the features
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef OP_FUNCTIONS
#define OP_FUNCTIONS

#include <algorithm>
#include <functional>

namespace allowed_op_funcs
{
/**
     * @brief Function to perform the addition operation
     *
     * @param size size of the array to perform the output on
     * @param in_0 Pointer to the data for the first input feature
     * @param in_1 Pointer to the data for the second input feature
     * @param alpha The feature scale parameter
     * @param a The feature shift parameter
     * @param out pointer to the output array
     */
inline void add(const int size,
                const double* in_0,
                const double* in_1,
                const double alpha,
                const double a,
                double* out)
{
    std::transform(in_0, in_0 + size, in_1, out, [&](double in_0, double in_1) {
        return in_0 + (alpha * in_1 + a);
    });
}

/**
     * @brief Function to perform the subtraction operation
     *
     * @param size size of the array to perform the output on
     * @param in_0 Pointer to the data for the first input feature
     * @param in_1 Pointer to the data for the second input feature
     * @param alpha The feature scale parameter
     * @param a The feature shift parameter
     * @param out pointer to the output array
     */
inline void sub(const int size,
                const double* in_0,
                const double* in_1,
                const double alpha,
                const double a,
                double* out)
{
    std::transform(in_0, in_0 + size, in_1, out, [&](double in_0, double in_1) {
        return in_0 - (alpha * in_1 + a);
    });
}

/**
     * @brief Function to perform the absolute difference operation
     *
     * @param size size of the array to perform the output on
     * @param in_0 Pointer to the data for the first input feature
     * @param in_1 Pointer to the data for the second input feature
     * @param alpha The feature scale parameter
     * @param a The feature shift parameter
     * @param out pointer to the output array
     */
inline void abs_diff(const int size,
                     const double* in_0,
                     const double* in_1,
                     const double alpha,
                     const double a,
                     double* out)
{
    std::transform(in_0, in_0 + size, in_1, out, [&](double in_0, double in_1) {
        return std::abs(in_0 - (alpha * in_1 + a));
    });
}

/**
     * @brief Function to perform the multiply operation
     *
     * @param size size of the array to perform the output on
     * @param in_0 Pointer to the data for the first input feature
     * @param in_1 Pointer to the data for the second input feature
     * @param alpha The feature scale parameter
     * @param a The feature shift parameter
     * @param out pointer to the output array
     */
inline void mult(const int size,
                 const double* in_0,
                 const double* in_1,
                 const double alpha,
                 const double a,
                 double* out)
{
    std::transform(in_0, in_0 + size, in_1, out, [&](double in_0, double in_1) {
        return in_0 * (alpha * in_1 + a);
    });
}

/**
     * @brief Function to perform the division operation
     *
     * @param size size of the array to perform the output on
     * @param in_0 Pointer to the data for the first input feature
     * @param in_1 Pointer to the data for the second input feature
     * @param alpha The feature scale parameter
     * @param a The feature shift parameter
     * @param out pointer to the output array
     */
inline void div(const int size,
                const double* in_0,
                const double* in_1,
                const double alpha,
                const double a,
                double* out)
{
    std::transform(in_0, in_0 + size, in_1, out, [&](double in_0, double in_1) {
        return in_0 / (alpha * in_1 + a);
    });
}

/**
     * @brief Function to perform the exponential operation
     *
     * @param size size of the array to perform the output on
     * @param in_0 Pointer to the data for the first input feature
     * @param alpha The feature scale parameter
     * @param a The feature shift parameter
     * @param out pointer to the output array
     */
inline void exp(const int size, const double* in_0, const double alpha, const double a, double* out)
{
    std::transform(in_0, in_0 + size, out, [&](double in_0) { return std::exp(alpha * in_0 + a); });
}

/**
     * @brief Function to perform the negative exponential operation
     *
     * @param size size of the array to perform the output on
     * @param in_0 Pointer to the data for the first input feature
     * @param alpha The feature scale parameter
     * @param a The feature shift parameter
     * @param out pointer to the output array
     */
inline void neg_exp(
    const int size, const double* in_0, const double alpha, const double a, double* out)
{
    std::transform(
        in_0, in_0 + size, out, [&](double in_0) { return std::exp(-1.0 * (alpha * in_0 + a)); });
}

/**
     * @brief Function to perform the square operation
     *
     * @param size size of the array to perform the output on
     * @param in_0 Pointer to the data for the first input featur
     * @param alpha The feature scale parameter
     * @param a The feature shift parametere
     * @param out pointer to the output array
     */
inline void sq(const int size, const double* in_0, const double alpha, const double a, double* out)
{
    std::transform(in_0, in_0 + size, out, [&](double in_0) {
        double param_in = alpha * in_0 + a;
        return param_in * param_in;
    });
}

/**
     * @brief Function to perform the cube operation
     *
     * @param size size of the array to perform the output on
     * @param in_0 Pointer to the data for the first input featur
     * @param alpha The feature scale parameter
     * @param a The feature shift parametere
     * @param out pointer to the output array
     */
inline void cb(const int size, const double* in_0, const double alpha, const double a, double* out)
{
    std::transform(in_0, in_0 + size, out, [&](double in_0) {
        double param_in = alpha * in_0 + a;
        return param_in * param_in * param_in;
    });
}

/**
     * @brief Function to perform the sixth power operation
     *
     * @param size size of the array to perform the output on
     * @param in_0 Pointer to the data for the first input feature
     * @param alpha The feature scale parameter
     * @param a The feature shift parameter
     * @param out pointer to the output array
     */
inline void sixth_pow(
    const int size, const double* in_0, const double alpha, const double a, double* out)
{
    std::transform(
        in_0, in_0 + size, out, [&](double in_0) { return std::pow(alpha * in_0 + a, 6.0); });
}

/**
     * @brief Function to perform the cube root operation
     *
     * @param size size of the array to perform the output on
     * @param in_0 Pointer to the data for the first input feature
     * @param alpha The feature scale parameter
     * @param a The feature shift parameter
     * @param out pointer to the output array
     */
inline void cbrt(
    const int size, const double* in_0, const double alpha, const double a, double* out)
{
    std::transform(
        in_0, in_0 + size, out, [&](double in_0) { return std::pow(alpha * in_0 + a, 1.0 / 3.0); });
}

/**
     * @brief Function to perform the square root operation
     *
     * @param size size of the array to perform the output on
     * @param in_0 Pointer to the data for the first input feature
     * @param alpha The feature scale parameter
     * @param a The feature shift parameter
     * @param out pointer to the output array
     */
inline void sqrt(
    const int size, const double* in_0, const double alpha, const double a, double* out)
{
    std::transform(
        in_0, in_0 + size, out, [&](double in_0) { return std::sqrt(alpha * in_0 + a); });
}

/**
     * @brief Function to perform the inverse operation
     *
     * @param size size of the array to perform the output on
     * @param in_0 Pointer to the data for the first input feature
     * @param alpha The feature scale parameter
     * @param a The feature shift parameter
     * @param out pointer to the output array
     */
inline void inv(const int size, const double* in_0, const double alpha, const double a, double* out)
{
    std::transform(in_0, in_0 + size, out, [&](double in_0) { return 1.0 / (alpha * in_0 + a); });
}

/**
     * @brief Function to perform the log operation
     *
     * @param size size of the array to perform the output on
     * @param in_0 Pointer to the data for the first input feature
     * @param alpha The feature scale parameter
     * @param a The feature shift parameter
     * @param out pointer to the output array
     */
inline void log(const int size, const double* in_0, const double alpha, const double a, double* out)
{
    std::transform(in_0, in_0 + size, out, [&](double in_0) { return std::log(alpha * in_0 + a); });
}

/**
     * @brief Function to perform the sin operation
     *
     * @param size size of the array to perform the output on
     * @param in_0 Pointer to the data for the first input feature
     * @param alpha The feature scale parameter
     * @param a The feature shift parameter
     * @param out pointer to the output array
     */
inline void sin(const int size, const double* in_0, const double alpha, const double a, double* out)
{
    std::transform(in_0, in_0 + size, out, [&](double in_0) { return std::sin(alpha * in_0 + a); });
}

/**
     * @brief Function to perform the cos operation
     *
     * @param size size of the array to perform the output on
     * @param in_0 Pointer to the data for the first input feature
     * @param alpha The feature scale parameter
     * @param a The feature shift parameter
     * @param out pointer to the output array
     */
inline void cos(const int size, const double* in_0, const double alpha, const double a, double* out)
{
    std::transform(in_0, in_0 + size, out, [&](double in_0) { return std::cos(alpha * in_0 + a); });
}

/**
     * @brief Function to perform the absolute value operation
     *
     * @param size size of the array to perform the output on
     * @param in_0 Pointer to the data for the first input feature
     * @param alpha The feature scale parameter
     * @param a The feature shift parameter
     * @param out pointer to the output array
     */
inline void abs(const int size, const double* in_0, const double alpha, const double a, double* out)
{
    std::transform(in_0, in_0 + size, out, [&](double in_0) { return std::abs(alpha * in_0 + a); });
}
};  // namespace allowed_op_funcs

#endif
