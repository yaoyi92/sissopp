// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/operator_nodes/allowed_operator_nodes/sqrt/parameterized_square_root.cpp
 *  @brief Implements a class for the parameterized version of square root operator
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 *
 *  This class represents the parameterized unary operator -> sqrt(alpha * A + a)
 */

#include "feature_creation/node/operator_nodes/allowed_operator_nodes/sqrt/parameterized_square_root.hpp"

BOOST_SERIALIZATION_ASSUME_ABSTRACT(SqrtParamNode)

void generateSqrtParamNode(std::vector<node_ptr>& feat_list,
                           const node_ptr feat,
                           unsigned long int& feat_ind,
                           const double l_bound,
                           const double u_bound,
                           std::shared_ptr<NLOptimizer> optimizer)
{
    if ((feat->type() == NODE_TYPE::SQ) || (feat->type() == NODE_TYPE::CB) ||
        (feat->type() == NODE_TYPE::SIX_POW) || (feat->type() == NODE_TYPE::CBRT) ||
        (feat->type() == NODE_TYPE::INV) || (feat->type() == NODE_TYPE::EXP) ||
        (feat->type() == NODE_TYPE::NEG_EXP) || (feat->type() == NODE_TYPE::PARAM_SQ) ||
        (feat->type() == NODE_TYPE::PARAM_CB) || (feat->type() == NODE_TYPE::PARAM_SIX_POW) ||
        (feat->type() == NODE_TYPE::PARAM_CBRT) || (feat->type() == NODE_TYPE::PARAM_INV) ||
        (feat->type() == NODE_TYPE::PARAM_EXP) || (feat->type() == NODE_TYPE::PARAM_NEG_EXP)

    )
    {
        return;
    }

    ++feat_ind;
    node_ptr new_feat = std::make_shared<SqrtParamNode>(feat, feat_ind, optimizer);
    // Domain dom = new_feat->domain();

    // If a scale parameter is 0.0 feature is invalid
    if ((std::abs(new_feat->parameters()[0]) <= 1e-10)
        // ((!dom.is_empty()) && (std::isnan(dom.end_points()[0]) || std::isnan(dom.end_points()[1])))
    )
    {
        return;
    }

    Domain dom_0 = (1 < nlopt_wrapper::MAX_PARAM_DEPTH)
                       ? new_feat->feat(0)->domain(new_feat->param_pointer() + 2, 2)
                       : new_feat->feat(0)->domain();
    if ((!dom_0.is_empty()) &&
        dom_0.parameterize(new_feat->param_pointer()[0], new_feat->param_pointer()[1])
                .end_points()[0] < 0.0)
    {
        return;
    }

    new_feat->set_value();

    // Check if the feature is NaN, constant, greater than the allowed max of less than the allowed min
    if (new_feat->is_nan() || new_feat->is_const() ||
        (util_funcs::max_abs_val<double>(new_feat->value_ptr(), new_feat->n_samp()) > u_bound) ||
        (util_funcs::max_abs_val<double>(new_feat->value_ptr(), new_feat->n_samp()) < l_bound))
    {
        return;
    }

    feat_list.push_back(new_feat);
}

SqrtParamNode::SqrtParamNode() {}

SqrtParamNode::SqrtParamNode(const node_ptr feat,
                             const unsigned long int feat_ind,
                             const double l_bound,
                             const double u_bound,
                             std::shared_ptr<NLOptimizer> optimizer)
    : SqrtNode(feat, feat_ind), _sign_alpha(1.0)
{
    if ((feat->type() == NODE_TYPE::SQ) || (feat->type() == NODE_TYPE::CB) ||
        (feat->type() == NODE_TYPE::SIX_POW) || (feat->type() == NODE_TYPE::CBRT) ||
        (feat->type() == NODE_TYPE::INV) || (feat->type() == NODE_TYPE::EXP) ||
        (feat->type() == NODE_TYPE::NEG_EXP) || (feat->type() == NODE_TYPE::PARAM_SQ) ||
        (feat->type() == NODE_TYPE::PARAM_CB) || (feat->type() == NODE_TYPE::PARAM_SIX_POW) ||
        (feat->type() == NODE_TYPE::PARAM_CBRT) || (feat->type() == NODE_TYPE::PARAM_INV) ||
        (feat->type() == NODE_TYPE::PARAM_EXP) || (feat->type() == NODE_TYPE::PARAM_NEG_EXP))
    {
        throw InvalidFeatureException();
    }

    _params.resize(n_params_possible(), 0.0);
    get_parameters(optimizer);

    Domain dom_0 = (1 < nlopt_wrapper::MAX_PARAM_DEPTH) ? feat->domain(&_params[2], 2)
                                                        : feat->domain();
    if ((dom_0.is_empty()) && dom_0.parameterize(_params[0], _params[1]).end_points()[0] < 0.0)
    {
        return;
    }

    // Domain dom = domain();
    // Check if the scale parameter is 0.0 or if the feature is NaN, constant, greater than the allowed max of less than the allowed min
    if ((std::abs(_params[0]) <= 1e-10) ||
        // ((!dom.is_empty()) && (std::isnan(dom.end_points()[0]) || std::isnan(dom.end_points()[1]))) ||
        is_nan() || is_const() ||
        (util_funcs::max_abs_val<double>(value_ptr(), _n_samp) > u_bound) ||
        (util_funcs::max_abs_val<double>(value_ptr(), _n_samp) < l_bound))
    {
        throw InvalidFeatureException();
    }
}

SqrtParamNode::SqrtParamNode(const node_ptr feat,
                             const unsigned long int feat_ind,
                             std::shared_ptr<NLOptimizer> optimizer)
    : SqrtNode(feat, feat_ind), _sign_alpha(1.0)
{
    _params.resize(n_params_possible(), 0.0);
    get_parameters(optimizer);
}

SqrtParamNode::SqrtParamNode(const node_ptr feat,
                             const unsigned long int feat_ind,
                             const double l_bound,
                             const double u_bound)
    : SqrtNode(feat, feat_ind), _sign_alpha(1.0)
{
    _params.resize(n_params_possible(), 0.0);
}

SqrtParamNode::SqrtParamNode(std::array<node_ptr, 1> feats,
                             const unsigned long int feat_ind,
                             std::vector<double> params)
    : SqrtNode(feats, feat_ind), _params(params)
{
}

node_ptr SqrtParamNode::hard_copy() const
{
    node_ptr cp = std::make_shared<SqrtParamNode>(_feats[0]->hard_copy(), _feat_ind);
    cp->set_selected(_selected);
    cp->set_d_mat_ind(_d_mat_ind);
    cp->set_parameters(_params.data());
    return cp;
}

void SqrtParamNode::get_parameters(std::shared_ptr<NLOptimizer> optimizer)
{
    // Change the sign of alpha as a control on linear dependency without forcing one sign or another
    _sign_alpha = 1.0;
    double min_res = optimizer->optimize_feature_params(this);
    std::vector<double> param_cp(_params);

    _sign_alpha = -1.0;
    double min_res_neg = optimizer->optimize_feature_params(this);
    if (min_res_neg > min_res)
    {
        std::copy_n(param_cp.data(), param_cp.size(), _params.data());
        _sign_alpha = 1.0;
    }
    else if (min_res_neg == std::numeric_limits<double>::infinity())
    {
        _params[0] = 0.0;
    }
}

void SqrtNode::set_value(const double* params,
                         int offset,
                         const bool for_comp,
                         const int depth) const
{
    bool is_root = (offset == -1);
    offset += is_root;

    double* vp_0;
    if (depth < nlopt_wrapper::MAX_PARAM_DEPTH)
    {
        vp_0 = _feats[0]->value_ptr(params + 2, 2 * offset, for_comp, depth + 1);
    }
    else
    {
        vp_0 = _feats[0]->value_ptr(2 * offset, for_comp);
    }

    double* val_ptr;
    if (_selected && is_root)
    {
        val_ptr = node_value_arrs::get_d_matrix_ptr(_d_mat_ind);
    }
    else
    {
        val_ptr = node_value_arrs::access_param_storage(rung(), offset, for_comp);
    }

    allowed_op_funcs::sqrt(_n_samp, vp_0, params[0], params[1], val_ptr);
}

void SqrtNode::set_test_value(const double* params,
                              int offset,
                              const bool for_comp,
                              const int depth) const
{
    offset += (offset == -1);
    double* vp_0;
    if (depth < nlopt_wrapper::MAX_PARAM_DEPTH)
    {
        vp_0 = _feats[0]->test_value_ptr(params + 2, 2 * offset, for_comp, depth + 1);
    }
    else
    {
        vp_0 = _feats[0]->test_value_ptr(2 * offset, for_comp);
    }

    allowed_op_funcs::sqrt(_n_samp_test,
                           vp_0,
                           params[0],
                           params[1],
                           node_value_arrs::access_param_storage_test(rung(), offset, for_comp));
}

void SqrtNode::set_bounds(double* lb, double* ub, const int depth) const
{
    lb[0] = 1.0;
    ub[0] = 1.0;

    if ((depth >= nlopt_wrapper::MAX_PARAM_DEPTH) || ((depth - rung()) == 0))
    {
        lb[1] = std::max(lb[1], -1 * _feats[0]->domain().end_points()[0]);
        ub[1] = std::max(ub[1], lb[1]);
        return;
    }

    _feats[0]->set_bounds(lb + 2, ub + 2, depth + 1);
}

void SqrtParamNode::set_bounds(double* lb, double* ub, const int depth) const
{
    lb[0] = _sign_alpha;
    ub[0] = _sign_alpha;

    if ((depth >= nlopt_wrapper::MAX_PARAM_DEPTH) || ((depth - rung()) == 0))
    {
        lb[1] = std::max(lb[1],
                         std::max(-1 * _sign_alpha * _feats[0]->domain().end_points()[0],
                                  -1 * _sign_alpha * _feats[0]->domain().end_points()[1]));
        ub[1] = std::max(ub[1], lb[1]);
        return;
    }

    _feats[0]->set_bounds(lb + 2, ub + 2, depth + 1);
}

void SqrtNode::initialize_params(double* params, const int depth) const
{
    params[0] = 1.0;

    double* val_ptr;
    if (depth >= nlopt_wrapper::MAX_PARAM_DEPTH)
    {
        val_ptr = _feats[0]->value_ptr();
        params[1] = std::max(0.0, -1.0 * (*std::min_element(val_ptr, val_ptr + _n_samp)));
        return;
    }
    _feats[0]->initialize_params(params + 2, depth + 1);

    val_ptr = _feats[0]->value_ptr(params + 2);
    params[1] = std::max(0.0, -1.0 * (*std::min_element(val_ptr, val_ptr + _n_samp)));
}

void SqrtParamNode::initialize_params(double* params, const int depth) const
{
    params[0] = _sign_alpha;

    double* val_ptr;
    if (depth >= nlopt_wrapper::MAX_PARAM_DEPTH)
    {
        val_ptr = _feats[0]->value_ptr();
        params[1] = std::max(
            0.0,
            -1.0 * _sign_alpha *
                (*std::min_element(val_ptr, val_ptr + _n_samp, [params](double v1, double v2) {
                    return params[0] * v1 < params[0] * v2;
                })));
        return;
    }
    _feats[0]->initialize_params(params + 2, depth + 1);

    val_ptr = _feats[0]->value_ptr(params + 2, -1, true, depth + 1);
    params[1] = std::max(
        0.0,
        -1.0 * _sign_alpha *
            (*std::min_element(val_ptr, val_ptr + _n_samp, [params](double v1, double v2) {
                return params[0] * v1 < params[0] * v2;
            })));
}

void SqrtParamNode::update_postfix(std::string& cur_expr, const bool add_params) const
{
    std::stringstream postfix;
    postfix << get_postfix_term();
    if (add_params)
    {
        postfix << ":" << std::setprecision(13) << std::scientific << _params[0];
        for (int pp = 1; pp < _params.size(); ++pp)
        {
            postfix << "," << std::setprecision(13) << std::scientific << _params[pp];
        }
    }
    cur_expr = postfix.str() + "|" + cur_expr;
    _feats[0]->update_postfix(cur_expr, false);
}
