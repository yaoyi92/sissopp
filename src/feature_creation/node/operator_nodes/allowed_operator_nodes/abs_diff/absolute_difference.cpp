// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/operator_nodes/allowed_operator_nodes/absolute_difference.hpp
 *  @brief Implements a class for the absolute difference operator
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 *
 *  This class represents the binary operator -> |A - B|
 */

#include "feature_creation/node/operator_nodes/allowed_operator_nodes/abs_diff/absolute_difference.hpp"

#include "feature_creation/node/operator_nodes/allowed_operator_nodes/sub/subtract.hpp"

void generateAbsDiffNode(std::vector<node_ptr>& feat_list,
                         const node_ptr feat_1,
                         const node_ptr feat_2,
                         unsigned long int& feat_ind,
                         const int max_leaves,
                         const double l_bound,
                         const double u_bound)
{
    // If the units do not match up the operation is invalid
    if ((feat_1->n_leaves() + feat_2->n_leaves() > max_leaves) ||
        (feat_1->unit() != feat_2->unit()))
    {
        return;
    }

    // Check if the feature would simplify to a less complicated one
    std::map<std::string, int> add_sub_leaves;
    int expected_abs_tot = 0;
    feat_1->update_add_sub_leaves(add_sub_leaves, 1, expected_abs_tot);
    feat_2->update_add_sub_leaves(add_sub_leaves, -1, expected_abs_tot);

    int leaves_v_expected = std::accumulate(
        add_sub_leaves.begin(), add_sub_leaves.end(), -1 * expected_abs_tot, [](int tot, auto el) {
            return tot + std::abs(el.second);
        });
    if ((add_sub_leaves.size() < 2) || std::abs((leaves_v_expected) != 0))
    {
        return;
    }

    // Check if feature is a constant multiple of a previous feature
    int add_sub_tot_first = std::abs(add_sub_leaves.begin()->second);
    if ((std::abs(add_sub_tot_first) > 1) &&
        std::all_of(add_sub_leaves.begin(), add_sub_leaves.end(), [&add_sub_tot_first](auto el) {
            return std::abs(el.second) == add_sub_tot_first;
        }))
    {
        return;
    }

    // If the difference is all positive taking the absolute value is not a valid operation
    ++feat_ind;
    node_ptr new_feat = std::make_shared<SubNode>(feat_1, feat_2, feat_ind);
    double* val_ptr = new_feat->value_ptr();

    if (*std::min_element(val_ptr, val_ptr + new_feat->n_samp()) > l_bound)
    {
        return;
    }

    new_feat = std::make_shared<AbsDiffNode>(feat_1, feat_2, feat_ind);
    new_feat->set_value();

    // Check if the feature is NaN, constant, greater than the allowed max of less than the allowed min
    if (new_feat->is_const() ||
        std::any_of(
            val_ptr,
            val_ptr + new_feat->n_samp(),
            [&u_bound](double d) { return !std::isfinite(d) || (std::abs(d) > u_bound); }) ||
        (util_funcs::max_abs_val<double>(val_ptr, new_feat->n_samp()) < l_bound))
    {
        return;
    }

    feat_list.push_back(new_feat);
}

AbsDiffNode::AbsDiffNode() {}

AbsDiffNode::AbsDiffNode(const node_ptr feat_1,
                         const node_ptr feat_2,
                         const unsigned long int feat_ind)
    : OperatorNode({feat_1, feat_2}, feat_ind)
{
}

AbsDiffNode::AbsDiffNode(std::array<node_ptr, 2> feats, const unsigned long int feat_ind)
    : OperatorNode(feats, feat_ind)
{
}

AbsDiffNode::AbsDiffNode(const node_ptr feat_1,
                         const node_ptr feat_2,
                         const unsigned long int feat_ind,
                         const int max_leaves,
                         const double l_bound,
                         const double u_bound)
    : OperatorNode({feat_1, feat_2}, feat_ind)
{
    // If the units do not match up the operation is invalid
    if ((feat_1->n_leaves() + feat_2->n_leaves() > max_leaves) ||
        (feat_1->unit() != feat_2->unit()))
    {
        throw InvalidFeatureException();
    }

    if (feat_1->n_leaves() + feat_2->n_leaves() > max_leaves)
    {
        throw InvalidFeatureException();
    }

    // Check if the feature would simplify to a less complicated one
    std::map<std::string, int> add_sub_leaves;
    int expected_abs_tot = 0;
    _feats[0]->update_add_sub_leaves(add_sub_leaves, 1, expected_abs_tot);
    _feats[1]->update_add_sub_leaves(add_sub_leaves, -1, expected_abs_tot);

    if ((add_sub_leaves.size() < 2))
    {
        throw InvalidFeatureException();
    }

    int leaves_v_expected = std::accumulate(
        add_sub_leaves.begin(), add_sub_leaves.end(), -1 * expected_abs_tot, [](int tot, auto el) {
            return tot + std::abs(el.second);
        });
    if (std::abs(leaves_v_expected) != 0)
    {
        throw InvalidFeatureException();
    }

    // Check if feature is a constant multiple of a previous feature
    int add_sub_tot_first = std::abs(add_sub_leaves.begin()->second);
    if ((std::abs(add_sub_tot_first) > 1) &&
        std::all_of(add_sub_leaves.begin(), add_sub_leaves.end(), [&add_sub_tot_first](auto el) {
            return std::abs(el.second) == add_sub_tot_first;
        }))
    {
        throw InvalidFeatureException();
    }

    // If the difference is all positive taking the absolute value is not a valid operation
    double* val_ptr = node_value_arrs::get_value_ptr(feat_ind, feat_ind, rung());
    allowed_op_funcs::sub(feat_1->n_samp(),
                          feat_1->value_ptr(rung() + 2),
                          feat_2->value_ptr(rung() + 1),
                          1.0,
                          0.0,
                          val_ptr);
    if (*std::min_element(val_ptr, val_ptr + _n_samp) > l_bound)
    {
        throw InvalidFeatureException();
    }

    set_value();
    // Check if the feature is NaN, constant, greater than the allowed max of less than the allowed min
    if (is_nan() || is_const() ||
        (util_funcs::max_abs_val<double>(value_ptr(), _n_samp) > u_bound) ||
        (util_funcs::max_abs_val<double>(value_ptr(), _n_samp) < l_bound))
    {
        throw InvalidFeatureException();
    }
}

node_ptr AbsDiffNode::hard_copy() const
{
    node_ptr cp = std::make_shared<AbsDiffNode>(
        _feats[0]->hard_copy(), _feats[1]->hard_copy(), _feat_ind);
    cp->set_selected(_selected);
    cp->set_d_mat_ind(_d_mat_ind);
    return cp;
}

void AbsDiffNode::update_add_sub_leaves(std::map<std::string, int>& add_sub_leaves,
                                        const int pl_mn,
                                        int& expected_abs_tot) const
{
    std::string key = expr();
    if (add_sub_leaves.count(key) > 0)
    {
        add_sub_leaves[key] += pl_mn;
    }
    else
    {
        add_sub_leaves[key] = pl_mn;
    }

    ++expected_abs_tot;
}

void AbsDiffNode::update_div_mult_leaves(std::map<std::string, double>& div_mult_leaves,
                                         const double fact,
                                         double& expected_abs_tot) const
{
    std::string key = expr();
    if (div_mult_leaves.count(key) > 0)
    {
        div_mult_leaves[key] += fact;
    }
    else
    {
        div_mult_leaves[key] = fact;
    }

    expected_abs_tot += std::abs(fact);
}

void AbsDiffNode::set_value(int offset, const bool for_comp) const
{
    double* val_ptr;
    if (_selected && (offset == -1))
    {
        offset += (offset == -1);
        val_ptr = node_value_arrs::get_d_matrix_ptr(_d_mat_ind);
    }
    else
    {
        offset += (offset == -1);
        val_ptr = node_value_arrs::get_value_ptr(_arr_ind, _feat_ind, rung(), offset, for_comp);
    }

    allowed_op_funcs::abs_diff(_n_samp,
                               _feats[0]->value_ptr(2 * offset, for_comp),
                               _feats[1]->value_ptr(2 * offset + 1, for_comp),
                               1.0,
                               0.0,
                               val_ptr);
}

void AbsDiffNode::set_test_value(int offset, const bool for_comp) const
{
    offset += (offset == -1);
    allowed_op_funcs::abs_diff(
        _n_samp_test,
        _feats[0]->test_value_ptr(2 * offset, for_comp),
        _feats[1]->test_value_ptr(2 * offset + 1, for_comp),
        1.0,
        0.0,
        node_value_arrs::get_test_value_ptr(_arr_ind, _feat_ind, rung(), offset, for_comp));
}
