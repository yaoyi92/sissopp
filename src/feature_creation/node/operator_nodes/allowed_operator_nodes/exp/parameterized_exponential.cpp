// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/operator_nodes/allowed_operator_nodes/exp/parameterized_exponetial.cpp
 *  @brief Implements a class for the parameterized version of the exponential operator
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 *
 *  This class represents the parameterized unary operator -> exp(alpha * A)
 */

#include "feature_creation/node/operator_nodes/allowed_operator_nodes/exp/parameterized_exponential.hpp"

BOOST_SERIALIZATION_ASSUME_ABSTRACT(ExpParamNode)

void generateExpParamNode(std::vector<node_ptr>& feat_list,
                          const node_ptr feat,
                          unsigned long int& feat_ind,
                          const double l_bound,
                          const double u_bound,
                          std::shared_ptr<NLOptimizer> optimizer)
{
    // If the input feature is an exponential, addition, subtraction, or logarithm this feature is invalid
    if ((feat->type() == NODE_TYPE::NEG_EXP) || (feat->type() == NODE_TYPE::EXP) ||
        (feat->type() == NODE_TYPE::ADD) || (feat->type() == NODE_TYPE::SUB) ||
        (feat->type() == NODE_TYPE::LOG) || (feat->type() == NODE_TYPE::PARAM_NEG_EXP) ||
        (feat->type() == NODE_TYPE::PARAM_EXP) || (feat->type() == NODE_TYPE::PARAM_ADD) ||
        (feat->type() == NODE_TYPE::PARAM_SUB) || (feat->type() == NODE_TYPE::PARAM_LOG))
    {
        return;
    }

    ++feat_ind;
    node_ptr new_feat = std::make_shared<ExpParamNode>(feat, feat_ind, optimizer);
    // Domain dom = new_feat->domain();

    // If a scale parameter is 0.0 feature is invalid
    if ((std::abs(new_feat->parameters()[0]) <= 1e-10)
        // ((!dom.is_empty()) && (std::isnan(dom.end_points()[0]) || std::isnan(dom.end_points()[1])))
    )
    {
        return;
    }

    new_feat->set_value();

    // Check if the feature is NaN, greater than the allowed max of less than the allowed min
    if (new_feat->is_const() || new_feat->is_nan() ||
        (util_funcs::max_abs_val<double>(new_feat->value_ptr(), new_feat->n_samp()) > u_bound) ||
        (util_funcs::max_abs_val<double>(new_feat->value_ptr(), new_feat->n_samp()) < l_bound))
    {
        return;
    }

    feat_list.push_back(new_feat);
}

ExpParamNode::ExpParamNode() {}

ExpParamNode::ExpParamNode(const node_ptr feat,
                           const unsigned long int feat_ind,
                           const double l_bound,
                           const double u_bound,
                           std::shared_ptr<NLOptimizer> optimizer)
    : ExpNode(feat, feat_ind)
{
    // If the input feature is an exponential, addition, subtraction, or logarithm this feature is invalid
    if ((feat->type() == NODE_TYPE::NEG_EXP) || (feat->type() == NODE_TYPE::EXP) ||
        (feat->type() == NODE_TYPE::ADD) || (feat->type() == NODE_TYPE::SUB) ||
        (feat->type() == NODE_TYPE::LOG) || (feat->type() == NODE_TYPE::PARAM_NEG_EXP) ||
        (feat->type() == NODE_TYPE::PARAM_EXP) || (feat->type() == NODE_TYPE::PARAM_ADD) ||
        (feat->type() == NODE_TYPE::PARAM_SUB) || (feat->type() == NODE_TYPE::PARAM_LOG))
    {
        throw InvalidFeatureException();
    }

    _params.resize(n_params_possible(), 0.0);
    get_parameters(optimizer);

    // Domain dom = domain();
    // Check if the scale parameter is 0.0 or if the feature is NaN, constant, greater than the allowed max of less than the allowed min
    if ((std::abs(_params[0]) <= 1e-10) ||
        // ((!dom.is_empty()) && (std::isnan(dom.end_points()[0]) || std::isnan(dom.end_points()[1]))) ||
        is_const() || is_nan() ||
        (util_funcs::max_abs_val<double>(value_ptr(), _n_samp) > u_bound) ||
        (util_funcs::max_abs_val<double>(value_ptr(), _n_samp) < l_bound))
    {
        throw InvalidFeatureException();
    }
}

ExpParamNode::ExpParamNode(const node_ptr feat,
                           const unsigned long int feat_ind,
                           std::shared_ptr<NLOptimizer> optimizer)
    : ExpNode(feat, feat_ind)
{
    _params.resize(n_params_possible(), 0.0);
    get_parameters(optimizer);
}

ExpParamNode::ExpParamNode(const node_ptr feat,
                           const unsigned long int feat_ind,
                           const double l_bound,
                           const double u_bound)
    : ExpNode(feat, feat_ind)
{
    _params.resize(n_params_possible(), 0.0);
}

ExpParamNode::ExpParamNode(std::array<node_ptr, 1> feats,
                           const unsigned long int feat_ind,
                           std::vector<double> params)
    : ExpNode(feats, feat_ind), _params(params)
{
}

node_ptr ExpParamNode::hard_copy() const
{
    node_ptr cp = std::make_shared<ExpParamNode>(_feats[0]->hard_copy(), _feat_ind);
    cp->set_selected(_selected);
    cp->set_d_mat_ind(_d_mat_ind);
    cp->set_parameters(_params.data());
    return cp;
}

void ExpParamNode::get_parameters(std::shared_ptr<NLOptimizer> optimizer)
{
    double min_res = optimizer->optimize_feature_params(this);
    if (min_res == std::numeric_limits<double>::infinity())
    {
        _params[0] = 0.0;
    }
}

void ExpNode::set_value(const double* params,
                        int offset,
                        const bool for_comp,
                        const int depth) const
{
    bool is_root = (offset == -1);
    offset += is_root;

    double* vp_0;
    if (depth < nlopt_wrapper::MAX_PARAM_DEPTH)
    {
        vp_0 = _feats[0]->value_ptr(params + 2, 2 * offset, for_comp, depth + 1);
    }
    else
    {
        vp_0 = _feats[0]->value_ptr(2 * offset, for_comp);
    }

    double* val_ptr;
    if (_selected && is_root)
    {
        val_ptr = node_value_arrs::get_d_matrix_ptr(_d_mat_ind);
    }
    else
    {
        val_ptr = node_value_arrs::access_param_storage(rung(), offset, for_comp);
    }

    allowed_op_funcs::exp(_n_samp, vp_0, params[0], params[1], val_ptr);
}

void ExpNode::set_test_value(const double* params,
                             int offset,
                             const bool for_comp,
                             const int depth) const
{
    offset += (offset == -1);

    double* vp_0;
    if (depth < nlopt_wrapper::MAX_PARAM_DEPTH)
    {
        vp_0 = _feats[0]->test_value_ptr(params + 2, 2 * offset, for_comp, depth + 1);
    }
    else
    {
        vp_0 = _feats[0]->test_value_ptr(2 * offset, for_comp);
    }

    allowed_op_funcs::exp(_n_samp_test,
                          vp_0,
                          params[0],
                          params[1],
                          node_value_arrs::access_param_storage_test(rung(), offset, for_comp));
}

void ExpNode::set_bounds(double* lb, double* ub, const int depth) const
{
    // The parameters of exponentials are dependent on the external shift/scale parameters, but physically relevant
    lb[0] = 0.0;
    lb[1] = 0.0;
    ub[1] = 0.0;

    if (depth >= nlopt_wrapper::MAX_PARAM_DEPTH)
    {
        return;
    }

    _feats[0]->set_bounds(lb + 2, ub + 2, depth + 1);
}

void ExpNode::initialize_params(double* params, const int depth) const
{
    params[0] = 1.0;
    params[1] = 0.0;

    if (depth >= nlopt_wrapper::MAX_PARAM_DEPTH)
    {
        return;
    }

    _feats[0]->initialize_params(params + 2, depth + 1);
}

void ExpParamNode::update_postfix(std::string& cur_expr, const bool add_params) const
{
    std::stringstream postfix;
    postfix << get_postfix_term();
    if (add_params)
    {
        postfix << ":" << std::setprecision(13) << std::scientific << _params[0];
        for (int pp = 1; pp < _params.size(); ++pp)
        {
            postfix << "," << std::setprecision(13) << std::scientific << _params[pp];
        }
    }
    cur_expr = postfix.str() + "|" + cur_expr;
    _feats[0]->update_postfix(cur_expr, false);
}
