// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/Node.hpp
 *  @brief Defines the base class for the objects that represent features
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 *
 *  This package represents the features in SISSO as a binary expression tree and are accessible from the root node of that tree.
 *  The node class are the vertices of the tree and represent initial features (FeatureNode) and all algebraic operators (OperatorNodes).
 *  The edges are the represented via the _feats member in OperatorNode that store the features the operation acts on (its children).
 */

#ifndef NODE
#define NODE

#include <boost/serialization/assume_abstract.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/unique_ptr.hpp>

#include "feature_creation/domain/Domain.hpp"
#include "feature_creation/node/value_storage/nodes_value_containers.hpp"
#include "feature_creation/units/Unit.hpp"
#include "utils/math_funcs.hpp"

#ifdef PY_BINDINGS
#include "python/py_binding_cpp_def/conversion_utils.hpp"
namespace py = pybind11;
#endif

// Forward Declaration of NLOptimizer
class NLOptimizer;

// DocString: cls_node
/**
 * @brief Base class for a Node.
 * @details The class is used to describe a Node on a binary expression tree. Features defined as the root of the binary expression tree.
 *
 */
class Node
{
    friend class boost::serialization::access;

    /**
     * @brief Serialization function to send over MPI
     *
     * @param ar Archive representation of node
     */
    template <typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        ar& _n_samp;
        ar& _n_samp_test;
        ar& _feat_ind;
        ar& _arr_ind;
        ar& _d_mat_ind;
        ar& _selected;
    }

protected:
    // clang-format off
    int _n_samp; //!< Number of samples in the training set
    int _n_samp_test; //!< Number of samples in the test set
    unsigned long int _feat_ind; //!< The unique index ID for the feature
    unsigned long int _arr_ind; //!< The index that is used to access the data stored in the central data storage
    int _d_mat_ind; //!< The index used to access the data stored in the description matrix

    bool _selected; //!< True if the feature is selected;
    // clang-format on
public:
    /**
     * @brief Base Constructor
     * @details This is only used for serialization
     */
    Node();

    // DocString: node_init
    /**
     * @brief Constructor that specifies feature index and number of samples
     *
     * @param feat_ind The index of the feature
     * @param n_samp The number of samples in the training set
     * @param n_samp_test  The number of samples in the test set
     */
    Node(const unsigned long int feat_ind, const int n_samp, int n_samp_test);

    /**
     * @brief Copy Constructor
     *
     * @param o Node to be copied
     */
    Node(const Node& o) = default;

    /**
     * @brief Move Constructor
     *
     * @param o Node to be moved
     */
    Node(Node&& o) = default;

    /**
     * @brief Copy Assignment operator
     *
     * @param o Node to be copied
     */
    Node& operator=(const Node& o) = default;

    /**
     * @brief Move Assignment operator
     *
     * @param o Node to be moved
     */
    Node& operator=(Node&& o) = default;

    /**
     * @brief Destructor
     */
    virtual ~Node();

    /**
     * @brief Makes a hard copy node (All members of the Node are independent of the original one)
     * @return A shared_ptr to the copied node
     */
    virtual std::shared_ptr<Node> hard_copy() const = 0;

    /**
     * @brief Relinks all features of an OperatorNodes's _feat member to Nodes inside of phi
     *
     * @param phi A vector containing all Nodes that this Node can relink to
     */
    virtual void reset_feats(std::vector<std::shared_ptr<Node>>& phi) = 0;

    // DocString: node_x_in_expr
    /**
     * @brief A vector storing the expressions for all primary features in the order they appear in the postfix expression
     */
    virtual std::vector<std::string> x_in_expr_list() const = 0;

    /**
     * @brief Update the number of primary features (non-unique) this feature contains (The number of leaves of the Binary Expression Tree)
     *
     * @param cur_n_leaves (int) A recurisve counting variable
     */
    virtual void update_n_leaves(int& cur_n_leaves) const = 0;

    // DocString: node_n_leaves
    /**
     * @brief The number of primary features (non-unique) this feature contains (The number of leaves of the Binary Expression Tree)
     *
     */
    int n_leaves() const;

    // DocString: node_reindex_1
    /**
     * @brief Reset _feat_ind and _arr_ind to ind
     *
     * @param ind (int) the new value of _feat_ind and _arr_ind
     */
    inline void reindex(const unsigned long int ind)
    {
        _feat_ind = ind;
        _arr_ind = ind;
    }

    // DocString: node_reindex_2
    /**
     * @brief Reset _feat_ind and _arr_ind to feat_ind and arr_ind, respectively
     *
     * @param feat_ind (int) the new value of _feat_ind
     * @param arr_ind (int) the new value of _arr_ind
     */
    inline void reindex(const unsigned long int feat_ind, const unsigned long int arr_ind)
    {
        _feat_ind = feat_ind;
        _arr_ind = arr_ind;
    }

    /**
     * @brief Get the score used to sort the features in the feature space (Based on the type of node and _feat_ind)
     *
     * @param max_ind The maximum index of the input nodes
     * @return The score used to sort the feature space
     */
    virtual unsigned long long sort_score(unsigned int max_ind) const = 0;

    // DocString: node_n_samp
    /**
     * @brief The number of samples in the training set
     */
    inline int n_samp() const { return _n_samp; }

    // DocString: node_n_samp_test
    /**
     * @brief The number of samples in the test set
     */
    inline int n_samp_test() const { return _n_samp_test; }

    // DocString: node_feat_ind
    /**
     * @brief The unique index ID for the feature
     */
    inline unsigned long int feat_ind() const { return _feat_ind; }

    // DocString: node_arr_ind
    /**
     * @brief The index that is used to access the data stored in the central data storage
     */
    inline unsigned long int arr_ind() const { return _arr_ind; }

    // DocString: node_selected
    /**
     * @brief True if the feature is selected
     */
    inline bool selected() const { return _selected; }

    /**
     * @brief Setter function for _selected
     *
     * @param sel True if the feature selected
     */
    inline void set_selected(const bool sel) { _selected = sel; }

    /**
     * @brief  Setter function for _d_mat_ind
     *
     * @param ind new _d_mat_ind
     */
    inline void set_d_mat_ind(const int ind) { _d_mat_ind = ind; }

    // DocString: node_d_mat_ind
    /**
     * @brief The index used to access the data stored in the description matrix
     */
    inline int d_mat_ind() const { return _d_mat_ind; }

    // DocString: node_expr
    /**
     * @brief A human readable equation representing the feature
     */
    virtual std::string expr() const = 0;

    /**
     * @brief Get the valid LaTeX expression that represents the feature
     */
    virtual std::string get_latex_expr() const = 0;

    // DocString: node_latex_expr
    /**
     * @brief The valid LaTeX expression that represents the feature
     */
    inline std::string latex_expr() const { return "$" + get_latex_expr() + "$"; }

    // DocString: node_unit
    /**
     * @brief The unit of the feature (Derived recursively from the primary features and operators)
     */
    virtual Unit unit() const = 0;

    /**
     * @brief A vector containing the values of the training set samples for the feature
     */
    virtual std::vector<double> value() const = 0;

    /**
     * @brief A vector containing the values of the test set samples for the feature
     */
    virtual std::vector<double> test_value() const = 0;

    // DocString: node_set_value
    /**
     * @brief Set the value of all training samples for the feature inside the central data storage arrays
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     */
    virtual void set_value(int offset = -1, const bool for_comp = false) const = 0;

    // DocString: node_set_stand_value
    /**
     * @brief Set the value of all training samples to the standardized values for the feature inside the central data storage arrays
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     */
    void set_standardized_value(const bool for_comp = false) const;

    /**
     * @brief The pointer to where the feature's training data is stored
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     *
     * @return pointer to the feature's training value
     */
    virtual double* value_ptr(int offset = -1, const bool for_comp = false) const = 0;

    /**
     * @brief The pointer to where the feature's standardized training data is stored
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     *
     * @return pointer to the feature's training value
     */
    double* stand_value_ptr(const bool for_comp = false) const;

    // DocString: node_set_test_value
    /**
     * @brief Set the value of all test samples for the feature inside the central data storage array
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     */
    virtual void set_test_value(int offset = -1, const bool for_comp = false) const = 0;

    // DocString: node_set_stand_test_value
    /**
     * @brief Set the value of all test samples to the standardized values for the feature inside the central data storage array
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     */
    void set_standardized_test_value(const bool for_comp = false) const;

    /**
     * @brief The pointer to where the feature's test data is stored
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     *
     * @return pointer to the feature's test values
     */
    virtual double* test_value_ptr(int offset = -1, const bool for_comp = false) const = 0;

    /**
     * @brief The pointer to where the feature's standardized test data is stored
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     *
     * @return pointer to the feature's test values
     */
    double* stand_test_value_ptr(const bool for_comp = false) const;

    // DocString: node_is_nan
    /**
     * @brief True if at least one of the values in the feature is NaN
     */
    virtual bool is_nan() const = 0;

    // DocString: node_is_const
    /**
     * @brief True if all values of the feature is constant
     */
    virtual bool is_const() const = 0;

    /**
     * @brief Returns the type of node this is
     */
    virtual NODE_TYPE type() const = 0;

    /**
     * @brief Update the rung of the feature (does nothing as the feature nodes are rung 0)
     *
     * @param cur_rung The current rung of the feature
     */
    virtual void rung_update(int& cur_rung) const = 0;

    // DocString: node_rung
    /**
     * @brief Return the rung of the feature (Height of the binary expression tree - 1)
     */
    int rung() const;

    // DocString: node_primary_feature_decomp
    /**
     * @brief A map representing the primary feature comprising a feature
     */
    std::map<std::string, int> primary_feature_decomp() const;

    /**
     * @brief Update the primary feature decomposition of a feature
     *
     * @param pf_decomp The primary feature decomposition of the feature calling this function.
     */
    virtual void update_primary_feature_decomp(std::map<std::string, int>& pf_decomp) const = 0;

    /**
     * @brief Converts a feature into a postfix expression
     *
     * @details Recursively creates a postfix representation of the string
     *
     * @param cur_expr The current expression
     * @param add_params If true include the parameters in the postfix expression
     * @return The current postfix expression of the feature
     */
    virtual void update_postfix(std::string& cur_expr, const bool add_params = true) const = 0;

    // DocString: node_postfix_expr
    /**
     * @brief A computer readable representation of the feature. Primary features represented by their index in phi_0, node types are represented by abbreviations, and the order is the same as the postfix representation of the expression
     */
    inline std::string postfix_expr() const
    {
        std::string cur_expr = "";
        update_postfix(cur_expr);
        return cur_expr.substr(0, cur_expr.size() - 1);
    }

    /**
     * @brief Get the term used in the postfix expression for this Node
     */
    virtual std::string get_postfix_term() const = 0;

    // DocString: node_matlab_expr
    /**
     * @brief The string that corresponds to a line of code that would evaluate the feature in matlab
     */
    virtual std::string matlab_fxn_expr() const = 0;

    /**
     * @brief update the dictionary used to check if an Add/Sub/AbsDiff node is valid
     *
     * @param add_sub_leaves the dictionary used to check if an Add/Sub node is valid
     * @param pl_mn 1 for addition and -1 for subtraction
     * @param expected_abs_tot The expected absolute sum of all values in add_sub_leaves
     */
    virtual void update_add_sub_leaves(std::map<std::string, int>& add_sub_leaves,
                                       const int pl_mn,
                                       int& expected_abs_tot) const = 0;

    /**
     * @brief update the dictionary used to check if an Mult/Div node is valid
     *
     * @param div_mult_leaves the dictionary used to check if an Mult/Div node is valid
     * @param fact amount to increment the element (a primary features) of the dictionary by
     * @param expected_abs_tot The expected absolute sum of all values in div_mult_leaves
     */
    virtual void update_div_mult_leaves(std::map<std::string, double>& div_mult_leaves,
                                        const double fact,
                                        double& expected_abs_tot) const = 0;

    /**
     * @brief Get the domain of a feature
     *
     * @return The domain of the feature
     */
    virtual Domain domain() const = 0;

#ifdef PARAMETERIZE
    // DocString: node_parameters
    /**
     * @brief The parameters used for including individual scale and bias terms to each operator in the Node
     */
    virtual std::vector<double> parameters() const = 0;

    /**
     * @brief The pointer to the head of the parameters used for including individual scale and bias terms to each operator in the Node
     */
    virtual inline const double* param_pointer() const
    {
        throw std::logic_error(
            "Trying to access the parameter pointer to a node with no parameters.");
    }

    /**
     * @brief Set the non-linear parameters
     * @param params The new scale and bias terms of this node
     * @param check_sz If true check the size of the parameters
     */
    virtual void set_parameters(const std::vector<double> params, const bool check_sz = true) = 0;

    /**
     * @brief Set the non-linear parameters
     * @param params The new scale and bias terms of this node
     */
    virtual void set_parameters(const double* params) = 0;

    // DocString: node_get_params
    /**
     * @brief Optimize the scale and bias terms for each operation in the Node.
     * @details Use optimizer to find the scale and bias terms that minimizes the associated loss function
     *
     * @param optimizer The optimizer used to evaluate the loss function for each optimization and find the optimal parameters
     */
    virtual void get_parameters(std::shared_ptr<NLOptimizer> optimizer) = 0;

    /**
     * @brief returns the number of parameters for this feature
     *
     * @param n_cur A recursive helper variable to count the number of parameters of the Node
     * @param depth How far down a given Node is from the root OperatorNode
     * @return the number of parameters (_params.size())
     */
    virtual int n_params_possible(const int n_cur, const int depth) const = 0;

    // DocString: node_n_params_possible
    /**
     * @brief returns the number of parameters for this feature
     */
    inline int n_params_possible() const { return n_params_possible(0, 1); }

    // DocString: node_n_params
    /**
     * @brief returns the number of actual parameters for this feature
     */
    virtual inline int n_params() const { return 0; }

    /**
     * @brief Set the value of all training samples for the feature inside the central data storage array
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     * @param depth (int) How far down a given Node is from the root OperatorNode
     */
    virtual void set_value(const double* params,
                           int offset = -1,
                           const bool for_comp = false,
                           const int depth = 1) const = 0;

    /**
     * @brief The pointer to where the feature's training data is stored
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     * @param depth (int) How far down a given Node is from the root OperatorNode
     * @returns The pointer to the feature's data
     */
    virtual double* value_ptr(const double* params,
                              int offset = -1,
                              const bool for_comp = false,
                              const int depth = 1) const = 0;

    /**
     * @brief Set the value of all test samples for the feature inside the central data storage array
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     * @param depth How far down a given Node is from the root OperatorNode
     */
    virtual void set_test_value(const double* params,
                                int offset = -1,
                                const bool for_comp = false,
                                const int depth = 1) const = 0;

    /**
     * @brief The pointer to where the feature's test data is stored
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     * @param depth How far down a given Node is from the root OperatorNode
     * @returns The pointer to the feature's data
     */
    virtual double* test_value_ptr(const double* params,
                                   int offset = -1,
                                   const bool for_comp = false,
                                   const int depth = 1) const = 0;

    /**
     * @brief The expression of the feature
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param depth How far down a given Node is from the root OperatorNode
     * @return A human readable equation representing the feature
     */
    virtual std::string expr(const double* params, const int depth = 1) const = 0;

    /**
     * @brief Get valid LaTeX equation that represents the feature
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param depth How far down a given Node is from the root OperatorNode
     * @return Get the valid LaTeX expression that represents the feature
     */
    virtual std::string get_latex_expr(const double* params, const int depth = 1) const = 0;

    // DocString: node_matlab_expr_param
    /**
     * @brief Get the string that corresponds to the code needed to evaluate the node in matlab
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param depth How far down a given Node is from the root OperatorNode
     * @return The matlab code for the feature
     */
    virtual std::string matlab_fxn_expr(const double* params, const int depth = 1) const = 0;

    /**
     * @brief Set the upper and lower bounds for the scale and bias term of this Node and its children
     *
     * @param lb A pointer to the location where the lower bounds for the scale and bias term of this Node is set
     * @param ub A pointer to the location where the upper bounds for the scale and bias term of this Node is set
     * @param depth How far down a given Node is from the root OperatorNode
     */
    virtual void set_bounds(double* lb, double* ub, const int depth = 1) const = 0;

    /**
     * @brief Initialize the scale and bias terms for this Node and its children
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param depth How far down a given Node is from the root OperatorNode
     */
    virtual void initialize_params(double* params, const int depth = 1) const = 0;

    /**
     * @brief Calculates the derivative of an operation with respect to the parameters for a given sample
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param samp_ind sample index number
     */
    virtual void param_derivative(const double* params,
                                  double* dfdp,
                                  const int depth = 1) const = 0;

    /**
     * @brief Get the parameter gradient for non-linear optimization
     *
     * @param grad pointer to the gradient storage
     * @param dfdp pointer to where the feature derivative pointers are located
     */
    virtual void gradient(double* grad, double* dfdp) const = 0;

    /**
     * @brief Get the parameter gradient for non-linear optimization
     *
     * @param grad pointer to the gradient storage
     * @param dfdp pointer to where the feature derivative pointers are located
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param depth The current depth in the binary expression tree
     */
    virtual void gradient(double* grad,
                          double* dfdp,
                          const double* params,
                          const int depth = 1) const = 0;

    /**
     * @brief Get the domain of a feature
     *
     * @param params Pointer to the scale and shift parameters of the feature
     * @return The domain of the feature
     */
    virtual Domain domain(const double* params, const int depth = 1) const = 0;
#endif

    // DocString: node_n_feats
    /**
     * @brief The number of features used for the operator (Number of child node)
     */
    virtual int n_feats() const = 0;

    // DocString: node_feat
    /**
     * @brief Return the ind^th feature stored by an operator node
     *
     * @param ind (int) the index of the feats list to be accessed
     * @return The feature stored in _feats[ind]
     */
    virtual std::shared_ptr<Node> feat(const int ind) const = 0;

#ifdef PY_BINDINGS

    // DocString: node_value_py
    /**
     * @brief An array containing the values of the training set samples for the feature
     */
    inline py::array_t<double> value_py() { return py::cast(value()); }

    // DocString: node_test_value_py
    /**
     * @brief An array containing the values of the test set samples for the feature
     */
    inline py::array_t<double> test_value_py() { return py::cast(test_value()); }

#endif
};

typedef std::shared_ptr<Node> node_ptr;

/**
 * @brief Exception used when a Node Object can't be created for some reason
 *
 */
class InvalidFeatureException : public std::exception
{
    inline const char* what() const throw() { return "Invalid Feature created"; }
};

#endif
