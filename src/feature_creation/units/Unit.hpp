// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/unit/Unit.hpp
 *  @brief Defines a class that represents Units of of the features
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 *
 *  A dictionary representation of the unit for a features (key = unit, value = power)
 */

#ifndef UNIT
#define UNIT

#include <boost/serialization/map.hpp>
#include <boost/serialization/string.hpp>
#include <map>
#include <memory>
#include <sstream>

#include "utils/string_utils.hpp"

using StringRange = boost::iterator_range<std::string::const_iterator>;

// DocString: cls_unit
/**
 * @brief Class to define the units of the features
 */
class Unit
{
private:
    friend class boost::serialization::access;
    /**
     * @brief Function to serialize the data for MPI communication
     */
    template <typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        ar& _dct;
    }

protected:
    // clang-format off
    std::map<std::string, double> _dct; //!< The dictionary representation of the unit <unit string, unit power>
    // clang-format on

public:
    // DocString: unit_base_const
    /**
     * @brief Base Constructor
     * @details Creates a unit with no entries
     */
    Unit();

    // DocString: unit_init_dict
    /**
     * @brief Constructor of the unit with the dictionary representation
     *
     * @param dct dictionary representation of the dictionary
     */
    Unit(const std::map<std::string, double> dct);

    // DocString: unit_init_str
    /**
     * @brief Construct the unit based on a string representation of the unit
     * @details Take a string representation of a unit, and build the Unit
     *
     * @param unit_str The string to build the unit
     */
    Unit(std::string unit_str);

    // DocString: unit_init_unit
    /**
     * @brief Copy Constructor
     *
     * @param o Unit to copy
     */
    Unit(const Unit& o);

    // DocString: unit_str
    /**
     * @brief Convert the unit into a string
     */
    std::string toString() const;

    // DocString: unit_latex_str
    /**
     * @brief Convert the unit into a latexified string
     */
    std::string toLatexString() const;

    // DocString: unit_mult
    /**
     * @brief Multiply operator for units
     *
     * @param unit_2 The second unit to multiply by
     * @return The product of this unit with unit_2
     */
    Unit operator*(const Unit unit_2) const;

    // DocString: unit_div
    /**
     * @brief Divide operator for units
     *
     * @param unit_2 The second unit to divide by
     * @return The quotient of this unit with unit_2
     */
    Unit operator/(const Unit unit_2) const;

    // DocString: unit_mult_assign
    /**
     * @brief Multiply assignment operator for units
     *
     * @param unit_2 The second unit to multiply this Unit by
     * @return The product of this unit with unit_2
     */
    Unit& operator*=(const Unit unit_2);

    // DocString: unit_div_assign
    /**
     * @brief Divide assignment operator for units
     *
     * @param unit_2 The second unit to divide this Unit by
     * @return The quotient of this unit with unit_2
     */
    Unit& operator/=(const Unit unit_2);

    // DocString: unit_pow
    /**
     * @brief Exponentiation operator for units
     *
     * @param power power to exponentiate the unit
     * @return The unit raised to the power
     */
    Unit operator^(const double power) const;

    // DocString: unit_inverse
    /**
     * @brief Inverse operator for units
     *
     * @return The inverse of this unit
     */
    Unit inverse() const;

    /**
     * @brief Determine if a second unit is equal to this one
     *
     * @param unit_2 The unit to compare against
     * @return True if unit_2 equals this unit
     */
    bool equal(Unit unit_2) const;

    // DocString: unit_eq
    /**
     * @brief Determine if a second unit is equal to this one
     *
     * @param unit_2 The unit to compare against
     * @return True if unit_2 equals this unit
     */
    inline bool operator==(const Unit unit_2) const { return equal(unit_2); }

    // DocString: unit_neq
    /**
     * @brief Determine if a second unit is not equal to this one
     *
     * @param unit_2 The unit to compare against
     * @return False if unit_2 equals this unit
     */
    inline bool operator!=(const Unit unit_2) const { return !equal(unit_2); }

    /**
     * @brief The dictionary
     */
    inline std::map<std::string, double> dct() const { return _dct; }
};

/**
 * @brief Operator to print the unit to a string stream
 *
 * @param outStream Stream to print the unit to
 * @param unit Unit to print to the string stream
 */
std::ostream& operator<<(std::ostream& outStream, const Unit& unit);

#endif
