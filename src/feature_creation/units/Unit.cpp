// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/unit/Unit.cpp
 *  @brief Implements a class that represents Units of of the features
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "feature_creation/units/Unit.hpp"

Unit::Unit() : _dct() {}
Unit::Unit(const std::map<std::string, double> dct) : _dct(dct) {}

Unit::Unit(std::string unit_str) : _dct()
{
    // Remove spaces and Unitless markers from the string
    boost::ireplace_all(unit_str, " ", "");
    boost::ireplace_all(unit_str, "Unitless", "");
    boost::ireplace_all(unit_str, "unitless", "");

    // Split the unit based on multiplication/division keys
    std::vector<std::string> split_unit;
    boost::algorithm::split(split_unit, unit_str, boost::algorithm::is_any_of("*/"));

    // Find where multiplication occurs
    std::vector<int> mult_ops;
    std::vector<StringRange> mult_range;
    boost::algorithm::find_all(mult_range, unit_str, "*");
    for (auto& it : mult_range)
    {
        mult_ops.push_back(it.begin() - unit_str.begin());
    }

    // Find where division occurs
    std::vector<int> div_ops;
    std::vector<StringRange> div_range;
    boost::algorithm::find_all(div_range, unit_str, "/");
    for (auto& it : div_range)
    {
        div_ops.push_back(it.begin() - unit_str.begin());
    }

    // Combine all operators
    std::vector<int> all_ops;
    all_ops.reserve(div_ops.size() + mult_ops.size());
    all_ops.insert(all_ops.end(), mult_ops.begin(), mult_ops.end());
    all_ops.insert(all_ops.end(), div_ops.begin(), div_ops.end());
    std::sort(all_ops.begin(), all_ops.end());

    // If division occurs ensure the exponent is -1 * actual exponent
    std::vector<double> ops((unit_str != "") + all_ops.size(), 1.0);
    for (int oo = 1; oo < ops.size(); ++oo)
    {
        if (std::any_of(div_ops.begin(), div_ops.end(), [&all_ops, &oo](int i) {
                return i == all_ops[oo - 1];
            }))
        {
            ops[oo] *= -1.0;
        }
    }

    // Create the unit dictionary with the correct exponents
    for (int oo = 0; oo < ops.size(); ++oo)
    {
        std::vector<std::string> get_power = str_utils::split_string_trim(split_unit[oo], "^");
        if (get_power[0][0] == '1')
        {
            continue;
        }
        if (get_power.size() > 1)
        {
            _dct[get_power[0]] += ops[oo] * std::stod(get_power[1]);
        }
        else
        {
            _dct[get_power[0]] += ops[oo];
        }
    }
}

Unit::Unit(const Unit& o) : _dct(o._dct) {}

std::string Unit::toString() const
{
    std::stringstream unit_rep;
    std::vector<std::string> keys;
    keys.reserve(_dct.size());

    for (auto& el : _dct)
    {
        keys.push_back(el.first);
    }

    std::sort(keys.begin(), keys.end());

    for (auto& key : keys)
    {
        if (_dct.at(key) == 1)
        {
            unit_rep << " * " << key;
        }
        else if (_dct.at(key) > 0)
        {
            unit_rep << " * " << key << "^" << _dct.at(key);
        }
        else if (_dct.at(key) < 0)
        {
            unit_rep << " * " << key << "^-" << std::abs(_dct.at(key));
        }
    }

    if (unit_rep.str().size() == 0)
    {
        return "Unitless";
    }

    return unit_rep.str().substr(3);
}

std::string Unit::toLatexString() const
{
    std::stringstream unit_rep;
    std::vector<std::string> keys;
    keys.reserve(_dct.size());

    for (auto& el : _dct)
    {
        keys.push_back(el.first);
    }

    std::sort(keys.begin(), keys.end());

    for (auto& key : keys)
    {
        if (_dct.at(key) == 1)
        {
            unit_rep << key;
        }
        else if (_dct.at(key) > 0)
        {
            unit_rep << key << "$^\\mathrm{" << _dct.at(key) << "}$";
        }
        else if (_dct.at(key) < 0)
        {
            unit_rep << key << "$^\\mathrm{-" << std::abs(_dct.at(key)) << "}$";
        }
    }

    if (unit_rep.str().size() == 0)
    {
        return "Unitless";
    }

    return unit_rep.str();
}

Unit Unit::operator*(const Unit unit_2) const
{
    std::map<std::string, double> to_out = dct();
    for (auto& el : unit_2.dct())
    {
        if (to_out.count(el.first) > 0)
        {
            to_out[el.first] += el.second;
        }
        else
        {
            to_out[el.first] = el.second;
        }
    }
    return Unit(to_out);
}

Unit Unit::operator/(const Unit unit_2) const
{
    std::map<std::string, double> to_out = dct();
    for (auto& el : unit_2.dct())
    {
        if (to_out.count(el.first) > 0)
        {
            to_out[el.first] -= el.second;
        }
        else
        {
            to_out[el.first] = -1.0 * el.second;
        }
    }

    return Unit(to_out);
}

Unit& Unit::operator*=(const Unit unit_2)
{
    for (auto& el : unit_2.dct())
    {
        if (_dct.count(el.first) > 0)
        {
            _dct[el.first] += el.second;
        }
        else
        {
            _dct[el.first] = el.second;
        }
    }
    return *this;
}

Unit& Unit::operator/=(const Unit unit_2)
{
    for (auto& el : unit_2.dct())
    {
        if (_dct.count(el.first) > 0)
        {
            _dct[el.first] -= el.second;
        }
        else
        {
            _dct[el.first] = -1.0 * el.second;
        }
    }

    return *this;
}

Unit Unit::operator^(const double power) const
{
    std::map<std::string, double> to_out = dct();
    if (std::abs(power) < 1e-10)
    {
        return Unit();
    }

    for (auto& el : to_out)
    {
        to_out[el.first] *= power;
    }
    return Unit(to_out);
}

Unit Unit::inverse() const
{
    std::map<std::string, double> to_out = dct();
    for (auto& el : to_out)
    {
        to_out[el.first] *= -1.0;
    }
    return Unit(to_out);
}

bool Unit::equal(const Unit unit_2) const
{
    for (auto& el : unit_2.dct())
    {
        if ((_dct.count(el.first) == 0) && (el.second != 0))
        {
            return false;
        }
    }

    for (auto& el : _dct)
    {
        if ((unit_2.dct().count(el.first) == 0) && (el.second != 0))
        {
            return false;
        }
        else if (unit_2.dct()[el.first] != el.second)
        {
            return false;
        }
    }

    if (unit_2.dct().size() == 0)
    {
        for (auto& el : _dct)
        {
            if ((_dct.count(el.first) != 0) && (el.second != 0))
            {
                return false;
            }
        }
    }
    return true;
}

std::ostream& operator<<(std::ostream& outStream, const Unit& unit)
{
    outStream << unit.toString();
    return outStream;
}
