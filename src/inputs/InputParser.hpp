// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file inputs/InputParser.hpp
 *  @brief Define the class that parses the input file
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 *
 *  This class parses and stores the parameters in the input file
 */

#ifndef SISSO_INPUTS
#define SISSO_INPUTS

#include <boost/filesystem.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/serialization/vector.hpp>
#include <chrono>
#include <fstream>
#include <iostream>
#include <iterator>
#include <random>

#include "feature_creation/node/FeatureNode.hpp"
#include "mpi_interface/MPI_Interface.hpp"

#ifdef PARAMETERIZE
#include "nl_opt/utils.hpp"
#endif

#ifdef PY_BINDINGS
#include "python/py_binding_cpp_def/conversion_utils.hpp"

namespace py = pybind11;
#endif

namespace pt = boost::property_tree;

// DocString: cls_input_parser
/**
 * @brief A class that parses the input files and constructs the FeatureSpace and SISSOSolver
 */
class InputParser
{
private:
    // clang-format off
    std::vector<std::string> _sample_ids_train; //!< Vector storing all sample ids for the training samples
    std::vector<std::string> _sample_ids_test; //!< Vector storing all sample ids for the test samples
    std::vector<std::string> _task_names; //!< Vector storing the ID of the task names

    std::vector<std::string> _allowed_param_ops; //!< Vector containing all allowed operators strings for operators with free parameters
    std::vector<std::string> _allowed_ops; //!< Vector containing all allowed operators strings
    std::vector<double> _prop_train; //!< The value of the property to evaluate the loss function against for the training set
    std::vector<double> _prop_test; //!< The value of the property to evaluate the loss function against for the test set

    std::vector<int> _leave_out_inds; //!< List of indexes from the initial data file in the test set
    std::vector<int> _task_sizes_train; //!< Number of training samples per task
    std::vector<int> _task_sizes_test; //!< Number of testing samples per task

    std::vector<FeatureNode> _phi_0; //!< A vector of FeatureNodes for the primary feature space

    Unit _prop_unit; //!< The Unit of the property

    std::string _filename; //!< Name of the input file
    std::string _data_file; //!< Name of the data file
    std::string _prop_key; //!< Key used to find the property column in the data file
    std::string _prop_label; //!< The label of the property
    std::string _task_key; //!< Key used to find the task column in the data file
    std::string _calc_type; //!< The type of LossFunction to use when projecting the features onto a property
    std::string _phi_out_file; //!< Filename of the file to output the feature set to

    std::shared_ptr<MPI_Interface> _mpi_comm; //!< The MPI communicator for the calculation

    double _cross_cor_max; //!< Maximum cross-correlation used for selecting features
    double _l_bound; //!< The lower bound for the maximum absolute value of the features
    double _u_bound; //!< The upper bound for the maximum absolute value of the features

    int _n_dim; //!< The maximum number of features allowed in the linear model
    int _max_rung; //!< Maximum rung for the feature creation
    int _n_rung_store; //!< The number of rungs to calculate and store the value of the features for all samples
    int _n_rung_generate; //!< Either 0 or 1, and is the number of rungs to generate on the fly during SIS
    int _n_sis_select; //!< Number of features to select during each SIS iteration
    int _n_samp; //!< Number of samples in the data set
    int _n_samp_train; //!< Number of samples in the training set
    int _n_samp_test; //!< Number of samples in the test set
    int _n_residual; //!< Number of residuals to pass to the next sis model
    int _n_models_store; //!< The number of models to output to files
    int _max_param_depth; //!< The maximum depth in the binary expression tree to set non-linear optimization
    int _nlopt_seed; //!< The seed used for the nlOpt library
    int _max_leaves; //!< The maximum number of primary features (with replacement) allowed in the features

    bool _fix_intercept; //!< If true the bias term is fixed at 0
    bool _data_file_relative_to_json; //!< If true then the data filepath is relative to the filename path
    bool _global_param_opt; //!< True if global optimization is requested for non-linear optimization of parameters (Can break reproducibility)
    bool _reparam_residual; //!< If True then reparameterize features using the residuals of each model
    bool _override_n_sis_select; //!< If true then n_sis_select can be changed if the feature set size is not large enough
    // clang-format on

public:
    // DocString: inputs_init_default
    /**
     * @brief Default constructor (Sets all values to the default valuse)
     * @details [long description]
     */
    InputParser();

    // DocString: inputs_init_filename
    /**
     * @brief Constructor of the InputParser
     *
     * @param fn filename for the input file
     */
    InputParser(std::string fn);

    /**
     * @brief Constructor of the InputParser
     *
     * @param IP Property tree generated from json file
     * @param fn filename for the input file
     * @param comm MPI communicator for the calculation
     */
    InputParser(pt::ptree ip, std::string fn, std::shared_ptr<MPI_Interface> comm);

    // DocString: inputs_init_copy
    /**
     * @brief Copy Constructor
     *
     * @param o InputParser to be copied
     */
    InputParser(const InputParser& o) = default;

    /**
     * @brief Move Constructor
     *
     * @param o InputParser to be moved
     */
    InputParser(InputParser&& o) = default;

    /**
     * @brief Copy Assignment operator
     *
     * @param o InputParser to be copied
     */
    InputParser& operator=(const InputParser& o) = default;

    /**
     * @brief Move Assignment operator
     *
     * @param o InputParser to be moved
     */
    InputParser& operator=(InputParser&& o) = default;

    /**
     * @brief Generate the feature space from input files and parameters
     *
     * @param headers column headers for all columns in the data file (expr of the Nodes)
     * @param units The units for the features
     * @param units The domains of the features
     * @param tasks map where keys are the task name and values are the number of samples in each task
     * @param taskind index in the columns that correspond the the task column
     */
    void generate_phi_0(std::vector<std::string> headers,
                        std::vector<Unit> units,
                        std::vector<Domain> domains,
                        std::map<std::string, std::vector<int>> tasks,
                        int taskind);

    // DocString: inputs_clear_data
    /**
     * @brief Clear the data of the features/training data for CV calculations
     */
    void clear_data();

    /**
     * @brief The MPI communicator for the calculation
     */
    inline std::shared_ptr<MPI_Interface> mpi_comm() { return _mpi_comm; }

    // DocString: inputs_sample_ids_train
    /**
     * @brief Vector storing all sample ids for the training samples
     */
    inline const std::vector<std::string>& sample_ids_train() const
    {
        if (_sample_ids_train.size() == 0)
        {
            throw std::logic_error("Accessing an unset member (_sample_ids_train).");
        }
        return _sample_ids_train;
    }

    /**
     * @brief Vector storing all sample ids for the training samples
     */
    inline std::vector<std::string> sample_ids_train_copy() const
    {
        if (_sample_ids_train.size() == 0)
        {
            throw std::logic_error("Accessing an unset member (_sample_ids_train).");
        }
        return _sample_ids_train;
    }

    /**
     * @brief Set Vector storing all sample ids for the training samples
     */
    void set_sample_ids_train(std::vector<std::string> sample_ids_train);

    // DocString: inputs_sample_ids_test
    /**
     * @brief Vector storing all sample ids for the test samples
     */
    inline const std::vector<std::string>& sample_ids_test() const
    {
        if ((_n_samp_test != 0) && (_sample_ids_test.size() != _n_samp_test))
        {
            throw std::logic_error("Accessing an unset member (_sample_ids_test).");
        }
        return _sample_ids_test;
    }

    /**
     * @brief Vector storing all sample ids for the test samples
     */
    inline std::vector<std::string> sample_ids_test_copy() const
    {
        if ((_n_samp_test != 0) && (_sample_ids_test.size() != _n_samp_test))
        {
            throw std::logic_error("Accessing an unset member (_sample_ids_test).");
        }
        return _sample_ids_test;
    }

    /**
     * @brief Set Vector storing all sample ids for the test samples
     */
    void set_sample_ids_test(std::vector<std::string> sample_ids_test);

    // DocString: inputs_task_names
    /**
     * @brief Vector storing the ID of the task names
     */
    inline const std::vector<std::string>& task_names() const
    {
        if (_task_names.size() == 0)
        {
            throw std::logic_error("Accessing an unset member (_task_names).");
        }
        return _task_names;
    }

    /**
     * @brief Vector storing the ID of the task names
     */
    inline std::vector<std::string> task_names_copy() const
    {
        if (_task_names.size() == 0)
        {
            throw std::logic_error("Accessing an unset member (_task_names).");
        }
        return _task_names;
    }

    /**
     * @brief Set Vector storing the ID of the task names
     */
    void set_task_names(std::vector<std::string> task_names);

    // DocString: inputs_allowed_param_ops
    /**
     * @brief Vector containing all allowed operators strings for operators with free parameters
     */
    inline const std::vector<std::string>& allowed_param_ops() const { return _allowed_param_ops; }

    /**
     * @brief Vector containing all allowed operators strings for operators with free parameters
     */
    inline std::vector<std::string> allowed_param_ops_copy() const { return _allowed_param_ops; }

    /**
     * @brief Set Vector containing all allowed operators strings for operators with free parameters
     */
    inline void set_allowed_param_ops(std::vector<std::string> allowed_param_ops)
    {
        _allowed_param_ops = allowed_param_ops;
    }

    // DocString: inputs_allowed_ops
    /**
     * @brief Vector containing all allowed operators strings
     */
    inline const std::vector<std::string>& allowed_ops() const { return _allowed_ops; }

    /**
     * @brief Vector containing all allowed operators strings
     */
    inline std::vector<std::string> allowed_ops_copy() const { return _allowed_ops; }

    /**
     * @brief Set Vector containing all allowed operators strings
     */
    inline void set_allowed_ops(std::vector<std::string> allowed_ops)
    {
        _allowed_ops = allowed_ops;
    }

    /**
     * @brief The value of the property to evaluate the loss function against for the training set
     */
    inline const std::vector<double>& prop_train() const
    {
        if (_prop_train.size() == 0)
        {
            throw std::logic_error("Accessing an unset member (_prop_train).");
        }
        return _prop_train;
    }

    /**
     * @brief The value of the property to evaluate the loss function against for the training set
     */
    inline std::vector<double> prop_train_copy() const
    {
        if (_prop_train.size() == 0)
        {
            throw std::logic_error("Accessing an unset member (_prop_train).");
        }
        return _prop_train;
    }

    /**
     * @brief Set The value of the property to evaluate the loss function against for the training set
     */
    void set_prop_train(std::vector<double> prop_train);

    /**
     * @brief The value of the property to evaluate the loss function against for the test set
     */
    inline const std::vector<double>& prop_test() const
    {
        if (_prop_test.size() != _n_samp_test)
        {
            throw std::logic_error("Accessing an unset member (_prop_test).");
        }
        return _prop_test;
    }

    /**
     * @brief The value of the property to evaluate the loss function against for the test set
     */
    inline std::vector<double> prop_test_copy() const
    {
        if (_prop_test.size() != _n_samp_test)
        {
            throw std::logic_error("Accessing an unset member (_prop_test).");
        }
        return _prop_test;
    }

    /**
     * @brief Set The value of the property to evaluate the loss function against for the test set
     */
    void set_prop_test(std::vector<double> prop_test);

    // DocString: inputs_leave_out_inds
    /**
     * @brief List of indexes from the initial data file in the test set
     */
    inline const std::vector<int>& leave_out_inds() const
    {
        if (_leave_out_inds.size() != _n_samp_test)
        {
            throw std::logic_error("Accessing an unset member (_leave_out_inds).");
        }
        return _leave_out_inds;
    }

    /**
     * @brief List of indexes from the initial data file in the test set
     */
    inline std::vector<int> leave_out_inds_copy() const
    {
        if (_leave_out_inds.size() != _n_samp_test)
        {
            throw std::logic_error("Accessing an unset member (_leave_out_inds).");
        }
        return _leave_out_inds;
    }

    /**
     * @brief Set List of indexes from the initial data file in the test set
     */
    void set_leave_out_inds(std::vector<int> leave_out_inds);

    // DocString: inputs_task_sizes_train
    /**
     * @brief Number of training samples per task
     */
    inline const std::vector<int>& task_sizes_train() const
    {
        if (_task_sizes_train.size() == 0)
        {
            throw std::logic_error("Accessing an unset member (_task_sizes_train).");
        }
        return _task_sizes_train;
    }

    /**
     * @brief Number of training samples per task
     */
    inline std::vector<int> task_sizes_train_copy() const
    {
        if (_task_sizes_train.size() == 0)
        {
            throw std::logic_error("Accessing an unset member (_task_sizes_train).");
        }
        return _task_sizes_train;
    }

    /**
     * @brief Set Number of training samples per task
     */
    void set_task_sizes_train(std::vector<int> task_sizes_train);

    // DocString: inputs_task_sizes_test
    /**
     * @brief Number of testing samples per task
     */
    inline const std::vector<int>& task_sizes_test() const
    {
        if (_task_sizes_test.size() == 0)
        {
            throw std::logic_error("Accessing an unset member (_task_sizes_test).");
        }
        return _task_sizes_test;
    }

    /**
     * @brief Number of testing samples per task
     */
    inline std::vector<int> task_sizes_test_copy() const
    {
        if (_task_sizes_test.size() == 0)
        {
            throw std::logic_error("Accessing an unset member (_task_sizes_test).");
        }
        return _task_sizes_test;
    }

    /**
     * @brief Set Number of testing samples per task
     */
    void set_task_sizes_test(std::vector<int> task_sizes_test);

    // DocString: inputs_phi_0
    /**
     * @brief A vector of FeatureNodes for the primary feature space
     */
    inline const std::vector<FeatureNode>& phi_0() const
    {
        if (_phi_0.size() == 0)
        {
            throw std::logic_error("Accessing an unset member (_phi_0).");
        }
        return _phi_0;
    }

    /**
     * @brief A vector of FeatureNodes for the primary feature space
     */
    inline std::vector<FeatureNode> phi_0_copy() const
    {
        if (_phi_0.size() == 0)
        {
            throw std::logic_error("Accessing an unset member (_phi_0).");
        }
        return _phi_0;
    }

    /**
     * @brief Return phi_0 as a vector of node_ptrs
     *
     */
    std::vector<node_ptr> phi_0_ptrs() const;

    /**
     * @brief Set A vector of FeatureNodes for the primary feature space
     */
    void set_phi_0(std::vector<node_ptr> phi_0);

    /**
     * @brief Set A vector of FeatureNodes for the primary feature space
     */
    void set_phi_0(std::vector<FeatureNode> phi_0);

    // DocString: inputs_get_prop_unit
    /**
     * @brief The Unit of the property
     */
    inline Unit prop_unit() const { return _prop_unit; }

    // DocString: inputs_set_prop_unit
    /**
     * @brief Set The Unit of the property
     */
    inline void set_prop_unit(Unit prop_unit) { _prop_unit = prop_unit; }

    // DocString: inputs_get_filename
    /**
     * @brief Name of the input file
     */
    inline std::string filename() const { return _filename; }

    // DocString: inputs_set_filename
    /**
     * @brief Set Name of the input file
     */
    inline void set_filename(const std::string filename) { _filename = filename; }

    // DocString: inputs_get_phi_out_file
    /**
     * @brief Filename of the file to output the feature set to
     */
    inline std::string phi_out_file() const { return _phi_out_file; }

    // DocString: inputs_set_phi_out_file
    /**
     * @brief Set tilename of the file to output the feature set to
     */
    inline void set_phi_out_file(const std::string phi_out_file) { _phi_out_file = phi_out_file; }

    // DocString: inputs_get_data_file
    /**
     * @brief Name of the data file
     */
    inline std::string data_file() const { return _data_file; }

    // DocString: inputs_set_data_file
    /**
     * @brief Set Name of the data file
     */
    inline void set_data_file(const std::string data_file) { _data_file = data_file; }

    // DocString: inputs_get_prop_key
    /**
     * @brief Key used to find the property column in the data file
     */
    inline std::string prop_key() const { return _prop_key; }

    // DocString: inputs_set_prop_key
    /**
     * @brief Set Key used to find the property column in the data file
     */
    inline void set_prop_key(const std::string prop_key) { _prop_key = prop_key; }

    // DocString: inputs_get_prop_label
    /**
     * @brief The label of the property
     */
    inline std::string prop_label() const { return _prop_label; }

    // DocString: inputs_set_prop_label
    /**
     * @brief Set The label of the property
     */
    inline void set_prop_label(std::string prop_label) { _prop_label = prop_label; }

    // DocString: inputs_get_task_key
    /**
     * @brief Key used to find the task column in the data file
     */
    inline std::string task_key() const { return _task_key; }

    // DocString: inputs_set_task_key
    /**
     * @brief Set Key used to find the task column in the data file
     */
    inline void set_task_key(const std::string task_key) { _task_key = task_key; }

    // DocString: inputs_get_calc_type
    /**
     * @brief The type of LossFunction to use when projecting the features onto a property
     */
    inline std::string calc_type() const { return _calc_type; }

    // DocString: inputs_set_calc_type
    /**
     * @brief Set The type of LossFunction to use when projecting the features onto a property
     */
    inline void set_calc_type(const std::string calc_type) { _calc_type = calc_type; }

    // DocString: inputs_get_cross_cor_max
    /**
     * @brief Maximum cross-correlation used for selecting features
     */
    inline double cross_cor_max() const { return _cross_cor_max; }

    // DocString: inputs_set_cross_cor_max
    /**
     * @brief Set Maximum cross-correlation used for selecting features
     */
    inline void set_cross_cor_max(double cross_cor_max) { _cross_cor_max = cross_cor_max; }

    // DocString: inputs_get_l_bound
    /**
     * @brief The lower bound for the maximum absolute value of the features
     */
    inline double l_bound() const { return _l_bound; }

    // DocString: inputs_set_l_bound
    /**
     * @brief Set The lower bound for the maximum absolute value of the features
     */
    inline void set_l_bound(double l_bound)
    {
        if (l_bound > _u_bound)
        {
            throw std::logic_error("The new lower bound is larger than the current upper bound");
            ;
        }
        _l_bound = l_bound;
    }

    // DocString: inputs_get_u_bound
    /**
     * @brief The upper bound for the maximum absolute value of the features
     */
    inline double u_bound() const { return _u_bound; }

    // DocString: inputs_set_u_bound
    /**
     * @brief Set The upper bound for the maximum absolute value of the features
     */
    inline void set_u_bound(double u_bound)
    {
        if (u_bound < _l_bound)
        {
            throw std::logic_error("The new upper bound is smaller than the current lower bound");
            ;
        }
        _u_bound = u_bound;
    }

    // DocString: inputs_get_n_dim
    /**
     * @brief The maximum number of features allowed in the linear model
     */
    inline int n_dim() const { return _n_dim; }

    // DocString: inputs_set_n_dim
    /**
     * @brief Set The maximum number of features allowed in the linear model
     */
    inline void set_n_dim(const int n_dim) { _n_dim = n_dim; }

    // DocString: inputs_get_max_rung
    /**
     * @brief Maximum rung for the feature creation
     */
    inline int max_rung() const { return _max_rung; }

    // DocString: inputs_set_max_rung
    /**
     * @brief Set Maximum rung for the feature creation
     */
    inline void set_max_rung(const int max_rung)
    {
        _max_rung = max_rung;
        node_value_arrs::set_max_rung(max_rung, _allowed_param_ops.size() > 0);
    }

    // DocString: inputs_get_n_rung_store
    /**
     * @brief The number of rungs to calculate and store the value of the features for all samples
     */
    inline int n_rung_store() const { return _n_rung_store; }

    // DocString: inputs_set_n_rung_store
    /**
     * @brief Set The number of rungs to calculate and store the value of the features for all samples
     */
    inline void set_n_rung_store(const int n_rung_store) { _n_rung_store = n_rung_store; }

    // DocString: inputs_get_n_rung_generate
    /**
     * @brief Either 0 or 1, and is the number of rungs to generate on the fly during SIS
     */
    inline int n_rung_generate() const { return _n_rung_generate; }

    // DocString: inputs_set_n_rung_generate
    /**
     * @brief Set Either 0 or 1, and is the number of rungs to generate on the fly during SIS
     */
    inline void set_n_rung_generate(const int n_rung_generate)
    {
        _n_rung_generate = n_rung_generate;
    }

    // DocString: inputs_get_n_sis_select
    /**
     * @brief Number of features to select during each SIS iteration
     */
    inline int n_sis_select() const { return _n_sis_select; }

    // DocString: inputs_set_n_sis_select
    /**
     * @brief Set Number of features to select during each SIS iteration
     */
    inline void set_n_sis_select(const int n_sis_select) { _n_sis_select = n_sis_select; }

    // DocString: inputs_get_n_samp
    /**
     * @brief Number of samples in the data set
     */
    inline int n_samp() const { return _n_samp; }

    // DocString: inputs_get_n_samp_train
    /**
     * @brief Number of samples in the training set
     */
    inline int n_samp_train() const { return _n_samp_train; }

    // DocString: inputs_get_n_samp_test
    /**
     * @brief Number of samples in the test set
     */
    inline int n_samp_test() const { return _n_samp_test; }

    // DocString: inputs_get_n_residual
    /**
     * @brief Number of residuals to pass to the next sis model
     */
    inline int n_residual() const { return _n_residual; }

    // DocString: inputs_set_n_residual
    /**
     * @brief Set Number of residuals to pass to the next sis model
     */
    inline void set_n_residual(const int n_residual) { _n_residual = n_residual; }

    // DocString: inputs_get_n_models_store
    /**
     * @brief The number of models to output to files
     */
    inline int n_models_store() const { return _n_models_store; }

    // DocString: inputs_set_n_models_store
    /**
     * @brief Set The number of models to output to files
     */
    inline void set_n_models_store(const int n_models_store) { _n_models_store = n_models_store; }

    // DocString: inputs_get_max_param_depth
    /**
     * @brief The maximum depth in the binary expression tree to set non-linear optimization
     */
    inline int max_param_depth() const { return _max_param_depth; }

    // DocString: inputs_set_max_param_depth
    /**
     * @brief Set The maximum depth in the binary expression tree to set non-linear optimization
     */
    inline void set_max_param_depth(const int max_param_depth)
    {
#ifdef PARAMETERIZE
        nlopt_wrapper::MAX_PARAM_DEPTH = max_param_depth;
#endif
        _max_param_depth = max_param_depth;
    }

    // DocString: inputs_get_nlopt_seed
    /**
     * @brief The seed used for the nlOpt library
     */
    inline int nlopt_seed() const { return _nlopt_seed; }

    // DocString: inputs_set_nlopt_seed
    /**
     * @brief Set The seed used for the nlOpt library
     */
    inline void set_nlopt_seed(const int nlopt_seed) { _nlopt_seed = nlopt_seed; }

    // DocString: inputs_get_fix_intercept
    /**
     * @brief If true the bias term is fixed at 0
     */
    inline bool fix_intercept() const { return _fix_intercept; }

    // DocString: inputs_set_fix_intercept
    /**
     * @brief Set If true the bias term is fixed at 0
     */
    inline void set_fix_intercept(const bool fix_intercept) { _fix_intercept = fix_intercept; }

    // DocString: inputs_get_global_param_opt
    /**
     * @brief True if global optimization is requested for non-linear optimization of parameters (Can break reproducibility)
     */
    inline bool global_param_opt() const { return _global_param_opt; }

    // DocString: inputs_set_global_param_opt
    /**
     * @brief Set True if global optimization is requested for non-linear optimization of parameters (Can break reproducibility)
     */
    inline void set_global_param_opt(const bool global_param_opt)
    {
        _global_param_opt = global_param_opt;
    }

    // DocString: inputs_get_reparam_residual
    /**
     * @brief If True then reparameterize features using the residuals of each model
     */
    inline bool reparam_residual() const { return _reparam_residual; }

    // DocString: inputs_set_reparam_residual
    /**
     * @brief Set If True then reparameterize features using the residuals of each model
     */
    inline void set_reparam_residual(const bool reparam_residual)
    {
        _reparam_residual = reparam_residual;
    }

    // DocString: inputs_max_leaves
    /**
     * @brief The maximum number of primary features (with replacement) allowed in the features
     */
    inline int max_leaves() const { return _max_leaves; }

    // DocString: inputs_set_max_leaves
    /**
     * @brief Set the maximum number of primary features (with replacement) allowed in the features
     */
    inline void set_max_leaves(const int max_leaves) { _max_leaves = max_leaves; }

    // DocString: inputs_override_n_sis_select
    /**
     * @brief If True then allow sis to override n_sis_select if not enough features present
     */
    inline bool override_n_sis_select() const { return _override_n_sis_select; }

    // DocString: inputs_set_override_n_sis_select
    /**
     * @brief Set If True then allow sis to override n_sis_select if not enough features present
     */
    inline void set_override_n_sis_select(const bool override_n_sis_select)
    {
        _override_n_sis_select = override_n_sis_select;
    }
#ifdef PY_BINDINGS
    // DocString: inputs_get_prop_train_arr
    /**
     * @brief The value of the property to evaluate the loss function against for the training set
     */
    inline py::array_t<double> prop_train_arr() const { return py::cast(prop_train_copy()); }

    // DocString: inputs_set_prop_train_arr
    /**
     * @brief Set The value of the property to evaluate the loss function against for the training set
     */
    inline void set_prop_train_arr(py::array_t<double> prop_train)
    {
        set_prop_train(python_conv_utils::from_array<double>(prop_train));
    }

    // DocString: inputs_get_prop_test_arr
    /**
     * @brief The value of the property to evaluate the loss function against for the test set
     */
    inline py::array_t<double> prop_test_arr() const { return py::cast(prop_test_copy()); }

    // DocString: inputs_set_prop_test_arr
    /**
     * @brief Set The value of the property to evaluate the loss function against for the test set
     */
    inline void set_prop_test_arr(py::array_t<double> prop_test)
    {
        set_prop_test(python_conv_utils::from_array<double>(prop_test));
    }
#endif
};
/**
 * @brief      strips comments from the input file
 *
 * @param      filename  The filename of the file to strip
 */
void strip_comments(std::string& filename);

/**
 * @brief      boost json to std::vector<T>
 *
 * @param[in]  pt          property tree
 * @param[in]  key         property tree key
 *
 * @tparam     T           double, int, std::string
 *
 * @return     json input as a std::vector<T>
 */
template <typename T>
std::vector<T> as_vector(pt::ptree const& pt, pt::ptree::key_type const& key)
{
    std::vector<T> r;
    if (pt.count(key) == 0) return r;

    for (auto& item : pt.get_child(key)) r.push_back(item.second.get_value<T>());
    return r;
}

/**
 * @brief      boost json to std::vector<T>
 *
 * @param[in]  pt          property tree
 *
 * @tparam     T           double, int, std::string
 *
 * @return     json input as a std::vector<T>
 */
template <typename T>
std::vector<T> as_vector(pt::ptree const& pt)
{
    std::vector<T> r;
    for (auto& item : pt) r.push_back(item.second.get_value<T>());
    return r;
}

/**
 * @brief Get a boost property tree from a json file
 *
 * @param fn The input json file
 * @param mpi_comm The MPI communicator for the calculation
 *
 * @return The property tree representation of the json file
 */
pt::ptree get_prop_tree(std::string fn, std::shared_ptr<MPI_Interface>& mpi_comm);

#endif
