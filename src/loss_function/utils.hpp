// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file loss_function/utils.hpp
 *  @brief Defines utilities for generating and copying LossFunctions
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef LOSS_FUNCTION_UTILS
#define LOSS_FUNCTION_UTILS

#include "loss_function/LossFunctionConvexHull.hpp"
#include "loss_function/LossFunctionLogPearsonRMSE.hpp"

namespace loss_function_util
{
/**
 * Create a shared_ptr to a new LossFunction
 *
 * @param type The type of LossFunction to create
 * @param prop_train The value of the property to evaluate the loss function against for the training set
 * @param prop_test The value of the property to evaluate the loss function against for the test set
 * @param task_sizes_train Number of training samples per task
 * @param task_sizes_test Number of testing samples per task
 * @param fix_intercept If true then the bias term is fixed at 0
 * @return A shared_ptr to the newly created LossFunction
 */
std::shared_ptr<LossFunction> get_loss_function(std::string type,
                                                std::vector<double> prop_train,
                                                std::vector<double> prop_test,
                                                std::vector<int> task_sizes_train,
                                                std::vector<int> task_sizes_test,
                                                bool fix_intercept = false);

/**
 * @brief Make a copy of a LossFunction and return a shared_ptr to it
 *
 * @param loss A shared_ptr to the LossFunction to be copied
 * @return The shared_ptr to the copied LossFunction
 */
std::shared_ptr<LossFunction> copy(std::shared_ptr<LossFunction> loss);

};  // namespace loss_function_util

#endif
