// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file loss_function/LossFunctionPearsonRMSE.cpp
 *  @brief Implements the class that uses a Pearson correlation projection operator and a least-squares regression objective function
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "loss_function/LossFunctionPearsonRMSE.hpp"

LossFunctionPearsonRMSE::LossFunctionPearsonRMSE(std::vector<double> prop_train,
                                                 std::vector<double> prop_test,
                                                 std::vector<int> task_sizes_train,
                                                 std::vector<int> task_sizes_test,
                                                 bool fix_intercept,
                                                 int n_feat)
    : LossFunction(prop_train, prop_test, task_sizes_train, task_sizes_test, fix_intercept, n_feat),
      _a(_n_dim * _n_samp, 1.0),
      _b(prop_train),
      _c(_n_task, 0.0),
      _lwork(0)
{
    set_nfeat(_n_feat);
    set_opt_lwork();
    prepare_project();
}

LossFunctionPearsonRMSE::LossFunctionPearsonRMSE(std::shared_ptr<LossFunction> o)
    : LossFunction(o), _a(_n_dim * _n_samp, 1.0), _b(_prop_train), _c(_n_task, 0.0), _lwork(0)
{
    set_nfeat(_n_feat);
    set_opt_lwork();
    prepare_project();
}

void LossFunctionPearsonRMSE::set_nfeat(int n_feat)
{
    _n_feat = n_feat;
    _n_dim = n_feat + (!_fix_intercept);

    _a.resize(_n_samp * _n_dim);
    _c.resize(_n_project_prop * _n_task, 0.0);
    _coefs.resize(_n_dim * _n_task);
    _scores.resize(_n_project_prop);

    set_opt_lwork();
}

void LossFunctionPearsonRMSE::reset_projection_prop(
    const std::vector<std::vector<model_node_ptr>>& models)
{
    _n_project_prop = models.size();
    _projection_prop.resize(_n_samp * _n_project_prop);
    _projection_prop_std.resize(_n_samp * _n_project_prop);
    for (int mm = 0; mm < _n_project_prop; ++mm)
    {
        double loss = (*this)(models[mm]);
        std::copy_n(_error_train.data(), _error_train.size(), &_projection_prop[mm * _n_samp]);
    }

    set_nfeat(models.back().size() + 1);
    prepare_project();
}

void LossFunctionPearsonRMSE::set_opt_lwork()
{
    std::vector<double> work(1, 0.0);
    int info = 0, rank = 0;

    dgels_('N', _n_samp, _n_dim, 1, _a.data(), _n_samp, _b.data(), _n_samp, work.data(), -1, &info);

    if (info == 0)
    {
        _lwork = static_cast<int>(std::round(work[0]));
        _work.resize(_lwork, 0.0);
    }
    else
    {
        throw std::logic_error("Failed to get lwork.");
    }
}

void LossFunctionPearsonRMSE::prepare_project()
{
    for (int pp = 0; pp < _n_project_prop; ++pp)
    {
        int start = 0;
        for (int tt = 0; tt < _n_task; ++tt)
        {
            double prop_mean = util_funcs::mean(_projection_prop.data() + pp * _n_samp + start,
                                                _task_sizes_train[tt]);

            double prop_std = util_funcs::stand_dev(_projection_prop.data() + pp * _n_samp + start,
                                                    _task_sizes_train[tt]);
            std::transform(
                _projection_prop.begin() + pp * _n_samp + start,
                _projection_prop.begin() + pp * _n_samp + start + _task_sizes_train[tt],
                _projection_prop_std.begin() + pp * _task_sizes_train[tt] + start * _n_project_prop,
                [prop_mean, prop_std](double p) { return (p - prop_mean) / prop_std; });
            start += _task_sizes_train[tt];
        }
    }
}
double LossFunctionPearsonRMSE::project(const node_ptr& feat)
{
    std::fill_n(_scores.begin(), _scores.size(), 0.0);
    return calc_max_pearson(feat->value_ptr(-1, true));
}

double LossFunctionPearsonRMSE::calc_max_pearson(double* feat_val_ptr)
{
    int start = 0;
    for (int tt = 0; tt < _task_sizes_train.size(); ++tt)
    {
        double feat_std = util_funcs::stand_dev(feat_val_ptr + start, _task_sizes_train[tt]);
        dgemm_('T',
               'N',
               _n_project_prop,
               1,
               _task_sizes_train[tt],
               1.0 / feat_std / static_cast<double>(_task_sizes_train[tt]),
               &_projection_prop_std[start * _n_project_prop],
               _task_sizes_train[tt],
               feat_val_ptr + start,
               _task_sizes_train[tt],
               0.0,
               &_c[tt * _n_project_prop],
               _n_project_prop);
        std::transform(_scores.begin(),
                       _scores.end(),
                       &_c[tt * _n_project_prop],
                       _scores.begin(),
                       [](double score, double cc) { return score + cc * cc; });

        start += _task_sizes_train[tt];
    }
    return *std::max_element(_scores.begin(), _scores.end()) / static_cast<double>(-1 * _n_task);
}

double LossFunctionPearsonRMSE::operator()(const std::vector<int>& inds)
{
    std::copy_n(_prop_train.data(), _n_samp, _b.data());
    std::fill_n(_a.begin(), _a.size(), 1.0);
    int start = 0;
    int tt = 0;
    int info = 0;

    while ((tt < _n_task) && (info == 0))
    {
        set_a(inds, tt, start);
        info = least_squares(tt, start);
        set_prop_train_est(inds, tt, start);

        start += _task_sizes_train[tt];
        ++tt;
    }

    if (info != 0)
    {
        return -1.0 + info / std::abs(info) * 0.5;
    }

    std::transform(_prop_train.begin(),
                   _prop_train.end(),
                   _prop_train_est.begin(),
                   _error_train.begin(),
                   [](double p, double p_est) { return p - p_est; });

    return std::sqrt(
        1.0 / static_cast<double>(_n_samp) *
        std::accumulate(_error_train.begin(), _error_train.end(), 0.0, [](double total, double te) {
            return total + te * te;
        }));
}

double LossFunctionPearsonRMSE::operator()(const std::vector<model_node_ptr>& feats)
{
    std::copy_n(_prop_train.data(), _n_samp, _b.data());
    std::fill_n(_a.begin(), _a.size(), 1.0);
    int start = 0;
    int tt = 0;
    int info = 0;
    while ((tt < _n_task) && (info == 0))
    {
        set_a(feats, tt, start);
        info = least_squares(tt, start);
        set_prop_train_est(feats, tt, start);

        start += _task_sizes_train[tt];
        ++tt;
    }

    if (info != 0)
    {
        return -1.0 + info / std::abs(info) * 0.5;
    }

    std::transform(_prop_train.begin(),
                   _prop_train.end(),
                   _prop_train_est.begin(),
                   _error_train.begin(),
                   [](double p, double p_est) { return p - p_est; });

    return std::sqrt(
        1.0 / static_cast<double>(_n_samp) *
        std::accumulate(_error_train.begin(), _error_train.end(), 0.0, [](double total, double te) {
            return total + te * te;
        }));
}

double LossFunctionPearsonRMSE::test_loss(const std::vector<model_node_ptr>& feats)
{
    double train_rmse = (*this)(feats);
    if (train_rmse < 0.0)
    {
        throw std::logic_error("Failed to solve least squares problem.");
    }

    int start = 0;
    for (int tt = 0; tt < _task_sizes_test.size(); ++tt)
    {
        set_prop_test_est(feats, tt, start);
        start += _task_sizes_test[tt];
    }

    std::transform(_prop_test.begin(),
                   _prop_test.end(),
                   _prop_test_est.begin(),
                   _error_test.begin(),
                   [](double p, double p_est) { return p - p_est; });

    return std::sqrt(
        1.0 / static_cast<double>(_n_samp_test) *
        std::accumulate(_error_test.begin(), _error_test.end(), 0.0, [](double total, double te) {
            return total + te * te;
        }));
}

void LossFunctionPearsonRMSE::set_a(const std::vector<int>& inds, int taskind, int start)
{
    for (int ff = 0; ff < inds.size(); ++ff)
    {
        std::copy_n(node_value_arrs::get_d_matrix_ptr(inds[ff]) + start,
                    _task_sizes_train[taskind],
                    &_a[ff * _task_sizes_train[taskind] + start * _n_dim]);
    }
}

void LossFunctionPearsonRMSE::set_a(const std::vector<model_node_ptr>& feats,
                                    int taskind,
                                    int start)
{
    for (int ff = 0; ff < feats.size(); ++ff)
    {
        std::copy_n(feats[ff]->value_ptr() + start,
                    _task_sizes_train[taskind],
                    &_a[ff * _task_sizes_train[taskind] + start * _n_dim]);
    }
}

int LossFunctionPearsonRMSE::least_squares(int taskind, int start)
{
    int info;
    dgels_('N',
           _task_sizes_train[taskind],
           _n_dim,
           1,
           &_a[start * _n_dim],
           _task_sizes_train[taskind],
           &_b[start],
           _task_sizes_train[taskind],
           _work.data(),
           _lwork,
           &info);
    std::copy_n(&_b[start], _n_dim, &_coefs[taskind * _n_dim]);
    return info;
}

void LossFunctionPearsonRMSE::set_prop_train_est(const std::vector<int>& inds,
                                                 int taskind,
                                                 int start)
{
    std::fill_n(_prop_train_est.begin() + start,
                _task_sizes_train[taskind],
                (!_fix_intercept) * _coefs[(taskind + 1) * _n_dim - 1]);

    for (int ff = 0; ff < inds.size(); ++ff)
    {
        daxpy_(_task_sizes_train[taskind],
               _coefs[taskind * _n_dim + ff],
               node_value_arrs::get_d_matrix_ptr(inds[ff]) + start,
               1,
               &_prop_train_est[start],
               1);
    }
}

void LossFunctionPearsonRMSE::set_prop_train_est(const std::vector<model_node_ptr>& feats,
                                                 int taskind,
                                                 int start)
{
    std::fill_n(_prop_train_est.begin() + start,
                _task_sizes_train[taskind],
                (!_fix_intercept) * _coefs[(taskind + 1) * _n_dim - 1]);

    for (int ff = 0; ff < feats.size(); ++ff)
    {
        daxpy_(_task_sizes_train[taskind],
               _coefs[taskind * _n_dim + ff],
               feats[ff]->value_ptr() + start,
               1,
               &_prop_train_est[start],
               1);
    }
}

void LossFunctionPearsonRMSE::set_prop_test_est(const std::vector<model_node_ptr>& feats,
                                                int taskind,
                                                int start)
{
    std::fill_n(_prop_test_est.begin() + start,
                _task_sizes_test[taskind],
                (!_fix_intercept) * _coefs[(taskind + 1) * _n_dim - 1]);

    for (int ff = 0; ff < feats.size(); ++ff)
    {
        daxpy_(_task_sizes_test[taskind],
               _coefs[taskind * _n_dim + ff],
               feats[ff]->test_value_ptr() + start,
               1,
               &_prop_test_est[start],
               1);
    }
}
