// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file loss_function/LossFunction.cpp
 *  @brief Implements a base class used to calculate the projection score and l0-regularization objective function
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "loss_function/LossFunction.hpp"

LossFunction::LossFunction(std::vector<double> prop_train,
                           std::vector<double> prop_test,
                           std::vector<int> task_sizes_train,
                           std::vector<int> task_sizes_test,
                           bool fix_intercept,
                           int n_feat)
    : _projection_prop(prop_train),
      _projection_prop_std(prop_train),
      _prop_train(prop_train),
      _prop_test(prop_test),
      _prop_train_est(prop_train.size(), 0.0),
      _prop_test_est(prop_test.size(), 0.0),
      _error_train(prop_train.size(), 0.0),
      _error_test(prop_test.size(), 0.0),
      _task_sizes_train(task_sizes_train),
      _task_sizes_test(task_sizes_test),
      _n_samp(std::accumulate(task_sizes_train.begin(), task_sizes_train.end(), 0)),
      _n_samp_test(std::accumulate(task_sizes_test.begin(), task_sizes_test.end(), 0)),
      _n_task(task_sizes_train.size()),
      _n_project_prop(_projection_prop.size() / _n_samp),
      _n_feat(n_feat),
      _n_dim(n_feat + (!fix_intercept)),
      _fix_intercept(fix_intercept)
{
    set_nfeat(_n_feat);
}

LossFunction::LossFunction(std::shared_ptr<LossFunction> o)
    : _projection_prop(o->prop_project_copy()),
      _projection_prop_std(o->prop_project_std_copy()),
      _prop_train(o->prop_train_copy()),
      _prop_test(o->prop_test_copy()),
      _prop_train_est(o->prop_train_est_copy()),
      _prop_test_est(o->prop_test_est_copy()),
      _error_train(o->error_train_copy()),
      _error_test(o->error_test_copy()),
      _coefs(o->coefs()),
      _task_sizes_train(o->task_sizes_train()),
      _task_sizes_test(o->task_sizes_test()),
      _n_samp(o->n_samp()),
      _n_samp_test(o->n_samp_test()),
      _n_task(o->n_task()),
      _n_project_prop(o->prop_project_copy().size() / o->n_samp()),
      _n_feat(o->n_feat()),
      _n_dim(o->n_dim()),
      _fix_intercept(o->fix_intercept())
{
}
