// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file loss_function/utils.cpp
 *  @brief Implements utilities for generating and copying LossFunctions
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "loss_function/utils.hpp"
std::shared_ptr<LossFunction> loss_function_util::get_loss_function(
    std::string type,
    std::vector<double> prop_train,
    std::vector<double> prop_test,
    std::vector<int> task_sizes_train,
    std::vector<int> task_sizes_test,
    bool fix_intercept)
{
    if (type == "regression")
    {
        return std::make_shared<LossFunctionPearsonRMSE>(
            prop_train, prop_test, task_sizes_train, task_sizes_test, fix_intercept);
    }
    else if (type == "log_regression")
    {
        return std::make_shared<LossFunctionLogPearsonRMSE>(
            prop_train, prop_test, task_sizes_train, task_sizes_test, fix_intercept);
    }
    else if (type == "classification")
    {
        return std::make_shared<LossFunctionConvexHull>(
            prop_train, prop_test, task_sizes_train, task_sizes_test);
    }

    throw std::logic_error("LossFunction type was not defined.");
    return nullptr;
}

std::shared_ptr<LossFunction> loss_function_util::copy(std::shared_ptr<LossFunction> loss)
{
    if (loss->type() == LOSS_TYPE::PEARSON_RMSE)
    {
        return std::make_shared<LossFunctionPearsonRMSE>(loss);
    }
    else if (loss->type() == LOSS_TYPE::LOG_PEARSON_RMSE)
    {
        return std::make_shared<LossFunctionLogPearsonRMSE>(loss);
    }
    else if (loss->type() == LOSS_TYPE::CONVEX_HULL)
    {
        return std::make_shared<LossFunctionConvexHull>(loss);
    }
    throw std::logic_error("LossFunction type was not defined in the copy function.");
    return nullptr;
}
