// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file nl_opt/NLOptimizerRegression.hpp
 *  @brief Defines a class to perform non-linear optimization for regression problems
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef NL_OPTIMIZER_REGRESSION
#define NL_OPTIMIZER_REGRESSION

#include "nl_opt/NLOptimizer.hpp"

// DocString: cls_nlopt_reg
/**
 * @brief The optimizer used for regression problems (inherits from NLOptimizer)
 *
 */
class NLOptimizerRegression : public NLOptimizer
{
protected:
    // clang-format off
    std::vector<double> _feature_gradient; //!< vector used to calculate the contribution of feature derivatives to the gradient
    std::vector<double> _residuals; //!< storage space for the residuals
    const double _cauchy_scaling; //!< Scaling factor for calculating the cauchy loss function
    // clang-format on
public:
    /**
     * @brief Constructor
     *
     * @param task_sizes Number of training samples per task
     * @param prop The value of the property to evaluate the loss function against for the training set
     * @param n_rung Maximum rung of the features
     * @param objective The objective function to use for the optimization
     * @param max_param_depth The maximum depth in the binary expression tree to set non-linear optimization
     * @param cauchy_scaling The Cauchy scale factor used for calculating the residuals
     * @param log_reg True if the optimizer is for a log regressor
     * @param reset_max_param_depth If true reset the maximum parameter depth
     * @param local_opt_alg Algorithm used for local optimization
     */
    NLOptimizerRegression(const std::vector<int>& task_sizes,
                          const std::vector<double>& prop,
                          const int n_rung,
                          int max_param_depth = -1,
                          const double cauchy_scaling = 0.5,
                          const bool log_reg = false,
                          bool reset_max_param_depth = false,
                          const nlopt::algorithm local_opt_alg = nlopt::LN_SBPLX);

    /**
     * @brief Access one of the convex hull objects for projection (If using the Classifier)
     *
     * @param ind The index of the _convex_hull vector to access
     */
    inline std::shared_ptr<ConvexHull1D> convex_hull(int ind) const { return nullptr; }

    /**
     * @brief Access an element of parameter gradient for non-linear optimization
     *
     * @param ind The index of the gradient vector to access
     */
    inline double* feature_gradient(int ind) { return &_feature_gradient[ind]; }

    /**
     * @brief Access an element of the residuals for non-linear optimization
     *
     * @param ind The index of the gradient vector to access
     */
    inline double* residuals(int ind) { return &_residuals[ind]; }

    /**
     * @brief The Cauchy scaling factor for regression problems
     */
    inline double cauchy_scaling() const { return _cauchy_scaling; }

    /**
     * @brief Return the type of descriptor identifier that is used
     */
    virtual inline DI_TYPE type() { return DI_TYPE::REG; }
};

#endif
