// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file nl_opt/NLOptimizerClassification.cpp
 *  @brief Defines a class to perform non-linear optimization for classification problems
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "nl_opt/NLOptimizerClassification.hpp"

NLOptimizerClassification::NLOptimizerClassification(const std::vector<int>& task_sizes,
                                                     const std::vector<double>& prop,
                                                     const int n_rung,
                                                     int max_param_depth,
                                                     bool reset_max_param_depth,
                                                     const nlopt::algorithm local_opt_alg)
    : NLOptimizer(task_sizes,
                  prop,
                  n_rung,
                  nlopt_objectives::classification,
                  max_param_depth,
                  reset_max_param_depth,
                  local_opt_alg),
      _convex_hull(_n_prop)
{
    for (int pp = 0; pp < _n_prop; ++pp)
    {
        _convex_hull[pp] = std::make_shared<ConvexHull1D>(task_sizes, &_prop[_n_samp * pp]);
    }
}
