// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file nl_opt/NLOptimizerLogRegression.hpp
 *  @brief Defines a class to perform non-linear optimization for log regression problems
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef NL_OPTIMIZER_LOG_REGRESSION
#define NL_OPTIMIZER_LOG_REGRESSION

#include "nl_opt/NLOptimizerRegression.hpp"

// DocString: cls_nlopt_log_reg
/**
 * @brief The optimizer used for log regression problems (inherits from NLOptimizerRegression)
 *
 */
class NLOptimizerLogRegression : public NLOptimizerRegression
{
public:
    /**
     * @brief Constructor
     *
     * @param task_sizes Number of training samples per task
     * @param prop The value of the property to evaluate the loss function against for the training set
     * @param n_rung Maximum rung of the features
     * @param max_param_depth The maximum depth in the binary expression tree to set non-linear optimization
     * @param cauchy_scaling The Cauchy scale factor used for calculating the residuals
     * @param reset_max_param_depth If true reset the maximum parameter depth
     * @param local_opt_alg Algorithm used for local optimization
     */
    NLOptimizerLogRegression(const std::vector<int>& task_sizes,
                             const std::vector<double>& prop,
                             const int n_rung,
                             int max_param_depth = -1,
                             const double cauchy_scaling = 0.5,
                             bool reset_max_param_depth = false,
                             const nlopt::algorithm local_opt_alg = nlopt::LN_SBPLX);

    /**
     * @brief Return the type of descriptor identifier that is used
     */
    inline DI_TYPE type() { return DI_TYPE::LOG_REG; }
};

#endif
