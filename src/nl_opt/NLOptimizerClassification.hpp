// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file nl_opt/NLOptimizerClassification.hpp
 *  @brief Defines a class to perform non-linear optimization for classification problems
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef NL_OPTIMIZER_CLASSIFICATION
#define NL_OPTIMIZER_CLASSIFICATION

#include "nl_opt/NLOptimizer.hpp"

// DocString: cls_nlopt_class
/**
 * @brief The optimizer used for classification problems (inherits from NLOptimizer)
 *
 */
class NLOptimizerClassification : public NLOptimizer
{
protected:
    // clang-format off
    std::vector<std::shared_ptr<ConvexHull1D>> _convex_hull; //!< A vector containing the convex hull objects for projection (If using the Classifier)
    // clang-format on

public:
    /**
     * @brief Constructor
     *
     * @param task_sizes Number of training samples per task
     * @param prop The value of the property to evaluate the loss function against for the training set
     * @param n_rung Maximum rung of the features
     * @param max_param_depth The maximum depth in the binary expression tree to set non-linear optimization
     * @param reset_max_param_depth If true reset the maximum parameter depth
     * @param local_opt_alg Algorithm used for local optimization
     */
    NLOptimizerClassification(const std::vector<int>& task_sizes,
                              const std::vector<double>& prop,
                              const int n_rung,
                              int max_param_depth = -1,
                              bool reset_max_param_depth = false,
                              const nlopt::algorithm local_opt_alg = nlopt::LN_SBPLX);

    /**
     * @brief Access one of the convex hull objects for projection (If using the Classifier)
     *
     * @param ind The index of the _convex_hull vector to access
     */
    inline std::shared_ptr<ConvexHull1D> convex_hull(int ind) const { return _convex_hull[ind]; }

    /**
     * @brief Access an element of parameter gradient for non-linear optimization
     *
     * @param ind The index of the gradient vector to access
     */
    inline double* feature_gradient(int ind) { return nullptr; }

    /**
     * @brief Access an element of the residuals for non-linear optimization
     *
     * @param ind The index of the gradient vector to access
     */
    inline double* residuals(int ind) { return nullptr; }

    /**
     * @brief The Cauchy scaling factor for regression problems
     */
    inline double cauchy_scaling() const { return 0.0; }

    /**
     * @brief Return the type of descriptor identifier that is used
     */
    inline DI_TYPE type() { return DI_TYPE::CLASS; }
};

#endif
