// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file nl_opt/utils.hpp
 *  @brief Defines utilities to get optimizers
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */
#ifndef NL_OPTIMIZER_UTILS
#define NL_OPTIMIZER_UTILS

#include "nl_opt/NLOptimizerClassification.hpp"
#include "nl_opt/NLOptimizerLogRegression.hpp"

namespace nlopt_wrapper
{
/**
 * @brief Get an optimizer for the desired task
 *
 * @param project_type The type of projection operator to optimize the features for
 * @param task_sizes Number of training samples per task
 * @param prop The value of the property to evaluate the loss function against for the training set
 * @param n_rung Maximum rung of the features
 * @param max_param_depth The maximum depth in the binary expression tree to set non-linear optimization
 * @param cauchy_scaling The Cauchy scale factor used for calculating the residuals
 * @param max_param_depth The maximum depth in the binary expression tree to set non-linear optimization
 *
 * @return The correct optimizer
 */
std::shared_ptr<NLOptimizer> get_optimizer(std::string project_type,
                                           const std::vector<int>& task_sizes,
                                           const std::vector<double>& prop,
                                           const int n_rung,
                                           int max_param_depth = -1,
                                           const double cauchy_scaling = 0.5,
                                           bool reset_max_param_depth = false);

#ifdef PY_BINDINGS

// DocString: nlopt_wrapper_set_max_depth
/**
 * @brief Set the maximum parameter depth
 *
 * @param depth The new maximum paramter depth
 */
inline void set_max_param_depth(int depth) { MAX_PARAM_DEPTH = depth; }

// DocString: nlopt_wrapper_get_reg_optimizer_list_list
/**
 * @brief Get an optimizer for the desired task
 *
 * @param task_sizes Number of training samples per task
 * @param prop The value of the property to evaluate the loss function against for the training set
 * @param n_rung Maximum rung of the features
 * @param max_param_depth The maximum depth in the binary expression tree to set non-linear optimization
 * @param cauchy_scaling The Cauchy scale factor used for calculating the residuals
 * @param use_global True if global optimization is requested for non-linear optimization of parameters (Can break reproducibility)
 *
 * @return The correct optimizer
 */
inline NLOptimizerRegression get_reg_optimizer(std::vector<int> task_sizes,
                                               std::vector<double> prop,
                                               int n_rung,
                                               int max_param_depth = -1,
                                               double cauchy_scaling = 0.5,
                                               bool use_global = false)
{
    USE_GLOBAL = use_global;
    return NLOptimizerRegression(
        task_sizes, prop, n_rung, max_param_depth, cauchy_scaling, false, true);
}

// DocString: nlopt_wrapper_get_log_reg_optimizer_list_list
/**
 * @brief Get an optimizer for the desired task
 *
 * @param task_sizes Number of training samples per task
 * @param prop The value of the property to evaluate the loss function against for the training set
 * @param n_rung Maximum rung of the features
 * @param max_param_depth The maximum depth in the binary expression tree to set non-linear optimization
 * @param cauchy_scaling The Cauchy scale factor used for calculating the residuals
 * @param use_global True if global optimization is requested for non-linear optimization of parameters (Can break reproducibility)
 *
 * @return The correct optimizer
 */
inline NLOptimizerLogRegression get_log_reg_optimizer(std::vector<int> task_sizes,
                                                      std::vector<double> prop,
                                                      int n_rung,
                                                      int max_param_depth = -1,
                                                      double cauchy_scaling = 0.5,
                                                      bool use_global = false)
{
    USE_GLOBAL = use_global;
    return NLOptimizerLogRegression(
        task_sizes, prop, n_rung, max_param_depth, cauchy_scaling, true);
}

// DocString: nlopt_wrapper_get_class_optimizer_list_list
/**
 * @brief Get an optimizer for the desired task
 *
 * @param task_sizes Number of training samples per task
 * @param prop The value of the property to evaluate the loss function against for the training set
 * @param n_rung Maximum rung of the features
 * @param max_param_depth The maximum depth in the binary expression tree to set non-linear optimization
 * @param use_global True if global optimization is requested for non-linear optimization of parameters (Can break reproducibility)
 *
 * @return The correct optimizer
 */
inline NLOptimizerClassification get_class_optimizer(std::vector<int> task_sizes,
                                                     std::vector<double> prop,
                                                     int n_rung,
                                                     int max_param_depth = -1)
{
    USE_GLOBAL = true;
    return NLOptimizerClassification(task_sizes, prop, n_rung, max_param_depth, true);
}

#endif
}  // namespace nlopt_wrapper
#endif
