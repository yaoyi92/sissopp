// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file utils/enum.hpp
 *  @brief Define enum classes for the library
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef ENUMS
#define ENUMS
/**
     * @brief Specify the type of node is used
     *
     */
enum class NODE_TYPE
{
    ADD = 0,
    SUB = 1,
    ABS_DIFF = 2,
    MULT = 3,
    DIV = 4,
    EXP = 5,
    NEG_EXP = 6,
    INV = 7,
    SQ = 8,
    CB = 9,
    SIX_POW = 10,
    SQRT = 11,
    CBRT = 12,
    LOG = 13,
    ABS = 14,
    SIN = 15,
    COS = 16,
    PARAM_ADD = 20,
    PARAM_SUB = 21,
    PARAM_ABS_DIFF = 22,
    PARAM_MULT = 23,
    PARAM_DIV = 24,
    PARAM_EXP = 25,
    PARAM_NEG_EXP = 26,
    PARAM_INV = 27,
    PARAM_SQ = 28,
    PARAM_CB = 29,
    PARAM_SIX_POW = 30,
    PARAM_SQRT = 31,
    PARAM_CBRT = 32,
    PARAM_LOG = 33,
    PARAM_ABS = 34,
    PARAM_SIN = 35,
    PARAM_COS = 36,
    FEAT,
    MODEL_FEATURE,
    MAX
};

/**
     * @brief Specify the type of descriptor identifier used
     *
     */
enum class DI_TYPE
{
    CLASS,
    REG,
    LOG_REG
};

enum class LOSS_TYPE
{
    CONVEX_HULL,
    PEARSON_RMSE,
    LOG_PEARSON_RMSE
};
#endif
