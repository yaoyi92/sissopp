// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file utils/string_utils.hpp
 *  @brief Defines a set of functions to manipulate strings
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef STRING_UTILS
#define STRING_UTILS

#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>

namespace str_utils
{
/**
     * @brief split a string and trim whitespace
     *
     * @param str String to split
     * @param split_tokens Characters to split the string with
     * @return The vector with the string split along the split tokens
     */
std::vector<std::string> split_string_trim(const std::string str,
                                           const std::string split_tokens = ",;:");

// DocString: str_utils_latexify
/**
     * @brief Convert a string into a latex string
     *
     * @param str String to convert to a latex string
     * @return The LaTeXified version of the string
     */
std::string latexify(const std::string str);

// DocString: str_utils_matlabify
/**
     * @brief Convert a string into a matlab valid name
     *
     * @param str String to convert to a valid matlab name
     * @return The matlab safe version of the string
     */
std::string matlabify(const std::string str);

/**
     * @brief Convert an operator into a valid matlab string
     *
     * @param str String to remove operators from
     * @param op The operator to replace
     * @param op_str The string to replace the operator with
     * @return The string with the operators replaced
     */
std::string op2str(const std::string str, const std::string op, const std::string op_str);

/**
     * @brief Join a vector of strings together
     *
     * @param join_section string to put in between each element
     * @param start pointer to the first element of the vector to join
     * @param sz Number of elements of the vector to join
     * @return The joined string
     */
std::string join(std::string join_section, std::string* start, int sz);

/**
     * @brief Join a vector of strings together
     *
     * @param join_section string to put in between each element
     * @param list The vector of strings to join together
     * @return The joined string
     */
inline std::string join(std::string join_section, std::vector<std::string> list)
{
    return join(join_section, list.data(), list.size());
}
}  // namespace str_utils

#endif
