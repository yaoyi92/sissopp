// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file utils/string_utils.cpp
 *  @brief Implements a set of functions to manipulate strings
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "utils/string_utils.hpp"

std::vector<std::string> str_utils::split_string_trim(const std::string str,
                                                      const std::string split_tokens)
{
    std::vector<std::string> split_str;
    boost::algorithm::split(split_str, str, boost::algorithm::is_any_of(split_tokens));
    for (auto& str_sec : split_str)
    {
        boost::algorithm::trim(str_sec);
    }

    return split_str;
}

std::string str_utils::latexify(const std::string str)
{
    std::string to_ret = "";
    std::vector<std::string> split_str = split_string_trim(str, " ");
    for (auto& term_str : split_str)
    {
        std::vector<std::string> split_term_str = split_string_trim(term_str, "_");
        to_ret += split_term_str[0];
        if (split_term_str.size() > 1)
        {
            to_ret += "_{" + split_term_str[1];
            for (int ii = 2; ii < split_term_str.size(); ++ii)
            {
                to_ret += ", " + split_term_str[ii];
            }
            to_ret += "}";
        }
        to_ret += " ";
    }
    return to_ret.substr(0, to_ret.size() - 1);
}

std::string str_utils::op2str(const std::string str, const std::string op, const std::string op_str)
{
    std::string to_ret = "";
    std::vector<std::string> split_term_str = split_string_trim(str, op);
    to_ret += split_term_str[0];
    for (int ii = 1; ii < split_term_str.size(); ++ii)
    {
        to_ret += op_str + split_term_str[ii];
    }

    return to_ret;
}

std::string str_utils::matlabify(const std::string str)
{
    std::string to_ret = "";

    std::string copy_str = str;
    std::replace(copy_str.begin(), copy_str.end(), ' ', '_');

    std::vector<std::string> split_str = split_string_trim(str, "\\}{");
    for (auto& term_str : split_str)
    {
        std::string add_str = term_str;
        add_str = op2str(add_str, "/", "_div_");
        add_str = op2str(add_str, "*", "_mult_");
        add_str = op2str(add_str, "+", "_add_");
        add_str = op2str(add_str, "-", "_sub_");
        add_str = op2str(add_str, "^", "_pow_");
        to_ret += add_str;
    }
    return to_ret;
}

std::string str_utils::join(std::string join_section, std::string* start, int sz)
{
    std::string to_ret = start[0];
    for (int ii = 1; ii < sz; ++ii)
    {
        to_ret += join_section + start[ii];
    }
    return to_ret;
}
