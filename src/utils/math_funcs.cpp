// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file utils/math_funcs.cpp
 *  @brief Implements a set of functions to get standardized mathematical operations on
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "utils/math_funcs.hpp"

bool util_funcs::iterate(std::vector<int>& inds, int size, int incriment)
{
    if ((size == 1) && (inds[0] < incriment))
    {
        return false;
    }

    inds[size - 1] -= incriment;
    bool cont = true;
    if (inds[size - 1] < 0)
    {
        while (cont && (inds[size - 1] < 0))
        {
            cont = iterate(inds, size - 1, 1);
            inds[size - 1] += inds[size - 2];
        }
    }
    return cont;
}

void util_funcs::standardize(const double* val, const int sz, double* stand_val)
{
    double avg = mean(val, sz);
    double std = stand_dev(val, sz, avg);
    std::transform(val, val + sz, stand_val, [&](double vv) { return (vv - avg) / std; });
}

double util_funcs::log_r2(const double* a,
                          const double* b,
                          double* log_a,
                          const int size,
                          const double mean_b,
                          const double std_b)
{
    std::transform(a, a + size, log_a, [](double aa) { return std::log(aa); });
    return r2(b, log_a, size, mean_b, std_b);
}

double util_funcs::r(const double* a, const double* b, const int* sz, const int n_task)
{
    double result = 0.0;
    int pos = 0;
    for (int nt = 0; nt < n_task; ++nt)
    {
        result += r2(a + pos, b + pos, sz[nt]);
        pos += sz[nt];
    }

    return std::sqrt(result / static_cast<double>(n_task));
}

double util_funcs::r(const double* a,
                     const double* b,
                     const int* sz,
                     const double* mean_a,
                     const double* std_a,
                     const int n_task)
{
    double result = 0.0;
    int pos = 0;
    for (int nt = 0; nt < n_task; ++nt)
    {
        result += r2(a + pos, b + pos, sz[nt], mean_a[nt], std_a[nt]);
        pos += sz[nt];
    }

    return std::sqrt(result / static_cast<double>(n_task));
}

double util_funcs::r(const double* a,
                     const double* b,
                     const int* sz,
                     const double* mean_a,
                     const double* std_a,
                     const double* mean_b,
                     const double* std_b,
                     const int n_task)
{
    double result = 0.0;
    int pos = 0;
    for (int nt = 0; nt < n_task; ++nt)
    {
        result += r2(a + pos, b + pos, sz[nt], mean_a[nt], std_a[nt], mean_b[nt], std_b[nt]);
        pos += sz[nt];
    }

    return std::sqrt(result / static_cast<double>(n_task));
}

double util_funcs::r2(const double* a, const double* b, const int* sz, const int n_task)
{
    double result = 0.0;
    int pos = 0;
    for (int nt = 0; nt < n_task; ++nt)
    {
        result += r2(a + pos, b + pos, sz[nt]);
        pos += sz[nt];
    }

    return result / static_cast<double>(n_task);
}

double util_funcs::r2(const double* a,
                      const double* b,
                      const int* sz,
                      const double* mean_a,
                      const double* std_a,
                      const int n_task)
{
    double result = 0.0;
    int pos = 0;
    for (int nt = 0; nt < n_task; ++nt)
    {
        result += r2(a + pos, b + pos, sz[nt], mean_a[nt], std_a[nt]);
        pos += sz[nt];
    }

    return result / static_cast<double>(n_task);
}

double util_funcs::r2(const double* a,
                      const double* b,
                      const int* sz,
                      const double* mean_a,
                      const double* std_a,
                      const double* mean_b,
                      const double* std_b,
                      const int n_task)
{
    double result = 0.0;
    int pos = 0;
    for (int nt = 0; nt < n_task; ++nt)
    {
        result += r2(a + pos, b + pos, sz[nt], mean_a[nt], std_a[nt], mean_b[nt], std_b[nt]);
        pos += sz[nt];
    }

    return result / static_cast<double>(n_task);
}

double util_funcs::log_r2(
    const double* a, const double* b, double* log_a, const int* sz, const int n_task)
{
    double result = 0.0;
    int pos = 0;
    for (int nt = 0; nt < n_task; ++nt)
    {
        result += log_r2(a + pos, b + pos, log_a + pos, sz[nt]);
        pos += sz[nt];
    }

    return result / static_cast<double>(n_task);
}

double util_funcs::log_r2(const double* a,
                          const double* b,
                          double* log_a,
                          const int* sz,
                          const double* mean_b,
                          const double* std_b,
                          const int n_task)
{
    double result = 0.0;
    int pos = 0;
    for (int nt = 0; nt < n_task; ++nt)
    {
        result += log_r2(a + pos, b + pos, log_a + pos, sz[nt], mean_b[nt], std_b[nt]);
        pos += sz[nt];
    }

    return result / static_cast<double>(n_task);
}

void util_funcs::rank(const double* a, double* rank, int* index, const int size)
{
    std::iota(index, index + size, 0);
    std::sort(index, index + size, [a](int i1, int i2) { return a[i1] < a[i2]; });
    int ii = 1;
    int ii_start = 0;
    while (ii_start < size)
    {
        while ((ii < size) &&
               (std::abs(a[index[ii]] - a[index[ii - 1]]) <
                1e-10 * std::pow(10.0, std::floor(std::log10(std::abs(a[index[ii]]))))))
        {
            ++ii;
        }

        for (int jj = ii_start; jj < ii; ++jj)
        {
            rank[index[jj]] = static_cast<double>(ii + ii_start - 1) / 2.0 + 1.0;
        }
        ii_start = ii;
        ++ii;
    }
}

double util_funcs::spearman_r(
    const double* a, const double* b, double* rank_a, double* rank_b, int* index, const int size)
{
    rank(a, rank_a, index, size);
    rank(b, rank_b, index, size);
    return r(rank_a, rank_b, size);
}

double util_funcs::spearman_r(const double* a,
                              const double* b,
                              double* rank_a,
                              double* rank_b,
                              int* index,
                              const int* sz,
                              const int n_tasks)
{
    double result = 0.0;
    int pos = 0;
    for (int nt = 0; nt < n_tasks; ++nt)
    {
        result += spearman_r(a + pos, b + pos, rank_a, rank_b, index, sz[nt]);
        pos += sz[nt];
    }
    return result / static_cast<double>(n_tasks);
}
