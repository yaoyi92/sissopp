// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file utils/math_funcs.hpp
 *  @brief Defines a set of functions to get standardized mathematical operations on
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef MATH_FXN
#define MATH_FXN

#include <math.h>

#include <algorithm>
#include <functional>
#include <iostream>
#include <numeric>
#include <vector>

#include "utils/mkl_interface.hpp"

namespace util_funcs
{
/**
     * @brief Get the sign of a number
     *
     * @param number The number to get the sign of
     * @return -1 for negative, 0 for 0, and 1 for positive
     */
template <typename T>
inline T sign(T number)
{
    return (number > T(0)) - (number < T(0));
};

/**
     * @brief Round a number to arbitrary precision
     *
     * @param num The number to round
     * @param n The number of decimal digits after the decimal point
     *
     * @return The rounded number
     */
inline double round(double num, int n)
{
    double exp = std::floor(std::log10(std::abs(num)));
    return std::pow(10.0, exp - static_cast<double>(n)) *
           std::round(num * std::pow(10.0, static_cast<double>(n) - exp));
}

/**
     * @brief Round a number to arbitrary precision
     *
     * @param num The number to round
     * @param n The number of binary digits after the decimal point
     *
     * @return The rounded number
     */
inline double round2(double num, int n)
{
    double exp = std::ceil(std::log2(std::abs(num)));
    return std::pow(2.0, exp - static_cast<double>(n)) *
           std::round(num * std::pow(2.0, static_cast<double>(n) - exp));
}

/**
     * @brief Find the mean of of a vector
     *
     * @param start A pointer to the head of the vector
     * @param size The size of the vector
     * @return The mean of the vector
     */
template <typename T>
inline double mean(const T* start, int size)
{
    return static_cast<double>(std::accumulate(start, start + size, 0.0)) /
           static_cast<double>(size);
};

/**
     * @brief Find the mean of of a vector
     *
     * @param vec vector to find the mean of
     * @return the mean of the vector
     */
template <typename T>
inline double mean(const std::vector<T>& vec)
{
    return mean(vec.data(), vec.size());
};

/**
     * @brief Find the standard deviation of a vector

     * @param start A pointer to the head of the vector
     * @param size The size of the vector
     * @param vec_mean The mean of the vector
     * @return The standard deviation of the vector
     */
inline double stand_dev(const double* start, const int size, const double vec_mean)
{
    return std::sqrt(std::accumulate(start,
                                     start + size,
                                     0.0,
                                     [&vec_mean](double total, double val) {
                                         return total + (val - vec_mean) * (val - vec_mean);
                                     }) /
                     size);
};

/**
     * @brief Find the standard deviation of a vector
     *
     * @param start A pointer to the head of the vector
     * @param size The size of the vector
     * @return The standard deviation of the vector
     */
inline double stand_dev(const double* start, const int size)
{
    double vec_mean = mean<double>(start, size);
    return stand_dev(start, size, vec_mean);
};

/**
     * @brief Find the standard deviation of a vector
     *
     * @param vec The vector to calculate the stand deviation of.
     * @return The standard deviation
     */
inline double stand_dev(const std::vector<double>& vec)
{
    return stand_dev(vec.data(), vec.size());
};

/**
     * @brief Find the standard deviation of a vector
     *
     * @param vec The vector to calculate the stand deviation of.
     * @param vec_mean The mean of the vector
     * @return the standard deviation
     */
inline double stand_dev(const std::vector<double>& vec, const double vec_mean)
{
    return stand_dev(vec.data(), vec.size(), vec_mean);
};

/**
     * @brief Standardize a vector
     *
     * @param val pointer to the head of the vector to standardize
     * @param sz size of the vector
     * @param stand_val vector to the output vector
     */
void standardize(const double* val, int sz, double* stand_val);

/**
     * @brief Find the norm of a vector

     * @param start A pointer to the head of the vector
     * @param size The size of the vector
     * @return The norm of the vector
     */
inline double norm(const double* start, const int size)
{
    return std::sqrt(std::inner_product(start, start + size, start, 0.0));
};

/**
     * @brief Find the norm of a vector
     *
     * @param vec The vector to calculate the norm of.
     * @return the norm of the vector
     */
inline double norm(const std::vector<double>& vec) { return norm(vec.data(), vec.size()); };

/**
     * @brief The Pearson correlation for two vectors
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param size The size of the vector
     * @param mean_a The mean of the a vector
     * @param std_a The standard deviation of the a vector
     * @param mean_b The mean of the b vector
     * @param std_b The standard deviation of the b vector
     * @return The correlation coefficient between vector a and vector b
     */
inline double r(const double* a,
                const double* b,
                const int size,
                const double mean_a,
                const double std_a,
                const double mean_b,
                const double std_b)
{
    return 1.0 / (static_cast<double>(size) * std_a * std_b) *
           std::inner_product(a, a + size, b, -1.0 * static_cast<double>(size) * mean_a * mean_b);
}

/**
     * @brief The Pearson correlation for two vectors
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param size The size of the vector
     * @return The correlation coefficient between vector a and vector b
     */
inline double r(const double* a, const double* b, const int size)
{
    double mean_a = mean<double>(a, size);
    double std_a = stand_dev(a, size, mean_a);

    double mean_b = mean<double>(b, size);
    double std_b = stand_dev(b, size, mean_b);

    return r(a, b, size, mean_a, std_a, mean_b, std_b);
}

/**
     * @brief The Pearson correlation for two vectors
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param size The size of the vector
     * @param mean_a The mean of the a vector
     * @param std_a The standard deviation of the a vector
     * @return The correlation coefficient between vector a and vector b
     */
inline double r(
    const double* a, const double* b, const int size, const double mean_a, const double std_a)
{
    double mean_b = mean<double>(b, size);
    double std_b = stand_dev(b, size, mean_b);

    return r(a, b, size, mean_a, std_a, mean_b, std_b);
}

/**
     * @brief The Pearson correlation for two vectors
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param sz The start of vector that describes the sizes of the tasks to calculate the correlation on
     * @param n_tasks The number of tasks to average over
     * @return The average Pearson correlations
     */
double r(const double* a, const double* b, const int* sz, const int n_tasks);

/**
     * @brief The Pearson correlation for two vectors
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param sz The start of vector that describes the sizes of the tasks to calculate the correlation on
     * @param mean_a The mean of the a vector for each task
     * @param std_a The standard deviation of the a vector for each task
     * @param n_tasks The number of tasks to average over
     * @return The average Pearson correlations
     */
double r(const double* a,
         const double* b,
         const int* sz,
         const double* mean_a,
         const double* std_a,
         const int n_tasks);

/**
     * @brief The Pearson correlation for two vectors
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param sz The start of vector that describes the sizes of the tasks to calculate the correlation on
     * @param mean_a The mean of the a vector for each task
     * @param std_a The standard deviation of the b vector for each task
     * @param mean_b The mean of the a vector for each task
     * @param std_b The standard deviation of the b vector for each task
     * @param n_tasks The number of tasks to average over
     * @return The average Pearson correlations
     */
double r(const double* a,
         const double* b,
         const int* sz,
         const double* mean_a,
         const double* std_a,
         const double* mean_b,
         const double* std_b,
         const int n_tasks);

/**
     * @brief The Pearson correlation for two vectors
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param sizes The sizes of the tasks to calculate the correlation on
     * @return The average Pearson correlations
     */
inline double r(const double* a, const double* b, const std::vector<int>& sizes)
{
    return r(a, b, sizes.data(), sizes.size());
}

/**
     * @brief The Pearson correlation for two vectors
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param sizes The sizes of the tasks to calculate the correlation on
     * @param mean_a The mean of the a vector for each task
     * @param std_a The standard deviation of the a vector for each task
     * @return The average Pearson correlations
     */
inline double r(const double* a,
                const double* b,
                const std::vector<int>& sizes,
                const std::vector<double>& mean_a,
                const std::vector<double>& std_a)
{
    return r(a, b, sizes.data(), mean_a.data(), std_a.data(), sizes.size());
}

/**
     * @brief The Pearson correlation for two vectors
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param sizes The sizes of the tasks to calculate the correlation on
     * @param mean_a The mean of the a vector for each task
     * @param std_a The standard deviation of the a vector for each task
     * @param mean_b The mean of the b vector for each task
     * @param std_b The standard deviation of the b vector for each task
     * @return The average Pearson correlations
     */
inline double r(const double* a,
                const double* b,
                const std::vector<int>& sizes,
                const std::vector<double>& mean_a,
                const std::vector<double>& std_a,
                const std::vector<double>& mean_b,
                const std::vector<double>& std_b)
{
    return r(
        a, b, sizes.data(), mean_a.data(), std_a.data(), mean_b.data(), std_b.data(), sizes.size());
}

/**
     * @brief The Pearson correlation for two vectors
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param size The size of the vector
     * @param mean_a The mean of the a vector
     * @param std_a The standard deviation of the a vector
     * @param mean_b The mean of the b vector
     * @param std_b The standard deviation of the b vector
     * @return The coefficient of determination between vector a and vector b
     */
inline double r2(const double* a,
                 const double* b,
                 const int size,
                 const double mean_a,
                 const double std_a,
                 const double mean_b,
                 const double std_b)
{
    double r_val = r(a, b, size, mean_a, std_a, mean_b, std_b);
    return r_val * r_val;
}

/**
     * @brief The Pearson correlation for two vectors
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param size The size of the vector
     * @return The coefficient of determination between vector a and vector b
     */
inline double r2(const double* a, const double* b, const int size)
{
    double mean_a = mean<double>(a, size);
    double std_a = stand_dev(a, size, mean_a);

    double mean_b = mean<double>(b, size);
    double std_b = stand_dev(b, size, mean_b);

    return r2(a, b, size, mean_a, std_a, mean_b, std_b);
}

/**
     * @brief The Pearson correlation for two vectors
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param size The size of the vector
     * @param mean_a The mean of the a vector
     * @param std_a The standard deviation of the a vector
     * @return The coefficient of determination between vector a and vector b
     */
inline double r2(
    const double* a, const double* b, const int size, const double mean_a, const double std_a)
{
    double mean_b = mean<double>(b, size);
    double std_b = stand_dev(b, size, mean_b);

    return r2(a, b, size, mean_a, std_a, mean_b, std_b);
}

/**
     * @brief Calculate the average Coefficient of Determination between two vectors
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param sz The start of vector that describes the sizes of the tasks to calculate the correlation on
     * @param n_tasks The number of tasks to average over
     * @return The average Coefficient of Determination
     */
double r2(const double* a, const double* b, const int* sz, const int n_tasks);

/**
     * @brief Calculate the average Coefficient of Determination between two vectors
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param sz The start of vector that describes the sizes of the tasks to calculate the correlation on
     * @param mean_a The mean of the a vector for each task
     * @param std_a The standard deviation of the a vector for each task
     * @param n_tasks The number of tasks to average over
     * @return The average Coefficient of Determination
     */
double r2(const double* a,
          const double* b,
          const int* sz,
          const double* mean_a,
          const double* std_a,
          const int n_tasks);

/**
     * @brief Calculate the average Coefficient of Determination between two vectors
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param sz The start of vector that describes the sizes of the tasks to calculate the correlation on
     * @param mean_a The mean of the a vector for each task
     * @param std_a The standard deviation of the a vector for each task
     * @param mean_b The mean of the b vector for each task
     * @param std_b The standard deviation of the b vector for each task
     * @param n_tasks The number of tasks to average over
     * @return The average Coefficient of Determination
     */
double r2(const double* a,
          const double* b,
          const int* sz,
          const double* mean_a,
          const double* std_a,
          const double* mean_b,
          const double* std_b,
          const int n_tasks);

/**
     * @brief Calculate the average Coefficient of Determination between two vectors
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param sizes The sizes of the tasks to calculate the correlation on
     * @return The average Coefficient of Determination
     */
inline double r2(const double* a, const double* b, const std::vector<int>& sizes)
{
    return r2(a, b, sizes.data(), sizes.size());
}

/**
     * @brief Calculate the average Coefficient of Determination between two vectors
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param sizes The sizes of the tasks to calculate the correlation on
     * @param mean_a The mean of the a vector for each task
     * @param std_a The standard deviation of the a vector for each task
     * @return The average Coefficient of Determination
     */
inline double r2(const double* a,
                 const double* b,
                 const std::vector<int>& sizes,
                 const std::vector<double>& mean_a,
                 const std::vector<double>& std_a)
{
    return r2(a, b, sizes.data(), mean_a.data(), std_a.data(), sizes.size());
}

/**
     * @brief Calculate the average Coefficient of Determination between two vectors
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param sizes The sizes of the tasks to calculate the correlation on
     * @param mean_a The mean of the a vector for each task
     * @param std_a The standard deviation of the a vector for each task
     * @param mean_b The mean of the b vector for each task
     * @param std_b The standard deviation of the b vector for each task
     * @return The average Coefficient of Determination
     */
inline double r2(const double* a,
                 const double* b,
                 const std::vector<int>& sizes,
                 const std::vector<double>& mean_a,
                 const std::vector<double>& std_a,
                 const std::vector<double>& mean_b,
                 const std::vector<double>& std_b)
{
    return r2(
        a, b, sizes.data(), mean_a.data(), std_a.data(), mean_b.data(), std_b.data(), sizes.size());
}

/**
     * @brief Calculate the Coefficient of Determination between two vectors (For the log transformed problem)
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param log_a the pointer to the head of the vector used to store the log_transformed a value
     * @param size The size of the vector
     * @param mean_b The mean of the b vector for each task
     * @param std_b The standard deviation of the b vector for each task
     * @return The Coefficient of Determination
     */
double log_r2(const double* a,
              const double* b,
              double* log_a,
              const int size,
              const double mean_b,
              const double std_b);

/**
     * @brief Calculate the average Coefficient of Determination between two vectors (For the log transformed problem)
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param log_a the pointer to the head of the vector used to store the log_transformed a value
     * @param sz The start of vector that describes the sizes of the tasks to calculate the correlation on
     * @param n_tasks The number of tasks to average over
     * @return The average Coefficient of Determination
     */
double log_r2(const double* a, const double* b, double* log_a, const int* sz, const int n_tasks);

/**
     * @brief Calculate the average Coefficient of Determination between two vectors (For the log transformed problem)
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param log_a the pointer to the head of the vector used to store the log_transformed a value
     * @param sz The start of vector that describes the sizes of the tasks to calculate the correlation on
     * @param mean_b The mean of the b vector for each task
     * @param std_b The standard deviation of the b vector for each task
     * @param n_tasks The number of tasks to average over
     * @return The average Coefficient of Determination
     */
double log_r2(const double* a,
              const double* b,
              double* log_a,
              const int* sz,
              const double* mean_b,
              const double* std_b,
              const int n_tasks);

/**
     * @brief Calculate the Coefficient of Determination between two vectors (For the log transformed problem)
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param log_a the pointer to the head of the vector used to store the log_transformed a value
     * @param size The size of the vector
     * @return The Coefficient of Determination
     */
inline double log_r2(const double* a, const double* b, double* log_a, const int size)
{
    double mean_b = mean<double>(b, size);
    double std_b = stand_dev(b, size, mean_b);

    return log_r2(a, b, log_a, size, mean_b, std_b);
}

/**
     * @brief Calculate the average Coefficient of Determination between two vectors (For the log transformed problem)
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param log_a the pointer to the head of the vector used to store the log_transformed a value
     * @param sizes The sizes of the tasks to calculate the correlation on
     * @return The average Coefficient of Determination
     */
inline double log_r2(const double* a, const double* b, double* log_a, const std::vector<int>& sizes)
{
    return log_r2(a, b, log_a, sizes.data(), sizes.size());
}

/**
     * @brief Calculate the average Coefficient of Determination between two vectors (For the log transformed problem)
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param log_a the pointer to the head of the vector used to store the log_transformed a value
     * @param sizes The sizes of the tasks to calculate the correlation on
     * @param mean_b The mean of the b vector for each task
     * @param std_b The standard deviation of the b vector for each task
     * @return The average Coefficient of Determination
     */
inline double log_r2(const double* a,
                     const double* b,
                     double* log_a,
                     const std::vector<int>& sizes,
                     const std::vector<double>& mean_b,
                     const std::vector<double>& std_b)
{
    return log_r2(a, b, log_a, sizes.data(), mean_b.data(), std_b.data(), sizes.size());
}

/**
     * @brief Gets the rank variables for a vector
     *
     * @param a pointer to the head of the vector to find the rank of
     * @param rank pointer to the head of the vector to store the resulting ranks
     * @param index pointer to vector used store the sorted indexes
     * @param sz The size of the vector
     */
void rank(const double* a, double* rank, int* index, const int size);

/**
     * @brief Calculate the Spearman's rank correlation coefficient between two vectors
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param rank_a The pointer to the head of the vector used to rank of a
     * @param rank_b The pointer to the head of the vector used to rank of b
     * @param index The pointer used to store the sorted indexes
     * @param size The size of the vector
     * @return The Spearman Correlation
     */
double spearman_r(
    const double* a, const double* b, double* rank_a, double* rank_b, int* index, const int size);

/**
     * @brief Calculate the average Spearman's rank correlation coefficient between two vectors
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param rank_a The pointer to the head of the vector used to rank of a
     * @param rank_b The pointer to the head of the vector used to rank of b
     * @param index The pointer used to store the sorted indexes
     * @param sz The start of vector that describes the sizes of the tasks to calculate the correlation on
     * @param n_tasks The number of tasks to average over
     * @return The average Spearman Correlation
     */
double spearman_r(const double* a,
                  const double* b,
                  double* rank_a,
                  double* rank_b,
                  int* index,
                  const int* sz,
                  const int n_tasks);

/**
     * @brief Calculate the average Spearman's rank correlation coefficient between two vectors
     *
     * @param a The pointer to the head of the first vector
     * @param b The pointer to the head of the second vector
     * @param rank_a The pointer to the head of the vector used to rank of a
     * @param rank_b The pointer to the head of the vector used to rank of b
     * @param index The pointer used to store the sorted indexes
     * @param sizes The sizes of the tasks to calculate the correlation on
     * @return The average Spearman Correlation
     */
inline double spearman_r(const double* a,
                         const double* b,
                         double* rank_a,
                         double* rank_b,
                         int* index,
                         const std::vector<int>& sizes)
{
    return spearman_r(a, b, rank_a, rank_b, index, sizes.data(), sizes.size());
}

/**
     * @brief Sort a vector and return the indexes of the unsorted array that corresponds to the sorted one
     *
     * @param begin The starting point for the sorting
     * @param end The end point for the sorting
     * @param vec_begin A pointer to the head of the vector to sort
     */
template <typename T>
inline void argsort(int* begin, int* end, const T* vec_begin)
{
    std::sort(begin, end, [vec_begin](int i1, int i2) { return vec_begin[i1] < vec_begin[i2]; });
}

/**
     * @brief Sort a vector and return the indexes of the unsorted array that corresponds to the sorted one
     *
     * @param vec vector to sort
     * @return The indexes of the sorted array
     */
template <typename T>
std::vector<int> argsort(const std::vector<T>& vec)
{
    std::vector<int> index(vec.size());
    std::iota(index.begin(), index.end(), 0);

    argsort<T>(index.data(), index.data() + index.size(), vec.data());
    return index;
}

/**
     * @brief Sort a vector and return the indexes of the unsorted array that corresponds to the sorted one
     *
     * @param begin The starting point for the sorting
     * @param end The end point for the sorting
     * @param vec The vector to sort
     * @return The indexes of the sorted array
     */
template <typename T>
inline void argsort(int* begin, int* end, const std::vector<T>& vec)
{
    argsort<T>(begin, end, vec.data());
}

/**
     * @brief The maximum absolute value of the vector
     *
     * @param start The starting point for the comparison
     * @param end The ending point for the comparison
     * @return The maximum absolute value of the vector
     */
template <typename T>
inline T max_abs_val(T* start, T* end)
{
    return std::abs(
        *std::max_element(start, end, [](T a, T b) { return std::abs(a) < std::abs(b); }));
}

/**
     * @brief The maximum absolute value of the vector
     *
     * @param start The starting point for the comparison
     * @param size The size of the vector
     * @return The maximum absolute value of the vector
     */
template <typename T>
inline T max_abs_val(T* start, int size)
{
    return max_abs_val(start, start + size);
}

/**
     * @brief Iterate the set of indices to get all combinations
     *
     * @param inds The indices to iterate over
     * @param size The size of the indices
     * @param increment The amount to increment the indices
     * @return True if the all combinations have been explored
     */
bool iterate(std::vector<int>& inds, int size, int increment);
}  // namespace util_funcs

#endif
