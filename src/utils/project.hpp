// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file utils/project.hpp
 *  @brief Defines a set of functions to project features onto a second vector
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef UTILS_PROJECT
#define UTILS_PROJECT

#include <limits>

#include "loss_function/utils.hpp"
#ifdef PARAMETERIZE
#include "nl_opt/utils.hpp"
#endif

namespace project_funcs
{
/**
     * @brief Calculate the projection scores of a set of features to a vector via Pearson correlation
     *
     * @param prop The pointer to the property vector to calculate the Pearson correlation against
     * @param scores The pointer of the score vector
     * @param phi The set of features to calculate the Pearson correlation of
     * @param size Vector of the size of all of the tasks
     * @param n_prop The number of properties to calculate the Pearson Correlation of and return the maximum of
     */
void project_r(const double* prop,
               double* scores,
               const std::vector<node_ptr>& phi,
               const std::vector<int>& size,
               const int n_prop);

/**
     * @brief Calculate the projection scores of a set of features to a vector via Pearson correlation
     *
     * @param prop The pointer to the property vector to calculate the Pearson correlation against
     * @param scores The pointer of the score vector
     * @param phi The set of features to calculate the Pearson correlation of
     * @param size Vector of the size of all of the tasks
     * @param n_prop The number of properties to calculate the Pearson Correlation of and return the maximum of
     */
void project_r_no_omp(const double* prop,
                      double* scores,
                      const std::vector<node_ptr>& phi,
                      const std::vector<int>& size,
                      const int n_prop);

/**
     * @brief Calculate the projection score of a set of features
     *
     * @param loss The LossFunnction used for the projection
     * @param feats The set of features to calculate
     * @param scores A pointer to the head of the vector to output the scores to
     */
void project_loss(std::shared_ptr<LossFunction> loss,
                  const std::vector<node_ptr>& feats,
                  double* scores);

/**
     * @brief Calculate the projection score of a set of features without using OpenMP
     *
     * @param loss The LossFunnction used for the projection
     * @param feats The set of features to calculate
     * @param scores A pointer to the head of the vector to output the scores to
     */
void project_loss_no_omp(std::shared_ptr<LossFunction> loss,
                         const std::vector<node_ptr>& feats,
                         double* scores);
}  // namespace project_funcs

#endif
