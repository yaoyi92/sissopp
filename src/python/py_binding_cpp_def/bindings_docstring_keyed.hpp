// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file python/py_bindings_cpp_def/bindings.hpp
 *  @brief Definitions to convert C++ classes into python classes
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef PYTHON_BINDING
#define PYTHON_BINDINGS

#include <external/pybind11/include/pybind11/operators.h>

#include "descriptor_identifier/solver/SISSOClassifier.hpp"
#include "descriptor_identifier/solver/SISSOLogRegressor.hpp"
#include "python/py_binding_cpp_def/pickle.hpp"

#ifdef PARAMETERIZE
#include "nl_opt/utils.hpp"
#endif

namespace py = pybind11;

namespace sisso
{
/**
 * @brief Register all C++ classes as python classes
 */
void register_all(py::module& m);

static void register_Inputs(py::module& m);

namespace feature_creation
{
static void register_FeatureSpace(py::module& m);
static void register_Unit(py::module& m);
static void register_Domain(py::module& m);
#ifdef PARAMETERIZE
namespace nloptimizer
{
static void register_NLOptimizer(py::module& m);
static void register_NLOptimizerClassification(py::module& m);
static void register_NLOptimizerRegression(py::module& m);
static void register_NLOptimizerLogRegression(py::module& m);

}  // namespace nloptimizer
#endif
namespace node
{
/**
 * @brief Register the Node for accessing the object via python
 */
static void register_Node(py::module& m);

/**
 * @brief Register the FeatureNode for accessing the object via python
 */
static void register_FeatureNode(py::module& m);

/**
 * @brief Register the ModelNode for accessing the object via python
 */
static void register_ModelNode(py::module& m);

template <int N>
static void register_OperatorNode(py::module& m)
{
    std::string name = "OperatorNode" + std::to_string(N);
    py::class_<OperatorNode<N>, std::shared_ptr<OperatorNode<N>>, Node, PyOperatorNode<N>>
        operator_node(m, name.c_str(), "@DocString_cls_op_node@");

#ifdef PARAMETERIZE
    operator_node.def("get_parameters",
                      &OperatorNode<N>::get_parameters,
                      py::arg("optimizer"),
                      "@DocString_op_node_get_params@");
    // operator_node.def("set_parameters", py::overload_cast<py::array_t<double>>(&OperatorNode<N>::set_parameters), py::arg("params"), "@DocString_op_node_set_param_arr@");
    operator_node.def("set_parameters",
                      &OperatorNode<N>::set_parameters_py,
                      py::arg("params"),
                      "@DocString_op_node_set_param_list@");
#endif
}

/**
 * @brief Register the AddNode object for conversion to a python object
 */
static void register_AddNode(py::module& m);

/**
 * @brief Register the SubNode object for conversion to a python object
 */
static void register_SubNode(py::module& m);

/**
 * @brief Register the DivNode object for conversion to a python object
 */
static void register_DivNode(py::module& m);

/**
 * @brief Register the MultNode object for conversion to a python object
 */
static void register_MultNode(py::module& m);

/**
 * @brief Register the AbsDiffNode object for conversion to a python object
 */
static void register_AbsDiffNode(py::module& m);

/**
 * @brief Register the AbsNode object for conversion to a python object
 */
static void register_AbsNode(py::module& m);

/**
 * @brief Register the InvNode object for conversion to a python object
 */
static void register_InvNode(py::module& m);

/**
 * @brief Register the LogNode object for conversion to a python object
 */
static void register_LogNode(py::module& m);

/**
 * @brief Register the ExpNode object for conversion to a python object
 */
static void register_ExpNode(py::module& m);

/**
 * @brief Register the NegExpNode object for conversion to a python object
 */
static void register_NegExpNode(py::module& m);

/**
 * @brief Register the SinNode object for conversion to a python object
 */
static void register_SinNode(py::module& m);

/**
 * @brief Register the CosNode object for conversion to a python object
 */
static void register_CosNode(py::module& m);

/**
 * @brief Register the CbNode object for conversion to a python object
 */
static void register_CbNode(py::module& m);

/**
 * @brief Register the CbrtNode object for conversion to a python object
 */
static void register_CbrtNode(py::module& m);

/**
 * @brief Register the SqNode object for conversion to a python object
 */
static void register_SqNode(py::module& m);

/**
 * @brief Register the SqrtNode object for conversion to a python object
 */
static void register_SqrtNode(py::module& m);

/**
 * @brief Register the SixPowNode object for conversion to a python object
 */
static void register_SixPowNode(py::module& m);

#ifdef PARAMETERIZE
/**
 * @brief Register the AddParamNode object for conversion to a python object
 */
static void register_AddParamNode(py::module& m);

/**
 * @brief Register the SubParamNode object for conversion to a python object
 */
static void register_SubParamNode(py::module& m);

/**
 * @brief Register the DivParamNode object for conversion to a python object
 */
static void register_DivParamNode(py::module& m);

/**
 * @brief Register the MultParamNode object for conversion to a python object
 */
static void register_MultParamNode(py::module& m);

/**
 * @brief Register the AbsDiffParamNode object for conversion to a python object
 */
static void register_AbsDiffParamNode(py::module& m);

/**
 * @brief Register the AbsParamNode object for conversion to a python object
 */
static void register_AbsParamNode(py::module& m);

/**
 * @brief Register the InvParamNode object for conversion to a python object
 */
static void register_InvParamNode(py::module& m);

/**
 * @brief Register the LogParamNode object for conversion to a python object
 */
static void register_LogParamNode(py::module& m);

/*
 * @brief Register the ExpParamNode object for conversion to a python object
 */
static void register_ExpParamNode(py::module& m);

/**
 * @brief Register the NegExpParamNode object for conversion to a python object
 */
static void register_NegExpParamNode(py::module& m);

/**
 * @brief Register the SinParamNode object for conversion to a python object
 */
static void register_SinParamNode(py::module& m);

/**
 * @brief Register the CosParamNode object for conversion to a python object
 */
static void register_CosParamNode(py::module& m);

/**
 * @brief Register the CbParamNode object for conversion to a python object
 */
static void register_CbParamNode(py::module& m);

/**
 * @brief Register the CbrtParamNode object for conversion to a python object
 */
static void register_CbrtParamNode(py::module& m);

/**
 * @brief Register the SqParamNode object for conversion to a python object
 */
static void register_SqParamNode(py::module& m);

/**
 * @brief Register the SqrtParamNode object for conversion to a python object
 */
static void register_SqrtParamNode(py::module& m);

/**
 * @brief Register the SixPowParamNode object for conversion to a python object
 */
static void register_SixPowParamNode(py::module& m);
#endif
}  // namespace node
}  // namespace feature_creation

namespace descriptor_identifier
{
/**
 * @brief Register the Model object for conversion to a python object
 */
static void register_Model(py::module& m);

/**
 * @brief Register the Model object for conversion to a python object
 */
static void register_ModelRegressor(py::module& m);

/**
 * @brief Register the Model object for conversion to a python object
 */
static void register_ModelLogRegressor(py::module& m);

/**
 * @brief Register the Model object for conversion to a python object
 */
static void register_ModelClassifier(py::module& m);

/**
 * @brief Register the SISSORegressor object for conversion to a python object
 */
static void register_SISSOSolver(py::module& m);

/**
 * @brief Register the SISSORegressor object for conversion to a python object
 */
static void register_SISSORegressor(py::module& m);

/**
 * @brief Register the SISSORegressor object for conversion to a python object
 */
static void register_SISSOLogRegressor(py::module& m);

/**
 * @brief Register the SISSORegressor object for conversion to a python object
 */
static void register_SISSOClassifier(py::module& m);
}  // namespace descriptor_identifier
}  // namespace sisso

#endif
