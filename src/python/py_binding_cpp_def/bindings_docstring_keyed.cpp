// // Copyright 2021 Thomas A. R. Purcell
// //
// // Licensed under the Apache License, Version 2.0 (the "License");
// // you may not use this file except in compliance with the License.
// // You may obtain a copy of the License at
// //
// //     http://www.apache.org/licenses/LICENSE-2.0
// //
// // Unless required by applicable law or agreed to in writing, software
// // distributed under the License is distributed on an "AS IS" BASIS,
// // WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// // See the License for the specific language governing permissions and
// // limitations under the License.

// /** @file python/py_bindings_cpp_def/bindings.cpp
//  *  @brief Implements the functions used to convert C++ classes into python classes
//  *
//  *  @author Thomas A. R. Purcell (tpurcell90)
//  *  @bug No known bugs.
//  */

#include "python/py_binding_cpp_def/bindings.hpp"
namespace py = pybind11;

void sisso::register_all(py::module& m)
{
    sisso::register_Inputs(m);
    sisso::descriptor_identifier::register_Model(m);
    sisso::descriptor_identifier::register_ModelRegressor(m);
    sisso::descriptor_identifier::register_ModelLogRegressor(m);
    sisso::descriptor_identifier::register_ModelClassifier(m);
    sisso::descriptor_identifier::register_SISSOSolver(m);
    sisso::descriptor_identifier::register_SISSORegressor(m);
    sisso::descriptor_identifier::register_SISSOLogRegressor(m);
    sisso::descriptor_identifier::register_SISSOClassifier(m);
    sisso::feature_creation::register_FeatureSpace(m);
    sisso::feature_creation::register_Unit(m);
    sisso::feature_creation::register_Domain(m);
    sisso::feature_creation::node::register_Node(m);
    sisso::feature_creation::node::register_FeatureNode(m);
    sisso::feature_creation::node::register_ModelNode(m);
    sisso::feature_creation::node::register_OperatorNode<1>(m);
    sisso::feature_creation::node::register_OperatorNode<2>(m);
    sisso::feature_creation::node::register_AddNode(m);
    sisso::feature_creation::node::register_SubNode(m);
    sisso::feature_creation::node::register_DivNode(m);
    sisso::feature_creation::node::register_MultNode(m);
    sisso::feature_creation::node::register_AbsDiffNode(m);
    sisso::feature_creation::node::register_AbsNode(m);
    sisso::feature_creation::node::register_InvNode(m);
    sisso::feature_creation::node::register_LogNode(m);
    sisso::feature_creation::node::register_ExpNode(m);
    sisso::feature_creation::node::register_NegExpNode(m);
    sisso::feature_creation::node::register_SinNode(m);
    sisso::feature_creation::node::register_CosNode(m);
    sisso::feature_creation::node::register_CbNode(m);
    sisso::feature_creation::node::register_CbrtNode(m);
    sisso::feature_creation::node::register_SqNode(m);
    sisso::feature_creation::node::register_SqrtNode(m);
    sisso::feature_creation::node::register_SixPowNode(m);

    m.def("phi_selected_from_file",
          &str2node::phi_selected_from_file,
          py::arg("filename"),
          py::arg("phi_0"),
          py::arg("excluded_inds") = py::list(),
          "@DocString_node_utils_phi_sel_from_file@");

    m.def("initialize_values_arr",
          py::overload_cast<std::vector<int>, std::vector<int>, int, int>(
              &node_value_arrs::initialize_values_arr),
          py::arg("task_sz_train"),
          py::arg("task_sz_test"),
          py::arg("n_primary_feat"),
          py::arg("max_rung"),
          "@DocString_node_vals_ts_list_no_params@");

    m.def("initialize_d_matrix_arr",
          &node_value_arrs::initialize_d_matrix_arr,
          "@DocString_node_vals_init_d_mat@");

    m.def("finalize_values_arr",
          &node_value_arrs::finalize_values_arr,
          "@DocString_node_vals_finalize@");

    m.def("matlabify", &str_utils::matlabify, py::arg("str"), "@DocString_str_utils_matlabify@");

    m.def("latexify", &str_utils::latexify, py::arg("str"), "@DocString_str_utils_latexify@");

#ifdef PARAMETERIZE
    m.def("initialize_values_arr",
          py::overload_cast<std::vector<int>, std::vector<int>, int, int, bool>(
              &node_value_arrs::initialize_values_arr),
          py::arg("task_sz_train"),
          py::arg("task_sz_test"),
          py::arg("n_primary_feat"),
          py::arg("max_rung"),
          py::arg("use_params"),
          "@DocString_node_vals_ts_list@");
    sisso::feature_creation::node::register_AddParamNode(m);
    sisso::feature_creation::node::register_SubParamNode(m);
    sisso::feature_creation::node::register_DivParamNode(m);
    sisso::feature_creation::node::register_MultParamNode(m);
    sisso::feature_creation::node::register_AbsDiffParamNode(m);
    sisso::feature_creation::node::register_AbsParamNode(m);
    sisso::feature_creation::node::register_InvParamNode(m);
    sisso::feature_creation::node::register_LogParamNode(m);
    sisso::feature_creation::node::register_ExpParamNode(m);
    sisso::feature_creation::node::register_NegExpParamNode(m);
    sisso::feature_creation::node::register_SinParamNode(m);
    sisso::feature_creation::node::register_CosParamNode(m);
    sisso::feature_creation::node::register_CbParamNode(m);
    sisso::feature_creation::node::register_CbrtParamNode(m);
    sisso::feature_creation::node::register_SqParamNode(m);
    sisso::feature_creation::node::register_SqrtParamNode(m);
    sisso::feature_creation::node::register_SixPowParamNode(m);

    sisso::feature_creation::nloptimizer::register_NLOptimizer(m);
    sisso::feature_creation::nloptimizer::register_NLOptimizerClassification(m);
    sisso::feature_creation::nloptimizer::register_NLOptimizerRegression(m);
    sisso::feature_creation::nloptimizer::register_NLOptimizerLogRegression(m);

    m.def("initialize_param_storage",
          &node_value_arrs::initialize_param_storage,
          "@DocString_node_vals_init_param@");
    m.def("set_max_param_depth",
          &nlopt_wrapper::set_max_param_depth,
          "@DocString_nlopt_wrapper_set_max_param_depth@");
    m.def("get_reg_optimizer",
          py::overload_cast<std::vector<int>, std::vector<double>, int, int, double, bool>(
              &nlopt_wrapper::get_reg_optimizer),
          py::arg("task_sizes"),
          py::arg("prop"),
          py::arg("n_rung"),
          py::arg("max_param_depth") = -1,
          py::arg("cauchy_scaling") = 0.5,
          py::arg("use_global") = false,
          "@DocString_nlopt_wrapper_get_reg_optimizer_list_list");

    m.def("get_log_reg_optimizer",
          py::overload_cast<std::vector<int>, std::vector<double>, int, int, double, bool>(
              &nlopt_wrapper::get_log_reg_optimizer),
          py::arg("task_sizes"),
          py::arg("prop"),
          py::arg("n_rung"),
          py::arg("max_param_depth") = -1,
          py::arg("cauchy_scaling") = 0.5,
          py::arg("use_global") = false,
          "@DocString_nlopt_wrapper_get_log_reg_optimizer_list_list");

    m.def("get_class_optimizer",
          py::overload_cast<std::vector<int>, std::vector<double>, int, int>(
              &nlopt_wrapper::get_class_optimizer),
          py::arg("task_sizes"),
          py::arg("prop"),
          py::arg("n_rung"),
          py::arg("max_param_depth") = -1,
          "@DocString_nlopt_wrapper_get_class_optimizer_list_list");
#endif
}

void sisso::register_Inputs(py::module& m)
{
    py::class_<InputParser> inputs(m, "Inputs", "@DocString_cls_input_parser@");
    inputs.def(py::init<>(), "@DocString_inputs_init_default@");
    inputs.def(py::init<std::string>(), py::arg("filename"), "@DocString_inputs_init_filename@");
    inputs.def(py::init<InputParser>(), py::arg("o"), "@DocString_inputs_init_copy@");
    inputs.def("clear_data", &InputParser::clear_data, "@DocString_inputs_clear_data@");
    inputs.def_property_readonly("n_samp", &InputParser::n_samp, "@DocString_inputs_get_n_samp@");
    inputs.def_property_readonly(
        "n_samp_train", &InputParser::n_samp_train, "@DocString_inputs_get_n_samp_train@");
    inputs.def_property_readonly(
        "n_samp_test", &InputParser::n_samp_test, "@DocString_inputs_get_n_samp_test@");
    inputs.def_property("sample_ids_train",
                        &InputParser::sample_ids_train,
                        &InputParser::set_sample_ids_train,
                        "@DocString_inputs_sample_ids_train@");
    inputs.def_property("sample_ids_test",
                        &InputParser::sample_ids_test,
                        &InputParser::set_sample_ids_test,
                        "@DocString_inputs_sample_ids_test@");
    inputs.def_property("task_names",
                        &InputParser::task_names,
                        &InputParser::set_task_names,
                        "@DocString_inputs_task_names@");
    inputs.def_property("task_key",
                        &InputParser::task_key,
                        &InputParser::set_task_key,
                        "@DocString_inputs_get_task_key@");
    inputs.def_property("allowed_param_ops",
                        &InputParser::allowed_param_ops,
                        &InputParser::set_allowed_param_ops,
                        "@DocString_inputs_allowed_param_ops@");
    inputs.def_property("allowed_ops",
                        &InputParser::allowed_ops,
                        &InputParser::set_allowed_ops,
                        "@DocString_inputs_allowed_ops@");
    inputs.def_property("prop_train",
                        &InputParser::prop_train_arr,
                        &InputParser::set_prop_train_arr,
                        "@DocString_inputs_get_prop_train_arr@");
    inputs.def_property("prop_test",
                        &InputParser::prop_test_arr,
                        &InputParser::set_prop_test_arr,
                        "@DocString_inputs_get_prop_test_arr@");
    inputs.def_property("leave_out_inds",
                        &InputParser::leave_out_inds,
                        &InputParser::set_leave_out_inds,
                        "@DocString_inputs_leave_out_inds@");
    inputs.def_property("task_sizes_train",
                        &InputParser::task_sizes_train,
                        &InputParser::set_task_sizes_train,
                        "@DocString_inputs_task_sizes_train@");
    inputs.def_property("task_sizes_test",
                        &InputParser::task_sizes_test,
                        &InputParser::set_task_sizes_test,
                        "@DocString_inputs_task_sizes_test@");
    inputs.def_property("phi_0",
                        &InputParser::phi_0_ptrs,
                        py::overload_cast<std::vector<node_ptr>>(&InputParser::set_phi_0),
                        "@DocString_inputs_phi_0@");
    inputs.def_property("prop_unit",
                        &InputParser::prop_unit,
                        &InputParser::set_prop_unit,
                        "@DocString_inputs_get_prop_unit@");
    inputs.def_property("filename",
                        &InputParser::filename,
                        &InputParser::set_filename,
                        "@DocString_inputs_get_filename@");
    inputs.def_property("data_file",
                        &InputParser::data_file,
                        &InputParser::set_data_file,
                        "@DocString_inputs_get_data_file@");
    inputs.def_property("prop_key",
                        &InputParser::prop_key,
                        &InputParser::set_prop_key,
                        "@DocString_inputs_get_prop_key@");
    inputs.def_property("prop_label",
                        &InputParser::prop_label,
                        &InputParser::set_prop_label,
                        "@DocString_inputs_get_prop_label@");
    inputs.def_property("task_key",
                        &InputParser::task_key,
                        &InputParser::set_task_key,
                        "@DocString_inputs_get_task_key@");
    inputs.def_property("calc_type",
                        &InputParser::calc_type,
                        &InputParser::set_calc_type,
                        "@DocString_inputs_get_calc_type@");
    inputs.def_property("cross_cor_max",
                        &InputParser::cross_cor_max,
                        &InputParser::set_cross_cor_max,
                        "@DocString_inputs_get_cross_cor_max@");
    inputs.def_property("l_bound",
                        &InputParser::l_bound,
                        &InputParser::set_l_bound,
                        "@DocString_inputs_get_l_bound@");
    inputs.def_property("u_bound",
                        &InputParser::u_bound,
                        &InputParser::set_u_bound,
                        "@DocString_inputs_get_u_bound@");
    inputs.def_property(
        "n_dim", &InputParser::n_dim, &InputParser::set_n_dim, "@DocString_inputs_get_n_dim@");
    inputs.def_property("max_rung",
                        &InputParser::max_rung,
                        &InputParser::set_max_rung,
                        "@DocString_inputs_get_max_rung@");
    inputs.def_property("n_rung_store",
                        &InputParser::n_rung_store,
                        &InputParser::set_n_rung_store,
                        "@DocString_inputs_get_n_rung_store@");
    inputs.def_property("n_rung_generate",
                        &InputParser::n_rung_generate,
                        &InputParser::set_n_rung_generate,
                        "@DocString_inputs_get_n_rung_generate@");
    inputs.def_property("n_sis_select",
                        &InputParser::n_sis_select,
                        &InputParser::set_n_sis_select,
                        "@DocString_inputs_get_n_sis_select@");
    inputs.def_property("n_residual",
                        &InputParser::n_residual,
                        &InputParser::set_n_residual,
                        "@DocString_inputs_get_n_residual@");
    inputs.def_property("n_models_store",
                        &InputParser::n_models_store,
                        &InputParser::set_n_models_store,
                        "@DocString_inputs_get_n_models_store@");
    inputs.def_property("max_param_depth",
                        &InputParser::max_param_depth,
                        &InputParser::set_max_param_depth,
                        "@DocString_inputs_get_max_param_depth@");
    inputs.def_property("nlopt_seed",
                        &InputParser::nlopt_seed,
                        &InputParser::set_nlopt_seed,
                        "@DocString_inputs_get_nlopt_seed@");
    inputs.def_property("fix_intercept",
                        &InputParser::fix_intercept,
                        &InputParser::set_fix_intercept,
                        "@DocString_inputs_get_fix_intercept@");
    inputs.def_property("global_param_opt",
                        &InputParser::global_param_opt,
                        &InputParser::set_global_param_opt,
                        "@DocString_inputs_get_global_param_opt@");
    inputs.def_property("reparam_residual",
                        &InputParser::reparam_residual,
                        &InputParser::set_reparam_residual,
                        "@DocString_inputs_get_reparam_residual@");
    inputs.def_property("phi_out_file",
                        &InputParser::phi_out_file,
                        &InputParser::set_phi_out_file,
                        "@DocString_inputs_get_phi_out_file@");
    inputs.def_property("override_n_sis_select",
                        &InputParser::override_n_sis_select,
                        &InputParser::set_override_n_sis_select,
                        "@DocString_inputs_override_n_sis_select@");
    inputs.def_property("max_leaves",
                        &InputParser::max_leaves,
                        &InputParser::set_max_leaves,
                        "@DocString_inputs_max_leaves@");
}

void sisso::feature_creation::register_FeatureSpace(py::module& m)
{
    std::vector<int> inds_default;
    py::class_<FeatureSpace, std::shared_ptr<FeatureSpace>> feat_space(
        m, "FeatureSpace", "@DocString_cls_feat_space@");
    feat_space.def(py::init<InputParser>(), py::arg("inputs"), "@DocString_feat_space_init@");
    feat_space.def(py::init<std::string,
                            std::vector<node_ptr>,
                            std::vector<double>,
                            std::vector<int>,
                            std::string,
                            int,
                            double,
                            std::vector<int>,
                            bool>(),
                   py::arg("feature_file"),
                   py::arg("phi_0"),
                   py::arg("prop"),
                   py::arg("task_sizes"),
                   py::arg("project_type") = "regression",
                   py::arg("n_sis_select") = 1,
                   py::arg("cross_corr_max") = 1.0,
                   py::arg("excluded_inds") = py::list(),
                   py::arg("override_n_sis_select") = false,
                   "@DocString_feat_space_init_file_py_list@");
    feat_space.def("sis",
                   py::overload_cast<const std::vector<double>&>(&FeatureSpace::sis),
                   py::arg("prop"),
                   "@DocString_feat_space_sis_list@");
    feat_space.def("feat_in_phi",
                   &FeatureSpace::feat_in_phi,
                   py::arg("ind"),
                   "@DocString_feat_space_feat_in_phi@");
    feat_space.def("remove_feature",
                   &FeatureSpace::remove_feature,
                   py::arg("ind"),
                   "@DocString_feat_space_remove_feature@");
    feat_space.def("get_feature",
                   &FeatureSpace::get_feature,
                   py::arg("ind"),
                   "@DocString_feat_space_get_feature@");
    feat_space.def("output_phi", &FeatureSpace::output_phi, "@DocString_feat_space_output_phi@");
    feat_space.def(
        py::pickle(&python_pickling::pickle_feat_space, &python_pickling::unpickle_feat_space));
    feat_space.def_property_readonly(
        "phi_selected", &FeatureSpace::phi_selected, "@DocString_feat_space_phi_selected_py@");
    feat_space.def_property_readonly("phi0", &FeatureSpace::phi0, "@DocString_feat_space_phi0_py@");
    feat_space.def_property_readonly("phi", &FeatureSpace::phi, "@DocString_feat_space_phi_py@");
    feat_space.def_property_readonly(
        "scores", &FeatureSpace::scores_py, "@DocString_feat_space_scores_py@");
    feat_space.def_property_readonly(
        "prop", &FeatureSpace::prop_train, "@DocString_feat_space_prop@");
    feat_space.def_property_readonly("task_sizes_train",
                                     &FeatureSpace::task_sizes_train,
                                     "@DocString_feat_space_task_sizes_train_py@");
    feat_space.def_property_readonly(
        "allowed_ops", &FeatureSpace::allowed_ops, "@DocString_feat_space_allowed_ops_py@");
    feat_space.def_property_readonly("allowed_param_ops",
                                     &FeatureSpace::allowed_param_ops,
                                     "@DocString_feat_space_allowed_param_ops_py@");
    feat_space.def_property_readonly(
        "start_rung", &FeatureSpace::start_rung, "@DocString_feat_space_start_rung_py@");
    feat_space.def_property_readonly("feature_space_file",
                                     &FeatureSpace::feature_space_file,
                                     "@DocString_feat_space_feature_space_file@");
    feat_space.def_property_readonly("feature_space_summary_file",
                                     &FeatureSpace::feature_space_summary_file,
                                     "@DocString_feat_space_feature_space_summary_file@");
    feat_space.def_property_readonly(
        "phi_out_file", &FeatureSpace::phi_out_file, "@DocString_feat_space_phi_out_file@");
    feat_space.def_property_readonly(
        "l_bound", &FeatureSpace::l_bound, "@DocString_feat_space_l_bound@");
    feat_space.def_property_readonly(
        "u_bound", &FeatureSpace::u_bound, "@DocString_feat_space_u_bound@");
    feat_space.def_property_readonly(
        "cross_cor_max", &FeatureSpace::cross_cor_max, "@DocString_feat_space_cross_cor_max@");
    feat_space.def_property_readonly(
        "max_rung", &FeatureSpace::max_rung, "@DocString_feat_space_max_rung@");
    feat_space.def_property_readonly(
        "n_sis_select", &FeatureSpace::n_sis_select, "@DocString_feat_space_n_sis_select@");
    feat_space.def_property_readonly(
        "n_samp_train", &FeatureSpace::n_samp_train, "@DocString_feat_space_n_samp_train@");
    feat_space.def_property_readonly(
        "n_feat", &FeatureSpace::n_feat, "@DocString_feat_space_n_feat@");
    feat_space.def_property_readonly(
        "n_rung_store", &FeatureSpace::n_rung_store, "@DocString_feat_space_n_rung_store@");
    feat_space.def_property_readonly("n_rung_generate",
                                     &FeatureSpace::n_rung_generate,
                                     "@DocString_feat_space_n_rung_generate@");
    feat_space.def_property_readonly("parameterized_feats_allowed",
                                     &FeatureSpace::parameterized_feats_allowed,
                                     "@DocString_feat_space_param_feats_allowed@");
}

void sisso::feature_creation::register_Unit(py::module& m)
{
    py::class_<Unit> unit(m, "Unit", "@DocString_cls_unit@");
    unit.def(py::init<>());
    unit.def(py::init<std::map<std::string, double>>());
    unit.def(py::init<std::string>());
    unit.def(py::init<Unit&>());
    unit.def("__str__", &Unit::toString, "@DocString_unit_str@");
    unit.def("__repr__", &Unit::toString, "@DocString_unit_str@");
    unit.def("inverse", &Unit::inverse, "@DocString_unit_inverse@");
    unit.def(py::self * py::self);
    unit.def(py::self / py::self);
    unit.def(py::self *= py::self);
    unit.def(py::self /= py::self);
    unit.def(py::self == py::self);
    unit.def(py::self != py::self);
    unit.def("__pow__", &Unit::operator^, py::arg("pow"), "@DocString_unit_pow@");
    unit.def(py::pickle(&python_pickling::pickle_unit, &python_pickling::unpickle_unit));
    unit.def_property_readonly("latex_str", &Unit::toLatexString, "@DocString_unit_latex_str@");
}

void sisso::feature_creation::register_Domain(py::module& m)
{
    py::class_<Domain> domain(m, "Domain", "@DocString_cls_domain@");
    domain.def(py::init<>(), "@DocString_domain_base_const");
    domain.def(py::init<std::string>(), py::arg("domain_str"), "@DocString_domain_@");
    domain.def(py::init<std::array<double, 2>>(), py::arg("end_pts"), "@DocString_domain_@");
    domain.def(py::init<std::array<double, 2>, std::vector<double>>(),
               py::arg("end_pts"),
               py::arg("excluded_pts"),
               "@DocString_domain_@");
    domain.def(py::init<Domain&>(), py::arg("o"), "@DocString_domain_@");
    domain.def("__str__", &Domain::toString, "@DocString_domain_str@");
    domain.def("__repr__", &Domain::toString, "@DocString_domain_str@");
    domain.def("__pow__", &Domain::operator^, "@DocString_domain_pow@");
    domain.def("__abs__", &Domain::abs, "@DocString_domain_abs@");
    domain.def("abs", &Domain::abs, "@DocString_domain_abs@");
    domain.def("neg", &Domain::neg, "@DocString_domain_neg@");
    domain.def("inv", &Domain::inv, "@DocString_domain_inv@");
    domain.def("exp", &Domain::exp, "@DocString_domain_exp@");
    domain.def("log", &Domain::log, "@DocString_domain_log@");
    domain.def("sqrt", &Domain::sqrt, "@DocString_domain_sqrt@");
    domain.def("cbrt", &Domain::cbrt, "@DocString_domain_cbrt@");
    domain.def("pow", &Domain::pow, py::arg("power"), "@DocString_domain_pow@");
    domain.def("parameterize",
               &Domain::parameterize,
               py::arg("scale"),
               py::arg("shift"),
               "@DocString_domain_param@");
    domain.def(py::self + py::self);
    domain.def(py::self - py::self);
    domain.def(py::self * py::self);
    domain.def(py::self / py::self);
    domain.def(py::self + double());
    domain.def(py::self - double());
    domain.def(py::self * double());
    domain.def(py::self / double());
    domain.def(double() + py::self);
    domain.def(double() - py::self);
    domain.def(double() * py::self);
    domain.def(double() / py::self);
    domain.def_property_readonly(
        "end_points", &Domain::end_points, "@DocString_domain_end_points@");
    domain.def_property_readonly(
        "excluded_pts", &Domain::excluded_pts, "@DocString_domain_excluded_pts@");
}

#ifdef PARAMETERIZE
void sisso::feature_creation::nloptimizer::register_NLOptimizer(py::module& m)
{
    py::class_<NLOptimizer, std::shared_ptr<NLOptimizer>> nl_opt(
        m, "NLOptimizer", "@DocString_cls_nlopt_optimizer@");
    nl_opt.def("optimize_feature_params",
               &NLOptimizer::optimize_feature_params,
               py::arg("feat"),
               "@DocString_nloptimizer_optimize_feature_params@");
}

void sisso::feature_creation::nloptimizer::register_NLOptimizerClassification(py::module& m)
{
    py::class_<NLOptimizerClassification, std::shared_ptr<NLOptimizerClassification>, NLOptimizer>
        nl_opt(m, "NLOptimizerClassification", "@DocString_cls_nlopt_class@");
}

void sisso::feature_creation::nloptimizer::register_NLOptimizerRegression(py::module& m)
{
    py::class_<NLOptimizerRegression, std::shared_ptr<NLOptimizerRegression>, NLOptimizer> nl_opt(
        m, "NLOptimizerRegression", "@DocString_cls_nlopt_reg@");
}

void sisso::feature_creation::nloptimizer::register_NLOptimizerLogRegression(py::module& m)
{
    py::class_<NLOptimizerLogRegression, std::shared_ptr<NLOptimizerLogRegression>, NLOptimizer>
        nl_opt(m, "NLOptimizerLogRegression", "@DocString_cls_nlopt_log_reg@");
}

#endif

void sisso::feature_creation::node::register_Node(py::module& m)
{
    py::class_<Node, std::shared_ptr<Node>, PyNode<>> node(m, "Node", "@DocString_cls_node@");
    node.def_property_readonly("arr_ind", &Node::arr_ind, "@DocString_node_arr_ind@");
    node.def_property_readonly("feat_ind", &Node::feat_ind, "@DocString_node_feat_ind@");
    node.def_property_readonly("n_samp", &Node::n_samp, "@DocString_node_n_samp@");
    node.def_property_readonly("n_samp_test", &Node::n_samp_test, "@DocString_node_n_samp_test@");
    node.def_property_readonly("primary_feat_decomp",
                               &Node::primary_feature_decomp,
                               "@DocString_node_primary_feature_decomp@");
    node.def_property_readonly("rung", &Node::rung, "@DocString_node_rung@");
    node.def_property_readonly("n_leaves", &Node::n_leaves, "@DocString_node_n_leaves@");
    node.def_property_readonly("unit", &Node::unit, "@DocString_node_unit@");
    node.def_property_readonly(
        "expr", py::overload_cast<>(&Node::expr, py::const_), "@DocString_node_expr@");
    node.def_property_readonly("matlab_fxn_expr",
                               py::overload_cast<>(&Node::matlab_fxn_expr, py::const_),
                               "@DocString_node_matlab_expr@");
    node.def_property_readonly(
        "postfix_expr", &Node::postfix_expr, "@DocString_node_postfix_expr@");
    node.def_property_readonly("latex_expr", &Node::latex_expr, "@DocString_node_latex_expr@");
    node.def_property_readonly("n_feats", &Node::n_feats, "@DocString_node_n_feats@");
    node.def_property_readonly("value", &Node::value_py, "@DocString_node_value_py@");
    node.def_property_readonly(
        "test_value", &Node::test_value_py, "@DocString_node_test_value_py@");
    node.def_property_readonly(
        "x_in_expr_list", &Node::x_in_expr_list, "@DocString_node_x_in_expr@");
    node.def_property_readonly("is_nan", &Node::is_nan, "@DocString_node_is_nan@");
    node.def_property_readonly("is_const", &Node::is_const, "@DocString_node_is_const@");
    node.def_property_readonly(
        "domain", py::overload_cast<>(&Node::domain, py::const_), "@DocString_node_domain@");
    node.def_property(
        "selected", &Node::selected, &Node::set_selected, "@DocString_node_selected@");
    node.def_property(
        "d_mat_ind", &Node::d_mat_ind, &Node::set_d_mat_ind, "@DocString_node_d_mat_ind@");
    node.def("reindex",
             py::overload_cast<unsigned long int>(&Node::reindex),
             py::arg("feat_ind"),
             "@DocString_node_reindex_1@");
    node.def("reindex",
             py::overload_cast<unsigned long int, unsigned long int>(&Node::reindex),
             py::arg("feat_ind"),
             py::arg("arr_ind"),
             "@DocString_node_reindex_2@");
    node.def("feat", &Node::feat, py::arg("feat_ind"), "@DocString_node_feat@");
    node.def("set_test_value",
             py::overload_cast<int, bool>(&Node::set_test_value, py::const_),
             py::arg("offset"),
             py::arg("for_comp"),
             "@DocString_node_set_test_value@");
    node.def("set_value",
             py::overload_cast<int, bool>(&Node::set_value, py::const_),
             py::arg("offset"),
             py::arg("for_comp"),
             "@DocString_node_set_value@");
#ifdef PARAMETERIZE
    node.def_property_readonly("n_params_possible",
                               py::overload_cast<>(&Node::n_params_possible, py::const_),
                               "@DocString_node_n_params_possible@");
    node.def_property_readonly("n_params", &Node::n_params, "@DocString_node_n_params@");
    node.def_property_readonly("parameters", &Node::parameters, "@DocString_node_parameters@");
#endif
}

void sisso::feature_creation::node::register_FeatureNode(py::module& m)
{
    py::class_<FeatureNode, std::shared_ptr<FeatureNode>, Node, PyFeatureNode<>> feat_node(
        m, "FeatureNode", "@DocString_cls_feat_node@");
    feat_node.def(py::init<int, std::string, std::vector<double>, std::vector<double>, Unit>(),
                  py::arg("feat_ind"),
                  py::arg("expr"),
                  py::arg("value"),
                  py::arg("test_value"),
                  py::arg("unit"),
                  "@DocString_feat_node_init_list@");
    feat_node.def(
        py::init<int, std::string, std::vector<double>, std::vector<double>, Unit, Domain>(),
        py::arg("feat_ind"),
        py::arg("expr"),
        py::arg("value"),
        py::arg("test_value"),
        py::arg("unit"),
        py::arg("domain"),
        "@DocString_feat_node_init_list_domain@");
    feat_node.def(
        py::pickle(&python_pickling::pickle_feat_node, &python_pickling::unpickle_feat_node));
}

void sisso::feature_creation::node::register_ModelNode(py::module& m)
{
    py::class_<ModelNode, std::shared_ptr<ModelNode>, FeatureNode, Node, PyFeatureNode<ModelNode>>
        model_node(m, "ModelNode", "@DocString_cls_model_node@");

    model_node.def(py::init<int,
                            int,
                            std::string,
                            std::string,
                            std::string,
                            std::string,
                            std::vector<double>,
                            std::vector<double>,
                            std::vector<std::string>,
                            Unit>(),
                   py::arg("feat_ind"),
                   py::arg("rung"),
                   py::arg("expr"),
                   py::arg("latex_expr"),
                   py::arg("postfix_expr"),
                   py::arg("matlab_fxn_expr"),
                   py::arg("value"),
                   py::arg("test_value"),
                   py::arg("x_in_expr_list"),
                   py::arg("unit"),
                   "@DocString_model_node_init@");
    model_node.def(
        py::init<node_ptr>(), py::arg("in_node"), "@DocString_model_node_init_node_ptr@");
    model_node.def(
        "eval_many",
        py::overload_cast<std::map<std::string, py::array_t<double>>>(&ModelNode::eval_many_py),
        py::arg("x_in"),
        "@DocString_model_node_eval_many_dict@");
    model_node.def("eval_many",
                   py::overload_cast<py::array_t<double>>(&ModelNode::eval_many_py),
                   py::arg("x_in"),
                   "@DocString_model_node_eval_many_arr@");
    model_node.def("eval",
                   py::overload_cast<std::vector<double>>(&ModelNode::eval),
                   py::arg("x_in"),
                   "@DocString_model_node_eval_list@");
    model_node.def("eval",
                   py::overload_cast<std::map<std::string, double>>(&ModelNode::eval),
                   py::arg("x_in"),
                   "@DocString_model_node_eval_dict@");
    model_node.def(
        py::pickle(&python_pickling::pickle_model_node, &python_pickling::unpickle_model_node));
}

void sisso::feature_creation::node::register_AddNode(py::module& m)
{
    py::class_<AddNode, std::shared_ptr<AddNode>, OperatorNode<2>, PyAddNode<>> add_node(
        m, "AddNode", "@DocString_cls_add_node@");
    add_node.def(py::init<node_ptr, node_ptr, int, int, double, double>(),
                 py::arg("feat_1"),
                 py::arg("feat_2"),
                 py::arg("feat_ind"),
                 py::arg("max_leaves") = 1073741824,
                 py::arg("l_bound") = 1e-50,
                 py::arg("u_bound") = 1e50,
                 "@DocString_add_node_init@");
    add_node.def(py::pickle(&python_pickling::pickle_op_node<AddNode, 2>,
                            &python_pickling::unpickle_op_node<AddNode, 2>));
}

void sisso::feature_creation::node::register_SubNode(py::module& m)
{
    py::class_<SubNode, std::shared_ptr<SubNode>, OperatorNode<2>, PySubNode<>> sub_node(
        m, "SubNode", "@DocString_cls_sub_node@");
    sub_node.def(py::init<node_ptr, node_ptr, int, int, double, double>(),
                 py::arg("feat_1"),
                 py::arg("feat_2"),
                 py::arg("feat_ind"),
                 py::arg("max_leaves") = 1073741824,
                 py::arg("l_bound") = 1e-50,
                 py::arg("u_bound") = 1e50,
                 "@DocString_sub_node_init@");
    sub_node.def(py::pickle(&python_pickling::pickle_op_node<SubNode, 2>,
                            &python_pickling::unpickle_op_node<SubNode, 2>));
}

void sisso::feature_creation::node::register_DivNode(py::module& m)
{
    py::class_<DivNode, std::shared_ptr<DivNode>, OperatorNode<2>, PyDivNode<>> div_node(
        m, "DivNode", "@DocString_cls_div_node@");
    div_node.def(py::init<node_ptr, node_ptr, int, int, double, double>(),
                 py::arg("feat_1"),
                 py::arg("feat_2"),
                 py::arg("feat_ind"),
                 py::arg("max_leaves") = 1073741824,
                 py::arg("l_bound") = 1e-50,
                 py::arg("u_bound") = 1e50,
                 "@DocString_div_node_init@");
    div_node.def(py::pickle(&python_pickling::pickle_op_node<DivNode, 2>,
                            &python_pickling::unpickle_op_node<DivNode, 2>));
}

void sisso::feature_creation::node::register_MultNode(py::module& m)
{
    py::class_<MultNode, std::shared_ptr<MultNode>, OperatorNode<2>, PyMultNode<>> mult_node(
        m, "MultNode", "@DocString_cls_mult_node@");
    mult_node.def(py::init<node_ptr, node_ptr, int, int, double, double>(),
                  py::arg("feat_1"),
                  py::arg("feat_2"),
                  py::arg("feat_ind"),
                  py::arg("max_leaves") = 1073741824,
                  py::arg("l_bound") = 1e-50,
                  py::arg("u_bound") = 1e50,
                  "@DocString_mult_node_init@");
    mult_node.def(py::pickle(&python_pickling::pickle_op_node<MultNode, 2>,
                             &python_pickling::unpickle_op_node<MultNode, 2>));
}

void sisso::feature_creation::node::register_AbsDiffNode(py::module& m)
{
    py::class_<AbsDiffNode, std::shared_ptr<AbsDiffNode>, OperatorNode<2>, PyAbsDiffNode<>>
        abs_diff_node(m, "AbsDiffNode", "@DocString_cls_abs_diff_node@");
    abs_diff_node.def(py::init<node_ptr, node_ptr, int, int, double, double>(),
                      py::arg("feat_1"),
                      py::arg("feat_2"),
                      py::arg("feat_ind"),
                      py::arg("max_leaves") = 1073741824,
                      py::arg("l_bound") = 1e-50,
                      py::arg("u_bound") = 1e50,
                      "@DocString_abs_diff_node_init@");
    abs_diff_node.def(py::pickle(&python_pickling::pickle_op_node<AbsDiffNode, 2>,
                                 &python_pickling::unpickle_op_node<AbsDiffNode, 2>));
}

void sisso::feature_creation::node::register_AbsNode(py::module& m)
{
    py::class_<AbsNode, std::shared_ptr<AbsNode>, OperatorNode<1>, PyAbsNode<>> abs_node(
        m, "AbsNode", "@DocString_cls_abs_node@");
    abs_node.def(py::init<node_ptr, int, double, double>(),
                 py::arg("feat"),
                 py::arg("feat_ind"),
                 py::arg("l_bound") = 1e-50,
                 py::arg("u_bound") = 1e50,
                 "@DocString_abs_node_init@");
    abs_node.def(py::pickle(&python_pickling::pickle_op_node<AbsNode, 1>,
                            &python_pickling::unpickle_op_node<AbsNode, 1>));
}

void sisso::feature_creation::node::register_InvNode(py::module& m)
{
    py::class_<InvNode, std::shared_ptr<InvNode>, OperatorNode<1>, PyInvNode<>> inv_node(
        m, "InvNode", "@DocString_cls_inv_node@");
    inv_node.def(py::init<node_ptr, int, double, double>(),
                 py::arg("feat"),
                 py::arg("feat_ind"),
                 py::arg("l_bound") = 1e-50,
                 py::arg("u_bound") = 1e50,
                 "@DocString_inv_node_init@");
    inv_node.def(py::pickle(&python_pickling::pickle_op_node<InvNode, 1>,
                            &python_pickling::unpickle_op_node<InvNode, 1>));
}

void sisso::feature_creation::node::register_LogNode(py::module& m)
{
    py::class_<LogNode, std::shared_ptr<LogNode>, OperatorNode<1>, PyLogNode<>> log_node(
        m, "LogNode", "@DocString_cls_log_node@");
    log_node.def(py::init<node_ptr, int, double, double>(),
                 py::arg("feat"),
                 py::arg("feat_ind"),
                 py::arg("l_bound") = 1e-50,
                 py::arg("u_bound") = 1e50,
                 "@DocString_log_node_init@");
    log_node.def(py::pickle(&python_pickling::pickle_op_node<LogNode, 1>,
                            &python_pickling::unpickle_op_node<LogNode, 1>));
}

void sisso::feature_creation::node::register_ExpNode(py::module& m)
{
    py::class_<ExpNode, std::shared_ptr<ExpNode>, OperatorNode<1>, PyExpNode<>> exp_node(
        m, "ExpNode", "@DocString_cls_exp_node@");
    exp_node.def(py::init<node_ptr, int, double, double>(),
                 py::arg("feat"),
                 py::arg("feat_ind"),
                 py::arg("l_bound") = 1e-50,
                 py::arg("u_bound") = 1e50,
                 "@DocString_exp_node_init@");
    exp_node.def(py::pickle(&python_pickling::pickle_op_node<ExpNode, 1>,
                            &python_pickling::unpickle_op_node<ExpNode, 1>));
}

void sisso::feature_creation::node::register_NegExpNode(py::module& m)
{
    py::class_<NegExpNode, std::shared_ptr<NegExpNode>, OperatorNode<1>, PyNegExpNode<>>
        neg_exp_node(m, "NegExpNode", "@DocString_cls_neg_exp_node@");
    neg_exp_node.def(py::init<node_ptr, int, double, double>(),
                     py::arg("feat"),
                     py::arg("feat_ind"),
                     py::arg("l_bound") = 1e-50,
                     py::arg("u_bound") = 1e50,
                     "@DocString_neg_exp_node_init@");
    neg_exp_node.def(py::pickle(&python_pickling::pickle_op_node<NegExpNode, 1>,
                                &python_pickling::unpickle_op_node<NegExpNode, 1>));
}

void sisso::feature_creation::node::register_SinNode(py::module& m)
{
    py::class_<SinNode, std::shared_ptr<SinNode>, OperatorNode<1>, PySinNode<>> sin_node(
        m, "SinNode", "@DocString_cls_sin_node@");
    sin_node.def(py::init<node_ptr, int, double, double>(),
                 py::arg("feat"),
                 py::arg("feat_ind"),
                 py::arg("l_bound") = 1e-50,
                 py::arg("u_bound") = 1e50,
                 "@DocString_sin_node_init@");
    sin_node.def(py::pickle(&python_pickling::pickle_op_node<SinNode, 1>,
                            &python_pickling::unpickle_op_node<SinNode, 1>));
}

void sisso::feature_creation::node::register_CosNode(py::module& m)
{
    py::class_<CosNode, std::shared_ptr<CosNode>, OperatorNode<1>, PyCosNode<>> cos_node(
        m, "CosNode", "@DocString_cls_cos_node@");
    cos_node.def(py::init<node_ptr, int, double, double>(),
                 py::arg("feat"),
                 py::arg("feat_ind"),
                 py::arg("l_bound") = 1e-50,
                 py::arg("u_bound") = 1e50,
                 "@DocString_cos_node_init@");
    cos_node.def(py::pickle(&python_pickling::pickle_op_node<CosNode, 1>,
                            &python_pickling::unpickle_op_node<CosNode, 1>));
}

void sisso::feature_creation::node::register_CbNode(py::module& m)
{
    py::class_<CbNode, std::shared_ptr<CbNode>, OperatorNode<1>, PyCbNode<>> cb_node(
        m, "CbNode", "@DocString_cls_cb_node@");
    cb_node.def(py::init<node_ptr, int, double, double>(),
                py::arg("feat"),
                py::arg("feat_ind"),
                py::arg("l_bound") = 1e-50,
                py::arg("u_bound") = 1e50,
                "@DocString_cb_node_init@");
    cb_node.def(py::pickle(&python_pickling::pickle_op_node<CbNode, 1>,
                           &python_pickling::unpickle_op_node<CbNode, 1>));
}

void sisso::feature_creation::node::register_CbrtNode(py::module& m)
{
    py::class_<CbrtNode, std::shared_ptr<CbrtNode>, OperatorNode<1>, PyCbrtNode<>> cbrt_node(
        m, "CbrtNode", "@DocString_cls_cbrt_node@");
    cbrt_node.def(py::init<node_ptr, int, double, double>(),
                  py::arg("feat"),
                  py::arg("feat_ind"),
                  py::arg("l_bound") = 1e-50,
                  py::arg("u_bound") = 1e50,
                  "@DocString_cbrt_node_init@");
    cbrt_node.def(py::pickle(&python_pickling::pickle_op_node<CbrtNode, 1>,
                             &python_pickling::unpickle_op_node<CbrtNode, 1>));
}

void sisso::feature_creation::node::register_SqNode(py::module& m)
{
    py::class_<SqNode, std::shared_ptr<SqNode>, OperatorNode<1>, PySqNode<>> sq_node(
        m, "SqNode", "@DocString_cls_sq_node@");
    sq_node.def(py::init<node_ptr, int, double, double>(),
                py::arg("feat"),
                py::arg("feat_ind"),
                py::arg("l_bound") = 1e-50,
                py::arg("u_bound") = 1e50,
                "@DocString_sq_node_init@");
    sq_node.def(py::pickle(&python_pickling::pickle_op_node<SqNode, 1>,
                           &python_pickling::unpickle_op_node<SqNode, 1>));
}

void sisso::feature_creation::node::register_SqrtNode(py::module& m)
{
    py::class_<SqrtNode, std::shared_ptr<SqrtNode>, OperatorNode<1>, PySqrtNode<>> sqrt_node(
        m, "SqrtNode", "@DocString_cls_sqrt_node@");
    sqrt_node.def(py::init<node_ptr, int, double, double>(),
                  py::arg("feat"),
                  py::arg("feat_ind"),
                  py::arg("l_bound") = 1e-50,
                  py::arg("u_bound") = 1e50,
                  "@DocString_sqrt_node_init@");
    sqrt_node.def(py::pickle(&python_pickling::pickle_op_node<SqrtNode, 1>,
                             &python_pickling::unpickle_op_node<SqrtNode, 1>));
}

void sisso::feature_creation::node::register_SixPowNode(py::module& m)
{
    py::class_<SixPowNode, std::shared_ptr<SixPowNode>, OperatorNode<1>, PySixPowNode<>>
        six_pow_node(m, "SixPowNode", "@DocString_cls_six_pow_node@");
    six_pow_node.def(py::init<node_ptr, int, double, double>(),
                     py::arg("feat"),
                     py::arg("feat_ind"),
                     py::arg("l_bound") = 1e-50,
                     py::arg("u_bound") = 1e50,
                     "@DocString_six_pow_node_init@");
    six_pow_node.def(py::pickle(&python_pickling::pickle_op_node<SixPowNode, 1>,
                                &python_pickling::unpickle_op_node<SixPowNode, 1>));
}

#ifdef PARAMETERIZE
void sisso::feature_creation::node::register_AddParamNode(py::module& m)
{
    py::class_<AddParamNode, std::shared_ptr<AddParamNode>, AddNode, PyAddNode<AddParamNode>>
        add_param_node(m, "AddParamNode", "@DocString_cls_param_add_node@");
    add_param_node.def(py::init<node_ptr, node_ptr, int, int, double, double>(),
                       py::arg("feat_1"),
                       py::arg("feat_2"),
                       py::arg("feat_ind"),
                       py::arg("max_leaves") = 1073741824,
                       py::arg("l_bound") = 1e-50,
                       py::arg("u_bound") = 1e50,
                       "@DocString_add_param_node_init@");
    add_param_node.def(py::pickle(&python_pickling::pickle_param_op_node<AddParamNode, 2>,
                                  &python_pickling::unpickle_param_op_node<AddParamNode, 2>));
}

void sisso::feature_creation::node::register_SubParamNode(py::module& m)
{
    py::class_<SubParamNode, std::shared_ptr<SubParamNode>, SubNode, PySubNode<SubParamNode>>
        sub_param_node(m, "SubParamNode", "@DocString_cls_sub_param_node@");
    sub_param_node.def(py::init<node_ptr, node_ptr, int, int, double, double>(),
                       py::arg("feat_1"),
                       py::arg("feat_2"),
                       py::arg("feat_ind"),
                       py::arg("max_leaves") = 1073741824,
                       py::arg("l_bound") = 1e-50,
                       py::arg("u_bound") = 1e50,
                       "@DocString_sub_param_node_init@");
    sub_param_node.def(py::pickle(&python_pickling::pickle_param_op_node<SubParamNode, 2>,
                                  &python_pickling::unpickle_param_op_node<SubParamNode, 2>));
}

void sisso::feature_creation::node::register_DivParamNode(py::module& m)
{
    py::class_<DivParamNode, std::shared_ptr<DivParamNode>, DivNode, PyDivNode<DivParamNode>>
        div_param_node(m, "DivParamNode", "@DocString_cls_div_param_node@");
    div_param_node.def(py::init<node_ptr, node_ptr, int, int, double, double>(),
                       py::arg("feat_1"),
                       py::arg("feat_2"),
                       py::arg("feat_ind"),
                       py::arg("max_leaves") = 1073741824,
                       py::arg("l_bound") = 1e-50,
                       py::arg("u_bound") = 1e50,
                       "@DocString_div_param_node_init@");
    div_param_node.def(py::pickle(&python_pickling::pickle_param_op_node<DivParamNode, 2>,
                                  &python_pickling::unpickle_param_op_node<DivParamNode, 2>));
}

void sisso::feature_creation::node::register_MultParamNode(py::module& m)
{
    py::class_<MultParamNode, std::shared_ptr<MultParamNode>, MultNode, PyMultNode<MultParamNode>>
        mult_param_node(m, "MultParamNode", "@DocString_cls_mult_param_node@");
    mult_param_node.def(py::init<node_ptr, node_ptr, int, int, double, double>(),
                        py::arg("feat_1"),
                        py::arg("feat_2"),
                        py::arg("feat_ind"),
                        py::arg("max_leaves") = 1073741824,
                        py::arg("l_bound") = 1e-50,
                        py::arg("u_bound") = 1e50,
                        "@DocString_mult_param_node_init@");
    mult_param_node.def(py::pickle(&python_pickling::pickle_param_op_node<MultParamNode, 2>,
                                   &python_pickling::unpickle_param_op_node<MultParamNode, 2>));
}

void sisso::feature_creation::node::register_AbsDiffParamNode(py::module& m)
{
    py::class_<AbsDiffParamNode,
               std::shared_ptr<AbsDiffParamNode>,
               AbsDiffNode,
               PyAbsDiffNode<AbsDiffParamNode>>
        abs_diff_param_node(m, "AbsDiffParamNode", "@DocString_cls_abs_diff_param_node@");
    abs_diff_param_node.def(py::init<node_ptr, node_ptr, int, int, double, double>(),
                            py::arg("feat_1"),
                            py::arg("feat_2"),
                            py::arg("feat_ind"),
                            py::arg("max_leaves") = 1073741824,
                            py::arg("l_bound") = 1e-50,
                            py::arg("u_bound") = 1e50,
                            "@DocString_abs_diff_param_node_init@");
    abs_diff_param_node.def(
        py::pickle(&python_pickling::pickle_param_op_node<AbsDiffParamNode, 2>,
                   &python_pickling::unpickle_param_op_node<AbsDiffParamNode, 2>));
}

void sisso::feature_creation::node::register_AbsParamNode(py::module& m)
{
    py::class_<AbsParamNode, std::shared_ptr<AbsParamNode>, AbsNode, PyAbsNode<AbsParamNode>>
        abs_param_node(m, "AbsParamNode", "@DocString_cls_abs_param_node@");
    abs_param_node.def(py::init<node_ptr, int, double, double>(),
                       py::arg("feat"),
                       py::arg("feat_ind"),
                       py::arg("l_bound") = 1e-50,
                       py::arg("u_bound") = 1e50,
                       "@DocString_abs_param_node_init@");
    abs_param_node.def(py::pickle(&python_pickling::pickle_param_op_node<AbsParamNode, 1>,
                                  &python_pickling::unpickle_param_op_node<AbsParamNode, 1>));
}

void sisso::feature_creation::node::register_InvParamNode(py::module& m)
{
    py::class_<InvParamNode, std::shared_ptr<InvParamNode>, InvNode, PyInvNode<InvParamNode>>
        inv_param_node(m, "InvParamNode", "@DocString_cls_inv_param_node@");
    inv_param_node.def(py::init<node_ptr, int, double, double>(),
                       py::arg("feat"),
                       py::arg("feat_ind"),
                       py::arg("l_bound") = 1e-50,
                       py::arg("u_bound") = 1e50,
                       "@DocString_inv_param_node_init@");
    inv_param_node.def(py::pickle(&python_pickling::pickle_param_op_node<InvParamNode, 1>,
                                  &python_pickling::unpickle_param_op_node<InvParamNode, 1>));
}

void sisso::feature_creation::node::register_LogParamNode(py::module& m)
{
    py::class_<LogParamNode, std::shared_ptr<LogParamNode>, LogNode, PyLogNode<LogParamNode>>
        log_param_node(m, "LogParamNode", "@DocString_cls_log_param_node@");
    log_param_node.def(py::init<node_ptr, int, double, double>(),
                       py::arg("feat"),
                       py::arg("feat_ind"),
                       py::arg("l_bound") = 1e-50,
                       py::arg("u_bound") = 1e50,
                       "@DocString_log_param_node_init@");
    log_param_node.def(py::pickle(&python_pickling::pickle_param_op_node<LogParamNode, 1>,
                                  &python_pickling::unpickle_param_op_node<LogParamNode, 1>));
}

void sisso::feature_creation::node::register_ExpParamNode(py::module& m)
{
    py::class_<ExpParamNode, std::shared_ptr<ExpParamNode>, ExpNode, PyExpNode<ExpParamNode>>
        exp_param_node(m, "ExpParamNode", "@DocString_cls_exp_param_node@");
    exp_param_node.def(py::init<node_ptr, int, double, double>(),
                       py::arg("feat"),
                       py::arg("feat_ind"),
                       py::arg("l_bound") = 1e-50,
                       py::arg("u_bound") = 1e50,
                       "@DocString_exp_param_node_init@");
    exp_param_node.def(py::pickle(&python_pickling::pickle_param_op_node<ExpParamNode, 1>,
                                  &python_pickling::unpickle_param_op_node<ExpParamNode, 1>));
}

void sisso::feature_creation::node::register_NegExpParamNode(py::module& m)
{
    py::class_<NegExpParamNode,
               std::shared_ptr<NegExpParamNode>,
               NegExpNode,
               PyNegExpNode<NegExpParamNode>>
        neg_exp_param_node(m, "NegExpParamNode", "@DocString_cls_neg_exp_param_node@");
    neg_exp_param_node.def(py::init<node_ptr, int, double, double>(),
                           py::arg("feat"),
                           py::arg("feat_ind"),
                           py::arg("l_bound") = 1e-50,
                           py::arg("u_bound") = 1e50,
                           "@DocString_neg_exp_param_node_init@");
    neg_exp_param_node.def(
        py::pickle(&python_pickling::pickle_param_op_node<NegExpParamNode, 1>,
                   &python_pickling::unpickle_param_op_node<NegExpParamNode, 1>));
}

void sisso::feature_creation::node::register_SinParamNode(py::module& m)
{
    py::class_<SinParamNode, std::shared_ptr<SinParamNode>, SinNode, PySinNode<SinParamNode>>
        sin_param_node(m, "SinParamNode", "@DocString_cls_sin_param_node@");
    sin_param_node.def(py::init<node_ptr, int, double, double>(),
                       py::arg("feat"),
                       py::arg("feat_ind"),
                       py::arg("l_bound") = 1e-50,
                       py::arg("u_bound") = 1e50,
                       "@DocString_sin_param_node_init@");
    sin_param_node.def(py::pickle(&python_pickling::pickle_param_op_node<SinParamNode, 1>,
                                  &python_pickling::unpickle_param_op_node<SinParamNode, 1>));
}

void sisso::feature_creation::node::register_CosParamNode(py::module& m)
{
    py::class_<CosParamNode, std::shared_ptr<CosParamNode>, CosNode, PyCosNode<CosParamNode>>
        cos_param_node(m, "CosParamNode", "@DocString_cls_cos_param_node@");
    cos_param_node.def(py::init<node_ptr, int, double, double>(),
                       py::arg("feat"),
                       py::arg("feat_ind"),
                       py::arg("l_bound") = 1e-50,
                       py::arg("u_bound") = 1e50,
                       "@DocString_cos_param_node_init@");
    cos_param_node.def(py::pickle(&python_pickling::pickle_param_op_node<CosParamNode, 1>,
                                  &python_pickling::unpickle_param_op_node<CosParamNode, 1>));
}

void sisso::feature_creation::node::register_CbParamNode(py::module& m)
{
    py::class_<CbParamNode, std::shared_ptr<CbParamNode>, CbNode, PyCbNode<CbParamNode>>
        cb_param_node(m, "CbParamNode", "@DocString_cls_cb_param_node@");
    cb_param_node.def(py::init<node_ptr, int, double, double>(),
                      py::arg("feat"),
                      py::arg("feat_ind"),
                      py::arg("l_bound") = 1e-50,
                      py::arg("u_bound") = 1e50,
                      "@DocString_cb_param_node_init@");
    cb_param_node.def(py::pickle(&python_pickling::pickle_param_op_node<CbParamNode, 1>,
                                 &python_pickling::unpickle_param_op_node<CbParamNode, 1>));
}

void sisso::feature_creation::node::register_CbrtParamNode(py::module& m)
{
    py::class_<CbrtParamNode, std::shared_ptr<CbrtParamNode>, CbrtNode, PyCbrtNode<CbrtParamNode>>
        cbrt_param_node(m, "CbrtParamNode", "@DocString_cls_cbrt_param_node@");
    cbrt_param_node.def(py::init<node_ptr, int, double, double>(),
                        py::arg("feat"),
                        py::arg("feat_ind"),
                        py::arg("l_bound") = 1e-50,
                        py::arg("u_bound") = 1e50,
                        "@DocString_cbrt_param_node_init@");
    cbrt_param_node.def(py::pickle(&python_pickling::pickle_param_op_node<CbrtParamNode, 1>,
                                   &python_pickling::unpickle_param_op_node<CbrtParamNode, 1>));
}

void sisso::feature_creation::node::register_SqParamNode(py::module& m)
{
    py::class_<SqParamNode, std::shared_ptr<SqParamNode>, SqNode, PySqNode<SqParamNode>>
        sq_param_node(m, "SqParamNode", "@DocString_cls_sq_param_node@");
    sq_param_node.def(py::init<node_ptr, int, double, double>(),
                      py::arg("feat"),
                      py::arg("feat_ind"),
                      py::arg("l_bound") = 1e-50,
                      py::arg("u_bound") = 1e50,
                      "@DocString_sq_param_node_init@");
    sq_param_node.def(py::pickle(&python_pickling::pickle_param_op_node<SqParamNode, 1>,
                                 &python_pickling::unpickle_param_op_node<SqParamNode, 1>));
}

void sisso::feature_creation::node::register_SqrtParamNode(py::module& m)
{
    py::class_<SqrtParamNode, std::shared_ptr<SqrtParamNode>, SqrtNode, PySqrtNode<SqrtParamNode>>
        sqrt_param_node(m, "SqrtParamNode", "@DocString_cls_sqrt_param_node@");
    sqrt_param_node.def(py::init<node_ptr, int, double, double>(),
                        py::arg("feat"),
                        py::arg("feat_ind"),
                        py::arg("l_bound") = 1e-50,
                        py::arg("u_bound") = 1e50,
                        "@DocString_sqrt_param_node_init@");
    sqrt_param_node.def(py::pickle(&python_pickling::pickle_param_op_node<SqrtParamNode, 1>,
                                   &python_pickling::unpickle_param_op_node<SqrtParamNode, 1>));
}

void sisso::feature_creation::node::register_SixPowParamNode(py::module& m)
{
    py::class_<SixPowParamNode,
               std::shared_ptr<SixPowParamNode>,
               SixPowNode,
               PySixPowNode<SixPowParamNode>>
        six_pow_param_node(m, "SixPowParamNode", "@DocString_cls_six_pow_param_node@");
    six_pow_param_node.def(py::init<node_ptr, int, double, double>(),
                           py::arg("feat"),
                           py::arg("feat_ind"),
                           py::arg("l_bound") = 1e-50,
                           py::arg("u_bound") = 1e50,
                           "@DocString_six_pow_param_node_init@");
    six_pow_param_node.def(
        py::pickle(&python_pickling::pickle_param_op_node<SixPowParamNode, 1>,
                   &python_pickling::unpickle_param_op_node<SixPowParamNode, 1>));
}
#endif

void sisso::descriptor_identifier::register_Model(py::module& m)
{
    py::class_<Model, std::shared_ptr<Model>, PyModel<>> model(m, "Model", "@DocString_cls_model@");
    model.def("eval",
              py::overload_cast<std::vector<double>>(&Model::eval_py, py::const_),
              py::arg("x_in"),
              "@DocString_model_eval_list@");
    model.def("eval",
              py::overload_cast<std::map<std::string, double>>(&Model::eval_py, py::const_),
              py::arg("x_in"),
              "@DocString_model_eval_dict@");
    model.def("eval_many",
              py::overload_cast<py::array_t<double>>(&Model::eval_many_py, py::const_),
              py::arg("x_in"),
              "@DocString_model_eval_many_arr@");
    model.def("eval_many",
              py::overload_cast<std::map<std::string, py::array_t<double>>>(&Model::eval_many_py,
                                                                            py::const_),
              py::arg("x_in"),
              "@DocString_model_eval_many_dict@");
    model.def("eval",
              py::overload_cast<std::vector<double>, std::string>(&Model::eval, py::const_),
              py::arg("x_in"),
              py::arg("task"),
              "@DocString_model_eval_list_task@");
    model.def(
        "eval",
        py::overload_cast<std::map<std::string, double>, std::string>(&Model::eval, py::const_),
        py::arg("x_in"),
        py::arg("task"),
        "@DocString_model_eval_dict_task@");
    model.def("eval_many",
              py::overload_cast<py::array_t<double>, std::vector<std::string>>(&Model::eval_many_py,
                                                                               py::const_),
              py::arg("x_in"),
              py::arg("tasks"),
              "@DocString_model_eval_many_arr_task_list@");

    model.def(
        "eval_many",
        py::overload_cast<std::map<std::string, py::array_t<double>>, std::vector<std::string>>(
            &Model::eval_many_py, py::const_),
        py::arg("x_in"),
        py::arg("tasks"),
        "@DocString_model_eval_many_dict_task_list@");

    model.def("prediction_to_file",
              py::overload_cast<std::string,
                                py::array_t<double>,
                                py::array_t<double>,
                                std::vector<std::string>,
                                std::vector<std::string>,
                                std::vector<int>>(&Model::prediction_to_file_py, py::const_),
              py::arg("filename"),
              py::arg("prop"),
              py::arg("x_in"),
              py::arg("sample_ids"),
              py::arg("task_ids") = py::list(),
              py::arg("excluded_inds") = py::list(),
              "@DocString_model_prediction_to_file_arr_list_list@");

    model.def("prediction_to_file",
              py::overload_cast<std::string,
                                py::array_t<double>,
                                std::map<std::string, py::array_t<double>>,
                                std::vector<std::string>,
                                std::vector<std::string>,
                                std::vector<int>>(&Model::prediction_to_file_py, py::const_),
              py::arg("filename"),
              py::arg("prop"),
              py::arg("x_in"),
              py::arg("sample_ids"),
              py::arg("task_ids") = py::list(),
              py::arg("excluded_inds") = py::list(),
              "@DocString_model_prediction_to_file_dict_list_list@");

    model.def("__str__", &Model::toString, "@DocString_model_str@");
    model.def("__repr__", &Model::toString, "@DocString_model_str@");
    model.def("write_matlab_fxn",
              &Model::write_matlab_fxn,
              py::arg("fxn_filename"),
              "@DocString_model_to_matlab@");
    model.def("to_file",
              py::overload_cast<std::string, bool>(&Model::to_file, py::const_),
              py::arg("filename"),
              py::arg("train"),
              "@DocString_model_to_file@");
    model.def_property("task_eval",
                       &Model::get_task_eval,
                       &Model::set_task_eval,
                       "@DocString_model_get_task_eval@");
    model.def_property_readonly("latex_str", &Model::toLatexString, "@DocString_model_latex_str@");
    model.def_property_readonly(
        "n_samp_train", &Model::n_samp_train, "@DocString_model_n_samp_train@");
    model.def_property_readonly(
        "n_samp_test", &Model::n_samp_test, "@DocString_model_n_samp_test@");
    model.def_property_readonly("n_dim", &Model::n_dim, "@DocString_model_n_dim@");
    model.def_property_readonly(
        "prop_train", &Model::prop_train_py, "@DocString_model_prop_train@");
    model.def_property_readonly("prop_test", &Model::prop_test_py, "@DocString_model_prop_test@");
    model.def_property_readonly(
        "train_error", &Model::train_error_py, "@DocString_model_train_error@");
    model.def_property_readonly(
        "test_error", &Model::test_error_py, "@DocString_model_test_error@");
    model.def_property_readonly(
        "fit", &Model::prop_train_est_py, "@DocString_model_prop_train_est@");
    model.def_property_readonly(
        "predict", &Model::prop_test_est_py, "@DocString_model_prop_test_est@");
    model.def_property_readonly("feats", &Model::feats, "@DocString_model_feats@");
    model.def_property_readonly("coefs", &Model::coefs, "@DocString_model_coefs@");
    model.def_property_readonly("prop_unit", &Model::prop_unit, "@DocString_model_prop_unit@");
    model.def_property_readonly("prop_label", &Model::prop_label, "@DocString_model_prop_label@");
    model.def_property_readonly(
        "leave_out_inds", &Model::leave_out_inds, "@DocString_model_leave_out_inds@");
    model.def_property_readonly(
        "task_sizes_train", &Model::task_sizes_train, "@DocString_model_task_sizes_train@");
    model.def_property_readonly(
        "task_sizes_test", &Model::task_sizes_test, "@DocString_model_task_sizes_test@");
    model.def_property_readonly(
        "sample_ids_train", &Model::sample_ids_train, "@DocString_model_sample_ids_train@");
    model.def_property_readonly(
        "sample_ids_test", &Model::sample_ids_test, "@DocString_model_sample_ids_test@");
    model.def_property_readonly("task_names", &Model::task_names, "@DocString_model_task_names@");
    model.def_property_readonly(
        "fix_intercept", &Model::fix_intercept, "@DocString_model_fix_intercept@");
}

void sisso::descriptor_identifier::register_ModelRegressor(py::module& m)
{
    py::class_<ModelRegressor, std::shared_ptr<ModelRegressor>, Model, PyModelRegressor<>>
        model_reg(m, "ModelRegressor", "@DocString_cls_model_reg@");
    model_reg.def(
        py::init<std::string>(), py::arg("train_file"), "@DocString_model_reg_init_train@");
    model_reg.def(py::init<std::string, std::string>(),
                  py::arg("train_file"),
                  py::arg("test_file"),
                  "@DocString_model_reg_init_test_train@");
    model_reg.def(py::pickle(&python_pickling::pickle_model<ModelRegressor>,
                             &python_pickling::unpickle_model<ModelRegressor>));
    model_reg.def_property_readonly("r2", &ModelRegressor::r2, "@DocString_model_reg_r2@");
    model_reg.def_property_readonly(
        "test_r2", &ModelRegressor::test_r2, "@DocString_model_reg_test_r2@");
    model_reg.def_property_readonly("rmse", &ModelRegressor::rmse, "@DocString_model_reg_rmse@");
    model_reg.def_property_readonly(
        "test_rmse", &ModelRegressor::test_rmse, "@DocString_model_reg_test_rmse@");
    model_reg.def_property_readonly(
        "max_ae", &ModelRegressor::max_ae, "@DocString_model_reg_max_ae@");
    model_reg.def_property_readonly(
        "test_max_ae", &ModelRegressor::test_max_ae, "@DocString_model_reg_test_max_ae@");
    model_reg.def_property_readonly("mae", &ModelRegressor::mae, "@DocString_model_reg_mae@");
    model_reg.def_property_readonly(
        "test_mae", &ModelRegressor::test_mae, "@DocString_model_reg_test_mae@");
    model_reg.def_property_readonly("mape", &ModelRegressor::mape, "@DocString_model_reg_mape@");
    model_reg.def_property_readonly(
        "test_mape", &ModelRegressor::test_mape, "@DocString_model_reg_test_mape@");
    model_reg.def_property_readonly("percentile_25_ae",
                                    &ModelRegressor::percentile_25_ae,
                                    "@DocString_model_reg_percentile_25_ae@");
    model_reg.def_property_readonly("percentile_25_test_ae",
                                    &ModelRegressor::percentile_25_test_ae,
                                    "@DocString_model_reg_test_percentile_25_ae@");
    model_reg.def_property_readonly("percentile_50_ae",
                                    &ModelRegressor::percentile_50_ae,
                                    "@DocString_model_reg_percentile_50_ae@");
    model_reg.def_property_readonly("percentile_50_test_ae",
                                    &ModelRegressor::percentile_50_test_ae,
                                    "@DocString_model_reg_test_percentile_50_ae@");
    model_reg.def_property_readonly("percentile_75_ae",
                                    &ModelRegressor::percentile_75_ae,
                                    "@DocString_model_reg_percentile_75_ae@");
    model_reg.def_property_readonly("percentile_75_test_ae",
                                    &ModelRegressor::percentile_75_test_ae,
                                    "@DocString_model_reg_test_percentile_75_ae@");
    model_reg.def_property_readonly("percentile_95_ae",
                                    &ModelRegressor::percentile_95_ae,
                                    "@DocString_model_reg_percentile_95_ae@");
    model_reg.def_property_readonly("percentile_95_test_ae",
                                    &ModelRegressor::percentile_95_ae,
                                    "@DocString_model_reg_test_percentile_95_ae@");
    ;
}

void sisso::descriptor_identifier::register_ModelLogRegressor(py::module& m)
{
    py::class_<ModelLogRegressor,
               std::shared_ptr<ModelLogRegressor>,
               ModelRegressor,
               PyModelRegressor<ModelLogRegressor>>
        model_log_reg(m, "ModelLogRegressor", "@DocString_cls_model_log_reg@");
    model_log_reg.def(
        py::init<std::string>(), py::arg("train_file"), "@DocString_model_log_reg_init_train@");
    model_log_reg.def(py::init<std::string, std::string>(),
                      py::arg("train_file"),
                      py::arg("test_file"),
                      "@DocString_model_log_reg_init_test_train@");
    model_log_reg.def(py::pickle(&python_pickling::pickle_model<ModelLogRegressor>,
                                 &python_pickling::unpickle_model<ModelLogRegressor>));
}

void sisso::descriptor_identifier::register_ModelClassifier(py::module& m)
{
    py::class_<ModelClassifier, std::shared_ptr<ModelClassifier>, Model, PyModelClassifier<>>
        model_class(m, "ModelClassifier", "@DocString_cls_model_class@");
    model_class.def(
        py::init<std::string>(), py::arg("train_file"), "@DocString_model_class_init_train@");
    model_class.def(py::init<std::string, std::string>(),
                    py::arg("train_file"),
                    py::arg("test_file"),
                    "@DocString_model_class_init_test_train@");
    model_class.def(py::init<ModelClassifier,
                             std::vector<std::vector<double>>,
                             py::array_t<double>,
                             py::array_t<double>>(),
                    py::arg("o"),
                    py::arg("new_coefs"),
                    py::arg("prop_train_est"),
                    py::arg("prop_test_est"),
                    "@DocString_model_class_init_new_coefs_list@");
    model_class.def(py::pickle(&python_pickling::pickle_model<ModelClassifier>,
                               &python_pickling::unpickle_model<ModelClassifier>));
    model_class.def_property_readonly("percent_error",
                                      &ModelClassifier::percent_train_error,
                                      "@DocString_model_class_precent_train_error@");
    model_class.def_property_readonly("percent_test_error",
                                      &ModelClassifier::percent_test_error,
                                      "@DocString_model_class_precent_test_error@");
    model_class.def_property_readonly("n_convex_overlap_train",
                                      &ModelClassifier::n_convex_overlap_train,
                                      "@DocString_model_class_n_convex_overlap_train@");
    model_class.def_property_readonly("n_convex_overlap_test",
                                      &ModelClassifier::n_convex_overlap_test,
                                      "@DocString_model_class_n_convex_overlap_test@");
    model_class.def_property_readonly("n_svm_misclassified_train",
                                      &ModelClassifier::n_svm_misclassified_train,
                                      "@DocString_model_class_n_svm_misclassified_train@");
    model_class.def_property_readonly("n_svm_misclassified_test",
                                      &ModelClassifier::n_svm_misclassified_test,
                                      "@DocString_model_class_n_svm_misclassified_test@");
}

void sisso::descriptor_identifier::register_SISSOSolver(py::module& m)
{
    py::class_<SISSOSolver, std::shared_ptr<SISSOSolver>, PySISSOSolver<>> sisso_solver(
        m, "SISSOSolver", "@DocString_cls_sisso@");
    sisso_solver.def(py::init_alias<InputParser, std::shared_ptr<FeatureSpace>>(),
                     py::arg("inputs"),
                     py::arg("feat_space"),
                     "@DocString_sisso_di_init@");
    sisso_solver.def("fit", &SISSOSolver::fit, "@DocString_sisso_di_fit@");
    sisso_solver.def_property_readonly(
        "prop_train", &SISSOSolver::prop_train_py, "@DocString_sisso_di_prop_train_py@");
    sisso_solver.def_property_readonly(
        "prop_test", &SISSOSolver::prop_test_py, "@DocString_sisso_di_prop_test_py@");
    sisso_solver.def_property_readonly(
        "prop_label", &SISSOSolver::prop_label, "@DocString_sisso_di_prop_label_py@");
    sisso_solver.def_property_readonly(
        "prop_unit", &SISSOSolver::prop_unit, "@DocString_sisso_di_prop_unit_py@");
    sisso_solver.def_property_readonly(
        "n_samp", &SISSOSolver::n_samp, "@DocString_sisso_di_n_samp@");
    sisso_solver.def_property_readonly("n_dim", &SISSOSolver::n_dim, "@DocString_sisso_di_n_dim@");
    sisso_solver.def_property_readonly(
        "n_residual", &SISSOSolver::n_residual, "@DocString_sisso_di_n_residual@");
    sisso_solver.def_property_readonly(
        "n_models_store", &SISSOSolver::n_models_store, "@DocString_sisso_di_n_models_store@");
    sisso_solver.def_property_readonly(
        "fix_intercept", &SISSOSolver::fix_intercept, "@DocString_sisso_di_fix_intercept@");
    sisso_solver.def_property_readonly(
        "feat_space", &SISSOSolver::feat_space, "@DocString_sisso_di_feat_space@");
    sisso_solver.def_property_readonly("task_sizes_train",
                                       &SISSOSolver::task_sizes_train,
                                       "@DocString_sisso_di_task_sizes_train@");
    sisso_solver.def_property_readonly(
        "task_sizes_test", &SISSOSolver::task_sizes_test, "@DocString_sisso_di_task_sizes_test@");
    sisso_solver.def_property_readonly(
        "task_names", &SISSOSolver::task_names, "@DocString_sisso_di_task_names@");
    sisso_solver.def_property_readonly("sample_ids_train",
                                       &SISSOSolver::sample_ids_train,
                                       "@DocString_sisso_di_sample_ids_train@");
    sisso_solver.def_property_readonly(
        "sample_ids_test", &SISSOSolver::sample_ids_test, "@DocString_sisso_di_sample_ids_test@");
    sisso_solver.def_property_readonly(
        "leave_out_inds", &SISSOSolver::leave_out_inds, "@DocString_sisso_di_leave_out_inds@");
    sisso_solver.def_property_readonly(
        "feat_space", &SISSOSolver::feat_space, "@DocString_sisso_di_feat_space@");
}

void sisso::descriptor_identifier::register_SISSORegressor(py::module& m)
{
    py::class_<SISSORegressor, std::shared_ptr<SISSORegressor>, SISSOSolver, PySISSORegressor<>>
        sisso_reg(m, "SISSORegressor", "@DocString_cls_sisso_reg@");
    sisso_reg.def(py::init<InputParser, std::shared_ptr<FeatureSpace>>(),
                  py::arg("inputs"),
                  py::arg("feat_space"),
                  "@DocString_sisso_reg_init@");
    sisso_reg.def_property_readonly(
        "models", &SISSORegressor::models, "@DocString_sisso_reg_models_py@");
    sisso_reg.def(py::pickle(&python_pickling::pickle_solver<SISSORegressor>,
                             &python_pickling::unpickle_solver<SISSORegressor, ModelRegressor>));
    sisso_reg.def_property_readonly(
        "models", &SISSORegressor::models, "@DocString_sisso_reg_models_py@");
}

void sisso::descriptor_identifier::register_SISSOLogRegressor(py::module& m)
{
    py::class_<SISSOLogRegressor,
               std::shared_ptr<SISSOLogRegressor>,
               SISSORegressor,
               PySISSORegressor<SISSOLogRegressor>>
        sisso_log_reg(m, "SISSOLogRegressor", "@DocString_cls_sisso_log_reg@");
    sisso_log_reg.def(py::init<InputParser, std::shared_ptr<FeatureSpace>>(),
                      py::arg("inputs"),
                      py::arg("feat_space"),
                      "@DocString_sisso_log_reg_init@");
    sisso_log_reg.def_property_readonly(
        "models", &SISSOLogRegressor::models, "@DocString_sisso_log_reg_models_py@");
    sisso_log_reg.def(
        py::pickle(&python_pickling::pickle_solver<SISSOLogRegressor>,
                   &python_pickling::unpickle_solver<SISSOLogRegressor, ModelLogRegressor>));
    sisso_log_reg.def_property_readonly(
        "models", &SISSOLogRegressor::models, "@DocString_sisso_log_reg_models_py@");
}

void sisso::descriptor_identifier::register_SISSOClassifier(py::module& m)
{
    py::class_<SISSOClassifier, std::shared_ptr<SISSOClassifier>, SISSOSolver, PySISSOClassifier<>>
        sisso_class(m, "SISSOClassifier", "@DocString_cls_sisso_class@");
    sisso_class.def(py::init<InputParser, std::shared_ptr<FeatureSpace>>(),
                    py::arg("inputs"),
                    py::arg("feat_space"),
                    "@DocString_sisso_class_init@");
    sisso_class.def_property_readonly(
        "models", &SISSOClassifier::models, "@DocString_sisso_class_models_py@");
    sisso_class.def(
        py::pickle(&python_pickling::pickle_solver<SISSOClassifier>,
                   &python_pickling::unpickle_solver<SISSOClassifier, ModelClassifier>));
    sisso_class.def_property_readonly(
        "models", &SISSOClassifier::models, "@DocString_sisso_class_models_py@");
}
