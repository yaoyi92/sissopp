// Copyright 2022 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file python/py_binding_cpp_def/pickle.hpp
 *  @brief Define a set of functions for pickling objects passed to c++
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef UTILS_PY_PICKLE
#define UTILS_PY_PICKLE

#include <external/pybind11/include/pybind11/numpy.h>
#include <external/pybind11/include/pybind11/pybind11.h>
#include <external/pybind11/include/pybind11/stl.h>

#include <algorithm>
#include <array>
#include <iostream>
#include <map>
#include <vector>

#include "descriptor_identifier/solver/SISSOClassifier.hpp"
#include "descriptor_identifier/solver/SISSOLogRegressor.hpp"

namespace python_pickling
{
namespace py = pybind11;

inline py::tuple pickle_feat_node(const FeatureNode& node)
{
    return py::make_tuple(node.feat_ind(),
                          node.expr(),
                          node.value(),
                          node.test_value(),
                          node.unit(),
                          node.selected(),
                          node.d_mat_ind());
}

inline FeatureNode unpickle_feat_node(py::tuple t)
{
    FeatureNode node(t[0].cast<unsigned long int>(),
                     t[1].cast<std::string>(),
                     t[2].cast<std::vector<double>>(),
                     t[3].cast<std::vector<double>>(),
                     t[4].cast<Unit>());

    node.set_selected(t[5].cast<bool>());
    node.set_d_mat_ind(t[6].cast<int>());

    return node;
}

inline py::tuple pickle_model_node(const ModelNode& node)
{
    return py::make_tuple(node.feat_ind(),
                          node.rung(),
                          node.expr(),
                          node.latex_expr(),
                          node.postfix_expr(),
                          node.matlab_fxn_expr(),
                          node.value(),
                          node.test_value(),
                          node.x_in_expr_list(),
                          node.unit());
}

inline ModelNode unpickle_model_node(py::tuple t)
{
    return ModelNode(t[0].cast<unsigned long int>(),
                     t[1].cast<int>(),
                     t[2].cast<std::string>(),
                     t[3].cast<std::string>(),
                     t[4].cast<std::string>(),
                     t[5].cast<std::string>(),
                     t[6].cast<std::vector<double>>(),
                     t[7].cast<std::vector<double>>(),
                     t[8].cast<std::vector<std::string>>(),
                     t[9].cast<Unit>());
}

template <typename NodeType, int N>
py::tuple pickle_op_node(const NodeType& node)
{
    std::array<node_ptr, N> feats;
    for (int ff = 0; ff < N; ++ff)
    {
        feats[ff] = node.feat(ff);
    }
    return py::make_tuple(
        feats, node.feat_ind(), node.arr_ind(), node.selected(), node.d_mat_ind());
}

template <typename NodeType, int N>
NodeType unpickle_op_node(py::tuple t)
{
    NodeType node(t[0].cast<std::array<node_ptr, N>>(), t[1].cast<unsigned long int>());

    node.reindex(t[1].cast<unsigned long int>(), t[2].cast<unsigned long int>());
    node.set_selected(t[3].cast<bool>());
    node.set_d_mat_ind(t[4].cast<int>());

    if (node.rung() > node_value_arrs::MAX_RUNG)
    {
        node_value_arrs::set_max_rung(node.rung(), (node_value_arrs::N_PARAM_OP_SLOTS > 0));
    }
    return node;
}

template <typename NodeType, int N>
py::tuple pickle_param_op_node(const NodeType& node)
{
    std::array<node_ptr, N> feats;
    for (int ff = 0; ff < N; ++ff)
    {
        feats[ff] = node.feat(ff);
    }
    return py::make_tuple(feats,
                          node.feat_ind(),
                          node.parameters(),
                          node.arr_ind(),
                          node.selected(),
                          node.d_mat_ind());
}

template <typename NodeType, int N>
NodeType unpickle_param_op_node(py::tuple t)
{
    NodeType node(t[0].cast<std::array<node_ptr, N>>(),
                  t[1].cast<unsigned long int>(),
                  t[2].cast<std::vector<double>>());

    node.reindex(t[1].cast<unsigned long int>(), t[3].cast<unsigned long int>());
    node.set_selected(t[4].cast<bool>());
    node.set_d_mat_ind(t[5].cast<int>());

    if (node.rung() > node_value_arrs::MAX_RUNG)
    {
        node_value_arrs::set_max_rung(node.rung(), true);
    }
    return node;
}

template <typename ModelType>
py::tuple pickle_model(const ModelType& model)
{
    return py::make_tuple(model.prop_label(),
                          model.prop_unit(),
                          model.prop_train(),
                          model.prop_test(),
                          model.task_sizes_train(),
                          model.task_sizes_test(),
                          model.feats(),
                          model.leave_out_inds(),
                          model.sample_ids_train(),
                          model.sample_ids_test(),
                          model.task_names(),
                          model.fix_intercept());
}

template <typename ModelType>
ModelType unpickle_model(py::tuple t)
{
    return ModelType(t[0].cast<std::string>(),
                     t[1].cast<Unit>(),
                     t[2].cast<std::vector<double>>(),
                     t[3].cast<std::vector<double>>(),
                     t[4].cast<std::vector<int>>(),
                     t[5].cast<std::vector<int>>(),
                     t[6].cast<std::vector<model_node_ptr>>(),
                     t[7].cast<std::vector<int>>(),
                     t[8].cast<std::vector<std::string>>(),
                     t[9].cast<std::vector<std::string>>(),
                     t[10].cast<std::vector<std::string>>(),
                     t[11].cast<bool>());
}

inline py::tuple pickle_unit(const Unit& unit) { return py::make_tuple(unit.toString()); }

inline Unit unpickle_unit(py::tuple t) { return Unit(t[0].cast<std::string>()); }

template <typename SolverType>
py::tuple pickle_solver(const SolverType& solver)
{
    return py::make_tuple(solver.sample_ids_train(),
                          solver.sample_ids_test(),
                          solver.task_names(),
                          solver.task_sizes_train(),
                          solver.task_sizes_test(),
                          solver.leave_out_inds(),
                          solver.prop_unit(),
                          solver.prop_label(),
                          solver.prop_train(),
                          solver.prop_test(),
                          solver.feat_space(),
                          solver.n_dim(),
                          solver.n_residual(),
                          solver.n_models_store(),
                          solver.fix_intercept(),
                          solver.models());
}

template <typename SolverType, typename ModelType>
SolverType unpickle_solver(py::tuple t)
{
    return SolverType(t[0].cast<std::vector<std::string>>(),
                      t[1].cast<std::vector<std::string>>(),
                      t[2].cast<std::vector<std::string>>(),
                      t[3].cast<std::vector<int>>(),
                      t[4].cast<std::vector<int>>(),
                      t[5].cast<std::vector<int>>(),
                      t[6].cast<Unit>(),
                      t[7].cast<std::string>(),
                      t[8].cast<std::vector<double>>(),
                      t[9].cast<std::vector<double>>(),
                      t[10].cast<std::shared_ptr<FeatureSpace>>(),
                      t[11].cast<int>(),
                      t[12].cast<int>(),
                      t[13].cast<int>(),
                      t[14].cast<bool>(),
                      t[15].cast<std::vector<std::vector<ModelType>>>());
}

py::tuple pickle_feat_space(const FeatureSpace& feat_space);

inline FeatureSpace unpickle_feat_space(py::tuple t)
{
    return FeatureSpace(t[0].cast<std::vector<node_ptr>>(),
                        t[1].cast<std::vector<node_ptr>>(),
                        t[2].cast<std::vector<node_ptr>>(),
                        t[3].cast<std::vector<std::string>>(),
                        t[4].cast<std::vector<std::string>>(),
                        t[5].cast<std::vector<double>>(),
                        t[6].cast<std::vector<double>>(),
                        t[7].cast<std::vector<int>>(),
                        t[8].cast<std::vector<int>>(),
                        t[9].cast<std::string>(),
                        t[10].cast<std::string>(),
                        t[11].cast<std::string>(),
                        t[12].cast<std::string>(),
                        t[13].cast<double>(),
                        t[14].cast<double>(),
                        t[15].cast<double>(),
                        t[16].cast<int>(),
                        t[17].cast<int>(),
                        t[18].cast<int>(),
                        t[19].cast<int>(),
                        t[20].cast<std::vector<node_ptr>>(),
                        t[21].cast<std::vector<int>>(),
                        t[22].cast<std::vector<int>>(),
                        t[23].cast<int>(),
                        t[24].cast<bool>());
}
};  // namespace python_pickling
#endif
