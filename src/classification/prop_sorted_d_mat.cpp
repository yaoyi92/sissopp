// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file classification/prop_sorted_d_mat.cpp
 *  @brief Implements the functions for the central storage area for a sorted descriptor matrix based on the task/class of a sample
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "classification/prop_sorted_d_mat.hpp"

std::vector<double> prop_sorted_d_mat::SORTED_D_MATRIX;

std::vector<int> prop_sorted_d_mat::CLASS_START;
std::vector<int> prop_sorted_d_mat::N_SAMPLES_PER_CLASS;

int prop_sorted_d_mat::N_TASK = 0;
int prop_sorted_d_mat::N_CLASS = 0;
int prop_sorted_d_mat::N_FEATURES = 0;
int prop_sorted_d_mat::N_SAMPLES = 0;

void prop_sorted_d_mat::initialize_sorted_d_matrix_arr(int n_feat,
                                                       int n_task,
                                                       int n_class,
                                                       std::vector<int> n_samples_per_class)
{
    if (n_samples_per_class.size() != n_task * n_class)
    {
        throw std::logic_error(
            "n_samples_per_class does not have the correct number of elements, it should be: " +
            std::to_string(n_class * n_task) + ".");
    }

    N_TASK = n_task;
    N_CLASS = n_class;
    N_FEATURES = n_feat;

    N_SAMPLES_PER_CLASS = n_samples_per_class;
    CLASS_START = std::vector<int>(N_SAMPLES_PER_CLASS.size(), 0);

    for (int cc = 1; cc < N_TASK * N_CLASS; ++cc)
    {
        CLASS_START[cc] = CLASS_START[cc - 1] + N_SAMPLES_PER_CLASS[cc - 1];
    }

    N_SAMPLES = std::accumulate(N_SAMPLES_PER_CLASS.begin(), N_SAMPLES_PER_CLASS.end(), 0);
    SORTED_D_MATRIX = std::vector<double>(N_FEATURES * N_SAMPLES, 0.0);
}

void prop_sorted_d_mat::resize_sorted_d_matrix_arr(int n_feats)
{
    N_FEATURES = n_feats;
    SORTED_D_MATRIX.resize(N_FEATURES * N_SAMPLES);
}

void prop_sorted_d_mat::finalize_sorted_d_matrix_arr()
{
    SORTED_D_MATRIX.resize(0);

    CLASS_START.resize(0);
    N_SAMPLES_PER_CLASS.resize(0);

    N_TASK = 0;
    N_CLASS = 0;
    N_FEATURES = 0;
    N_SAMPLES = 0;
}
