// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file classification/LPWrapper.hpp
 *  @brief Defines a class used to wrap the Coin-Clp Simplex Library, and facilitate its use with SISSO++
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef COIN_CLP_WRAPPER
#define COIN_CLP_WRAPPER

#include <algorithm>
#include <coin/ClpSimplex.hpp>

#include "classification/prop_sorted_d_mat.hpp"
#include "utils/mkl_interface.hpp"

// DocString: cls_lp_wrapper
/**
 * @brief A wrapper to the ClpSimplex library to facilitate use with SISSO++
 */
class LPWrapper
{
protected:
    // clang-format off
    ClpSimplex _simplex; //!< LP algorithm used to determine if a point is inside the convex hull

    std::vector<double> _elements; //!< The elements of the A matrix (descriptor matrix) for the linear programing problem
    std::vector<double> _row_lower; //!< The lower bound for the row constraints (value of the data point to check)
    std::vector<double> _row_upper; //!< The upper bound for the row constraints (value of the data point to check)
    std::vector<double> _row_lower_test; //!< The lower bound for the row constraints for the test set (value of the data point to check)
    std::vector<double> _row_upper_test; //!< The upper bound for the row constraints for the test set (value of the data point to check)
    std::vector<double> _objective; //!< The objective function to optimize over (all 0's)
    std::vector<double> _column_lower; //!< The lower bound for the column constraints (all 0's)
    std::vector<double> _column_upper; //!< The upper bound for the column constraints (all infinity)

    std::vector<int> _column_start; //!< Vector len(n_samples) of the starting column for the A matrix
    std::vector<int> _row; //!< Vector storing what row each data point is in
    std::vector<int> _row_test; //!< Vector storing what row each data point is in for the test set

    std::vector<int> _n_row_per_class; //!< //!< A vector storing the number of rows in each class/task combination for the training set [task * N_TASK * N_CLASS + class]
    std::vector<int> _n_row_per_class_test; //!< //!< A vector storing the number of rows in each class/task combination for the test set [task * N_TASK * N_CLASS + class]

    double _tol; //!< tolerance value (\epsilon) for the final row constraint 1 +/- _tol for the upper and lower constraint

    int _n_dim; //!< The number of dimensions for the SVM problem
    int _n_samp; //!< The number of samples in the training data per feature
    int _n_samp_test; //!< The number of samples in the test data per feature
    int _n_class; //!< The number of classes in the data set
    int _n_row;  //!< The number of rows in the A matrix
    int _task_num; //!< The current task number the LPWrapper is used for
    int _n_col;  //!< The number of columns in the A matrix
    int _n_overlap; //!< The number of misclassified data points in the training set
    int _n_overlap_test; //!< The number of misclassified data points in the test set
    int _n_pts_check; //!< The number of points to check
    int _n_pts_check_test; //!< The number of points to check in the test set
    // clang-format on

public:
    /**
     * @brief The constructor for the LPWrapper
     *
     * @param samp_per_class A vector storing the number of rows in each class/task combination for the training set [task * N_TASK * N_CLASS + class]
     * @param task_num The task ID number used to perform the calculation
     * @param n_class Number of classes in the dataset
     * @param n_dim Number of dimensions of the problem
     * @param n_samp Number of samples in the dataset
     * @param tol The tolerance used to have a fuzzy border around the convex hull
     * @param samp_per_class A vector storing number of test samples per class
     * @param n_samp_test TheA vector storing tnumber of rows in each class/task combination for the test set [task * N_TASK * N_CLASS + class]
     */
    LPWrapper(std::vector<int> samp_per_class,
              const int task_num,
              const int n_class,
              const int n_dim,
              const int n_samp,
              const double tol,
              std::vector<int> samp_per_class_test = {},
              const int n_samp_test = 0);

    /**
     * @brief Copy Constructor
     *
     * @param o LPWrapper to be copied
     */
    LPWrapper(const LPWrapper& o);

    LPWrapper(LPWrapper&& o) = delete;

    /**
     * @brief Copy Assignment operator
     *
     * @param o LPWrapper to be copied
     */
    LPWrapper& operator=(const LPWrapper& o);

    LPWrapper& operator=(LPWrapper&& o) = delete;

    /**
     * @brief Set up the constraint matrix for the LP problem
     */
    void setup_constraints();

    /**
     * @brief Copies the data from a set of feature indexes (sorted_dmatrix) into the x_space
     *
     * @param cls Index defining what class to copy
     * @param inds A vector storing the _d_mat_ind of the features to pull data from the Descriptor Matrix storage array
     */
    void copy_data(const int cls, const std::vector<int> inds);

    /**
     * @brief Copies the data from a set data pointers
     *
     * @param cls Index defining what class to copy
     * @param val_ptrs A vector storing pointers to the features training data
     * @param test_val_ptrs A vector storing pointers to the features test data
     */
    void copy_data(const int cls,
                   const std::vector<double*> val_ptrs,
                   const std::vector<double*> test_val_ptrs);

    /**
     * @brief Calculate the number of points in the overlap region of the convex hulls for the training set
     *
     * @param inds A vector storing the _d_mat_ind of the features to pull data from the Descriptor Matrix storage array
     */
    int get_n_overlap(const std::vector<int> inds);

    /**
     * @brief Calculate the number of points in the overlap region of the convex hulls for the training and test sets and set the error vectors
     *
     * @param val_ptrs A vector storing pointers to the features training data
     * @param test_val_ptrs A vector storing pointers to the features test data
     * @param error Pointer to the head of the error vector
     * @param test_error Pointer to the head of the test error vector
     */
    void set_n_overlap(const std::vector<double*> val_ptrs,
                       const std::vector<double*> test_val_ptrs,
                       double* error,
                       double* test_error);

    /**
     * @brief The number of classes in the training set
     */
    inline int n_class() const { return _n_class; }

    /**
     * @brief The index of the current task that is stored in the LPWrapper
     */
    inline int task_num() const { return _task_num; }

    /**
     * @brief The number of dimensions of the Convex Hulls
     */
    inline int n_dim() const { return _n_dim; }

    /**
     * @brief The number of samples in the training set
     */
    inline int n_samp() const { return _n_samp; }

    /**
     * @brief The number of samples in the test set
     */
    inline int n_samp_test() const { return _n_samp_test; }

    /**
     * @brief Number of misclassified samples in the data set
     */
    inline int n_overlap() const { return _n_overlap; }

    /**
     * @brief Number of misclassified samples in the test set
     */
    inline int n_overlap_test() const { return _n_overlap_test; }
};

#endif
