// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file classification/SVMWrapper.cpp
 *  @brief Implements a class used to wrap libsvm in more C++ oriented data structure
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "classification/SVMWrapper.hpp"

SVMWrapper::SVMWrapper(const int n_class, const int n_dim, const int n_samp, const double* prop)
    : SVMWrapper(1000.0, n_class, n_dim, n_samp, prop)
{
}

SVMWrapper::SVMWrapper(const int n_class, const int n_dim, const std::vector<double> prop)
    : SVMWrapper(n_class, n_dim, prop.size(), prop.data())
{
}

SVMWrapper::SVMWrapper(
    const double C, const int n_class, const int n_dim, const int n_samp, const double* prop)
    : _model(nullptr),
      _y(prop, prop + n_samp),
      _y_est(n_samp),
      _x_space(n_samp * (n_dim + 1)),
      _x(n_samp),
      _coefs(n_class * (n_class - 1) / 2, std::vector<double>(n_dim, 0.0)),
      _C(C),
      _intercept(n_class * (n_class - 1) / 2, 0.0),
      _w_remap(n_dim, 1.0),
      _b_remap(n_dim, 0.0),
      _n_dim(n_dim),
      _n_samp(n_samp),
      _n_class(n_class)
{
    std::vector<double> unique_prop_vals = vector_utils::unique(_y);
    std::map<double, double> rep_map;
    for (int cc = 0; cc < _n_class; ++cc)
    {
        _map_prop_vals[static_cast<double>(cc)] = unique_prop_vals[cc];
        rep_map[unique_prop_vals[cc]] = static_cast<double>(cc);
    }
    std::transform(_y.begin(), _y.end(), _y.begin(), [&](double val) { return rep_map[val]; });
    setup_parameter_obj(_C);
    setup_x_space();

    _prob.l = _n_samp;
    _prob.y = _y.data();
    _prob.x = _x.data();
}

SVMWrapper::SVMWrapper(const double C,
                       const int n_class,
                       const int n_dim,
                       const std::vector<double> prop)
    : SVMWrapper(C, n_class, n_dim, prop.size(), prop.data())
{
}

SVMWrapper::SVMWrapper(const SVMWrapper& o)
    : _model(nullptr),
      _y(o._y),
      _y_est(o._y_est),
      _x_space(o._n_samp * (o._n_dim + 1)),
      _x(o._n_samp),
      _coefs(o._coefs),
      _intercept(o._intercept),
      _w_remap(o._w_remap),
      _b_remap(o._b_remap),
      _map_prop_vals(o._map_prop_vals),
      _C(o._C),
      _n_dim(o._n_dim),
      _n_samp(o._n_samp),
      _n_class(o._n_class),
      _n_misclassified(o._n_misclassified)
{
    setup_parameter_obj(_C);
    setup_x_space();

    _prob.l = _n_samp;
    _prob.y = _y.data();
    _prob.x = _x.data();
}

SVMWrapper::~SVMWrapper()
{
    svm_destroy_param(&_param);
    if (_model)
    {
        svm_free_and_destroy_model(&_model);
    }
}

void SVMWrapper::setup_parameter_obj(const double C)
{
    _param.svm_type = C_SVC;
    _param.kernel_type = LINEAR;
    _param.degree = 3;
    _param.gamma = 0;  // 1/num_features
    _param.coef0 = 0;
    _param.nu = 0.5;
    _param.cache_size = 100;
    _param.C = C;
    _param.eps = 1e-4;
    _param.p = 0.1;
    _param.shrinking = 1;
    _param.probability = 0;
    _param.nr_weight = 0;
    _param.weight_label = NULL;
    _param.weight = NULL;
}

void SVMWrapper::setup_x_space()
{
    for (int ss = 0; ss < _n_samp; ++ss)
    {
        for (int dd = 0; dd < _n_dim + 1; ++dd)
        {
            _x_space[ss * (_n_dim + 1) + dd].index = -1;
            _x_space[ss * (_n_dim + 1) + dd].value = 0.0;
        }

        _x[ss] = &_x_space[ss * (_n_dim + 1)];
    }
}

void SVMWrapper::copy_data(const std::vector<int> inds, const int task)
{
    if ((task < 0) || (task >= node_value_arrs::TASK_SZ_TRAIN.size()))
    {
        throw std::logic_error("The requested task is invalid.");
    }

    if (inds.size() > _n_dim)
    {
        throw std::logic_error("Size of the inds vector is larger than _n_dim");
    }

    for (int ii = 0; ii < inds.size(); ++ii)
    {
        double* val_ptr = node_value_arrs::get_d_matrix_ptr(inds[ii], task);

        _b_remap[ii] = *std::min_element(val_ptr, val_ptr + _n_samp);
        _w_remap[ii] = 1.0 / (*std::max_element(val_ptr, val_ptr + _n_samp) - _b_remap[ii]);

        for (auto el : _x)
        {
            el[ii].index = ii;
            el[ii].value = (*val_ptr - _b_remap[ii]) * _w_remap[ii];
            ++val_ptr;
        }
    }
}

void SVMWrapper::copy_data(const std::vector<double*> val_ptrs)
{
    if (val_ptrs.size() > _n_dim)
    {
        throw std::logic_error("Size of the val_ptrs vector is larger than _n_dim");
    }

    for (int ii = 0; ii < val_ptrs.size(); ++ii)
    {
        double* val_ptr = val_ptrs[ii];

        _b_remap[ii] = *std::min_element(val_ptr, val_ptr + _n_samp);
        _w_remap[ii] = 1.0 / (*std::max_element(val_ptr, val_ptr + _n_samp) - _b_remap[ii]);

        for (auto el : _x)
        {
            el[ii].index = ii;
            el[ii].value = (*val_ptr - _b_remap[ii]) * _w_remap[ii];
            ++val_ptr;
        }
    }
}

void SVMWrapper::train(const bool remap_coefs)
{
    if (_model)
    {
        svm_free_and_destroy_model(&_model);
    }
    std::fill_n(_intercept.begin(), _intercept.size(), 0.0);
    for (auto& coef_list : _coefs)
    {
        std::fill_n(coef_list.begin(), coef_list.size(), 0.0);
    }
    _model = svm_train(&_prob, &_param);
    std::copy_n(_model->rho, _intercept.size(), _intercept.data());

    int class_combo = 0;
    for (int c1 = 0; c1 < _model->nr_class; ++c1)
    {
        int n_sv_c1 = _model->nSV[c1];
        int start_c1 = std::accumulate(_model->nSV, _model->nSV + c1, 0);

        for (int c2 = c1 + 1; c2 < _model->nr_class; ++c2)
        {
            int n_sv_c2 = _model->nSV[c2];
            int start_c2 = std::accumulate(_model->nSV, _model->nSV + c2, 0);
            for (int dd = 0; dd < _n_dim; ++dd)
            {
                for (int sv = start_c1; sv < start_c1 + n_sv_c1; ++sv)
                {
                    _coefs[class_combo][dd] -= _model->sv_coef[c2 - 1][sv] *
                                               _model->SV[sv][dd].value;
                }

                for (int sv = start_c2; sv < start_c2 + n_sv_c2; ++sv)
                {
                    _coefs[class_combo][dd] -= _model->sv_coef[c1][sv] * _model->SV[sv][dd].value;
                }

                if (remap_coefs)
                {
                    _coefs[class_combo][dd] *= _w_remap[dd];
                    _intercept[class_combo] -= _coefs[class_combo][dd] * _b_remap[dd];
                }
            }
            ++class_combo;
        }
    }

    std::transform(_x.begin(), _x.end(), _y_est.begin(), [this](svm_node* sn) {
        return svm_predict(_model, sn);
    });
    _n_misclassified = 0;
    for (int ss = 0; ss < _n_samp; ++ss)
    {
        _n_misclassified += _y[ss] != _y_est[ss];
    }
    std::transform(_y_est.begin(), _y_est.end(), _y_est.begin(), [this](double val) {
        return _map_prop_vals[val];
    });
}

void SVMWrapper::train(const std::vector<int> inds, const int task, const bool remap_coefs)
{
    copy_data(inds, task);
    train(remap_coefs);
}

void SVMWrapper::train(const std::vector<double*> val_ptrs, const bool remap_coefs)
{
    copy_data(val_ptrs);
    train(remap_coefs);
}

std::vector<double> SVMWrapper::predict(const int n_samp_test, const std::vector<double*> val_ptrs)
{
    if (_model == nullptr)
    {
        throw std::logic_error("SVM model has not been trained, can't make a prediction.");
    }

    if (val_ptrs.size() > _n_dim)
    {
        throw std::logic_error("Size of the val_ptrs vector is larger than _n_dim");
    }

    std::vector<double> y_est_test(n_samp_test, 0.0);

    std::vector<svm_node> x_space_test(n_samp_test * (val_ptrs.size() + 1));
    std::vector<svm_node*> x_test(n_samp_test);

    for (int ss = 0; ss < n_samp_test; ++ss)
    {
        for (int dd = 0; dd < val_ptrs.size(); ++dd)
        {
            x_space_test[ss * (val_ptrs.size() + 1) + dd].index = dd;
            x_space_test[ss * (val_ptrs.size() + 1) + dd].value = (val_ptrs[dd][ss] -
                                                                   _b_remap[dd]) *
                                                                  _w_remap[dd];
        }

        x_space_test[ss * (val_ptrs.size() + 1) + val_ptrs.size()].index = -1;
        x_space_test[ss * (val_ptrs.size() + 1) + val_ptrs.size()].value = 0;

        x_test[ss] = &x_space_test[ss * (val_ptrs.size() + 1)];
    }
    std::transform(x_test.begin(), x_test.end(), y_est_test.begin(), [this](svm_node* sn) {
        return svm_predict(_model, sn);
    });
    std::transform(y_est_test.begin(), y_est_test.end(), y_est_test.begin(), [this](double val) {
        return _map_prop_vals[val];
    });
    return y_est_test;
}

double SVMWrapper::predict(const double* pt, int n_feat) const
{
    if (_model == nullptr)
    {
        throw std::logic_error("SVM model has not been trained, can't make a prediction.");
    }
    if (n_feat != _n_dim)
    {
        throw std::logic_error("Size of the pt vector is not the same as _n_dim");
    }

    std::vector<svm_node> x_space_test(_n_dim + 1);

    for (int dd = 0; dd < _n_dim; ++dd)
    {
        x_space_test[dd].index = dd;
        x_space_test[dd].value = (pt[dd] - _b_remap[dd]) * _w_remap[dd];
    }

    x_space_test[_n_dim].index = -1;
    x_space_test[_n_dim].value = 0;

    double y_est = svm_predict(_model, x_space_test.data());
    return (_map_prop_vals.find(y_est))->second;
}
