// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file classification/LPWrapper.cpp
 *  @brief Implements a class used to wrap the Coin-Clp Simplex Library, and facilitate its use with SISSO++
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "classification/LPWrapper.hpp"

#include <iomanip>
LPWrapper::LPWrapper(std::vector<int> samp_per_class,
                     const int task_num,
                     const int n_class,
                     const int n_dim,
                     const int n_samp,
                     const double tol,
                     std::vector<int> samp_per_class_test,
                     const int n_samp_test)
    : _elements((n_dim + 1) * n_samp, 0.0),
      _row_lower((n_dim + 1) * n_samp, 1.0),
      _row_upper((n_dim + 1) * n_samp, 1.0),
      _row_lower_test((n_dim + 1) * n_samp_test),
      _row_upper_test((n_dim + 1) * n_samp_test),
      _objective(n_samp, 0.0),
      _column_lower(n_samp, 0.0),
      _column_upper(n_samp, COIN_DBL_MAX),
      _column_start(n_samp, 0),
      _row((n_dim + 1) * n_samp, 0),
      _row_test((n_dim + 1) * n_samp_test),
      _n_row_per_class(samp_per_class),
      _n_row_per_class_test(samp_per_class_test),
      _tol(tol),
      _n_dim(n_dim),
      _n_samp(n_samp),
      _n_samp_test(n_samp_test),
      _n_class(n_class),
      _n_row(n_dim + 1),
      _n_col(0),
      _n_overlap(0),
      _n_overlap_test(0),
      _n_pts_check(0),
      _task_num(task_num)
{
    if (_n_row_per_class_test.size() == 0)
    {
        _n_row_per_class_test.resize(_n_row_per_class.size(), 0);
    }

    if (std::accumulate(_n_row_per_class.begin(), _n_row_per_class.end(), 0) != _n_samp)
    {
        throw std::logic_error("The sum of elements in samp_per_class must equal n_samp.");
    }
    if (std::accumulate(_n_row_per_class_test.begin(), _n_row_per_class_test.end(), 0) !=
        _n_samp_test)
    {
        throw std::logic_error(
            "The sum of elements in samp_per_class_test must equal n_samp_test.");
    }
    setup_constraints();
}

LPWrapper::LPWrapper(const LPWrapper& o)
    : _elements(o._elements),
      _row_lower(o._row_lower),
      _row_upper(o._row_upper),
      _row_lower_test(o._row_lower_test),
      _row_upper_test(o._row_upper_test),
      _objective(o._objective),
      _column_lower(o._column_lower),
      _column_upper(o._column_upper),
      _column_start(o._column_start),
      _row(o._row),
      _row_test(o._row_test),
      _n_row_per_class(o._n_row_per_class),
      _n_row_per_class_test(o._n_row_per_class_test),
      _tol(o._tol),
      _n_dim(o._n_dim),
      _n_samp(o._n_samp),
      _n_samp_test(o._n_samp_test),
      _n_class(o._n_class),
      _n_row(o._n_row),
      _task_num(o._task_num),
      _n_col(o._n_col),
      _n_overlap(o._n_overlap),
      _n_overlap_test(o._n_overlap_test),
      _n_pts_check(o._n_pts_check),
      _n_pts_check_test(o._n_pts_check_test)
{
    setup_constraints();
}

LPWrapper& LPWrapper::operator=(const LPWrapper& o)
{
    _elements = o._elements;
    _row_lower = o._row_lower;
    _row_upper = o._row_upper;
    _row_lower_test = o._row_lower_test;
    _row_upper_test = o._row_upper_test;
    _objective = o._objective;
    _column_lower = o._column_lower;
    _column_upper = o._column_upper;
    _column_start = o._column_start;
    _row = o._row;
    _row_test = o._row_test;
    _n_row_per_class = o._n_row_per_class;
    _n_row_per_class_test = o._n_row_per_class_test;
    _tol = o._tol;
    _n_dim = o._n_dim;
    _n_samp = o._n_samp;
    _n_samp_test = o._n_samp_test;
    _n_class = o._n_class;
    _n_row = o._n_row;
    _task_num = o._task_num;
    _n_col = o._n_col;
    _n_overlap = o._n_overlap;
    _n_overlap_test = o._n_overlap_test;
    _n_pts_check = o._n_pts_check;
    _n_pts_check_test = o._n_pts_check_test;

    setup_constraints();

    return *this;
}

void LPWrapper::setup_constraints()
{
    _simplex.setLogLevel(0);
    _simplex.setMoreSpecialOptions(8192 + 64 + 2);

    std::iota(_column_start.begin(), _column_start.end(), 0);
    std::transform(
        _column_start.begin(), _column_start.end(), _column_start.begin(), [this](int ii) {
            return (_n_dim + 1) * ii;
        });

    std::fill_n(_elements.begin(), _elements.size(), 1.0);
    std::fill_n(_row_lower.begin(), _row_lower.size(), 1.0 - _tol);
    std::fill_n(_row_upper.begin(), _row_upper.size(), 1.0 + _tol);

    std::fill_n(_row_lower_test.begin(), _row_lower_test.size(), 1.0 - _tol);
    std::fill_n(_row_upper_test.begin(), _row_upper_test.size(), 1.0 + _tol);

    for (int dd = 0; dd < _n_dim + 1; ++dd)
    {
        scopy_(_n_samp, std::vector<int>(_n_samp, dd).data(), 1, &_row[dd], _n_dim + 1);
        scopy_(
            _n_samp_test, std::vector<int>(_n_samp_test, dd).data(), 1, &_row_test[dd], _n_dim + 1);
    }
}

void LPWrapper::copy_data(const int cls, const std::vector<int> inds)
{
    if (inds.size() > _n_dim)
    {
        throw std::logic_error("Size of the inds vector is larger than _n_dim");
    }

    for (int ii = 0; ii < _n_dim; ++ii)
    {
        dcopy_(_n_col,
               prop_sorted_d_mat::access_sorted_d_matrix(inds[ii], _task_num, cls),
               1,
               &_elements[ii],
               _n_row);
    }

    int n_copied = 0;
    _n_pts_check = 0;
    for (int cc = 0; cc < _n_class; ++cc)
    {
        if (cc == cls)
        {
            continue;
        }
        for (int ii = 0; ii < inds.size(); ++ii)
        {
            dcopy_(_n_row_per_class[cc],
                   prop_sorted_d_mat::access_sorted_d_matrix(inds[ii], _task_num, cc),
                   1,
                   &_row_lower[n_copied + ii],
                   _n_row);
            dcopy_(_n_row_per_class[cc],
                   prop_sorted_d_mat::access_sorted_d_matrix(inds[ii], _task_num, cc),
                   1,
                   &_row_upper[n_copied + ii],
                   _n_row);
        }
        n_copied += _n_row_per_class[cc] * _n_row;
        _n_pts_check += _n_row_per_class[cc];
    }

    _simplex.loadProblem(_n_col,
                         _n_row,
                         _column_start.data(),
                         _row.data(),
                         _elements.data(),
                         _column_lower.data(),
                         _column_upper.data(),
                         _objective.data(),
                         _row_lower.data(),
                         _row_upper.data());
}

void LPWrapper::copy_data(const int cls,
                          const std::vector<double*> val_ptrs,
                          const std::vector<double*> test_val_ptrs)
{
    if (val_ptrs.size() > _n_dim)
    {
        throw std::logic_error("Size of the val_ptrs vector is larger than _n_dim");
    }
    if (test_val_ptrs.size() > _n_dim)
    {
        throw std::logic_error("Size of the test_val_ptrs vector is larger than _n_dim");
    }

    for (int ii = 0; ii < _n_dim; ++ii)
    {
        dcopy_(_n_col,
               val_ptrs[ii] +
                   std::accumulate(_n_row_per_class.begin(), _n_row_per_class.begin() + cls, 0),
               1,
               &_elements[ii],
               _n_dim + 1);
    }

    int n_copied = 0;
    int n_copied_test = 0;
    _n_pts_check = 0;
    _n_pts_check_test = 0;
    for (int cc = 0; cc < _n_class; ++cc)
    {
        if (cc == cls)
        {
            continue;
        }

        int class_start = std::accumulate(
            _n_row_per_class.begin(), _n_row_per_class.begin() + cc, 0);
        int class_start_test = std::accumulate(
            _n_row_per_class_test.begin(), _n_row_per_class_test.begin() + cc, 0);
        for (int ii = 0; ii < val_ptrs.size(); ++ii)
        {
            dcopy_(_n_row_per_class[cc],
                   val_ptrs[ii] + class_start,
                   1,
                   &_row_lower[n_copied + ii],
                   _n_row);
            dcopy_(_n_row_per_class[cc],
                   val_ptrs[ii] + class_start,
                   1,
                   &_row_upper[n_copied + ii],
                   _n_row);
        }
        n_copied += _n_row_per_class[cc] * _n_row;
        _n_pts_check += _n_row_per_class[cc];

        for (int ii = 0; ii < test_val_ptrs.size(); ++ii)
        {
            dcopy_(_n_row_per_class_test[cc],
                   test_val_ptrs[ii] + class_start_test,
                   1,
                   &_row_lower_test[n_copied_test + ii],
                   _n_row);
            dcopy_(_n_row_per_class_test[cc],
                   test_val_ptrs[ii] + class_start_test,
                   1,
                   &_row_upper_test[n_copied_test + ii],
                   _n_row);
        }
        n_copied_test += _n_row_per_class_test[cc] * _n_row;
        _n_pts_check_test += _n_row_per_class_test[cc];
    }

    _simplex.loadProblem(_n_col,
                         _n_row,
                         _column_start.data(),
                         _row.data(),
                         _elements.data(),
                         _column_lower.data(),
                         _column_upper.data(),
                         _objective.data(),
                         _row_lower.data(),
                         _row_upper.data());
}

int LPWrapper::get_n_overlap(const std::vector<int> inds)
{
    _n_overlap = 0;

    for (int cc = 0; cc < _n_class; ++cc)
    {
        _n_col = _n_row_per_class[cc];
        copy_data(cc, inds);
        for (int ii = 0; ii < _n_pts_check; ++ii)
        {
            _simplex.chgRowLower(&_row_lower[ii * _n_row]);
            _simplex.chgRowUpper(&_row_upper[ii * _n_row]);
            _simplex.dual();
            _n_overlap += _simplex.primalFeasible();
        }
    }
    return _n_overlap;
}

void LPWrapper::set_n_overlap(const std::vector<double*> val_ptrs,
                              const std::vector<double*> test_val_ptrs,
                              double* error,
                              double* test_error)
{
    _n_overlap = 0;
    _n_overlap_test = 0;
    for (int cc = 0; cc < _n_class; ++cc)
    {
        _n_col = _n_row_per_class[cc];
        copy_data(cc, val_ptrs, test_val_ptrs);

        std::vector<int> error_ind(_n_pts_check);
        std::vector<int> test_error_ind(_n_pts_check_test);
        int start = 0;
        int start_test = 0;
        for (int c1 = 0; c1 < _n_class; ++c1)
        {
            if (cc == c1)
            {
                continue;
            }

            std::iota(error_ind.begin() + start,
                      error_ind.begin() + start + _n_row_per_class[c1],
                      std::accumulate(_n_row_per_class.begin(), _n_row_per_class.begin() + c1, 0));

            std::iota(test_error_ind.begin() + start_test,
                      test_error_ind.begin() + start_test + _n_row_per_class_test[c1],
                      std::accumulate(
                          _n_row_per_class_test.begin(), _n_row_per_class_test.begin() + c1, 0));

            start += _n_row_per_class[c1];
            start_test += _n_row_per_class_test[c1];
        }
        for (int ii = 0; ii < _n_pts_check; ++ii)
        {
            _simplex.chgRowLower(&_row_lower[ii * _n_row]);
            _simplex.chgRowUpper(&_row_upper[ii * _n_row]);

            _simplex.dual();
            _n_overlap += _simplex.primalFeasible();
            error[error_ind[ii]] += static_cast<double>(_simplex.primalFeasible());
        }

        for (int ii = 0; ii < _n_pts_check_test; ++ii)
        {
            _simplex.chgRowLower(&_row_lower_test[ii * _n_row]);
            _simplex.chgRowUpper(&_row_upper_test[ii * _n_row]);

            _simplex.dual();
            _n_overlap_test += _simplex.primalFeasible();
            test_error[test_error_ind[ii]] += static_cast<double>(_simplex.primalFeasible());
        }
    }
}
