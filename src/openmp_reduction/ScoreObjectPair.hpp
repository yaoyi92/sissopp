// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file utils/ScObjPair.hpp
 *  @brief Functions to be used in OpenMP
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef UTILS_SC_OBJ_PAIR
#define UTILS_SC_OBJ_PAIR

#include "feature_creation/node/Node.hpp"

template <typename T>
class ScObjPair
{
private:
    friend class boost::serialization::access;
    /**
     * @brief Serialization function to send over MPI
     *
     * @param ar Archive representation of node
     */
    template <typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        ar& _obj;
        ar& _score;
    }

protected:
    T _obj;         //!< The object paired with the score
    double _score;  //!< The score

public:
    /**
     * @brief Default Constructor
     */
    ScObjPair() : _score(std::numeric_limits<double>::infinity()) {}

    /**
     * @brief Constructor
     *
     * @param obj The objure
     * @param score The score
     */
    ScObjPair(T obj, double score) : _obj(obj), _score(score) {}

    /**
     * @brief Copy Constructor
     *
     * @param o The pair to be copied
     */
    ScObjPair(const ScObjPair& o) = default;

    /**
     * @brief Move Constructor
     *
     * @param o The pair to be copied
     */
    ScObjPair(ScObjPair&& o) = default;

    /**
     * @brief Copy Assignment operator
     *
     * @param o The pair to be copied
     */
    ScObjPair& operator=(const ScObjPair& o) = default;

    /**
     * @brief Move Assignment operator
     *
     * @param o The pair to be moved
     */
    ScObjPair& operator=(ScObjPair&& o) = default;

    /**
     * @brief Less than operator
     *
     * @param pair_2 ScObjPair to compare against
     * @return True if _score is less than pair_2.score
     */
    inline bool operator<(const ScObjPair& pair_2) const { return _score < pair_2._score; }
    /**
     * @brief Greater than operator
     *
     * @param pair_2 ScObjPair to compare against
     * @return True if _score is greater than pair_2.score
     */
    inline bool operator>(const ScObjPair& pair_2) const { return _score > pair_2._score; }
    /**
     * @brief Less than  or equal to operator
     *
     * @param pair_2 ScObjPair to compare against
     * @return True if _score is less than or equal to  pair_2.score
     */
    inline bool operator<=(const ScObjPair& pair_2) const { return _score <= pair_2._score; }
    /**
     * @brief Greater than  or equal to operator
     *
     * @param pair_2 ScObjPair to compare against
     * @return True if _score is greater than or equal to  pair_2.score
     */
    inline bool operator>=(const ScObjPair& pair_2) const { return _score >= pair_2._score; }

    /**
     * @brief Less than operator
     *
     * @param pair_2 ScObjPair to compare against
     * @return True if _score is less than pair_2.score
     */
    inline bool operator<(const ScObjPair& pair_2) { return _score < pair_2._score; }
    /**
     * @brief Greater than operator
     *
     * @param pair_2 ScObjPair to compare against
     * @return True if _score is greater than pair_2.score
     */
    inline bool operator>(const ScObjPair& pair_2) { return _score > pair_2._score; }
    /**
     * @brief Less than  or equal to operator
     *
     * @param pair_2 ScObjPair to compare against
     * @return True if _score is less than or equal to  pair_2.score
     */
    inline bool operator<=(const ScObjPair& pair_2) { return _score <= pair_2._score; }
    /**
     * @brief Greater than  or equal to operator
     *
     * @param pair_2 ScObjPair to compare against
     * @return True if _score is greater than or equal to  pair_2.score
     */
    inline bool operator>=(const ScObjPair& pair_2) { return _score >= pair_2._score; }

    /**
     * @brief The object of the pair
     */
    inline T obj() const { return _obj; }

    /**
     * @brief The score of the pair
     */
    inline double score() const { return _score; }

    /**
     * @brief The object of the pair
     */
    inline void set_obj(T new_obj) { _obj = new_obj; }

    /**
     * @brief The score of the pair
     */
    inline void set_score(double new_score) { _score = new_score; }
};

typedef ScObjPair<node_ptr> node_sc_pair;
typedef ScObjPair<std::vector<int>> inds_sc_pair;

#endif
