// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file descriptor_identifier/SISSOClassifier.cpp
 *  @brief Implements a class for solving classification problems with SISSO
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "descriptor_identifier/solver/SISSOClassifier.hpp"

SISSOClassifier::SISSOClassifier(const InputParser inputs,
                                 const std::shared_ptr<FeatureSpace> feat_space)
    : SISSOSolver(inputs, feat_space), _c(1000.0), _width(1.0e-5)
{
    setup_svm_d_mat();
}

SISSOClassifier::SISSOClassifier(std::vector<std::string> sample_ids_train,
                                 std::vector<std::string> sample_ids_test,
                                 std::vector<std::string> task_names,
                                 std::vector<int> task_sizes_train,
                                 std::vector<int> task_sizes_test,
                                 std::vector<int> leave_out_inds,
                                 Unit prop_unit,
                                 std::string prop_label,
                                 std::vector<double> prop_train,
                                 std::vector<double> prop_test,
                                 std::shared_ptr<FeatureSpace> feat_space,
                                 int n_dim,
                                 int n_residual,
                                 int n_models_store,
                                 bool fix_intercept,
                                 std::vector<std::vector<ModelClassifier>> models)
    : SISSOSolver(sample_ids_train,
                  sample_ids_test,
                  task_names,
                  task_sizes_train,
                  task_sizes_test,
                  leave_out_inds,
                  prop_unit,
                  prop_label,
                  prop_train,
                  prop_test,
                  "classification",
                  feat_space,
                  n_dim,
                  n_residual,
                  n_models_store,
                  fix_intercept),
      _c(1000.0),
      _width(1.0e-5),
      _models(models)
{
    setup_svm_d_mat();
}

void SISSOClassifier::setup_svm_d_mat()
{
    if (_fix_intercept)
    {
        std::cerr << "For classification the bias term can't be fixed at 0. Changing it now."
                  << std::endl;
        _fix_intercept = false;
    }
    setup_d_mat_transfer();

    int start = 0;
    for (int tt = 0; tt < _n_task; ++tt)
    {
        _svm_vec.push_back(SVMWrapper(
            _c, _loss->n_class(tt), _n_dim, _task_sizes_train[tt], _loss->prop_pointer() + start));
        start += _task_sizes_train[tt];
    }
}

void SISSOClassifier::setup_d_mat_transfer()
{
    _n_class = vector_utils::unique<double>(_loss->prop_train()).size();
    int task_start = 0;
    for (int tt = 0; tt < _n_task; ++tt)
    {
        std::vector<int> inds(_task_sizes_train[tt]);
        std::iota(inds.begin(), inds.end(), task_start);

        util_funcs::argsort<double>(inds.data(), inds.data() + inds.size(), _loss->prop_pointer());
        _sample_inds_to_sorted_dmat_inds[inds[0]] = task_start;

        int cls_start = 0;
        for (int ii = 1; ii < inds.size(); ++ii)
        {
            _sample_inds_to_sorted_dmat_inds[inds[ii]] = ii + task_start;
        }
        task_start += _task_sizes_train[tt];
    }
}

void SISSOClassifier::transfer_d_mat_to_sorted() const
{
    prop_sorted_d_mat::resize_sorted_d_matrix_arr(node_value_arrs::N_SELECTED);
    for (auto& el : _sample_inds_to_sorted_dmat_inds)
    {
        dcopy_(node_value_arrs::N_SELECTED,
               &node_value_arrs::D_MATRIX[el.first],
               node_value_arrs::N_SAMPLES,
               &prop_sorted_d_mat::SORTED_D_MATRIX[el.second],
               prop_sorted_d_mat::N_SAMPLES);
    }
}

std::array<double, 2> SISSOClassifier::svm_error(std::vector<SVMWrapper>& svm,
                                                 const std::vector<int>& feat_inds) const
{
    double error = 0.0;
    double dist_error = 0.0;
    for (int tt = 0; tt < _n_task; ++tt)
    {
        svm[tt].train(feat_inds, tt, false);
        error += svm[tt].n_misclassified();
        for (auto& coefs : svm[tt].coefs())
        {
            dist_error += 1.0 / util_funcs::norm(coefs.data(), feat_inds.size());
        }
    }
    dist_error /= static_cast<double>(_n_task * _n_class);
    return {error, dist_error};
}

void SISSOClassifier::update_min_inds_scores(const std::vector<int>& inds,
                                             double score,
                                             int max_error_ind,
                                             std::vector<inds_sc_pair>& min_sc_inds)
{
    // Make a copy of the SVM
    std::vector<SVMWrapper> svm_vec(_svm_vec);
    std::array<double, 2> svm_err = svm_error(svm_vec, inds);
    score += svm_err[0] + svm_err[1] / static_cast<double>(_n_samp);

    if (score < min_sc_inds[max_error_ind].score())
    {
        min_sc_inds[max_error_ind].set_score(score);
        min_sc_inds[max_error_ind].set_obj(inds);
    }
}

void SISSOClassifier::add_models(const std::vector<std::vector<int>> indexes)
{
    _models.push_back({});
    std::vector<std::vector<model_node_ptr>> min_nodes;

    for (auto& inds : indexes)
    {
        min_nodes.push_back(std::vector<model_node_ptr>(inds.size()));
        for (int ii = 0; ii < inds.size(); ++ii)
        {
            int index = inds[ii];
            min_nodes.back()[ii] = std::make_shared<ModelNode>(_feat_space->phi_selected()[index]);
        }
        ModelClassifier model(_prop_label,
                              _prop_unit,
                              loss_function_util::copy(_loss),
                              min_nodes.back(),
                              _leave_out_inds,
                              _sample_ids_train,
                              _sample_ids_test,
                              _task_names);
        _models.back().push_back(model);
    }
    min_nodes.resize(_n_residual);
    _loss->reset_projection_prop(min_nodes);
}

void SISSOClassifier::output_models()
{
    if (_mpi_comm->rank() == 0)
    {
        for (int rr = 0; rr < _n_models_store; ++rr)
        {
            _models.back()[rr].to_file("models/train_dim_" + std::to_string(_models.size()) +
                                       "_model_" + std::to_string(rr) + ".dat");
            if (_leave_out_inds.size() > 0)
            {
                _models.back()[rr].to_file("models/test_dim_" + std::to_string(_models.size()) +
                                               "_model_" + std::to_string(rr) + ".dat",
                                           false);
            }
        }
    }
}

bool SISSOClassifier::continue_calc(int dd)
{
    bool cont = ((dd <= _n_dim) &&
                 (*std::max_element(
                      _loss->prop_project_pointer(),
                      _loss->prop_project_pointer() + _loss->n_prop_project() * _n_samp) > 0.0));

    if (!cont && (dd <= _n_dim))
    {
        std::cerr << "WARNING: All points sperated before reaching the requested dimension."
                  << std::endl;
    }

    return cont;
}
