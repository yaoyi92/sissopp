// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file descriptor_identifier/SISSORegressor.cpp
 *  @brief Implements a class to solve regression problems with SISSO
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "descriptor_identifier/solver/SISSORegressor.hpp"

SISSORegressor::SISSORegressor(const InputParser inputs,
                               const std::shared_ptr<FeatureSpace> feat_space)
    : SISSOSolver(inputs, feat_space)
{
}

SISSORegressor::SISSORegressor(std::vector<std::string> sample_ids_train,
                               std::vector<std::string> sample_ids_test,
                               std::vector<std::string> task_names,
                               std::vector<int> task_sizes_train,
                               std::vector<int> task_sizes_test,
                               std::vector<int> leave_out_inds,
                               Unit prop_unit,
                               std::string prop_label,
                               std::vector<double> prop_train,
                               std::vector<double> prop_test,
                               std::shared_ptr<FeatureSpace> feat_space,
                               int n_dim,
                               int n_residual,
                               int n_models_store,
                               bool fix_intercept,
                               std::vector<std::vector<ModelRegressor>> models,
                               std::string calc_type)
    : SISSOSolver(sample_ids_train,
                  sample_ids_test,
                  task_names,
                  task_sizes_train,
                  task_sizes_test,
                  leave_out_inds,
                  prop_unit,
                  prop_label,
                  prop_train,
                  prop_test,
                  calc_type,
                  feat_space,
                  n_dim,
                  n_residual,
                  n_models_store,
                  fix_intercept),
      _models(models)
{
}

void SISSORegressor::add_models(const std::vector<std::vector<int>> indexes)
{
    _models.push_back({});
    std::vector<std::vector<model_node_ptr>> min_nodes;

    for (auto& inds : indexes)
    {
        min_nodes.push_back(std::vector<model_node_ptr>(inds.size()));
        for (int ii = 0; ii < inds.size(); ++ii)
        {
            int index = inds[ii];
            min_nodes.back()[ii] = std::make_shared<ModelNode>(_feat_space->phi_selected()[index]);
        }
        ModelRegressor model(_prop_label,
                             _prop_unit,
                             loss_function_util::copy(_loss),
                             min_nodes.back(),
                             _leave_out_inds,
                             _sample_ids_train,
                             _sample_ids_test,
                             _task_names);
        _models.back().push_back(model);
    }
    min_nodes.resize(_n_residual);
    _loss->reset_projection_prop(min_nodes);
}

void SISSORegressor::output_models()
{
    if (_mpi_comm->rank() == 0)
    {
        for (int rr = 0; rr < _n_models_store; ++rr)
        {
            _models.back()[rr].to_file("models/train_dim_" + std::to_string(_models.size()) +
                                       "_model_" + std::to_string(rr) + ".dat");
            if (_leave_out_inds.size() > 0)
            {
                _models.back()[rr].to_file("models/test_dim_" + std::to_string(_models.size()) +
                                               "_model_" + std::to_string(rr) + ".dat",
                                           false);
            }
        }
    }
}

void SISSORegressor::update_min_inds_scores(const std::vector<int>& inds,
                                            double score,
                                            int max_error_ind,
                                            std::vector<inds_sc_pair>& min_sc_inds)
{
    if (score < -1.0)
    {
        std::string err_msg =
            "A parameter passed to dgels was invalid. This is likely from a NaN in the descriptor "
            "matrix. The features that caused this are: ";
        for (auto& ind : inds)
        {
            err_msg += std::to_string(ind) + ": " + _feat_space->phi_selected()[ind]->expr() + ", ";
        }
        throw std::logic_error(err_msg.substr(0, err_msg.size() - 2) + ".");
    }
    else if (score < 0.0)
    {
        std::string
            err_msg = "Descriptor matrix is not full-rank. The features that caused this are: ";
        for (auto& ind : inds)
        {
            err_msg += std::to_string(ind) + ": " + _feat_space->phi_selected()[ind]->expr() + ", ";
        }
        std::cerr << err_msg.substr(0, err_msg.size() - 2) << "." << std::endl;
    }
    else if (score < min_sc_inds[max_error_ind].score())
    {
        min_sc_inds[max_error_ind].set_score(score);
        min_sc_inds[max_error_ind].set_obj(inds);
    }
}
