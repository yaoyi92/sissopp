// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file descriptor_identifier/solver/SISSOSolver.hpp
 *  @brief Defines the base class for creating solvers using SISSO
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef SISSO_DESCRIPTOR_ID
#define SISSO_DESCRIPTOR_ID

#include "descriptor_identifier/model/Model.hpp"
#include "feature_creation/feature_space/FeatureSpace.hpp"
#include "openmp_reduction/ScoreObjectPair.hpp"
#include "openmp_reduction/openmp_reduction.hpp"

// DocString: cls_sisso
/**
 * @brief Base class for creating Solvers for the DI step of SISSO
 */
class SISSOSolver
{
protected:
    // clang-format off
    const std::vector<std::string> _sample_ids_train; //!< Vector storing all sample ids for the training samples
    const std::vector<std::string> _sample_ids_test; //!< Vector storing all sample ids for the test samples
    const std::vector<std::string> _task_names; //!< Vector storing the ID of the task names

    const std::vector<int> _task_sizes_train; //!< Number of training samples per task
    const std::vector<int> _task_sizes_test; //!< Number of testing samples per task
    const std::vector<int> _leave_out_inds; //!< List of indexes from the initial data file in the test set

    const Unit _prop_unit; //!< The Unit of the property
    const std::string _prop_label; //!< The label of the property

    const std::shared_ptr<FeatureSpace> _feat_space; //!< Feature Space for the problem
    std::shared_ptr<MPI_Interface> _mpi_comm; //!< MPI Communicator for the calculation
    std::shared_ptr<LossFunction> _loss; //!< The loss function used to evaluate models

    const int _n_task; //!< The number of tasks
    const int _n_samp; //!< The number of training samples per feature
    const int _n_dim; //!< The maximum number of features allowed in the linear model
    const int _n_residual; //!< Number of residuals to pass to the next sis model
    const int _n_models_store; //!< The number of models to output to files

    bool _fix_intercept; //!< If true the bias term is fixed at 0
    // clang-format on
public:
    // DocString: sisso_di_init
    /**
     * @brief Constructor for the Solver
     *
     * @param inputs (Inputs) The InputParser object for the calculation
     * @param feat_space (FeatureSpace) Feature Space for the problem
     */
    SISSOSolver(const InputParser inputs, const std::shared_ptr<FeatureSpace> feat_space);

    /**
     * @brief Constructor for the Solver from unpickling
     *
     * @param sample_ids_train Vector storing all sample ids for the training samples
     * @param sample_ids_test Vector storing all sample ids for the test samples
     * @param task_names Vector storing the ID of the task names
     * @param task_sizes_train Number of training samples per task
     * @param task_sizes_test Number of testing samples per task
     * @param leave_out_inds List of indexes from the initial data file in the test set
     * @param prop_unit The Unit of the property
     * @param prop_label The label of the property
     * @param prop_train The property for all training samples
     * @param prop_test The property for all the test samples
     * @param calc_type The type of calculation
     * @param feat_space Feature Space for the problem
     * @param n_dim The maximum number of features allowed in the linear model
     * @param n_residual Number of residuals to pass to the next sis model
     * @param n_models_store The number of models to output to files
     * @param fix_intercept If true the bias term is fixed at 0
     */
    SISSOSolver(std::vector<std::string> sample_ids_train,
                std::vector<std::string> sample_ids_test,
                std::vector<std::string> task_names,
                std::vector<int> task_sizes_train,
                std::vector<int> task_sizes_test,
                std::vector<int> leave_out_inds,
                Unit prop_unit,
                std::string prop_label,
                std::vector<double> prop_train,
                std::vector<double> prop_test,
                std::string calc_type,
                std::shared_ptr<FeatureSpace> feat_space,
                int n_dim,
                int n_residual,
                int n_models_store,
                bool fix_intercept);

    /**
     * @brief If true calculate the model for the dimension dd
     *
     * @param dd Dimension of the model to train
     * @return true if the requested dimension should be calculated.
     */
    virtual inline bool continue_calc(int dd) { return dd <= _n_dim; }

    /**
     * @brief Output the models to files and copy the residuals
     */
    virtual void output_models() = 0;

    /**
     * @brief Perform any steps that need to be done to initialize the regularization
     */
    virtual inline void setup_regulairzation() {}

    /**
     * @brief Set the min_scores and min_inds vectors given a score and max_error_ind
     *
     * @param inds The current set of indexes
     * @param score The score for the current set of indexes
     * @param max_error_ind The current index of the maximum score among the best models
     * @param min_sc_inds Current list of the indexes that make the best models and their respective scores
     */
    virtual void update_min_inds_scores(const std::vector<int>& inds,
                                        double score,
                                        int max_error_ind,
                                        std::vector<inds_sc_pair>& min_sc_inds) = 0;

    /**
     * @brief Create a Model for a given set of features and store them in _models
     *
     * @param indexes Vector storing all of the indexes of features in _feat_space->phi_selected() to use for the model
     */
    virtual void add_models(const std::vector<std::vector<int>> indexes) = 0;

    // DocString: sisso_di_fit
    /**
     * @brief Iteratively run SISSO on the FeatureSpace an Property vector until the maximum dimenisonality is reached
     */
    void fit();

    /**
     * @brief Preform an l0-Regularization to find the best n_dim dimensional model
     *
     * @param n_dim The number of features to use in the linear model
     */
    void l0_regularization(const int n_dim);

    // DocString: sisso_di_feat_space
    /**
     * @brief The FeatureSpace object associated with this solver
     */
    inline std::shared_ptr<FeatureSpace> feat_space() const { return _feat_space; }

    /**
     * @brief The property vector for all of the training samples
     */
    inline std::vector<double> prop_train() const { return _loss->prop_train(); }

    /**
     * @brief The property vector for all of the test samples
     */
    inline std::vector<double> prop_test() const { return _loss->prop_test(); }

    // DocString: sisso_di_prop_unit_py
    /**
     * @brief The Unit of the property
     */
    inline Unit prop_unit() const { return _prop_unit; }

    // DocString: sisso_di_prop_label_py
    /**
     * @brief The label of the property
     */
    inline std::string prop_label() const { return _prop_label; }

    /**
     * @brief The error vector for the training data
     */
    inline std::vector<double> error() const { return _loss->error_train(); }

    // DocString: sisso_di_n_samp
    /**
     * @brief The number of samples in the training data per feature
     */
    inline int n_samp() const { return _n_samp; }

    // DocString: sisso_di_n_dim
    /**
     * @brief The maximum number of features allowed in the linear model
     */
    inline int n_dim() const { return _n_dim; }

    // DocString: sisso_di_n_residual
    /**
     * @brief The number of residuals each iteration of SIS acts on
     */
    inline int n_residual() const { return _n_residual; }

    // DocString: sisso_di_n_models_store
    /**
     * @brief The number of models to output to files
     */
    inline int n_models_store() const { return _n_models_store; }

    // DocString: sisso_di_fix_intercept
    /**
     * @brief If true the bias term is fixed at 0
     */
    inline bool fix_intercept() const { return _fix_intercept; }

    // DocString: sisso_di_task_sizes_train
    /**
     * @brief The number of training samples per task
     */
    inline std::vector<int> task_sizes_train() const { return _task_sizes_train; }

    // DocString: sisso_di_task_sizes_test
    /**
     * @brief The number of test samples per task
     */
    inline std::vector<int> task_sizes_test() const { return _task_sizes_test; }

    // DocString: sisso_di_leave_out_inds
    /**
     * @brief List of indexes from the initial data file in the test set
     */
    inline std::vector<int> leave_out_inds() const { return _leave_out_inds; }

    // DocString: sisso_di_sample_ids_train
    /**
     * @brief Vector storing all sample ids for the training samples
     */
    inline std::vector<std::string> sample_ids_train() const { return _sample_ids_train; }

    // DocString: sisso_di_sample_ids_test
    /**
     * @brief Vector storing all sample ids for the test samples
     */
    inline std::vector<std::string> sample_ids_test() const { return _sample_ids_test; }

    // DocString: sisso_di_task_names
    /**
     * @brief Vector storing the ID of the task names
     */
    inline std::vector<std::string> task_names() const { return _task_names; }

#ifdef PY_BINDINGS
    // DocString: sisso_di_prop_train_py
    /**
     * @brief The property vector for all of the training samples as a numpy array
     */
    inline py::array_t<double> prop_train_py() { return py::cast(_loss->prop_train()); }

    // DocString: sisso_di_prop_test_py
    /**
     * @brief The property error for all of the test samples as a numpy array
     */
    inline py::array_t<double> prop_test_py() { return py::cast(_loss->prop_test()); }

    /**
     * @brief The error vector for the training data as a numpy array
     */
    inline py::array_t<double> error_py() { return py::cast(_loss->error_train()); }
#endif
};

// GCOV_EXCL_START      GCOVR_EXCL_START        LCOV_EXCL_START
#ifdef PY_BINDINGS
namespace py = pybind11;
template <class SISSOSolverBase = SISSOSolver>
class PySISSOSolver : public SISSOSolverBase
{
public:
    using SISSOSolverBase::SISSOSolverBase;

    bool continue_calc(int dd) override
    {
        PYBIND11_OVERRIDE(bool, SISSOSolverBase, continue_calc, dd);
    }

    void output_models() override
    {
        PYBIND11_OVERRIDE_PURE(void, SISSOSolverBase, output_models, );
    }

    void setup_regulairzation() override
    {
        PYBIND11_OVERRIDE(void, SISSOSolverBase, setup_regulairzation, );
    }

    void update_min_inds_scores(const std::vector<int>& inds,
                                double score,
                                int max_error_ind,
                                std::vector<inds_sc_pair>& min_sc_inds) override
    {
        PYBIND11_OVERRIDE_PURE(
            void, SISSOSolverBase, update_min_inds_scores, inds, score, max_error_ind, min_sc_inds);
    }

    void add_models(const std::vector<std::vector<int>> indexes) override
    {
        PYBIND11_OVERRIDE_PURE(void, SISSOSolverBase, add_models, indexes);
    }
};
#endif
// GCOV_EXCL_STOP      GCOVR_EXCL_STOP        LCOV_EXCL_STOP

#endif
