// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file descriptor_identifier/model/Model.hpp
 *  @brief Defines the base class of the output Models for SISSO
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef MODEL
#define MODEL

#include <boost/filesystem.hpp>
#include <fstream>
#include <iomanip>

#include "loss_function/utils.hpp"

#ifdef PY_BINDINGS
namespace py = pybind11;
#endif

// DocString: cls_model
/**
 * @brief Base class to store the output models from SISSO
 */
class Model
{
protected:
    // clang-format off
    std::vector<std::string> _sample_ids_train; //!< Vector storing all sample ids for the training samples
    std::vector<std::string> _sample_ids_test; //!< Vector storing all sample ids for the test samples
    std::map<std::string, int> _task_names; //!< Map storing the ID of the task names and the index stored in the coefs array

    int _n_samp_train; //!< The number of samples per feature
    int _n_samp_test; //!< The number of test samples per feature
    int _n_dim; //!< The number of dimensions of the model

    std::vector<model_node_ptr> _feats; //!< List of features in the model
    std::vector<std::vector<double>> _coefs; //!< Coefficients for the features
    std::vector<int> _leave_out_inds; //!< The indexes used as the test set

    std::shared_ptr<LossFunction> _loss; //!< The LossFunction used to evaluate the model
    std::string _prop_label; //!< The property label for the model
    Unit _prop_unit; //!< The Unit for the property

    std::string _task_eval; //!< Which set of coefficients to use for evaluating the model for a new data set

    bool _fix_intercept; //!< If true fix intercept to 0
    // clang-format on
public:
    // DocString: model_init
    /**
     * @brief Construct a model (Base Constructor)
     */
    Model();

    // DocString: model_init_base
    /**
     * @brief Construct a Model using a loss function and a set of features
     *
     * @param prop_label The label for the property
     * @param prop_unit The unit of the property
     * @param loss The LossFunction used to calculate the model
     * @param feats The features of the model
     * @param leave_out_inds The indexes of the samples for the test set
     * @param sample_ids_train A vector storing all sample ids for the training samples
     * @param sample_ids_test A vector storing all sample ids for the test samples
     * @param task_names A vector storing the ID of the task names
     */
    Model(const std::string prop_label,
          const Unit prop_unit,
          const std::shared_ptr<LossFunction> loss,
          const std::vector<model_node_ptr> feats,
          const std::vector<int> leave_out_inds,
          const std::vector<std::string> sample_ids_train,
          const std::vector<std::string> sample_ids_test,
          const std::vector<std::string> task_names);

    // DocString: model_init_pickle
    /**
     * @brief Construct a Model using a loss function and a set of features
     *
     * @param prop_label The label for the property
     * @param prop_unit The unit of the property
     * @param prop_train The property vector of the training set
     * @param prop_test The property vector of the test set
     * @param task_sizes_train The number of samples in the training set for each task
     * @param task_sizes_test The number of samples in the test set for each task
     * @param feats The features of the model
     * @param leave_out_inds The indexes of the samples for the test set
     * @param sample_ids_train A vector storing all sample ids for the training samples
     * @param sample_ids_test A vector storing all sample ids for the test samples
     * @param task_names A vector storing the ID of the task names
     * @param fix_intercept If true set the bias term to 0
     */
    Model(std::string prop_label,
          Unit prop_unit,
          std::vector<double> prop_train,
          std::vector<double> prop_test,
          std::vector<int> task_sizes_train,
          std::vector<int> task_sizes_test,
          std::vector<model_node_ptr> feats,
          std::vector<int> leave_out_inds,
          std::vector<std::string> sample_ids_train,
          std::vector<std::string> sample_ids_test,
          std::vector<std::string> task_names,
          bool fix_intercept);

    /**
     * @brief Copy Constructor
     *
     * @param o Model to be copied
     */
    Model(const Model& o) = default;

    /**
     * @brief Move Constructor
     *
     * @param o Model to be moved
     */
    Model(Model&& o) = default;

    /**
     * @brief Copy Assignment operator
     *
     * @param o Model to be copied
     */
    Model& operator=(const Model& o) = default;

    /**
     * @brief Move Assignment operator
     *
     * @param o Model to be moved
     */
    Model& operator=(Model&& o) = default;

    /**
     * @brief Copy the training error into a different vector
     *
     * @param res Pointer to the head of the new vector to store the residual
     */
    virtual void copy_error(double* res) const = 0;

    /**
     * @brief Read an output file and extract all relevant information from it
     *
     * @param train_filename Name of the training output file
     * @param test_filename Name of the test output file
     */
    void populate_model(const std::string train_filename, const std::string test_filename = "");

    /**
     * @brief Create the LossFunction for the Model when constructed from a file
     *
     * @param prop_train The property vector for all of the training samples
     * @param prop_test The property vector for all of the test samples
     * @param task_sizes_train The number of training samples per task
     * @param task_sizes_test The number of test samples per task
     */
    virtual void make_loss(std::vector<double>& prop_train,
                           std::vector<double>& prop_test,
                           std::vector<int>& task_sizes_train,
                           std::vector<int>& task_sizes_test) = 0;

    /**
     * @brief The LossFunction used to evaluate the model
     */
    std::shared_ptr<LossFunction> loss() { return _loss; }

    /**
     * @brief Based off of the model line in a model file determine if the bias term is 0.0
     *
     * @param model_line the model line from the file
     * @return True if the intercept should be fixed
     */
    virtual bool has_fixed_intercept(std::string model_line) = 0;

    /**
     * @brief Set error summary variables from a file
     *
     * @param error_line the line to set the error from
     * @param train True if file is for training data
     */
    virtual void set_error_from_file(std::string error_line, bool train) = 0;

    /**
     * @brief Check if a given line summarizes the error of a model
     *
     * @param line to see if it contains information about the error
     * @return True if the line summarizes the error
     */
    virtual bool is_error_line(std::string line) = 0;

    /**
     * @brief Fills _feats with the correct features based on a set of expressions
     *
     * @param feat_exprs Vectors containing the features to be created
     * @param feat_vals The data for the feature's training data
     * @param feat_tset_vals The data for the feature's test data
     */
    void create_model_nodes_from_expressions(std::vector<std::string> feat_exprs,
                                             std::vector<std::vector<double>> feat_vals,
                                             std::vector<std::vector<double>> feat_test_vals);

    /**
     * @brief Convert a numpy array to an x_in vector ptr
     *
     * @param x_in array to be converted
     * @return The vector representation of x_in
     */
    std::vector<double> convert_x_in(std::vector<double> x_in) const;

    /**
     * @brief Convert a dictionary to an x_in vector ptr
     *
     * @param x_in dictionary to be converted
     * @return The vector representation of x_in
     */
    std::vector<double> convert_x_in(std::map<std::string, double> x_in) const;

    /**
     * @brief Convert a numpy array to an x_in vector ptr
     *
     * @param x_in array to be converted shape (n_samples, n_primary_features)
     * @return The vector representation of x_in
     */
    std::vector<std::vector<double>> convert_x_in(std::vector<std::vector<double>> x_in) const;

    /**
     * @brief Convert a dictionary to an x_in vector ptr
     *
     * @param x_in dictionary to be converted
     * @return The vector representation of x_in
     */
    std::vector<std::vector<double>> convert_x_in(
        std::map<std::string, std::vector<double>> x_in) const;

    /**
     * @brief Checks and correct a task list for evaluating multiple data poitns
     *
     * @param tasks The list of tasks
     * @param n_pts number of pointsto evaluate
     *
     * @return The task list with correct values
     */
    std::vector<std::string> check_tasks(std::vector<std::string> tasks, int n_pts) const;

    /**
     * @brief Evaluate the value of the features for a new sample
     *
     * @param x_in The data point to evaluate the model (order the same as appending the results of _feats[nn]->x_in_expr_list() for all feature)
     *
     * @return A vector containing the value of each selected feature for the new sample
     */
    std::vector<double> eval_features(double* x_in) const;

    /**
     * @brief Evaluate the value of the features for a new set of samples
     *
     * @param x_in A vector of pointers to the set of values for new data points (one pointer per each feature and order the same as appending the results of _feats[nn]->x_in_expr_list() for all feature)
     *
     * @return A vector of vectors containing all values of each selected feature at each point in x_in
     */
    std::vector<std::vector<double>> eval_features(std::vector<double>* x_in) const;

    /**
     * @brief Evaluate the model for a new point
     *
     * @param feature_values The value of each selected feature at the given point
     * @param task The task associated with the point
     *
     * @return The prediction of the model for a given data point
     */
    virtual double eval_from_feature_vals(std::vector<double>& feature_vals,
                                          std::string task) const = 0;

    /**
     * @brief Evaluate the model for a new set of new points
     *
     * @param feature_values The value of each selected feature at each new point
     * @param task The task associated with each new point
     *
     * @return The prediction of the model for the set of data poitns
     */
    virtual std::vector<double> eval_from_feature_vals(
        std::vector<std::vector<double>>& feature_vals,
        const std::vector<std::string>& tasks) const = 0;

    /**
     * @brief Evaluate the model for a new point
     *
     * @param x_in pointer to the new data point (order the same as appending the results of _feats[nn]->x_in_expr_list() for all feature)
     * @param task The task associated with the point
     *
     * @return The prediction of the model for a given data point
     */
    double eval(double* x_in, const std::string task) const;

    /**
     * @brief Evaluate the model for a new set of new points
     *
     * @param x_in  a vector of pointers to the set of values for new data points (one pointer per each feature and order the same as appending the results of _feats[nn]->x_in_expr_list() for all feature)
     * @param tasks The tasks associated with each point
     *
     * @return The prediction of the model for a given set of data points
     */
    std::vector<double> eval(std::vector<double>* x_in,
                             const std::vector<std::string>& tasks) const;

    // DocString: model_eval_list_task
    /**
     * @brief Evaluate the model for a new point
     *
     * @param x_in (list of float) a vector containing the new data point
     * @param task (str) The task associated with the point
     *
     * @return The prediction of the model for a given data point
     */
    double eval(std::vector<double> x_in, const std::string task) const;

    // DocString: model_eval_many_list_task
    /**
     * @brief Evaluate the model for a new set of new points
     *
     * @param x_in (list of list of float) a vector of a vectors containing the new data points to evaluate the models over (one vector per feature in the data set)
     * @param tasks (list of str) The tasks associated with each point
     *
     * @return The prediction of the model for a given set of data points
     */
    std::vector<double> eval(std::vector<std::vector<double>> x_in,
                             const std::vector<std::string>& tasks) const;

    // DocString: model_eval_dict_task
    /**
     * @brief Evaluate the model for a new point
     *
     * @param x_in (dict, key: str, val:float) A map of the feature names to data associated with each feature
     * @param task (str) The task associated with the point
     *
     * @return The prediction of the model for a given data point
     */
    double eval(std::map<std::string, double> x_in, const std::string task) const;

    // DocString: model_eval_many_dict_task_list
    /**
     * @brief Evaluate the model for a new set of new points
     *
     * @param x_in (dict, key: str, val: np.ndarray) A map of the feature names to data associated with each feature
     * @param tasks (list of str) The tasks associated with each point
     *
     * @return The prediction of the model for a given set of data points
     */
    std::vector<double> eval(std::map<std::string, std::vector<double>> x_in,
                             const std::vector<std::string>& tasks) const;

    // DocString: model_set_task_eval
    /**
     * @brief Sets which set of coefficients the model should use for evaluating new data points
     *
     * @param task Task to evaluate
     */
    inline void set_task_eval(int task)
    {
        auto task_it = _task_names.begin();
        for (int tt = 0; tt < task; ++tt)
        {
            ++task_it;
        }
        _task_eval = task_it->first;
    }

    // DocString: model_get_task_eval
    /**
     * @brief Gets which set of coefficients the model should use for evaluating new data points
     */
    inline int get_task_eval() const { return _task_names.find(_task_eval)->second; }

    // DocString: model_n_samp_train
    /**
     * @brief The number of samples in the training data per feature
     */
    inline int n_samp_train() const { return _n_samp_train; }

    // DocString: model_n_samp_test
    /**
     * @brief The number of samples in the test data per feature
     */
    inline int n_samp_test() const { return _n_samp_test; }

    // DocString: model_n_dim
    /**
     * @brief The dimensionality of the data
     */
    inline int n_dim() const { return _n_dim; }

    // DocString: model_prop_unit
    /**
     * @brief The unit of the property
     */
    inline Unit prop_unit() const { return _prop_unit; }

    // DocString: model_prop_label
    /**
     * @brief The label for the property
     */
    inline std::string prop_label() const { return _prop_label; }

    // DocString: model_to_file
    /**
     * @brief Convert the Model into an output file
     *
     * @param filename (str) The name of the file to output to
     * @param train (bool) If true output the training data
     */
    void to_file(const std::string filename, const bool train = true) const;

    /**
     * @brief Convert a set of inputs into a prediction and output that to a file
     *
     * @param filename (str) The name of the file to output to
     * @param prop The true values of the property for the given set of inputs
     * @param x_in The set data for a set of new data points (size of n_feature x n_points, and order the same as appending the results of _feats[nn]->x_in_expr_list() for all feature)
     * @param sample_ids The sample ids for all of the samples
     * @param task_size A map of listing the number of samples in each task
     * @param excluded_inds Indexes used for cross-validation
     */
    void prediction_to_file(const std::string filename,
                            const std::vector<double> prop,
                            std::vector<double>* x_in,
                            const std::vector<std::string>& sample_ids,
                            const std::vector<std::string>& task_ids,
                            const std::vector<int>& excluded_inds) const;

    /**
     * @brief Convert a set of inputs into a prediction and output that to a file
     *
     * @param filename (str) The name of the file to output to
     * @param prop The true values of the property for the given set of inputs
     * @param x_in The data set of the new points to evaluate and store inside the output file
     * @param sample_ids The sample ids for all of the samples
     * @param task_size A map of listing the number of samples in each task
     * @param excluded_inds Indexes used for cross-validation
     */
    void prediction_to_file(const std::string filename,
                            const std::vector<double> prop,
                            std::vector<std::vector<double>> x_in,
                            const std::vector<std::string> sample_ids,
                            const std::vector<std::string> task_ids,
                            const std::vector<int> excluded_inds) const;

    /**
     * @brief Convert a set of inputs into a prediction and output that to a file
     *
     * @param filename (str) The name of the file to output to
     * @param prop The true values of the property for the given set of inputs
     * @param x_in The data set of the new points to evaluate and store inside the output file
     * @param sample_ids The sample ids for all of the samples
     * @param task_size A map of listing the number of samples in each task
     * @param excluded_inds Indexes used for cross-validation
     */
    void prediction_to_file(const std::string filename,
                            const std::vector<double> prop,
                            std::map<std::string, std::vector<double>> x_in,
                            const std::vector<std::string> sample_ids,
                            const std::vector<std::string> task_ids,
                            const std::vector<int> excluded_inds) const;

    /**
     * @brief Convert the Model into an output file
     *
     * @param filename (str) The name of the file to output to
     * @param prop The true values of the property for the given set of inputs
     * @param prop_est The estimated values of the property for the given set of inputs
     * @param feat_vals The value of the selected features for each sample
     * @param sample_ids The sample ids for all of the samples
     * @param task_size A map of listing the number of samples in each task
     * @param excluded_inds Indexes used for cross-validation
     * @param error_summary A string charecterizing the average error of the model
     */
    void to_file(const std::string filename,
                 const std::vector<double>& prop,
                 const std::vector<double>& prop_est,
                 const std::vector<std::vector<double>>& feat_vals,
                 const std::vector<std::string>& sample_ids,
                 std::map<std::string, int>& task_sizes,
                 const std::vector<int>& excluded_inds,
                 const std::string error_summary) const;

    /**
     * @brief Get the block used to summarize the error in the output files
     *
     * @param train True if summary is for the training file
     */
    virtual std::string error_summary_string(bool train) const = 0;

    /**
     * @brief Get the block used to summarize the error in the output files
     *
     * @param prop The value of the true property
     * @param prop_est The value of the estimated property
     *
     * @returns error_summary The summary of the model error
     */
    virtual std::string error_summary_string(std::vector<double> prop,
                                             std::vector<double> prop_est) const = 0;

    /**
     * @brief Get the coefficients list header for output file
     */
    virtual std::string write_coefs() const = 0;

    // DocString: model_fix_intercept
    /**
     * @brief True if the bias term is 0.0
     */
    inline bool fix_intercept() const { return _fix_intercept; }

    // DocString: model_coefs
    /**
     * @brief The coefficient array for the model (n_task, n_dim + bias term)
     */
    inline std::vector<std::vector<double>> coefs() const { return _coefs; }

    /**
     * @brief Convert the model to a string

     * @return The string representation of the model
     */
    virtual std::string toString() const = 0;

    // DocString: model_latex_str
    /**
     * @brief Convert the model to a latexified string
     */
    virtual std::string toLatexString() const = 0;

    /**
     * @brief Get the matlab expression for this model

     * @return The matlab expression for the model
     */
    virtual std::string matlab_expr() const = 0;

    // DocString: model_to_matlab
    /**
     * @brief Convert the model into a Matlab function
     *
     * @param fxn_filename name of the file to print the function to
     */
    void write_matlab_fxn(std::string fxn_filename);

    /**
     * @brief The property vector for all of the training samples
     */
    virtual inline std::vector<double> prop_train() const { return _loss->prop_train(); }

    /**
     * @brief The property vector for all of the test samples
     */
    virtual inline std::vector<double> prop_test() const { return _loss->prop_test(); }

    /**
     * @brief The estimated property vector for all of the training samples
     */
    virtual inline std::vector<double> prop_train_est() const { return _loss->prop_train_est(); }

    /**
     * @brief The estimated property vector for all of the test samples
     */
    virtual inline std::vector<double> prop_test_est() const { return _loss->prop_test_est(); }

    // DocString: model_task_sizes_train
    /**
     * @brief The number of training samples per task
     */
    inline std::vector<int> task_sizes_train() const { return _loss->task_sizes_train(); }

    // DocString: model_task_sizes_test
    /**
     * @brief The number of test samples per task
     */
    inline std::vector<int> task_sizes_test() const { return _loss->task_sizes_test(); }

    /**
     * @brief The error vector for the training data as a numpy array
     */
    virtual inline std::vector<double> train_error() const { return _loss->error_train(); }

    /**
     * @brief The error vector for the test data as a numpy array
     */
    virtual inline std::vector<double> test_error() const { return _loss->error_test(); }

    // DocString: model_leave_out_inds
    /**
     * @brief The indexes of the samples for the test set
     */
    inline std::vector<int> leave_out_inds() const { return _leave_out_inds; }

    // DocString: model_feats
    /**
     * @brief A list of all the features of the model
     */
    inline std::vector<model_node_ptr> feats() const { return _feats; }

    // DocString: model_sample_ids_train
    /**
     * @brief The sample ids for the training set
     */
    inline std::vector<std::string> sample_ids_train() const { return _sample_ids_train; }

    // DocString: model_sample_ids_test
    /**
     * @brief The sample ids for the test set
     */
    inline std::vector<std::string> sample_ids_test() const { return _sample_ids_test; }

    // DocString: model_task_names
    /**
     * @brief The names of each task
     */
    inline std::vector<std::string> task_names() const
    {
        std::vector<std::string> tns(_task_names.size());
        std::transform(
            _task_names.begin(), _task_names.end(), tns.begin(), [](auto& tn) { return tn.first; });
        return tns;
    }

#ifdef PY_BINDINGS

    // DocString: model_prop_train
    /**
     * @brief The property vector for all of the training samples
     */
    inline py::array_t<double> prop_train_py()
    {
        return py::array_t<double>(_n_samp_train, prop_train().data());
    }

    // DocString: model_prop_test
    /**
     * @brief The property vector for all of the test samples
     */
    inline py::array_t<double> prop_test_py()
    {
        return py::array_t<double>(_n_samp_test, prop_test().data());
    }

    // DocString: model_prop_train_est
    /**
     * @brief The estimated property vector for all of the training samples
     */
    inline py::array_t<double> prop_train_est_py()
    {
        return py::array_t<double>(_n_samp_train, prop_train_est().data());
    }

    // DocString: model_prop_test_est
    /**
     * @brief The estimated property vector for all of the test samples
     */
    inline py::array_t<double> prop_test_est_py()
    {
        return py::array_t<double>(_n_samp_test, prop_test_est().data());
    }

    // DocString: model_train_error
    /**
     * @brief The error vector for the training data as a numpy array
     */
    inline py::array_t<double> train_error_py()
    {
        return py::array_t<double>(_n_samp_train, train_error().data());
    }

    // DocString: model_test_error
    /**
     * @brief The error vector for the test data as a numpy array
     */
    inline py::array_t<double> test_error_py()
    {
        return py::array_t<double>(_n_samp_test, test_error().data());
    }

    // DocString: model_leave_out_inds_arr
    /**
     * @brief The indexes of the samples for the test set
     */
    inline py::array_t<int> leave_out_inds_py()
    {
        return py::array_t<int>(_leave_out_inds.size(), _leave_out_inds.data());
    }

    // DocString: model_eval_list
    /**
     * @brief Evaluate the model for a new point
     *
     * @param x_in (list) The data point to evaluate the model (order the same as appending the results of _feats[nn]->x_in_expr_list() for all feature)
     * @return The prediction of the model for a given data point
     */
    inline double eval_py(std::vector<double> x_in) const { return eval(x_in, _task_eval); }

    // DocString: model_eval_dict
    /**
     * @brief Evaluate the model for a new point
     *
     * @param x_in_dct (dict) Dictionary describing the new point (\"feature expr\": value)
     * @return The prediction of the model for a given data point
     */
    inline double eval_py(std::map<std::string, double> x_in) const
    {
        return eval(x_in, _task_eval);
    }

    /**
     * @brief Checks and correct a task list for evaluating multiple data poitns
     *
     * @param tasks The list of tasks
     * @param n_pts number of pointsto evaluate
     *
     * @return The task list with correct values
     */
    inline std::vector<std::string> check_tasks(py::array tasks, int n_pts) const
    {
        return check_tasks(python_conv_utils::from_string_array(tasks), n_pts);
    }

    // DocString: model_eval_many_arr_task_list
    /**
     * @brief Evaluate the model for a set of new points
     *
     * @param x_in (np.ndarray, shape: (n_samples, n_primary_feats)) The set data for a set of new data points (size of n_feature x n_points, and order the same as appending the results of _feats[nn]->x_in_expr_list() for all feature)
     * @param tasks (list) The list of the tasks to use for each point
     * @return The prediction of the model for a given data point
     */
    py::array_t<double> eval_many_py(py::array_t<double> x_in,
                                     std::vector<std::string> tasks) const;

    // DocString: model_eval_many_dict_task_arr
    /**
     * @brief Evaluate the model for a set of new points
     *
     * @param x_in_dct (dict) The set of data points to evaluate the model. Keys must be strings representing feature expressions and vectors must be the same length
     * @param tasks (np.ndarray) The list of the tasks to use for each point
     * @return The prediction of the model for a given data point
     */
    py::array_t<double> eval_many_py(std::map<std::string, py::array_t<double>> x_in,
                                     std::vector<std::string> tasks) const;

    // DocString: model_eval_many_arr
    /**
     * @brief Evaluate the model for a set of new points
     *
     * @param x_in (np.ndarray, shape: (n_samples, n_primary_feats)) The set data for a set of new data points (size of n_feature x n_points, and order the same as appending the results of _feats[nn]->x_in_expr_list() for all feature)
     * @return The prediction of the model for a given data point
     */
    inline py::array_t<double> eval_many_py(py::array_t<double> x_in) const
    {
        return eval_many_py(x_in, std::vector<std::string>(0));
    }

    // DocString: model_eval_many_dict
    /**
     * @brief Evaluate the model for a set of new points
     *
     * @param x_in_dct (dict) The set of data points to evaluate the model. Keys must be strings representing feature expressions and vectors must be the same length
     * @return The prediction of the model for a given data point
     */
    inline py::array_t<double> eval_many_py(std::map<std::string, py::array_t<double>> x_in) const
    {
        return eval_many_py(x_in, std::vector<std::string>(0));
    }

    // DocString: model_prediction_to_file_arr_list_list
    /**
     * @brief Convert a set of inputs into a prediction and output that to a file
     *
     * @param filename (str) The name of the file to output to
     * @param prop (np.ndarray) The true values of the property for the given set of inputs
     * @param x_in (np.ndarray, shape: (n_samples, n_primary_feats)) The set data for a set of new data points (size of n_feature x n_points, and order the same as appending the results of _feats[nn]->x_in_expr_list() for all feature)
     * @param sample_ids (np.ndarray) The sample ids for all of the samples
     * @param task_size (np.ndarray) A map of listing the number of samples in each task
     * @param excluded_inds (np.ndarray) Indexes used for cross-validation
     */
    inline void prediction_to_file_py(std::string filename,
                                      py::array_t<double> prop,
                                      py::array_t<double> x_in,
                                      std::vector<std::string> sample_ids,
                                      std::vector<std::string> task_ids,
                                      std::vector<int> excluded_inds) const
    {
        prediction_to_file(filename,
                           python_conv_utils::from_array<double>(prop),
                           python_conv_utils::from_2D_array_transpose<double>(x_in),
                           sample_ids,
                           task_ids,
                           excluded_inds);
    }

    // DocString: model_prediction_to_file_dict_list_list
    /**
     * @brief Convert a set of inputs into a prediction and output that to a file
     *
     * @param filename (str) The name of the file to output to
     * @param prop (np.ndarray) The true values of the property for the given set of inputs
     * @param x_in (dict) The set data for a set of new data points (size of n_feature x n_points, and order the same as appending the results of _feats[nn]->x_in_expr_list() for all feature)
     * @param sample_ids (np.ndarray) The sample ids for all of the samples
     * @param task_size (np.ndarray) A map of listing the number of samples in each task
     * @param excluded_inds (np.ndarray) Indexes used for cross-validation
     */
    inline void prediction_to_file_py(std::string filename,
                                      py::array_t<double> prop,
                                      std::map<std::string, py::array_t<double>> x_in,
                                      std::vector<std::string> sample_ids,
                                      std::vector<std::string> task_ids,
                                      std::vector<int> excluded_inds) const
    {
        prediction_to_file(filename,
                           python_conv_utils::from_array<double>(prop),
                           python_conv_utils::convert_map<std::string, double>(x_in),
                           sample_ids,
                           task_ids,
                           excluded_inds);
    }
#endif
};

// GCOV_EXCL_START      GCOVR_EXCL_START        LCOV_EXCL_START
#ifdef PY_BINDINGS
template <class ModelBase = Model>
class PyModel : public ModelBase
{
    using ModelBase::ModelBase;
    // using Model::error_summary_string;

    std::string error_summary_string(std::vector<double> prop,
                                     std::vector<double> prop_est) const override
    {
        PYBIND11_OVERRIDE_PURE(std::string, ModelBase, error_summary_string, prop, prop_est);
    }

    std::string error_summary_string(bool train) const override
    {
        PYBIND11_OVERRIDE_PURE(std::string, ModelBase, error_summary_string, train);
    }

    std::string toString() const override
    {
        PYBIND11_OVERRIDE_PURE(std::string, ModelBase, toString);
    }

    std::string toLatexString() const override
    {
        PYBIND11_OVERRIDE_PURE(std::string, ModelBase, toLatexString);
    }

    std::string matlab_expr() const override
    {
        PYBIND11_OVERRIDE_PURE(std::string, ModelBase, matlab_expr);
    }

    std::vector<double> prop_train() const override
    {
        PYBIND11_OVERRIDE(std::vector<double>, ModelBase, prop_train);
    }

    std::vector<double> prop_test() const override
    {
        PYBIND11_OVERRIDE(std::vector<double>, ModelBase, prop_test);
    }

    std::vector<double> prop_train_est() const override
    {
        PYBIND11_OVERRIDE(std::vector<double>, ModelBase, prop_train_est);
    }

    std::vector<double> prop_test_est() const override
    {
        PYBIND11_OVERRIDE(std::vector<double>, ModelBase, prop_test_est);
    }

    std::vector<double> train_error() const override
    {
        PYBIND11_OVERRIDE(std::vector<double>, ModelBase, train_error);
    }

    std::vector<double> test_error() const override
    {
        PYBIND11_OVERRIDE(std::vector<double>, ModelBase, test_error);
    }
};
#endif
// GCOV_EXCL_STOP      GCOVR_EXCL_STOP        LCOV_EXCL_STOP
#endif
