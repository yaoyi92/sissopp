// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file descriptor_identifier/model/Model.cpp
 *  @brief Implements the base class of the output Models for SISSO
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "descriptor_identifier/model/Model.hpp"

Model::Model(const std::string prop_label,
             const Unit prop_unit,
             const std::shared_ptr<LossFunction> loss,
             const std::vector<model_node_ptr> feats,
             const std::vector<int> leave_out_inds,
             const std::vector<std::string> sample_ids_train,
             const std::vector<std::string> sample_ids_test,
             const std::vector<std::string> task_names)
    : _sample_ids_train(sample_ids_train),
      _sample_ids_test(sample_ids_test),
      _n_samp_train(feats[0]->n_samp()),
      _n_samp_test(feats[0]->n_samp_test()),
      _n_dim(feats.size()),
      _feats(feats),
      _leave_out_inds(leave_out_inds),
      _loss(loss),
      _prop_label(prop_label),
      _prop_unit(prop_unit),
      _task_eval(task_names[0]),
      _fix_intercept(loss->fix_intercept())
{
    for (int tn = 0; tn < task_names.size(); ++tn)
    {
        _task_names[task_names[tn]] = tn;
    }

    _loss->set_nfeat(_feats.size());
    (*_loss)(_feats);
    _loss->test_loss(_feats);

    for (int cc = 0; cc < _loss->coefs().size() / (_loss->n_dim()); ++cc)
    {
        _coefs.push_back(std::vector<double>(_loss->n_dim()));
        std::copy_n(&_loss->coefs()[cc * _loss->n_dim()], _loss->n_dim(), _coefs.back().data());
    }
}

Model::Model(std::string prop_label,
             Unit prop_unit,
             std::vector<double> prop_train,
             std::vector<double> prop_test,
             std::vector<int> task_sizes_train,
             std::vector<int> task_sizes_test,
             std::vector<model_node_ptr> feats,
             std::vector<int> leave_out_inds,
             std::vector<std::string> sample_ids_train,
             std::vector<std::string> sample_ids_test,
             std::vector<std::string> task_names,
             bool fix_intercept)
    : _sample_ids_train(sample_ids_train),
      _sample_ids_test(sample_ids_test),
      _n_samp_train(feats[0]->n_samp()),
      _n_samp_test(feats[0]->n_samp_test()),
      _n_dim(feats.size()),
      _feats(feats),
      _leave_out_inds(leave_out_inds),
      _prop_label(prop_label),
      _prop_unit(prop_unit),
      _task_eval(task_names[0]),
      _fix_intercept(fix_intercept)
{
    for (int tn = 0; tn < task_names.size(); ++tn)
    {
        _task_names[task_names[tn]] = tn;
    }
}

Model::Model() {}

std::vector<std::string> Model::check_tasks(std::vector<std::string> tasks, int n_pts) const
{
    if (tasks.size() == 0)
    {
        tasks = std::vector<std::string>(n_pts, _task_eval);
    }
    else
    {
        if (tasks.size() != n_pts)
        {
            throw std::logic_error(
                "The number of task ids are not the same as the number of the passed data points.");
        }
    }
    return tasks;
}

std::vector<double> Model::eval_features(double* x_in) const
{
    std::vector<double> feature_vals(_feats.size());
    for (int ff = 0; ff < _feats.size(); ++ff)
    {
        feature_vals[ff] = _feats[ff]->eval(x_in);
        x_in += _feats[ff]->n_leaves();
    }
    return feature_vals;
}
std::vector<std::vector<double>> Model::eval_features(std::vector<double>* x_in) const
{
    std::vector<std::vector<double>> feature_vals(_feats.size(), std::vector<double>(x_in->size()));
    for (int ff = 0; ff < _feats.size(); ++ff)
    {
        feature_vals[ff] = _feats[ff]->eval(x_in);
        x_in += _feats[ff]->n_leaves();
    }
    return feature_vals;
}

double Model::eval(double* x_in, const std::string task) const
{
    std::vector<double> fv = eval_features(x_in);
    return eval_from_feature_vals(fv, task);
}

std::vector<double> Model::eval(std::vector<double>* x_in,
                                const std::vector<std::string>& tasks) const
{
    std::vector<std::vector<double>> fv = eval_features(x_in);
    return eval_from_feature_vals(fv, tasks);
}

double Model::eval(std::vector<double> x_in, const std::string task) const
{
    std::vector<double> x_in_converted = convert_x_in(x_in);
    return eval(x_in_converted.data(), task);
}

std::vector<double> Model::eval(std::vector<std::vector<double>> x_in,
                                const std::vector<std::string>& tasks) const
{
    std::vector<std::vector<double>> x_in_converted = convert_x_in(x_in);
    return eval(x_in_converted.data(), tasks);
}

double Model::eval(std::map<std::string, double> x_in, const std::string task) const
{
    std::vector<double> x_in_converted = convert_x_in(x_in);
    return eval(x_in_converted.data(), task);
}

std::vector<double> Model::eval(std::map<std::string, std::vector<double>> x_in,
                                const std::vector<std::string>& tasks) const
{
    std::vector<std::vector<double>> x_in_converted = convert_x_in(x_in);
    return eval(x_in_converted.data(), tasks);
}

void Model::prediction_to_file(const std::string filename,
                               const std::vector<double> prop,
                               std::vector<double>* x_in,
                               const std::vector<std::string>& sample_ids,
                               const std::vector<std::string>& task_ids,
                               const std::vector<int>& excluded_inds) const
{
    std::vector<std::vector<double>> feat_vals = eval_features(x_in);
    std::vector<double> prop_est = eval_from_feature_vals(feat_vals, task_ids);

    std::map<std::string, int> task_sz;
    for (auto& task : task_ids)
    {
        if (task_sz.count(task) == 0)
        {
            task_sz[task] = 1;
        }
        else
        {
            task_sz[task] += 1;
        }
    }

    std::vector<double> prop_sorted(prop.size());
    std::vector<double> prop_est_sorted(prop_est.size());
    std::vector<std::string> sample_ids_sorted(sample_ids.size());
    std::vector<std::vector<double>> feature_vals_sorted(_feats.size(),
                                                         std::vector<double>(prop.size()));
    int s1 = 0;
    int s1_max = 0;
    for (auto& task : task_sz)
    {
        s1_max += task.second;
        for (int s2 = 0; s2 < prop.size(); ++s2)
        {
            if (task_ids[s2] == task.first)
            {
                prop_sorted[s1] = prop[s2];
                prop_est_sorted[s1] = prop_est[s2];
                sample_ids_sorted[s1] = sample_ids[s2];
                for (int ff = 0; ff < _feats.size(); ++ff)
                {
                    feature_vals_sorted[ff][s1] = feat_vals[ff][s2];
                }
                ++s1;
            }
        }
        if (s1 != s1_max)
        {
            throw std::logic_error(
                "Values sorted by task are no longer matching the number of samples were each "
                "task.");
        }
    }

    std::string error_summary = error_summary_string(prop, prop_est);
    to_file(filename,
            prop_sorted,
            prop_est_sorted,
            feature_vals_sorted,
            sample_ids_sorted,
            task_sz,
            excluded_inds,
            error_summary);
}

void Model::prediction_to_file(const std::string filename,
                               const std::vector<double> prop,
                               std::vector<std::vector<double>> x_in,
                               const std::vector<std::string> sample_ids,
                               const std::vector<std::string> task_ids,
                               const std::vector<int> excluded_inds) const
{
    std::vector<std::vector<double>> x_in_converted = convert_x_in(x_in);
    std::vector<std::string> task_vec = check_tasks(task_ids, x_in_converted[0].size());

    prediction_to_file(filename, prop, x_in_converted.data(), sample_ids, task_vec, excluded_inds);
}

void Model::prediction_to_file(const std::string filename,
                               const std::vector<double> prop,
                               std::map<std::string, std::vector<double>> x_in,
                               const std::vector<std::string> sample_ids,
                               const std::vector<std::string> task_ids,
                               const std::vector<int> excluded_inds) const
{
    std::vector<std::vector<double>> x_in_converted = convert_x_in(x_in);
    std::vector<std::string> task_vec = check_tasks(task_ids, x_in_converted[0].size());

    prediction_to_file(filename, prop, x_in_converted.data(), sample_ids, task_vec, excluded_inds);
}

void Model::to_file(const std::string filename, const bool train) const
{
    std::vector<double> prop, prop_est;
    std::vector<int> task_sz_vec, excluded_inds;
    std::vector<std::string> sample_ids;
    std::vector<std::vector<double>> feat_vals(_feats.size());

    if (train)
    {
        prop = prop_train();
        prop_est = prop_train_est();
        for (int ff = 0; ff < _feats.size(); ++ff)
        {
            feat_vals[ff] = _feats[ff]->value();
        }
        task_sz_vec = task_sizes_train();
        sample_ids = _sample_ids_train;
    }
    else
    {
        prop = prop_test();
        prop_est = prop_test_est();
        for (int ff = 0; ff < _feats.size(); ++ff)
        {
            feat_vals[ff] = _feats[ff]->test_value();
        }
        task_sz_vec = task_sizes_test();
        excluded_inds = _leave_out_inds;
        sample_ids = _sample_ids_test;
    }

    std::map<std::string, int> task_sizes;
    for (auto task : _task_names)
    {
        task_sizes[task.first] = task_sz_vec[task.second];
    }
    to_file(filename,
            prop,
            prop_est,
            feat_vals,
            sample_ids,
            task_sizes,
            excluded_inds,
            error_summary_string(train));
}

void Model::to_file(const std::string filename,
                    const std::vector<double>& prop,
                    const std::vector<double>& prop_est,
                    const std::vector<std::vector<double>>& feat_vals,
                    const std::vector<std::string>& sample_ids,
                    std::map<std::string, int>& task_sizes,
                    const std::vector<int>& excluded_inds,
                    const std::string error_summary) const
{
    if (prop.size() != prop_est.size())
    {
        throw std::logic_error("The property and estimated prpoerty vector are not the same size.");
    }

    if (prop.size() != sample_ids.size())
    {
        throw std::logic_error("The property and the sample ids vector are not the same size.");
    }
    for (auto& feat_val : feat_vals)
    {
        if (prop.size() != feat_val.size())
        {
            throw std::logic_error(
                "The property and the feature value vectors are not the same size.");
        }
    }

    std::vector<int> task_sz(_task_names.size(), 0);
    int n_samp = 0;
    for (auto& ts : task_sizes)
    {
        if (_task_names.count(ts.first) == 0)
        {
            throw std::logic_error("The task " + ts.first +
                                   " was not included in the training set.");
        }
        else
        {
            task_sz[_task_names.at(ts.first)] = ts.second;
        }
        n_samp += ts.second;
    }

    if (prop.size() != n_samp)
    {
        throw std::logic_error(
            "The property vector and the number of samples in the task sizes are not the same");
    }

    boost::filesystem::path p(filename.c_str());
    boost::filesystem::path parent = p.remove_filename();
    if (parent.string().size() > 0)
    {
        boost::filesystem::create_directories(parent);
    }

    std::ofstream out_file_stream = std::ofstream();
    out_file_stream.open(filename);

    out_file_stream << "# " << toString() << std::endl;
    out_file_stream << "# Property Label: $" << str_utils::latexify(_prop_label)
                    << "$; Unit of the Property: " << _prop_unit.toString() << std::endl;

    out_file_stream << error_summary;
    out_file_stream << write_coefs();

    out_file_stream << "# Feature Rung, Units, and Expressions" << std::endl;
    for (int ff = 0; ff < _feats.size(); ++ff)
    {
        out_file_stream << std::setw(6) << std::left << "# " + std::to_string(ff) + "; ";
        out_file_stream << std::to_string(_feats[ff]->rung()) + "; ";
        out_file_stream << std::setw(50) << _feats[ff]->unit().toString() + "; ";
        out_file_stream << _feats[ff]->postfix_expr() + "; " << _feats[ff]->expr() + "; ";
        out_file_stream << _feats[ff]->latex_expr() + "; ";
        out_file_stream << _feats[ff]->matlab_fxn_expr() + "; ";
        out_file_stream << boost::algorithm::join(_feats[ff]->x_in_expr_list(), ",");
        if (!_feats[ff]->domain().is_empty())
        {
            out_file_stream << ";" << _feats[ff]->domain() << std::endl;
        }
        else
        {
            out_file_stream << std::endl;
        }
    }

    int task_header_w = std::max(
        6,
        static_cast<int>(
            std::max_element(_task_names.begin(),
                             _task_names.end(),
                             [](auto s1, auto s2) { return s1.first.size() <= s2.first.size(); })
                ->first.size()));
    out_file_stream << "# Number of Samples Per Task" << std::endl;

    out_file_stream << std::setw(task_header_w) << std::left << "# Task" << std::setw(2) << ", "
                    << std::setw(24) << "n_samples" << std::endl;
    for (auto& task : _task_names)
    {
        out_file_stream << std::left << std::setw(task_header_w) << "# " + task.first
                        << std::setw(2) << ", ";
        out_file_stream << std::left << std::setw(22) << task_sz[task.second] << std::endl;
    }
    if (excluded_inds.size() > 0)
    {
        out_file_stream << "# Excluded Indexes: [ " << excluded_inds[0];
        for (int ii = 1; ii < excluded_inds.size(); ++ii)
        {
            out_file_stream << ", " << excluded_inds[ii];
        }
        out_file_stream << " ]" << std::endl;
    }

    int max_sample_id_len = 12;
    max_sample_id_len = std::max(
        max_sample_id_len,
        static_cast<int>(
            std::max_element(sample_ids.begin(),
                             sample_ids.end(),
                             [](std::string s1, std::string s2) { return s1.size() <= s2.size(); })
                ->size()));

    out_file_stream << "\n"
                    << std::setw(max_sample_id_len) << std::left << "# Sample ID" << std::setw(2)
                    << ", ";
    out_file_stream << std::setw(22) << std::left << "Property Value" << std::setw(2) << ", "
                    << std::setw(22) << " Property Value (EST)";
    for (int ff = 0; ff < _feats.size(); ++ff)
    {
        out_file_stream << std::setw(2) << ", " << std::setw(22)
                        << " Feature " + std::to_string(ff) + " Value";
    }
    out_file_stream << std::endl;

    for (int ss = 0; ss < n_samp; ++ss)
    {
        out_file_stream << std::left << std::setw(max_sample_id_len) << sample_ids[ss]
                        << std::setw(2) << ", ";
        out_file_stream << std::right << std::setw(22) << std::setprecision(15) << std::scientific
                        << prop[ss] << std::setw(2) << ", ";
        out_file_stream << std::setw(22) << prop_est[ss];
        for (int ff = 0; ff < _n_dim; ++ff)
        {
            out_file_stream << std::right << std::setw(2) << ", " << std::setw(22)
                            << std::setprecision(15) << feat_vals[ff][ss];
        }
        out_file_stream << std::endl;
    }
    out_file_stream.close();
}

void Model::write_matlab_fxn(std::string fxn_filename)
{
    if (fxn_filename.substr(fxn_filename.size() - 2, 2).compare(".m") != 0)
    {
        fxn_filename += ".m";
    }

    boost::filesystem::path p(fxn_filename.c_str());
    std::string fxn_name = p.filename().string();
    fxn_name = fxn_name.substr(0, fxn_name.size() - 2);
    boost::filesystem::path parent = p.remove_filename();
    if (parent.string().size() > 0)
    {
        boost::filesystem::create_directories(parent);
    }

    std::ofstream out_file_stream = std::ofstream();
    out_file_stream.open(fxn_filename);

    // Get the list of all unique leaves in all features
    std::vector<std::string> leaves;
    for (auto& feat : _feats)
    {
        std::vector<std::string> x_in = feat->x_in_expr_list();
        leaves.insert(leaves.end(), x_in.begin(), x_in.end());
    }
    leaves = vector_utils::unique<std::string>(leaves);
    std::transform(leaves.begin(), leaves.end(), leaves.begin(), [](std::string s) {
        return str_utils::matlabify(s);
    });

    // Write the header of the function
    out_file_stream << "function P = " << fxn_name << "(X)\n";
    out_file_stream << "% Returns the value of " << _prop_label << " = " << toString() << "\n%\n";
    out_file_stream << "% X = [\n";
    for (auto& leaf : leaves)
    {
        out_file_stream << "%     " << leaf << ",\n";
    }
    out_file_stream << "% ]\n\n";

    // Check X contains the correct number of columns
    out_file_stream << "if(size(X, 2) ~= " << leaves.size() << ")\n";
    out_file_stream << "    error(\"ERROR: X must have a size of " << leaves.size()
                    << " in the second dimension.\")\n";
    out_file_stream << "end\n";
    int len_rename = (*std::max_element(
                          leaves.begin(),
                          leaves.end(),
                          [](std::string s1, std::string s2) { return s1.size() < s2.size(); }))
                         .size();

    // Rename X
    for (int ll = 0; ll < leaves.size(); ++ll)
    {
        out_file_stream << std::left << std::setw(len_rename) << leaves[ll] << std::setw(3) << " = "
                        << "reshape(X(:, " << ll + 1 << "), 1, []);\n";
    }
    out_file_stream << "\n";

    // Set features
    for (int ff = 0; ff < _feats.size(); ++ff)
    {
        out_file_stream << "f" << ff << " = " << _feats[ff]->matlab_fxn_expr() << ";\n";
    }
    out_file_stream << "\n";

    // Set Constants
    if (_fix_intercept)
    {
        out_file_stream << "c0 = 0.0;\n";
    }
    else
    {
        out_file_stream << "c0 = " << std::scientific << std::setprecision(10) << _coefs[0].back()
                        << ";\n";
    }
    for (int ff = 0; ff < _feats.size(); ++ff)
    {
        out_file_stream << "a" << ff << " = " << std::scientific << std::setprecision(10)
                        << _coefs[0][ff] << ";\n";
    }
    out_file_stream << "\n";

    // Calculate the property
    out_file_stream << "P = reshape(" << matlab_expr() << ", [], 1);\nend\n";
    out_file_stream.close();
}

void Model::populate_model(const std::string train_filename, const std::string test_filename)
{
    bool with_test = test_filename.size() > 0;

    std::ifstream train_file_stream;
    train_file_stream.open(train_filename, std::ios::in);

    std::ifstream test_file_stream;
    test_file_stream.open(test_filename, std::ios::in);

    std::vector<std::string> feature_expr;
    std::vector<std::string> split_line;

    std::string line;
    std::string test_line;

    // Store model line
    std::string model_line;
    std::getline(train_file_stream, model_line);

    _fix_intercept = has_fixed_intercept(model_line);

    // Get the property unit and error
    std::string prop_desc_line;
    std::string error_line;
    std::string test_error_line;
    std::getline(train_file_stream, prop_desc_line);
    int n_line = 5;

    // Legacy Code so previous model files can be read in
    if (!is_error_line(prop_desc_line))
    {
        split_line = str_utils::split_string_trim(prop_desc_line);
        if (split_line.size() > 2)
        {
            _prop_label = split_line[1].substr(1, split_line[1].size() - 2);
            _prop_unit = Unit(split_line.back());
        }
        else
        {
            _prop_label = "Property";
            _prop_unit = Unit(split_line.back());
        }
        std::getline(train_file_stream, error_line);
    }
    else
    {
        _prop_label = "Property";
        _prop_unit = Unit();
        error_line = prop_desc_line;
    }

    set_error_from_file(error_line, true);
    if (with_test)
    {
        std::getline(test_file_stream, test_error_line);
        std::getline(test_file_stream, test_error_line);
        if (!is_error_line(test_error_line))
        {
            std::getline(test_file_stream, test_error_line);
        }
        set_error_from_file(test_error_line, false);
    }

    // Get coefficients
    std::getline(train_file_stream, line);
    std::getline(train_file_stream, line);

    int n_dim = 0;
    std::getline(train_file_stream, line);
    do
    {
        split_line = str_utils::split_string_trim(line);
        n_dim = split_line.size() - 3 + _fix_intercept;
        _coefs.push_back(std::vector<double>(n_dim + (!_fix_intercept), 0.0));
        std::transform(
            split_line.begin() + 1, split_line.end() - 1, _coefs.back().data(), [](std::string s) {
                return std::stod(s);
            });
        std::getline(train_file_stream, line);
        ++n_line;
    } while (line.substr(0, 14).compare("# Feature Rung") != 0);

    _n_dim = n_dim;
    std::getline(train_file_stream, line);
    for (int ff = 0; ff < _n_dim; ++ff)
    {
        feature_expr.push_back(line.substr(6));
        std::getline(train_file_stream, line);
        ++n_line;
    }

    std::getline(train_file_stream, line);

    // Get task sizes
    _n_samp_train = 0;
    _n_samp_test = 0;
    int n_task = 0;

    std::vector<int> task_sizes_train;
    std::vector<int> task_sizes_test;

    if (with_test)
    {
        for (int ll = 0; ll < n_line + 1; ++ll)
        {
            std::getline(test_file_stream, test_line);
        }
    }

    std::getline(train_file_stream, line);
    do
    {
        split_line = str_utils::split_string_trim(line);
        _task_names[split_line[0].substr(2)] = n_task;
        ++n_task;
        _n_samp_train += std::stoi(split_line[1]);
        task_sizes_train.push_back(std::stoi(split_line[1]));
        if (with_test)
        {
            split_line = str_utils::split_string_trim(test_line);
            if (_task_names.count(split_line[0].substr(2)) == 0)
            {
                throw std::logic_error("A task in the test set is not in the training set.");
            }

            if (_task_names[split_line[0].substr(2)] != n_task - 1)
            {
                throw std::logic_error(
                    "The task names for the test and train files are not in the same order.");
            }
            _n_samp_test += std::stoi(split_line[1]);
            task_sizes_test.push_back(std::stoi(split_line[1]));
            std::getline(test_file_stream, test_line);
        }
        else
        {
            task_sizes_test.push_back(0);
        }
        std::getline(train_file_stream, line);
    } while (line.size() > 0);

    set_task_eval(0);
    std::vector<double> prop_train(_n_samp_train);
    std::vector<double> prop_test(_n_samp_test);

    _sample_ids_train.resize(_n_samp_train);
    _sample_ids_test.resize(_n_samp_test);

    if (with_test)
    {
        split_line = str_utils::split_string_trim(test_line, "[]");
        split_line = str_utils::split_string_trim(split_line[1]);
        if (split_line.size() != _n_samp_test)
        {
            throw std::logic_error(
                "Number of test sample indexes (" + std::to_string(split_line.size()) +
                ") does not equal number of test samples (" + std::to_string(_n_samp_test) + ").");
        }
        for (auto& ind : split_line)
        {
            _leave_out_inds.push_back(std::stoi(ind));
        }
        std::getline(test_file_stream, test_line);
    }
    std::getline(train_file_stream, line);
    std::getline(test_file_stream, test_line);

    std::vector<std::vector<double>> feat_vals(n_dim, std::vector<double>(_n_samp_train, 0.0));
    std::vector<std::vector<double>> feat_test_vals(n_dim, std::vector<double>(_n_samp_test, 0.0));

    bool with_samp_id = false;
    for (int ns = 0; ns < _n_samp_train; ++ns)
    {
        std::getline(train_file_stream, line);
        split_line = str_utils::split_string_trim(line);
        if ((split_line.size() > _n_dim + 2))
        {
            with_samp_id = true;
            _sample_ids_train[ns] = split_line[0];
        }
        else
        {
            with_samp_id = false;
            _sample_ids_train[ns] = std::to_string(ns);
        }

        prop_train[ns] = std::stod(split_line[with_samp_id]);

        for (int nf = 0; nf < n_dim; ++nf)
        {
            feat_vals[nf][ns] = std::stod(split_line[2 + nf + with_samp_id]);
        }
    }

    for (int ns = 0; ns < _n_samp_test; ++ns)
    {
        std::getline(test_file_stream, test_line);
        split_line = str_utils::split_string_trim(test_line);

        if ((split_line.size() > _n_dim + 2))
        {
            with_samp_id = true;
            _sample_ids_test[ns] = split_line[0];
        }
        else
        {
            with_samp_id = false;
            _sample_ids_test[ns] = std::to_string(ns);
        }

        prop_test[ns] = std::stod(split_line[with_samp_id]);

        for (int nf = 0; nf < n_dim; ++nf)
        {
            feat_test_vals[nf][ns] = std::stod(split_line[2 + nf + with_samp_id]);
        }
    }
    train_file_stream.close();
    test_file_stream.close();

    make_loss(prop_train, prop_test, task_sizes_train, task_sizes_test);
    create_model_nodes_from_expressions(feature_expr, feat_vals, feat_test_vals);

    double loss_val = (*_loss)(_feats);
    double test_loss = _loss->test_loss(_feats);
}

void Model::create_model_nodes_from_expressions(std::vector<std::string> feat_exprs,
                                                std::vector<std::vector<double>> feat_vals,
                                                std::vector<std::vector<double>> feat_test_vals)
{
    std::vector<std::string> split_str;

    for (int ff = 0; ff < feat_exprs.size(); ++ff)
    {
        split_str = str_utils::split_string_trim(feat_exprs[ff], ";");
        int rung = std::stoi(split_str[0]);

        model_node_ptr feat;

        std::string unit_str = split_str[1];
        std::string postfix_expr = split_str[2];
        std::string expr = split_str[3];
        std::string latex_expr = split_str[4];
        std::string matlab_expr;
        std::vector<std::string> x_in_expr_list;
        Domain domain;
        if (split_str.size() > 6)
        {
            matlab_expr = split_str[5];
        }
        else
        {
            matlab_expr = "NaN";
        }

        if (split_str.size() > 7)
        {
            domain = Domain(split_str[7]);
            x_in_expr_list = str_utils::split_string_trim(split_str[split_str.size() - 2], ",");
        }
        else
        {
            x_in_expr_list = str_utils::split_string_trim(split_str[split_str.size() - 1], ",");
        }

        feat = std::make_shared<ModelNode>(ff,
                                           rung,
                                           expr,
                                           latex_expr,
                                           postfix_expr,
                                           matlab_expr,
                                           feat_vals[ff],
                                           feat_test_vals[ff],
                                           x_in_expr_list,
                                           Unit(unit_str),
                                           domain);
        _feats.push_back(feat);
    }
}

std::vector<double> Model::convert_x_in(std::vector<double> x_in) const
{
    std::vector<double> x_in_converted;
    for (auto& feat : _feats)
    {
        std::vector<std::string> postfix_split = str_utils::split_string_trim(feat->postfix_expr(),
                                                                              "|");

        for (auto& ps : postfix_split)
        {
            if (ps.find_first_not_of("0123456789") == std::string::npos)
            {
                x_in_converted.push_back(x_in[std::stoi(ps)]);
            }
        }
    }
    return x_in_converted;
}

std::vector<double> Model::convert_x_in(std::map<std::string, double> x_in) const
{
    std::vector<double> x_in_converted;
    for (auto& feat : _feats)
    {
        for (auto& in_expr : feat->x_in_expr_list())
        {
            if (x_in.count(in_expr) == 0)
            {
                throw std::logic_error("The value of " + in_expr + " is not in x_in.");
            }

            x_in_converted.push_back(x_in[in_expr]);
        }
    }
    return x_in_converted;
}

std::vector<std::vector<double>> Model::convert_x_in(std::vector<std::vector<double>> x_in) const
{
    std::vector<std::vector<double>> x_in_converted;
    for (auto& feat : _feats)
    {
        std::vector<std::string> postfix_split = str_utils::split_string_trim(feat->postfix_expr(),
                                                                              "|");

        for (auto& ps : postfix_split)
        {
            if (ps.find_first_not_of("0123456789") == std::string::npos)
            {
                x_in_converted.push_back(x_in[std::stoi(ps)]);
            }
        }
    }
    return x_in_converted;
}

std::vector<std::vector<double>> Model::convert_x_in(
    std::map<std::string, std::vector<double>> x_in) const
{
    int n_samp = x_in.begin()->second.size();
    for (auto& el : x_in)
    {
        if (el.second.size() != n_samp)
        {
            throw std::logic_error("The size of the vectors passed by x_in do not match.");
        }
    }

    std::vector<std::vector<double>> x_in_converted;
    for (auto& feat : _feats)
    {
        for (auto& in_expr : feat->x_in_expr_list())
        {
            if (x_in.count(in_expr) == 0)
            {
                throw std::logic_error("The value of " + in_expr + " is not in x_in.");
            }

            x_in_converted.push_back(x_in[in_expr]);
        }
    }
    return x_in_converted;
}
