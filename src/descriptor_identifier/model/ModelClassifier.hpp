// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file descriptor_identifier/model/ModelClassifier.hpp
 *  @brief Defines a class for the output models from solving a classification problem
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef MODEL_CLASSIFIER
#define MODEL_CLASSIFIER

#include "descriptor_identifier/model/Model.hpp"

#ifdef PY_BINDINGS
namespace py = pybind11;
#endif

// DocString: cls_model_class
/**
 * @brief Class to store the output models for classification problems with SISSO (inherits from Model)
 */
class ModelClassifier : public Model
{
    // clang-format off
    int _n_class; //!< The number of classes in the model

    int _train_n_convex_overlap; //!< Number of data points in regions where the convex hulls overlap in the training set
    int _test_n_convex_overlap; //!< Number of data points in regions where the convex hulls overlap for the test data set

    int _train_n_svm_misclassified; //!< The number of points misclassified by SVM (training set)
    int _test_n_svm_misclassified; //!< The number of points misclassified by SVM (test set)
    // clang-format on

public:
    using Model::eval;

    /**
     * @brief Construct a ModelClassifier using a loss function and a set of features
     *
     * @param prop_label The label for the property
     * @param prop_unit The unit of the property
     * @param loss The LossFunction used to calculate the model
     * @param feats The features of the model
     * @param leave_out_inds The indexes of the samples for the test set
     * @param sample_ids_train A vector storing all sample ids for the training samples
     * @param sample_ids_test A vector storing all sample ids for the test samples
     * @param task_names A vector storing the ID of the task names
     */
    ModelClassifier(const std::string prop_label,
                    const Unit prop_unit,
                    const std::shared_ptr<LossFunction> loss,
                    const std::vector<model_node_ptr> feats,
                    const std::vector<int> leave_out_inds,
                    const std::vector<std::string> sample_ids_train,
                    const std::vector<std::string> sample_ids_test,
                    const std::vector<std::string> task_names);

    // DocString: model_class_init_pickle
    /**
     * @brief Construct a Model using a loss function and a set of features
     *
     * @param prop_label The label for the property
     * @param prop_unit The unit of the property
     * @param prop_train The property vector of the training set
     * @param prop_test The property vector of the test set
     * @param task_sizes_train The number of samples in the training set for each task
     * @param task_sizes_test The number of samples in the test set for each task
     * @param feats The features of the model
     * @param leave_out_inds The indexes of the samples for the test set
     * @param sample_ids_train A vector storing all sample ids for the training samples
     * @param sample_ids_test A vector storing all sample ids for the test samples
     * @param task_names A vector storing the ID of the task names
     * @param fix_intercept If true set the bias term to 0
     */
    ModelClassifier(std::string prop_label,
                    Unit prop_unit,
                    std::vector<double> prop_train,
                    std::vector<double> prop_test,
                    std::vector<int> task_sizes_train,
                    std::vector<int> task_sizes_test,
                    std::vector<model_node_ptr> feats,
                    std::vector<int> leave_out_inds,
                    std::vector<std::string> sample_ids_train,
                    std::vector<std::string> sample_ids_test,
                    std::vector<std::string> task_names,
                    bool fix_intercept);

    // DocString: model_class_init_train
    /**
     * @brief Construct a ModelClassifier from a training output file
     *
     * @param train_file (str) Previously generated model output file for the training data
     */
    ModelClassifier(const std::string train_file);

    // DocString: model_class_init_test_train
    /**
     * @brief Construct a ModelClassifier from a training and testing output file
     *
     * @param train_file (str) Previously generated model output file for the training data
     * @param test_file (str) Previously generated model output file for the test data
     */
    ModelClassifier(const std::string train_file, const std::string test_file);

    /**
     * @brief Copy Constructor
     *
     * @param o ModelClassifier to be copied
     */
    ModelClassifier(const ModelClassifier& o) = default;

    /**
     * @brief Move Constructor
     *
     * @param o ModelClassifier to be moved
     */
    ModelClassifier(ModelClassifier&& o) = default;

    /**
     * @brief Copy Assignment operator
     *
     * @param o ModelClassifier to be copied
     */
    ModelClassifier& operator=(const ModelClassifier& o) = default;

    /**
     * @brief Move Assignment operator
     *
     * @param o ModelClassifier to be moved
     */
    ModelClassifier& operator=(ModelClassifier&& o) = default;

    /**
     * @brief Set the number of misclassified points from the loss function
     */
    void set_n_misclassified();

    /**
     * @brief Create the LossFunctionConvexHull for the Model when constructed from a file
     *
     * @param prop_train The property vector for all of the training samples
     * @param prop_test The property vector for all of the test samples
     * @param task_sizes_train The number of training samples per task
     * @param task_sizes_test The number of test samples per task
     */
    void make_loss(std::vector<double>& prop_train,
                   std::vector<double>& prop_test,
                   std::vector<int>& task_sizes_train,
                   std::vector<int>& task_sizes_test);

    /**
     * @brief Based off of the model line in a model file determine if the bias term is 0.0
     *
     * @param model_line the model line from the file
     * @return True if the intercept should be fixed
     */
    inline bool has_fixed_intercept(std::string model_line) { return false; }

    /**
     * @brief Check if a given line summarizes the error of a model
     *
     * @param line to see if it contains information about the error
     * @return True if the line summarizes the error
     */
    inline bool is_error_line(std::string line)
    {
        return line.substr(0, 42).compare("# # Samples in Convex Hull Overlap Region:") == 0;
    }

    /**
     * @brief Set error summary variables from a file
     *
     * @param error_line the line to set the error from
     * @param train True if file is for training data
     */
    void set_error_from_file(std::string error_line, bool train);

    /**
     * @brief Evaluate the model for a new point
     *
     * @param feature_values The value of each selected feature at the given point
     * @param task The task associated with the point
     *
     * @return The prediction of the model for a given data point
     */
    double eval_from_feature_vals(std::vector<double>& feature_vals, std::string task) const;

    /**
     * @brief Evaluate the model for a new set of new points
     *
     * @param feature_values The value of each selected feature at each new point
     * @param tasks The task associated with each new point
     *
     * @return The prediction of the model for the set of data poitns
     */
    std::vector<double> eval_from_feature_vals(std::vector<std::vector<double>>& feature_vals,
                                               const std::vector<std::string>& tasks) const;

    // DocString: model_class_str
    /**
     * @brief Convert the model to a string
     */
    std::string toString() const;

    // DocString: model_class_latex_str
    /**
     * @brief Convert the model to a latexified string
     */
    std::string toLatexString() const;

    /**
     * @brief Get the matlab expression for this model
     */
    std::string matlab_expr() const;

    /**
     * @brief Get the block used to summarize the error in the output files
     *
     * @param train True if summary is for the training file
     */
    std::string error_summary_string(bool train) const;

    /**
     * @brief Get the block used to summarize the error in the output files
     *
     * @param prop The value of the true property
     * @param prop_est The value of the estimated property
     *
     * @returns error_summary The summary of the model error
     */
    std::string error_summary_string(std::vector<double> prop, std::vector<double> prop_est) const;

    /**
     * @brief Get the coefficients list header for output file
     */
    std::string write_coefs() const;

    /**
     * @brief Copy the training error into a different vector
     *
     * @param res Pointer to the head of the new vector to store the residual
     */
    inline void copy_error(double* res) const
    {
        std::copy_n(_loss->error_train().data(), _n_samp_train, res);
    }

    /**
     * @brief Set the train/test error of the model using linear programming
     */
    void set_train_test_error();

    // DocString: model_class_n_convex_overlap_train
    /**
     * @brief The number of samples in overlapping convex hull regions (training data)
     */
    inline int n_convex_overlap_train() const { return _train_n_convex_overlap; }

    // DocString: model_class_n_convex_overlap_test
    /**
     * @brief The number of samples in overlapping convex hull regions (test data)
     */
    inline int n_convex_overlap_test() const { return _test_n_convex_overlap; }

    // DocString: model_class_n_svm_misclassified_train
    /**
     * @brief The number of samples misclassified by SVM (training data)
     */
    inline int n_svm_misclassified_train() const { return _train_n_svm_misclassified; }

    // DocString: model_class_n_svm_misclassified_test
    /**
     * @brief The number of samples misclassified by SVM (training data)
     */
    inline int n_svm_misclassified_test() const { return _test_n_svm_misclassified; }

    // DocString: model_class_precent_train_error
    /**
     * @brief Percent of all training samples that are in the Convex Hull overlap region
     */
    inline double percent_train_error() const
    {
        return 100.0 * static_cast<double>(_train_n_convex_overlap) /
               static_cast<double>(_n_samp_train);
    }

    // DocString: model_class_precent_test_error
    /**
     * @brief Percent of all test samples that are in the Convex Hull overlap region
     */
    inline double percent_test_error() const
    {
        return 100.0 * static_cast<double>(_test_n_convex_overlap) /
               static_cast<double>(_n_samp_test);
    }

#ifdef PY_BINDINGS
    // DocString: model_class_init_new_coefs_list
    /**
     * @brief Construct a new ModelClassifier with updated SVM coefficient
     *
     * @param o (Model) Model to be copied
     * @param new_coefs (list) The new coefficients
     * @param prop_train_est (list) The estimated property values for the training set
     * @param prop_test_est (list) The estimated property values for the validation set
     */
    ModelClassifier(const ModelClassifier& o,
                    std::vector<std::vector<double>> new_coefs,
                    py::array_t<double> prop_train_est,
                    py::array_t<double> prop_test_est);
#endif
};

/**
 * @brief Print a model to an string stream
 *
 * @param outStream The output stream the model is to be printed
 * @param model The model to be printed
 */
std::ostream& operator<<(std::ostream& outStream, const ModelClassifier& model);

// GCOV_EXCL_START      GCOVR_EXCL_START        LCOV_EXCL_START
#ifdef PY_BINDINGS
template <class ModelClassifierBase = ModelClassifier>
class PyModelClassifier : public PyModel<ModelClassifierBase>
{
    using PyModel<ModelClassifierBase>::PyModel;  // Inherit constructors

    std::string error_summary_string(std::vector<double> prop,
                                     std::vector<double> prop_est) const override
    {
        PYBIND11_OVERRIDE(std::string, ModelClassifierBase, error_summary_string, prop, prop_est);
    }

    std::string error_summary_string(bool train) const override
    {
        PYBIND11_OVERRIDE_PURE(std::string, ModelClassifierBase, error_summary_string, train);
    }

    std::string toString() const override
    {
        PYBIND11_OVERRIDE(std::string, ModelClassifierBase, toString);
    }

    std::string toLatexString() const override
    {
        PYBIND11_OVERRIDE(std::string, ModelClassifierBase, toLatexString);
    }

    std::string matlab_expr() const override
    {
        PYBIND11_OVERRIDE(std::string, ModelClassifierBase, matlab_expr);
    }
};
#endif
// GCOV_EXCL_STOP      GCOVR_EXCL_STOP        LCOV_EXCL_STOP
#endif
