// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file descriptor_identifier/model/ModelClassifier.cpp
 *  @brief Implements a class for the output models from solving a classification problem
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "descriptor_identifier/model/ModelClassifier.hpp"

ModelClassifier::ModelClassifier(const std::string prop_label,
                                 const Unit prop_unit,
                                 const std::shared_ptr<LossFunction> loss,
                                 const std::vector<model_node_ptr> feats,
                                 const std::vector<int> leave_out_inds,
                                 const std::vector<std::string> sample_ids_train,
                                 const std::vector<std::string> sample_ids_test,
                                 const std::vector<std::string> task_names)
    : Model(prop_label,
            prop_unit,
            loss,
            feats,
            leave_out_inds,
            sample_ids_train,
            sample_ids_test,
            task_names),
      _train_n_convex_overlap(0),
      _test_n_convex_overlap(0),
      _n_class(loss->n_class())
{
    set_n_misclassified();
}

ModelClassifier::ModelClassifier(std::string prop_label,
                                 Unit prop_unit,
                                 std::vector<double> prop_train,
                                 std::vector<double> prop_test,
                                 std::vector<int> task_sizes_train,
                                 std::vector<int> task_sizes_test,
                                 std::vector<model_node_ptr> feats,
                                 std::vector<int> leave_out_inds,
                                 std::vector<std::string> sample_ids_train,
                                 std::vector<std::string> sample_ids_test,
                                 std::vector<std::string> task_names,
                                 bool fix_intercept)
    : Model(prop_label,
            prop_unit,
            prop_train,
            prop_test,
            task_sizes_train,
            task_sizes_test,
            feats,
            leave_out_inds,
            sample_ids_train,
            sample_ids_test,
            task_names,
            fix_intercept),
      _train_n_convex_overlap(0),
      _test_n_convex_overlap(0)
{
    make_loss(prop_train, prop_test, task_sizes_train, task_sizes_test);
    _n_class = _loss->n_class();

    _loss->set_nfeat(_feats.size());
    (*_loss)(_feats);
    _loss->test_loss(_feats);

    for (int cc = 0; cc < _loss->coefs().size() / (_loss->n_dim()); ++cc)
    {
        _coefs.push_back(std::vector<double>(_loss->n_dim()));
        std::copy_n(&_loss->coefs()[cc * _loss->n_dim()], _loss->n_dim(), _coefs.back().data());
    }
    set_n_misclassified();
}

void ModelClassifier::set_n_misclassified()
{
    // Objects needed to calculate the number of points in the overlapping regions of the convex hulls
    set_train_test_error();

    std::vector<bool> train_misclassified(_n_samp_train);
    std::transform(_loss->prop_train_est().begin(),
                   _loss->prop_train_est().end(),
                   _loss->prop_train().begin(),
                   train_misclassified.begin(),
                   [](double p1, double p2) { return p1 != p2; });
    _train_n_svm_misclassified = std::accumulate(
        train_misclassified.begin(), train_misclassified.end(), 0);

    std::vector<bool> test_misclassified(_n_samp_test);
    std::transform(_loss->prop_test_est().begin(),
                   _loss->prop_test_est().end(),
                   _loss->prop_test().begin(),
                   test_misclassified.begin(),
                   [](double p1, double p2) { return p1 != p2; });
    _test_n_svm_misclassified = std::accumulate(
        test_misclassified.begin(), test_misclassified.end(), 0);
}

ModelClassifier::ModelClassifier(const std::string train_file)
    : _train_n_convex_overlap(0), _test_n_convex_overlap(0)
{
    populate_model(train_file);
    _n_class = _loss->n_class();

    int file_train_n_convex_overlap = _train_n_convex_overlap;
    _train_n_convex_overlap = 0;

    set_train_test_error();
    if ((file_train_n_convex_overlap != _train_n_convex_overlap))
    {
        throw std::logic_error("The file does not have the same convex overlap (" +
                               std::to_string(file_train_n_convex_overlap) +
                               ") as calculated here (" + std::to_string(_train_n_convex_overlap) +
                               ").");
    }
}
ModelClassifier::ModelClassifier(const std::string train_file, std::string test_file)
    : _train_n_convex_overlap(0), _test_n_convex_overlap(0)
{
    populate_model(train_file, test_file);
    _n_class = _loss->n_class();

    int file_train_n_convex_overlap = _train_n_convex_overlap;
    _train_n_convex_overlap = 0;

    int file_test_n_convex_overlap = _test_n_convex_overlap;
    _test_n_convex_overlap = 0;

    set_train_test_error();
    if ((file_train_n_convex_overlap != _train_n_convex_overlap) ||
        (file_test_n_convex_overlap != _test_n_convex_overlap))
    {
        throw std::logic_error("The file does not have the same convex overlap (" +
                               std::to_string(file_train_n_convex_overlap) + ", " +
                               std::to_string(file_test_n_convex_overlap) +
                               ") as calculated here (" + std::to_string(_train_n_convex_overlap) +
                               ", " + std::to_string(_test_n_convex_overlap) + ").");
    }
}

void ModelClassifier::make_loss(std::vector<double>& prop_train,
                                std::vector<double>& prop_test,
                                std::vector<int>& task_sizes_train,
                                std::vector<int>& task_sizes_test)
{
    _loss = std::make_shared<LossFunctionConvexHull>(
        prop_train, prop_test, task_sizes_train, task_sizes_test, _n_dim);
}

double ModelClassifier::eval_from_feature_vals(std::vector<double>& feature_vals,
                                               std::string task) const
{
    int task_ind = _task_names.find(task)->second;
    double result = _coefs[task_ind].back();

    return _loss->svm(task_ind)->predict(feature_vals);
}

std::vector<double> ModelClassifier::eval_from_feature_vals(
    std::vector<std::vector<double>>& feature_vals, const std::vector<std::string>& tasks) const
{
    std::vector<double> result(feature_vals[0].size(), 0.0);
    std::vector<double> feat_vals(_n_dim * feature_vals[0].size());
    std::vector<double*> pt_ptrs(feature_vals[0].size());

    for (int pp = 0; pp < feature_vals[0].size(); ++pp)
    {
        pt_ptrs[pp] = &feat_vals[pp * _n_dim];
    }

    for (int ff = 0; ff < _feats.size(); ++ff)
    {
        dcopy_(feature_vals[ff].size(), feature_vals[ff].data(), 1, &feat_vals[ff], _n_dim);
    }

    std::transform(pt_ptrs.begin(),
                   pt_ptrs.end(),
                   tasks.begin(),
                   result.begin(),
                   [this](double* pt, std::string task) {
                       int task_ind = _task_names.find(task)->second;
                       return _loss->svm(task_ind)->predict(pt, _n_dim);
                   });

    return result;
}
void ModelClassifier::set_error_from_file(std::string error_line, bool train)
{
    std::vector<std::string> split_line = str_utils::split_string_trim(error_line);

    if (train)
    {
        _train_n_convex_overlap = std::stod(split_line[1]);
        _train_n_svm_misclassified = std::stod(split_line[3]);
    }
    else
    {
        _test_n_convex_overlap = std::stod(split_line[1]);
        _test_n_svm_misclassified = std::stod(split_line[3]);
    }
}

void ModelClassifier::set_train_test_error()
{
    _train_n_convex_overlap = (*_loss)(_feats);
    _test_n_convex_overlap = _loss->test_loss(_feats);
}

std::string ModelClassifier::toString() const
{
    std::stringstream unit_rep;
    unit_rep << "[" << _feats[0]->expr();
    for (int ff = 1; ff < _feats.size(); ++ff)
    {
        unit_rep << ", " << _feats[ff]->expr();
    }
    unit_rep << "]";
    return unit_rep.str();
}

std::string ModelClassifier::toLatexString() const
{
    std::stringstream unit_rep;
    unit_rep << "[" << _feats[0]->latex_expr();
    for (int ff = 1; ff < _feats.size(); ++ff)
    {
        unit_rep << ", " << _feats[ff]->latex_expr();
    }
    unit_rep << "]";
    return unit_rep.str();
}

std::string ModelClassifier::matlab_expr() const
{
    std::string to_ret = "";
    to_ret += "c0";
    for (int ff = 0; ff < _feats.size(); ++ff)
    {
        to_ret += " + a" + std::to_string(ff) + " * f" + std::to_string(ff);
    }
    return to_ret;
}

std::ostream& operator<<(std::ostream& outStream, const ModelClassifier& model)
{
    outStream << model.toString();
    return outStream;
}

std::string ModelClassifier::error_summary_string(bool train) const
{
    std::stringstream error_stream;
    if (train)
    {
        error_stream << "# # Samples in Convex Hull Overlap Region: " << _train_n_convex_overlap
                     << ";";
        error_stream << "# Samples SVM Misclassified: " << n_svm_misclassified_train() << std::endl;
    }
    else
    {
        error_stream << "# # Samples in Convex Hull Overlap Region: " << _test_n_convex_overlap
                     << ";";
        error_stream << "# Samples SVM Misclassified: " << n_svm_misclassified_test() << std::endl;
    }

    return error_stream.str();
}

std::string ModelClassifier::error_summary_string(std::vector<double> prop,
                                                  std::vector<double> prop_est) const
{
    std::stringstream error_stream;
    error_stream << "# # Samples in Convex Hull Overlap Region: Unknown;";
    std::vector<int> misclassified(prop.size());
    std::transform(
        prop.begin(), prop.end(), prop_est.begin(), misclassified.begin(), [](double p, double pe) {
            return static_cast<int>(p != pe);
        });
    int n_svm_misclassified = std::accumulate(misclassified.begin(), misclassified.end(), 0);
    error_stream << "# Samples SVM Misclassified: " << n_svm_misclassified << std::endl;

    return error_stream.str();
}

std::string ModelClassifier::write_coefs() const
{
    std::stringstream coef_head_stream;
    coef_head_stream << "# Decision Boundaries" << std::endl;
    int n_db = _loss->n_class() * (_loss->n_class() - 1) / 2;
    int task_header_w = std::max(
        6,
        static_cast<int>(
            std::max_element(_task_names.begin(),
                             _task_names.end(),
                             [](auto s1, auto s2) { return s1.first.size() <= s2.first.size(); })
                ->first.size()));
    coef_head_stream << std::setw(task_header_w + 2) << std::left << "# Task";

    for (int cc = 0; cc < _coefs[0].size() - 1; ++cc)
    {
        coef_head_stream << std::setw(24) << " w" + std::to_string(cc);
    }
    coef_head_stream << " b" << std::endl;

    for (auto& task : _task_names)
    {
        int tt = task.second;
        n_db = _loss->n_class(tt) * (_loss->n_class(tt) - 1) / 2;
        int start_coefs = 0;
        for (int sc = 0; sc < tt; ++sc)
        {
            start_coefs += _loss->n_class(sc) * (_loss->n_class(sc) - 1) / 2;
        }
        for (int db = 0; db < n_db; ++db)
        {
            coef_head_stream << std::setw(task_header_w) << std::left
                             << "# " + task.first + "_" + _loss->coef_labels()[start_coefs]
                             << std::setw(2) << ", ";
            for (auto& coeff : _coefs[start_coefs])
            {
                coef_head_stream << std::setprecision(15) << std::scientific << std::right
                                 << std::setw(22) << coeff << std::setw(2) << ", ";
            }
            coef_head_stream << "\n";
            ++start_coefs;
        }
    }
    return coef_head_stream.str();
}
