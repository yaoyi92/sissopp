// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file descriptor_identifier/model/ModelLogRegressor.hpp
 *  @brief Defines a class for the output models from solving a log regression problem
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef MODEL_LOG_REGRESSOR
#define MODEL_LOG_REGRESSOR

#include "descriptor_identifier/model/ModelRegressor.hpp"

// DocString: cls_model_log_reg
/**
 * @brief Class to store the output models for log-regrssion problems with SISSO (inherits from ModelRegressor)
 */
class ModelLogRegressor : public ModelRegressor
{
public:
    using Model::eval;
    using ModelRegressor::error_summary_string;

    /**
     * @brief Default constructor
     */
    ModelLogRegressor();

    /**
     * @brief Construct a ModelLogRegressor using a loss function and a set of features
     *
     * @param prop_label The label for the property
     * @param prop_unit The unit of the property
     * @param loss The LossFunction used to calculate the model
     * @param feats The features of the model
     * @param leave_out_inds The indexes of the samples for the test set
     * @param sample_ids_train A vector storing all sample ids for the training samples
     * @param sample_ids_test A vector storing all sample ids for the test samples
     * @param task_names A vector storing the ID of the task names
     */
    ModelLogRegressor(const std::string prop_label,
                      const Unit prop_unit,
                      const std::shared_ptr<LossFunction> loss,
                      const std::vector<model_node_ptr> feats,
                      const std::vector<int> leave_out_inds,
                      const std::vector<std::string> sample_ids_train,
                      const std::vector<std::string> sample_ids_test,
                      const std::vector<std::string> task_names);

    // DocString: model_log_reg_init_pickle
    /**
     * @brief Construct a Model using a loss function and a set of features
     *
     * @param prop_label The label for the property
     * @param prop_unit The unit of the property
     * @param prop_train The property vector of the training set
     * @param prop_test The property vector of the test set
     * @param task_sizes_train The number of samples in the training set for each task
     * @param task_sizes_test The number of samples in the test set for each task
     * @param feats The features of the model
     * @param leave_out_inds The indexes of the samples for the test set
     * @param sample_ids_train A vector storing all sample ids for the training samples
     * @param sample_ids_test A vector storing all sample ids for the test samples
     * @param task_names A vector storing the ID of the task names
     * @param fix_intercept If true set the bias term to 0
     */
    ModelLogRegressor(std::string prop_label,
                      Unit prop_unit,
                      std::vector<double> prop_train,
                      std::vector<double> prop_test,
                      std::vector<int> task_sizes_train,
                      std::vector<int> task_sizes_test,
                      std::vector<model_node_ptr> feats,
                      std::vector<int> leave_out_inds,
                      std::vector<std::string> sample_ids_train,
                      std::vector<std::string> sample_ids_test,
                      std::vector<std::string> task_names,
                      bool fix_intercept);

    // DocString: model_log_reg_init_train
    /**
     * @brief Construct a ModelLogRegressor from a training output file
     *
     * @param train_file (str) Previously generated model output file for the training data
     */
    ModelLogRegressor(std::string train_file);

    // DocString: model_log_reg_init_test_train
    /**
     * @brief Construct a ModelLogRegressor from a training and testing output file
     *
     * @param train_file (str) Previously generated model output file for the training data
     * @param test_file (str) Previously generated model output file for the test data
     */
    ModelLogRegressor(std::string train_file, std::string test_file);

    /**
     * @brief Copy Constructor
     *
     * @param o ModelLogRegressor to be copied
     */
    ModelLogRegressor(const ModelLogRegressor& o) = default;

    /**
     * @brief Move Constructor
     *
     * @param o ModelLogRegressor to be moved
     */
    ModelLogRegressor(ModelLogRegressor&& o) = default;

    /**
     * @brief Copy Assignment operator
     *
     * @param o ModelLogRegressor to be copied
     */
    ModelLogRegressor& operator=(const ModelLogRegressor& o) = default;

    /**
     * @brief Move Assignment operator
     *
     * @param o ModelLogRegressor to be moved
     */
    ModelLogRegressor& operator=(ModelLogRegressor&& o) = default;

    /**
     * @brief Create the LossFunctionLogPearsonRMSE for the Model when constructed from a file
     *
     * @param prop_train The property vector for all of the training samples
     * @param prop_test The property vector for all of the test samples
     * @param task_sizes_train The number of training samples per task
     * @param task_sizes_test The number of test samples per task
     */
    void make_loss(std::vector<double>& prop_train,
                   std::vector<double>& prop_test,
                   std::vector<int>& task_sizes_train,
                   std::vector<int>& task_sizes_test);

    /**
     * @brief Based off of the model line in a model file determine if the bias term is 0.0
     *
     * @param model_line the model line from the file
     * @return True if the intercept should be fixed
     */
    inline bool has_fixed_intercept(std::string model_line)
    {
        return model_line.substr(0, 9).compare("# exp(c0)") != 0;
    }

    /**
     * @brief Evaluate the model for a new point
     *
     * @param feature_values The value of each selected feature at the given point
     * @param task The task associated with the point
     *
     * @return The prediction of the model for a given data point
     */
    double eval_from_feature_vals(std::vector<double>& feature_vals, std::string task) const;

    /**
     * @brief Evaluate the model for a new set of new points
     *
     * @param feature_values The value of each selected feature at each new point
     * @param task The task associated with each new point
     *
     * @return The prediction of the model for the set of data poitns
     */
    std::vector<double> eval_from_feature_vals(std::vector<std::vector<double>>& feature_vals,
                                               const std::vector<std::string>& tasks) const;

    // DocString: model_log_reg_str
    /**
     * @brief Convert the model to a string

     * @return The string representation of the model
     */
    std::string toString() const;

    // DocString: model_log_reg_latex_str
    /**
     * @brief Convert the model to a latexified string

     * @return The string representation of the model
     */
    std::string toLatexString() const;

    /**
     * @brief Get the matlab expression for this model

     * @return The matlab expression for the model
     */
    std::string matlab_expr() const;

    /**
     * @brief Copy the training error into a different vector
     *
     * @param res Pointer to the head of the new vector to store the residual
     */
    inline void copy_error(double* res) const
    {
        std::copy_n(_loss->error_train().data(), _n_samp_train, res);
    }

    /**
     * @brief Get the block used to summarize the error in the output files
     *
     * @param prop The value of the true property
     * @param prop_est The value of the estimated property
     *
     * @returns error_summary The summary of the model error
     */
    std::string error_summary_string(std::vector<double> prop, std::vector<double> prop_est) const;

    /**
     * @brief The property vector for all of the training samples
     */
    std::vector<double> prop_train() const;

    /**
     * @brief The property vector for all of the test samples
     */
    std::vector<double> prop_test() const;

    /**
     * @brief The estimated property vector for all of the training samples
     */
    std::vector<double> prop_train_est() const;

    /**
     * @brief The estimated property vector for all of the test samples
     */
    std::vector<double> prop_test_est() const;

    /**
     * @brief The error vector for the training data as a numpy array
     */
    std::vector<double> train_error() const;

    /**
     * @brief The error vector for the test data as a numpy array
     */
    std::vector<double> test_error() const;
};

/**
 * @brief Print a model to an string stream
 *
 * @param outStream The output stream the model is to be printed
 * @param model The model to be printed
 */
std::ostream& operator<<(std::ostream& outStream, const ModelLogRegressor& model);

#endif
