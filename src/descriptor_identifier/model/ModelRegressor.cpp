// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file descriptor_identifier/model/ModelRegressor.cpp
 *  @brief Implements a class for the output models from solving a regression problem
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "descriptor_identifier/model/ModelRegressor.hpp"

ModelRegressor::ModelRegressor() {}

ModelRegressor::ModelRegressor(const std::string prop_label,
                               const Unit prop_unit,
                               const std::shared_ptr<LossFunction> loss,
                               const std::vector<model_node_ptr> feats,
                               const std::vector<int> leave_out_inds,
                               const std::vector<std::string> sample_ids_train,
                               const std::vector<std::string> sample_ids_test,
                               const std::vector<std::string> task_names)
    : Model(prop_label,
            prop_unit,
            loss,
            feats,
            leave_out_inds,
            sample_ids_train,
            sample_ids_test,
            task_names)
{
    double rmse = (*_loss)(_feats);
}

ModelRegressor::ModelRegressor(std::string prop_label,
                               Unit prop_unit,
                               std::vector<double> prop_train,
                               std::vector<double> prop_test,
                               std::vector<int> task_sizes_train,
                               std::vector<int> task_sizes_test,
                               std::vector<model_node_ptr> feats,
                               std::vector<int> leave_out_inds,
                               std::vector<std::string> sample_ids_train,
                               std::vector<std::string> sample_ids_test,
                               std::vector<std::string> task_names,
                               bool fix_intercept)
    : Model(prop_label,
            prop_unit,
            prop_train,
            prop_test,
            task_sizes_train,
            task_sizes_test,
            feats,
            leave_out_inds,
            sample_ids_train,
            sample_ids_test,
            task_names,
            fix_intercept)
{
    make_loss(prop_train, prop_test, task_sizes_train, task_sizes_test);

    _loss->set_nfeat(_feats.size());
    (*_loss)(_feats);
    _loss->test_loss(_feats);

    for (int cc = 0; cc < _loss->coefs().size() / (_loss->n_dim()); ++cc)
    {
        _coefs.push_back(std::vector<double>(_loss->n_dim()));
        std::copy_n(&_loss->coefs()[cc * _loss->n_dim()], _loss->n_dim(), _coefs.back().data());
    }
    double rmse = (*_loss)(_feats);
}

ModelRegressor::ModelRegressor(const std::string train_file) { populate_model(train_file); }

ModelRegressor::ModelRegressor(const std::string train_file, std::string test_file)
{
    populate_model(train_file, test_file);
}

void ModelRegressor::make_loss(std::vector<double>& prop_train,
                               std::vector<double>& prop_test,
                               std::vector<int>& task_sizes_train,
                               std::vector<int>& task_sizes_test)
{
    _loss = std::make_shared<LossFunctionPearsonRMSE>(
        prop_train, prop_test, task_sizes_train, task_sizes_test, _fix_intercept, _n_dim);
}

double ModelRegressor::eval_from_feature_vals(std::vector<double>& feature_vals,
                                              std::string task) const
{
    int task_ind = _task_names.find(task)->second;
    double result = (!_fix_intercept) * _coefs[task_ind].back();

    for (int ff = 0; ff < _feats.size(); ++ff)
    {
        result += _coefs[task_ind][ff] * feature_vals[ff];
    }
    return result;
}

std::vector<double> ModelRegressor::eval_from_feature_vals(
    std::vector<std::vector<double>>& feature_vals, const std::vector<std::string>& tasks) const
{
    std::vector<double> result(feature_vals[0].size(), 0.0);
    std::transform(tasks.begin(), tasks.end(), result.begin(), [this](std::string task) {
        int task_ind = _task_names.find(task)->second;
        return _coefs[task_ind].back();
    });

    for (int ff = 0; ff < _feats.size(); ++ff)
    {
        std::vector<double> node_eval(feature_vals[ff].size());
        std::transform(tasks.begin(),
                       tasks.end(),
                       feature_vals[ff].begin(),
                       node_eval.begin(),
                       [this, ff](std::string task, double fv) {
                           int task_ind = _task_names.find(task)->second;
                           return _coefs[task_ind][ff] * fv;
                       });
        daxpy_(result.size(), 1.0, node_eval.data(), 1, result.data(), 1);
    }

    return result;
}

std::string ModelRegressor::toString() const
{
    std::stringstream model_rep;
    if (!_fix_intercept)
    {
        model_rep << "c0";
        for (int ff = 0; ff < _feats.size(); ++ff)
        {
            model_rep << " + a" << std::to_string(ff) << " * " << _feats[ff]->expr();
        }
    }
    else
    {
        model_rep << "a0 * " << _feats[0]->expr();
        for (int ff = 1; ff < _feats.size(); ++ff)
        {
            model_rep << " + a" << std::to_string(ff) << " * " << _feats[ff]->expr();
        }
    }
    return model_rep.str();
}

std::string ModelRegressor::toLatexString() const
{
    std::stringstream model_rep;
    if (_fix_intercept)
    {
        model_rep << "$a_0" << _feats[0]->get_latex_expr();
        for (int ff = 1; ff < _feats.size(); ++ff)
        {
            model_rep << " + a_" << std::to_string(ff) << _feats[ff]->get_latex_expr();
        }
        model_rep << "$";
    }
    else
    {
        model_rep << "$c_0";
        for (int ff = 0; ff < _feats.size(); ++ff)
        {
            model_rep << " + a_" << std::to_string(ff) << _feats[ff]->get_latex_expr();
        }
        model_rep << "$";
    }
    return model_rep.str();
}

std::string ModelRegressor::matlab_expr() const
{
    std::string to_ret = "";
    to_ret += "c0";
    for (int ff = 0; ff < _feats.size(); ++ff)
    {
        to_ret += " + a" + std::to_string(ff) + " * f" + std::to_string(ff);
    }
    return to_ret;
}

std::ostream& operator<<(std::ostream& outStream, const ModelRegressor& model)
{
    outStream << model.toString();
    return outStream;
}

std::string ModelRegressor::error_summary_string(bool train) const
{
    std::stringstream error_stream;
    if (train)
    {
        error_stream << "# RMSE: " << std::setprecision(15) << rmse() << "; Max AE: " << max_ae()
                     << std::endl;
    }
    else
    {
        error_stream << "# RMSE: " << std::setprecision(15) << test_rmse()
                     << "; Max AE: " << test_max_ae() << std::endl;
    }
    return error_stream.str();
}

std::string ModelRegressor::error_summary_string(std::vector<double> prop,
                                                 std::vector<double> prop_est) const
{
    std::vector<double> sq_err(prop.size(), 0.0);
    std::transform(
        prop.begin(), prop.end(), prop_est.begin(), sq_err.begin(), [](double p, double pe) {
            return std::pow(p - pe, 2.0);
        });
    double rmse = std::sqrt(util_funcs::mean(sq_err));
    double max_ae = std::sqrt(*std::max_element(sq_err.begin(), sq_err.end()));

    std::stringstream error_stream;
    error_stream << "# RMSE: " << std::setprecision(15) << rmse << "; Max AE: " << max_ae
                 << std::endl;
    return error_stream.str();
}

std::string ModelRegressor::write_coefs() const
{
    std::stringstream coef_head_stream;

    int task_header_w = std::max(
        6,
        static_cast<int>(
            std::max_element(_task_names.begin(),
                             _task_names.end(),
                             [](auto s1, auto s2) { return s1.first.size() <= s2.first.size(); })
                ->first.size()));

    coef_head_stream << "# Coefficients" << std::endl;
    coef_head_stream << std::setw(task_header_w + 2) << std::left << "# Task";

    for (int cc = 0; cc < _coefs[0].size() - (!_fix_intercept); ++cc)
    {
        coef_head_stream << std::setw(24) << " a" + std::to_string(cc);
    }

    if (!_fix_intercept)
    {
        coef_head_stream << " c0" << std::endl;
    }
    else
    {
        coef_head_stream << std::endl;
    }

    for (auto& task : _task_names)
    {
        coef_head_stream << std::setw(task_header_w) << std::left << "# " + task.first
                         << std::setw(2) << ", ";
        for (auto& coeff : _coefs[task.second])
        {
            coef_head_stream << std::setprecision(15) << std::scientific << std::right
                             << std::setw(22) << coeff << std::setw(2) << ", ";
        }
        coef_head_stream << "\n";
    }

    return coef_head_stream.str();
}

std::vector<double> ModelRegressor::sorted_error() const
{
    std::vector<double> et = train_error();
    std::vector<double> sorted_error(et.size(), 0.0);
    std::copy_n(et.data(), et.size(), sorted_error.data());
    std::transform(sorted_error.begin(), sorted_error.end(), sorted_error.begin(), [](double e) {
        return std::abs(e);
    });
    std::sort(sorted_error.begin(), sorted_error.end());
    return sorted_error;
}

std::vector<double> ModelRegressor::sorted_test_error() const
{
    std::vector<double> et = test_error();
    std::vector<double> sorted_error(et.size(), 0.0);
    std::copy_n(et.data(), et.size(), sorted_error.data());
    std::transform(sorted_error.begin(), sorted_error.end(), sorted_error.begin(), [](double e) {
        return std::abs(e);
    });
    std::sort(sorted_error.begin(), sorted_error.end());
    return sorted_error;
}

double ModelRegressor::mape() const
{
    std::vector<double> et = train_error();
    std::vector<double> pt = prop_train();
    std::vector<double> percent_error(et.size(), 0.0);
    std::transform(et.begin(), et.end(), pt.data(), percent_error.begin(), [](double e, double p) {
        return std::abs(e / p);
    });
    return util_funcs::mean<double>(percent_error);
}

double ModelRegressor::test_mape() const
{
    std::vector<double> et = test_error();
    std::vector<double> pt = prop_test();
    std::vector<double> percent_error(et.size(), 0.0);
    std::transform(et.begin(), et.end(), pt.data(), percent_error.begin(), [](double e, double p) {
        return std::abs(e / p);
    });
    return util_funcs::mean<double>(percent_error);
}
